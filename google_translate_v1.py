#!/usr/bin/python3.7
# Google translate API using python googletrans API
# Author: Jom Kuriakose
# Date: 21/11/2020

import sys

python_ver='python3.7'
code_name=sys.argv[0]

if len(sys.argv) != 3:
	print('googletrans API installed specifically for python3.7 version.')
	print('Usage: '+python_ver+' '+code_name+' '+'<source-text-file> <translated-text-file>')
	sys.exit()

src_file=sys.argv[1]
dest_file=sys.argv[2]
command='python_ver'+' '+code_name+' '+src_file+' '+dest_file

src_lang='en'
dest_lang='hi'

attempts=0
max_attempts=10
verbose=1
success=0

import googletrans

from googletrans import Translator

translator = Translator()

with open(src_file, encoding='utf-8') as f:
	contents = f.read()

while attempts<max_attempts:
	try:
		result = translator.translate(contents, src=src_lang, dest=dest_lang)
		break
	except Exception as e:
		attempts += 1
		if(verbose):
			print('Attempt: '+str(attempts)+' failed. Trying again...')
			print(str(e)+'\n')
		continue

if attempts==max_attempts:
	if(verbose):
		print('Try running again\nSince unofficial googletrans api is used, there can be network inconsistencies.\nPlease try again in that case.')
else:
	with open(dest_file, 'w') as f:
		f.write(result.text)
		success=1
		if(verbose):
			print('Attempt: '+str(attempts+1))
			print('Command: '+command)
			print('Source Language: '+googletrans.LANGUAGES[src_lang]+'\nDestination Language: '+googletrans.LANGUAGES[dest_lang])
			print('Translation successfully')

print('SUCCESS: '+str(success))

