#!/usr/bin/python3

# Google translate API using Google cloud translate API
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 19/12/2020

import sys
import argparse
import re
import pysrt
from google.cloud import translate_v2 as translate
translate_client = translate.Client()
lang_list = translate_client.get_languages()

# Global variables
max_chars=50000
successOrFail=['FAILED','SUCCESS']
file_types=['srt','txt','.srt','.txt']
srt_file_types=['srt','.srt']
txt_file_types=['txt','.txt']
src_file=''

# Flags
verbose=False
log_file=-1
success=0

# Print comment header
def code_head_print():
	code_head_str='Google translation wrapper using Google cloud translate API\n\n'
	code_head_str+='Author: Jom Kuriakose\n'
	code_head_str+='email: jom@cse.iitm.ac.in\n'
	code_head_str+='Date: 19/12/2020\n\n'
	code_head_str+='Languages supported are:\n\n'
	return code_head_str

# Print language list
def lang_list_print():
	global lang_list
	lang_list_str='Sl.No\tTag\tName\n'
	for item in enumerate(lang_list):
		lang_list_str+=str(item[0]+1)+'\t'+item[1]['language']+'\t'+item[1]['name']+'\n'
	return(lang_list_str)

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: ./google_translate_v6.py -v -s en -d hi -if txt -of txt -i input.txt -o output.txt -l log.txt\n'
	ex_cmd_str+='This command converts input txt file in English to output txt file in Hindi.\n\n'
	ex_cmd_str+='Example: ./google_translate_v6.py -v -s en -d hi -if srt -of srt -i input.srt -o output.srt -l log.txt\n'
	ex_cmd_str+='This command converts input srt file in English to output srt file in Hindi.'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+lang_list_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--verbose', help='Verbose mode (default=False)', required=False, action='store_true')
	parser.add_argument('-s','--source', help='Source language tag (default=en)', required=False, default='en')
	parser.add_argument('-d','--dest', help='Destination language tag (default=hi)', required=False, default='hi')
	parser.add_argument('-i','--input', help='Input filename', required=True)
	parser.add_argument('-o','--output', help='Output filename', required=True)
	parser.add_argument('-if','--input_filetype', help='Input file format -- .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-of','--output_filetype', help='Output file format -- .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-l','--log', help='Log file', required=False, default=-1)
	parser.add_argument('-a','--attempts', type=int, help='Number of attempts to translate. Set for python googletrans package, not needed for google cloud translation. (default=3)\n\n', required=False, default=3)
	args = parser.parse_args()
	return args

# Verbose mode and logging to log file
# verbose == 1 -- print messages
# success == 0/1 -- print success message and exit
# success == -1 -- otherwise
# log_file == -1 -- don't write to log file
# exit == 1 -- sys.exit()
def verbose_log(message,disp_success,error,exit):
	global verbose
	global src_file
	global log_file
	global success
	if(verbose):
		print(message)
		if(error != -1):
			print(error)
	if(log_file != -1):
		try:
			if not message.endswith('\n'):
				message+='\n'
			with open(log_file, 'a+') as f:
				f.write(message)
		except Exception as e:
			if(verbose):
				print('Writing to Log file failed.')
				print(str(e))
			print(log_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	if(disp_success):
		print(src_file+' '+str(success)+' '+successOrFail[success])
	if(exit):
		sys.exit()

# Search in the language list with language key to return index value
def lang_list_search(lang_key):
	global lang_list
	index = next((i for i, item in enumerate(lang_list) if item['language'] == lang_key), -1)
	return index

# Search in the subtitle list with subtitle index value to return the list index value
def sub_list_search(subs,index_val):
        index = next((i for i, item in enumerate(subs) if item.index == index_val), -1)
        if (index == -1):
                print('Subtitle index not found.')
                sys.exit()
        else:
                return index

# Read the input file
def read_input_file(src_file,input_file_type):
	if(input_file_type in txt_file_types):
		try:
			with open(src_file, encoding='utf-8') as f:
				contents = f.read()
		except Exception as e:
			verbose_log('Input file reading failed.',1,e,1)

	if(input_file_type in srt_file_types):
		try:
			with open(src_file, encoding='utf-8') as f:
				lines = f.readlines()
			contents = ''
			for line in lines:
				if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
					contents += line
			contents=contents.strip()
		except Exception as e:
			verbose_log('Input file reading failed.',1,e,1)
	return contents

# Check the number of characters in the contents
def check_char_len(contents):
	text = contents.split()
	len_chars = sum(len(word) for word in text)
	if(len_chars>=max_chars):
		verbose_log('Number of characters in input file should be less than 30000 for the script to work. In srt file the time information is not counted for calculating the number of characters.',1,-1,1)

# Detect language and check with source language
def check_detect_lang(contents,src_lang):
	global src_file
	global lang_list
	try:
		lang_detect = translate_client.detect_language(contents)
		lang_detect_index = lang_list_search(lang_detect['language'])
		verbose_log(src_file+': Source language detected - '+lang_list[lang_detect_index]['name'],0,-1,0)
		if(lang_detect['language']==src_lang):
			verbose_log('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are same.',0,-1,0)
		else:
			verbose_log('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are different.\nCheck input file and source language tag.',1,-1,1)
	except Exception as e:
		verbose_log('Source language detection failed.',1,e,1)

# Check language tags
def check_lang_tag(tag):
	tag_index = lang_list_search(tag)
	if(tag_index==-1):
		verbose_log('Language '+tag+' not supported',1,-1,1)
	else:
		return lang_list[tag_index]['name']

# Translate
def text_translate(contents,src_lang,dest_lang):
	try:
		result = translate_client.translate(contents, source_language=src_lang, target_language=dest_lang, format_='text')
		pass
	except Exception as e:
		raise e
	return result

# Write translation output 
def write_output_file(result,dest_file,output_file_type):
	global src_file
	if(output_file_type in txt_file_types):
		try:
			with open(dest_file, 'w') as f:
				f.write(result["translatedText"])
		except Exception as e:
			verbose_log('Ouptut file writing failed.',1,e,1)

	if(output_file_type in srt_file_types):
		try:
			with open(src_file, encoding='utf-8') as f:
				lines = f.readlines()
			counter=0
			first_line=0
			result_split=result["translatedText"].strip().split('\n')
			for line in lines:
				if(first_line==0):
					with open(dest_file, 'w') as f:
						f.write(line)
					first_line += 1
					continue
				if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
					with open(dest_file, 'a') as f:
						f.write(result_split[counter]+'\n')
					counter += 1
				else:
					with open(dest_file, 'a') as f:
						f.write(line)
		except Exception as e:
			verbose_log('Ouptut file writing failed.',1,e,1)

def main():
	args=parse_args()

	global verbose
	global src_file
	global log_file
	global success
	global lang_list

	verbose=args.verbose
	log_file=args.log
	
	src_file=args.input
	dest_file=args.output
	src_lang=args.source
	dest_lang=args.dest
	input_file_type=args.input_filetype
	output_file_type=args.output_filetype
	max_attempts=args.attempts

	if(log_file != -1):
		try:
			open(log_file, 'w').close()
		except Exception as e:
			verbose_log('Log File: '+log_file+' -- Creation failed',1,e,1)
	
	if not(input_file_type in file_types) or not(output_file_type in file_types):
		verbose_log('file_type not supported. Use txt or srt formats.',1,-1,1)
	
	# Check if the input file in exist and readable
	contents=read_input_file(src_file,input_file_type)

	# Check if the number of characters are less than 50k
	check_char_len(contents)

	# Detect the language and check if it is same as the input language
	check_detect_lang(contents,src_lang)
	
	# Check if the language tags exist
	src_language=check_lang_tag(src_lang)
	dest_language=check_lang_tag(dest_lang)

	# Run translation
	attempts=0
	while attempts<max_attempts:
		try:
			result=text_translate(contents,src_lang,dest_lang)
			break
		except Exception as e:
			attempts += 1
			verbose_log('Attempt: '+str(attempts)+' failed. Trying again...',1,e,1)
			continue	
	
	# Write output file		
	if attempts==max_attempts:
		verbose_log('Try running again.',0,-1,0)
	else:
		write_output_file(result,dest_file,output_file_type)
		success=1
		verbose_log('Attempt: '+str(attempts+1)+'\nSource Language: '+src_language+'\nDestination Language: '+dest_language+'\nTranslation successfully',0,-1,0)
			
# Main function
if __name__ == "__main__":
	main()
