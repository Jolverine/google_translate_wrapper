#!/bin/bash

wordListFile=$1
csvFile=$2
src_lang=$3
dest_lang=$4

echo ",$src_lang,$dest_lang,pos" > $csvFile
dos2unix $wordListFile
sed -i '' '/^[[:space:]]*$/d' $wordListFile
sed -i '' 's/[[:blank:]]*$//g' $wordListFile
paste -d',' $wordListFile $wordListFile | sed 's/^/,/g' | sed 's/$/,noun/g' >> $csvFile

#sed 's/^/,/g' $wordListFile | sed 's/$/,noun/g' >> $csvFile

