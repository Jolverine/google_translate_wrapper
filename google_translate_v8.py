#!/usr/bin/python3

# Google translate API using Google cloud translate API
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 08/01/2021

# Import packages
import sys
import argparse
import re
import datetime
import pysrt
from google.cloud import translate_v2 as translate
translate_client = translate.Client()
lang_list = translate_client.get_languages()

# Global variables
max_chars=50000
successOrFail=['FAILED','SUCCESS']
file_types=['srt','txt','.srt','.txt']
srt_file_types=['srt','.srt']
txt_file_types=['txt','.txt']
src_file=''
subs=''

# Flags
verbose=False
log_file=-1
success=0

# Print comment header
def code_head_print():
	code_head_str='Google translation wrapper using Google cloud translate API\n\n'
	code_head_str+='Author: Jom Kuriakose\n'
	code_head_str+='email: jom@cse.iitm.ac.in\n'
	code_head_str+='Date: 08/01/2021\n\n'
	code_head_str+='Languages supported are:\n\n'
	return code_head_str

# Print language list
def lang_list_print():
	global lang_list
	lang_list_str='Sl.No\tTag\tName\n'
	for item in enumerate(lang_list):
		lang_list_str+=str(item[0]+1)+'\t'+item[1]['language']+'\t'+item[1]['name']+'\n'
	return(lang_list_str)

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: ./google_translate_v8.py -v -l log.txt -s en -d hi -if txt -of txt -i input.txt -o output.txt\n'
	ex_cmd_str+='This command converts input txt file in English to output txt file in Hindi.\n\n'
	ex_cmd_str+='Example: ./google_translate_v8.py -v -l log.txt -s en -d hi -if srt -of srt -i input.srt -o output.srt\n'
	ex_cmd_str+='This command converts input srt file in English to output srt file in Hindi.'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+lang_list_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--verbose', help='Verbose mode (default=False)', required=False, action='store_true')
	parser.add_argument('-l','--log_filename', help='Log filename', required=False, default=-1)
	parser.add_argument('-s','--source_language_tag', help='Source language tag (default=en)', required=False, default='en')
	parser.add_argument('-d','--destination_language_tag', help='Destination language tag (default=hi)', required=False, default='hi')
	parser.add_argument('-if','--input_filetype', help='Input file format -- .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-of','--output_filetype', help='Output file format -- .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-i','--input_filename', help='Input filename', required=True)
	parser.add_argument('-o','--output_filename', help='Output filename\n\n', required=True)
	args = parser.parse_args()
	return args

# Verbose mode and logging to log file
def verbose_log(message,disp_success,error,exit):
	global verbose
	global src_file
	global log_file
	global success
	global successOrFail
	if(verbose):
		print(message)
		if(error != -1):
			print(error)
	if(log_file != -1):
		try:
			if not message.endswith('\n'):
				message+='\n'
			with open(log_file, 'a+') as f:
				f.write(message)
		except Exception as e:
			if(verbose):
				print(str(e))
				print('[ERROR] Writing to Log file ('+log_file+') failed.\n--------------------------------------------------\n')
			print(log_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	if(disp_success):
		print(src_file+' '+str(success)+' '+successOrFail[success])
	if(exit):
		sys.exit()

# Search in the language list with language key to return index value
def lang_list_search(lang_key):
	verbose_log('[OK] Searching language tag in language list.',0,-1,0)
	global lang_list
	index = next((i for i, item in enumerate(lang_list) if item['language'] == lang_key), -1)
	return index

# Search in the subtitle list with subtitle index value to return the list index value
def sub_list_search(subs,index_val):
	verbose_log('[OK] Searching for subtitle index in subtitle file.',0,-1,0)
	index = next((i for i, item in enumerate(subs) if item.index == index_val), -1)
	if (index == -1):
		verbose_log('[ERROR] Subtitle index not found.\n--------------------------------------------------\n',1,-1,1)
	else:
		return index

# Read the input file
def read_input_file(src_file,input_file_type):
	verbose_log('[OK] Reading input file.',0,-1,0)
	global subs
	global txt_file_types
	global srt_file_types
	if(input_file_type in txt_file_types):
		try:
			with open(src_file, encoding='utf-8') as f:
				contents = f.read()
		except Exception as e:
			verbose_log('[ERROR] Input text file reading failed.\n--------------------------------------------------\n',1,e,1)

	if(input_file_type in srt_file_types):
		try:
			subs = pysrt.open(src_file, encoding='utf-8')
			contents = ''
			for sub in subs:
				contents += ' '.join(sub.text.split('\n'))+'\n'
			contents=contents.rstrip('\n')
		except Exception as e:
			verbose_log('[ERROR] Input srt file reading failed.\n--------------------------------------------------\n',1,e,1)
	return contents

# Check the number of characters in the contents
def check_char_len(contents):
	global max_chars
	verbose_log('[OK] Checking number of characters in input file. (Max: '+str(max_chars)+')',0,-1,0)
	text = contents.split()
	len_chars = sum(len(word) for word in text)
	if(len_chars>=max_chars):
		verbose_log('[ERROR] Number of characters in input file should be less than '+str(max_chars)+' for the script to work. In srt file the time information is not counted for calculating the number of characters.\n--------------------------------------------------\n',1,-1,1)

# Detect language and check with source language
def check_detect_lang(contents,src_lang):
	verbose_log('[OK] Detecting language of input file.',0,-1,0)
	global src_file
	global lang_list
	try:
		lang_detect = translate_client.detect_language(contents)
		lang_detect_index = lang_list_search(lang_detect['language'])
		verbose_log('[OK] '+src_file+': Source language detected - '+lang_list[lang_detect_index]['name'],0,-1,0)
		if(lang_detect['language']==src_lang):
			verbose_log('[OK] Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are same.',0,-1,0)
		else:
			verbose_log('[ERROR] Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are different.\nCheck input file and source language tag.\n--------------------------------------------------\n',1,-1,1)
	except Exception as e:
		verbose_log('[ERROR] Source language detection failed.\n--------------------------------------------------\n',1,e,1)

# Check language tags
def check_lang_tag(tag):
	verbose_log('[OK] Checking language tag validity',0,-1,0)
	tag_index = lang_list_search(tag)
	if(tag_index==-1):
		verbose_log('[ERROR] Language '+tag+' not supported.\n--------------------------------------------------\n',1,-1,1)
	else:
		return lang_list[tag_index]['name']

# Translate
def text_translate(contents,src_lang,dest_lang):
	verbose_log('[OK] Translating input file.',0,-1,0)
	try:
		result = translate_client.translate(contents, source_language=src_lang, target_language=dest_lang, format_='text')
	except Exception as e:	
		verbose_log('[ERROR] Transcription failed.\n--------------------------------------------------\n',1,e,1)
	return result

# Write translation output 
def write_output_file(result,dest_file,output_file_type):
	verbose_log('[OK] Writing to output file.',0,-1,0)
	global subs
	global txt_file_types
	global srt_file_types
	if(output_file_type in txt_file_types):
		try:
			with open(dest_file, 'w') as f:
				f.write(result["translatedText"])
		except Exception as e:
			verbose_log('[ERROR] Ouptut text file writing failed.\n--------------------------------------------------\n',1,e,1)

	if(output_file_type in srt_file_types):
		try:
			counter=0
			result_split=result["translatedText"].strip().split('\n')
			print(result["translatedText"])
			print(len(result_split))
			print(len(subs))
			for i in range(0,len(subs)):
				subs[i].text=result_split[counter]
				counter += 1
			subs.clean_indexes()
			subs.save(dest_file, encoding='utf-8')
		except Exception as e:
			verbose_log('[ERROR] Ouptut file writing failed.\n--------------------------------------------------\n',1,e,1)

def main():
	global verbose
	global log_file
	global src_file
	global success
	global lang_list
	global file_types
	
	args=parse_args()
	
	verbose=args.verbose
	log_file=args.log_filename
	src_file=args.input_filename
	dest_file=args.output_filename
	src_lang=args.source_language_tag
	dest_lang=args.destination_language_tag
	input_file_type=args.input_filetype
	output_file_type=args.output_filetype
	
	if(log_file != -1):
		try:
			with open(log_file, 'a+') as f:
				f.write('\n\n----------------------------------------------------------------------------------------------------\nTime: '+str(datetime.datetime.now())+'\nCommand: '+str(" ".join(sys.argv[:])))
		except Exception as e:
			verbose_log('[ERROR] Log File: '+log_file+' -- Creation failed.\n--------------------------------------------------\n',1,e,1)

	verbose_log('\n--------------------------------------------------\nCode Running\n--------------------------------------------------',0,-1,0)
	verbose_log('[OK] Reading command line arguments.',0,-1,0)

	if not(input_file_type in file_types) or not(output_file_type in file_types):
		verbose_log('[ERROR] file_type not supported. Use txt or srt formats.\n--------------------------------------------------\n',1,-1,1)
	
	# Check if the input file in exist and readable
	contents=read_input_file(src_file,input_file_type)

	# Check if the number of characters are less than 50k
	check_char_len(contents)

	# Detect the language and check if it is same as the input language
	check_detect_lang(contents,src_lang)
	
	# Check if the language tags exist
	src_language=check_lang_tag(src_lang)
	dest_language=check_lang_tag(dest_lang)

	# Run translation
	result=text_translate(contents,src_lang,dest_lang)

	# Write output
	write_output_file(result,dest_file,output_file_type)
	verbose_log('--------------------------------------------------\nDone\n--------------------------------------------------',0,-1,0)
	success=1
	verbose_log('Input Filename: '+src_file+'\nInput Language: '+src_language+'\nOutput Filename: '+dest_file+'\nOutput Language: '+dest_language+'\nTranslation successfully\n--------------------------------------------------\n',1,-1,0)

# Main function
if __name__ == "__main__":
	main()
