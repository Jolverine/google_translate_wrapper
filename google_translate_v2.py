#!/usr/bin/python3.7
# Google translate API using python googletrans API
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 21/11/2020

import sys
import argparse
import googletrans

def main():
	parser = argparse.ArgumentParser(description='Google translation wrapper using python googletrans API\n\nAuthor: Jom Kuriakose\nemail: jom@cse.iitm.ac.in\nDate: 21/11/2020\n\nLanguages supported are:\n'+str(googletrans.LANGUAGES)+'\n\nPlease run this script using python version 3.7, Since the googletrans API is installed for this version of python.\n\nExample: python3.7 google_translate_v2.py -v -s en -d hi -i input.txt -o output.txt\nThis command converts input file in English to ouput in Hindi.',formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--verbose', help='Verbose mode (default=False)', required=False, action='store_true')
	parser.add_argument('-s','--source', help='Source language tag (default=en)', required=False, default='en')
	parser.add_argument('-d','--dest', help='Destination language tag (default=hi)', required=False, default='hi')
	parser.add_argument('-i','--input', help='Input filename', required=True)
	parser.add_argument('-o','--output', help='Output filename', required=True)
	parser.add_argument('-a','--attempts', type=int, help='Number of attempts to translate. More than one attempt is needed, because the googletrans API used is not official and can have network issues sometimes. (default=3)', required=False, default=3)
	args = parser.parse_args()

	python_ver='python3.7'
	code_name=sys.argv[0]

	#if len(sys.argv) != 3:
	#	print('googletrans API installed specifically for python3.7 version.')
	#	print('Usage: '+python_ver+' '+code_name+' '+'<source-text-file> <translated-text-file>')
	#	sys.exit()

	src_file=args.input
	dest_file=args.output
	command=python_ver+' '+code_name+' '+src_file+' '+dest_file

	#src_lang='en'
	src_lang=args.source
	#dest_lang='hi'
	dest_lang=args.dest

	attempts=0
	max_attempts=args.attempts
	verbose=args.verbose
	success=0
	successOrFail=['FAILED','SUCCESS']

	from googletrans import Translator
	translator = Translator()

	# Check if the language tags exist
	if(checkKey(googletrans.LANGUAGES,src_lang)==0):
		if(verbose):
			print('Source language '+src_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()

	if(checkKey(googletrans.LANGUAGES,dest_lang)==0):
		if(verbose):
			print('Destination language '+dest_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	
	# Check if the input file in exist and readbale
	try:
		with open(src_file, encoding='utf-8') as f:
			contents = f.read()
	except Exception as e:
		print(str(e))
		print('Input file reading failed.')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()		

	while attempts<max_attempts:
		try:
			result = translator.translate(contents, src=src_lang, dest=dest_lang)
			break
		except Exception as e:
			attempts += 1
			if(verbose):
				print('Attempt: '+str(attempts)+' failed. Trying again...')
				print(str(e)+'\n')
			continue

	if attempts==max_attempts:
		if(verbose):
			print('Try running again\nSince unofficial googletrans api is used, there can be network inconsistencies.\nPlease try again in that case.')
	else:
		try:
			with open(dest_file, 'w') as f:
				f.write(result.text)
		except Exception as e:
			print(str(e))
			print('Ouptut file writing failed.')
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
			
		success=1
		if(verbose):
			print('Attempt: '+str(attempts+1))
			print('Command: '+command)
			print('Source Language: '+googletrans.LANGUAGES[src_lang]+'\nDestination Language: '+googletrans.LANGUAGES[dest_lang])
			print('Translation successfully')
			
	print(src_file+' '+str(success)+' '+successOrFail[success])

# Check if the key is present in dictionary
def checkKey(dictionary, key):
	if key in dictionary.keys():
		return 1
	else:
		return 0

# Main function
if __name__ == "__main__":
	main()
