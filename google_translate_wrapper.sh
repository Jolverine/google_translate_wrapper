#!/bin/bash

# export GOOGLE_APPLICATION_CREDENTIALS="/music/jom/S2S_Project/google_translate_wrapper/google_keys/eng-to-ind-lang-transation-5c5cab615464.json"

inputfile=$1
outputfile=$2
langtag=$3

filename=`basename $inputfile .txt`

split -l 100 --additional-suffix=.split --numeric-suffixes=1 $inputfile ${filename}_

ls ${filename}_*.split > split_list.txt

rm $outputfile
while read line; do
		./google_translate_v9.py -v -l log.txt -s hi -d $langtag -if txt -of txt -i $line -o ${line}_trans
		cat ${line}_trans >> $outputfile
done < split_list.txt

rm *.split
rm *.split_trans

