engineering drawing,இன்ஜினியரிங் ட்ராயிங்
computer graphics,கம்ப்யூட்டர் கிராபிக்ஸ்
orthographic projections,ஆர்த்தோகிராஃபிக்  ப்ரொஜெக்ஷன்ஸ்
conic sections,கோனிக் செக்ஷன்ஸ்
isometric projection,ஐசோமெட்ரிக் ப்ரொஜெக்ஷன்ஸ்
design products,டிசைன் ப்ரொஜெக்ட்ஸ்
basic drawing instruments,பேசிக் ட்ரயிங் இன்ஸ்ட்ருமென்ட்ஸ்
lettering,லெட்டரிங்
introduction,இண்ட்ரொடக்ஷன்
technical drawing,டெக்னிகல் ட்ரயிங்
letter,லெட்டர்
lower case,லோவர் கேஸ்
upper case,அப்பர் கேஸ்
dimension,டைமென்ஷன்
tolerance,டாலெரன்ஸ்
layout,லேஅவுட்
sheet,ஷீட்
square,ஸ்கொயர்
compasses,காம்பஸஸ்
dividers,டிவைடர்ஸ்
line,லைன்
curve,கர்வ்
standard,ஸ்டாண்டர்ட்
uniformity,யூனிஃபோர்மிட்டி
society,சொசைட்டி
automotive,ஆட்டோமோட்டிவ்
bureau,ப்யூரோ
associations,அஸோஸியேஷன்ஸ்
template,டெம்ப்லேட்
edge,எட்ஜ்
margin,மார்ஜின்
protocols,ப்ரோடோகால்ஸ்
perforation,பெர்ஃபோரேஷன்
numerals,ந்யூமெரல்ஸ்
direction,டைரக்ஷன்ஸ்
vertical,வெர்ட்டிக்கல்
horizontal,ஹரிசாண்டல்
grids,க்ரிட்ஸ்
angular,ஆங்குலர்
cylinder,சிலிண்டர்
arrow,ஏரோ
extension,எக்ஸ்டென்ஷன்
arc,ஆர்க்
symbol,ஸிம்பல்
parenthesis,பரன்த்திஸிஸ்
radial line,ரேடியல் லைன்
fundamental,ஃபண்டமெண்டல்
scooped,ஸ்கூப்ட்
dashed,டேஷ்ட்
radius,ரேடியஸ்
arrow,ஆர்ரோவ்
diameter,டையாமீட்டர்
