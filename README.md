# **Google translation wrapper using Google cloud translate API**

## ```./google_translate_v8.py -h```

#### usage: ```./google_translate_v8.py [-h] [-v] [-l LOG_FILENAME] [-s SOURCE_LANGUAGE_TAG] [-d DESTINATION_LANGUAGE_TAG] [-if INPUT_FILETYPE] [-of OUTPUT_FILETYPE] -i INPUT_FILENAME -o OUTPUT_FILENAME```

*Author: Jom Kuriakose*\
*email: jom@cse.iitm.ac.in*\
*Date: 08/01/2021*

Languages supported are:

| Sl.No | Tag   | Name                  |
|-------|-------|-----------------------|
| 1     | af    | Afrikaans             |
| 2     | sq    | Albanian              |
| 3     | am    | Amharic               |
| 4     | ar    | Arabic                |
| 5     | hy    | Armenian              |
| 6     | az    | Azerbaijani           |
| 7     | eu    | Basque                |
| 8     | be    | Belarusian            |
| 9     | bn    | Bengali               |
| 10    | bs    | Bosnian               |
| 11    | bg    | Bulgarian             |
| 12    | ca    | Catalan               |
| 13    | ceb   | Cebuano               |
| 14    | ny    | Chichewa              |
| 15    | zh-CN | Chinese (Simplified)  |
| 16    | zh-TW | Chinese (Traditional) |
| 17    | co    | Corsican              |
| 18    | hr    | Croatian              |
| 19    | cs    | Czech                 |
| 20    | da    | Danish                |
| 21    | nl    | Dutch                 |
| 22    | en    | English               |
| 23    | eo    | Esperanto             |
| 24    | et    | Estonian              |
| 25    | tl    | Filipino              |
| 26    | fi    | Finnish               |
| 27    | fr    | French                |
| 28    | fy    | Frisian               |
| 29    | gl    | Galician              |
| 30    | ka    | Georgian              |
| 31    | de    | German                |
| 32    | el    | Greek                 |
| 33    | gu    | Gujarati              |
| 34    | ht    | Haitian Creole        |
| 35    | ha    | Hausa                 |
| 36    | haw   | Hawaiian              |
| 37    | iw    | Hebrew                |
| 38    | hi    | Hindi                 |
| 39    | hmn   | Hmong                 |
| 40    | hu    | Hungarian             |
| 41    | is    | Icelandic             |
| 42    | ig    | Igbo                  |
| 43    | id    | Indonesian            |
| 44    | ga    | Irish                 |
| 45    | it    | Italian               |
| 46    | ja    | Japanese              |
| 47    | jw    | Javanese              |
| 48    | kn    | Kannada               |
| 49    | kk    | Kazakh                |
| 50    | km    | Khmer                 |
| 51    | rw    | Kinyarwanda           |
| 52    | ko    | Korean                |
| 53    | ku    | Kurdish (Kurmanji)    |
| 54    | ky    | Kyrgyz                |
| 55    | lo    | Lao                   |
| 56    | la    | Latin                 |
| 57    | lv    | Latvian               |
| 58    | lt    | Lithuanian            |
| 59    | lb    | Luxembourgish         |
| 60    | mk    | Macedonian            |
| 61    | mg    | Malagasy              |
| 62    | ms    | Malay                 |
| 63    | ml    | Malayalam             |
| 64    | mt    | Maltese               |
| 65    | mi    | Maori                 |
| 66    | mr    | Marathi               |
| 67    | mn    | Mongolian             |
| 68    | my    | Myanmar (Burmese)     |
| 69    | ne    | Nepali                |
| 70    | no    | Norwegian             |
| 71    | or    | Odia (Oriya)          |
| 72    | ps    | Pashto                |
| 73    | fa    | Persian               |
| 74    | pl    | Polish                |
| 75    | pt    | Portuguese            |
| 76    | pa    | Punjabi               |
| 77    | ro    | Romanian              |
| 78    | ru    | Russian               |
| 79    | sm    | Samoan                |
| 80    | gd    | Scots Gaelic          |
| 81    | sr    | Serbian               |
| 82    | st    | Sesotho               |
| 83    | sn    | Shona                 |
| 84    | sd    | Sindhi                |
| 85    | si    | Sinhala               |
| 86    | sk    | Slovak                |
| 87    | sl    | Slovenian             |
| 88    | so    | Somali                |
| 89    | es    | Spanish               |
| 90    | su    | Sundanese             |
| 91    | sw    | Swahili               |
| 92    | sv    | Swedish               |
| 93    | tg    | Tajik                 |
| 94    | ta    | Tamil                 |
| 95    | tt    | Tatar                 |
| 96    | te    | Telugu                |
| 97    | th    | Thai                  |
| 98    | tr    | Turkish               |
| 99    | tk    | Turkmen               |
| 100   | uk    | Ukrainian             |
| 101   | ur    | Urdu                  |
| 102   | ug    | Uyghur                |
| 103   | uz    | Uzbek                 |
| 104   | vi    | Vietnamese            |
| 105   | cy    | Welsh                 |
| 106   | xh    | Xhosa                 |
| 107   | yi    | Yiddish               |
| 108   | yo    | Yoruba                |
| 109   | zu    | Zulu                  |
| 110   | he    | Hebrew                |
| 111   | zh    | Chinese (Simplified)  |

*Example:* ```./google_translate_v8.py -v -l log.txt -s en -d hi -if txt -of txt -i input.txt -o output.txt```\
This command converts input txt file in English to output txt file in Hindi.

*Example:* ```./google_translate_v8.py -v -l log.txt -s en -d hi -if srt -of srt -i input.srt -o output.srt```\
This command converts input srt file in English to output srt file in Hindi.

optional arguments:
```
  -h, --help            show this help message and exit
  -v, --verbose         Verbose mode (default=False)
  -l LOG_FILENAME, --log_filename LOG_FILENAME
                        Log filename
  -s SOURCE_LANGUAGE_TAG, --source_language_tag SOURCE_LANGUAGE_TAG
                        Source language tag (default=en)
  -d DESTINATION_LANGUAGE_TAG, --destination_language_tag DESTINATION_LANGUAGE_TAG
                        Destination language tag (default=hi)
  -if INPUT_FILETYPE, --input_filetype INPUT_FILETYPE
                        Input file format -- .txt or .srt (default=txt)
  -of OUTPUT_FILETYPE, --output_filetype OUTPUT_FILETYPE
                        Output file format -- .txt or .srt (default=txt)
  -i INPUT_FILENAME, --input_filename INPUT_FILENAME
                        Input filename
  -o OUTPUT_FILENAME, --output_filename OUTPUT_FILENAME
                        Output filename
```

#### Requirements (Packages)
```bash
sudo apt-get install python3 # Python version >=3
sudo pip3 install pysrt # Package for reading srt files
```
#### Google translate packages
```bash
pip install google-cloud-translate==2.0.1
pip install --upgrade google-cloud-translate
```
#### Export Google service key
```bash
export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/my-key.json"
```
