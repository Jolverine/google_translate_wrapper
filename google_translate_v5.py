#!/usr/bin/python3

# Google translate API using Google cloud translate API
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 15/12/2020

import sys
import argparse
import re
from google.cloud import translate_v2 as translate
translate_client = translate.Client()
lang_list = translate_client.get_languages()

def main():
	parser = argparse.ArgumentParser(description='Google translation wrapper using Google cloud translate API\n\nAuthor: Jom Kuriakose\nemail: jom@cse.iitm.ac.in\nDate: 15/12/2020\n\nLanguages supported are:\n'+str(lang_list)+'\n\nExample: ./google_translate_v5.py -v -s en -d hi -f txt -i input.txt -o output.txt\nThis command converts input txt file in English to output in Hindi.\n\nExample: ./google_translate_v5.py -v -s en -d hi -f srt -i input.srt -o output.srt\nThis command converts input srt file in English to output in Hindi.',formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--verbose', help='Verbose mode (default=False)', required=False, action='store_true')
	parser.add_argument('-s','--source', help='Source language tag (default=en)', required=False, default='en')
	parser.add_argument('-d','--dest', help='Destination language tag (default=hi)', required=False, default='hi')
	parser.add_argument('-i','--input', help='Input filename', required=True)
	parser.add_argument('-o','--output', help='Output filename', required=True)
	parser.add_argument('-if','--input_filetype', help='The input file format can be either .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-of','--output_filetype', help='The output file format can be either .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-a','--attempts', type=int, help='Number of attempts to translate. This was set for googletrans api. May not be needed for google cloud api. (default=3)', required=False, default=3)
	args = parser.parse_args()

	code_name=sys.argv[0]
	success=0
	src_file=args.input
	dest_file=args.output
	src_lang=args.source
	dest_lang=args.dest
	attempts=0
	max_attempts=args.attempts
	verbose=args.verbose
	successOrFail=['FAILED','SUCCESS']
	input_file_type=args.input_filetype
	output_file_type=args.output_filetype
	file_types=['srt','txt','.srt','.txt']
	
	if not(input_file_type in file_types) or not(output_file_type in file_types):
		if(verbose):
			print(file_type+' not supported. Use txt or srt formats.')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	
	# Check if the input file in exist and readable
	if(input_file_type in ['txt','.txt']):
		try:
			with open(src_file, encoding='utf-8') as f:
				contents = f.read()
		except Exception as e:
			if(verbose):
				print('Input file reading failed.')
				print(str(e))
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	
	if(input_file_type in ['srt','.srt']):
		try:
			with open(src_file, encoding='utf-8') as f:
				lines = f.readlines()
			contents = ''
			for line in lines:
				if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
					contents += line
		except Exception as e:
			if(verbose):
				print('Input file reading failed.')
				print(str(e))
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()

	contents=contents.strip()
	# Check if the number of characters are less than 15k
	text = contents.split()
	len_chars = sum(len(word) for word in text)
	if(len_chars>=30000):
		if(verbose):
			print('Number of characters in input file should be less than 30000 for the script to work. In srt file the time information is not counted for calculating the number of characters.')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()

	try:
		lang_detect = translate_client.detect_language(contents)
		lang_detect_index = list_search(lang_list,lang_detect['language'])
		if(verbose):
			print(src_file+': Source language detected - '+str(lang_list[lang_detect_index]['name']))
		if(lang_detect['language']==src_lang):
			if(verbose):
				print('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are same.')
		else:
			if(verbose):
				print('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect['language']+') are different.')
				print('Check input file and source language tag.')
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	except Exception as e:
		if(verbose):
			print('Source language detection failed.')
			print(str(e))	

	# Check if the language tags exist
	src_lang_index = list_search(lang_list,src_lang)
	if(src_lang_index==-1):
		if(verbose):
			print('Source language '+src_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	else:
		src_language = lang_list[src_lang_index]['name']

	dest_lang_index = list_search(lang_list,dest_lang)
	if(dest_lang_index==-1):
		if(verbose):
			print('Destination language '+dest_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	else:
		dest_language = lang_list[dest_lang_index]['name']
	
	while attempts<max_attempts:
		try:
			result = translate_client.translate(contents, source_language=src_lang, target_language=dest_lang, format_='text')
			break
		except Exception as e:
			attempts += 1
			if(verbose):
				print('Attempt: '+str(attempts)+' failed. Trying again...')
				print(str(e))
			continue

	if attempts==max_attempts:
		if(verbose):
			print('Try running again\nSince unofficial googletrans api is used, there can be network inconsistencies.\nPlease try again in that case.')
	else:
		if(output_file_type in ['txt','.txt']):
			try:
				with open(dest_file, 'w') as f:
					f.write(result["translatedText"])
			except Exception as e:
				if(verbose):
					print('Ouptut file writing failed.')
					print(str(e))
				print(src_file+' '+str(success)+' '+successOrFail[success])
				sys.exit()

		if(output_file_type in ['srt','.srt']):
			try:
				with open(src_file, encoding='utf-8') as f:
					lines = f.readlines()
				counter=0
				first_line=0
				result_split=result["translatedText"].strip().split('\n')
				for line in lines:
					if(first_line==0):
						with open(dest_file, 'w') as f:
							f.write(line)
						first_line += 1
						continue
					if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
						with open(dest_file, 'a') as f:
							f.write(result_split[counter]+'\n')
						counter += 1
					else:
						with open(dest_file, 'a') as f:
							f.write(line)

			except Exception as e:
				if(verbose):
					print('Ouptut file writing failed.')
					print(str(e))
				print(src_file+' '+str(success)+' '+successOrFail[success])
				sys.exit()
			
		success=1
		if(verbose):
			print('Attempt: '+str(attempts+1))
			print('Source Language: '+src_language+'\nDestination Language: '+dest_language)
			print('Translation successfully')
			
	print(src_file+' '+str(success)+' '+successOrFail[success])

# Search in the language list with language key
def list_search(lang_list,lang_key):
	index = next((i for i, item in enumerate(lang_list) if item['language'] == lang_key), -1)
	return index

# Main function
if __name__ == "__main__":
	main()
