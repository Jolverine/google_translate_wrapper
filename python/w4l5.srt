1
00:00:03,319 --> 00:00:13,943
So, we have seen this kind of a simultaneous  assignment  where we take three names on the left and assign them to three  values  in the right and we enclose these in these round brackets.

2
00:00:13,968 --> 00:00:20,079
So this kind of a sequence of  values  with around bracket is called a tuple.

3
00:00:20,079 --> 00:00:23,760
So, normally we talk about pairs, triples,quadruples

4
00:00:23,760 --> 00:00:27,399
but in general when it goes to  values  of  k  we call them  k-tupless.

5
00:00:27,890 --> 00:00:34,990
Now, in python tuples are also  valid values , you can take a single name and assign it a tuple of  values .

6
00:00:35,000 --> 00:00:47,140
For instance, we can take a two dimensional point the  x coordinates three point five  and  four point eight  and say that point has the value  three point five  ,  four point eight  and this is not a list, but a tuple.

7
00:00:47,140 --> 00:00:49,600
And we will see in a minute what is the principle is.

8
00:00:49,600 --> 00:00:58,359
Similarly, we can say that a date is made up of three parts a day a month and year and we can enclose this into a  three valued  or  triple .

9
00:01:00,679 --> 00:01:05,590
So, tuple behaves like a list, so it is a kind of sequence

10
00:01:05,590 --> 00:01:08,620
So, like strings and lists.

11
00:01:08,620 --> 00:01:15,808
in a tuple, you can extract one element of a sequence. So, we can say that the zeroth value in point is the x coordinate.

12
00:01:15,833 --> 00:01:21,730
So, this would assign the value to  two point five  to the value x to the name x coordinate.

13
00:01:21,739 --> 00:01:28,230
Or we can take a slice, we can say that. If we want only seven to two thousand and thirteen

14
00:01:28,230 --> 00:01:34,263
We take date and take the date slice from one to the end, then we will get seven comma two thousand and thirteen.

15
00:01:35,450 --> 00:01:41,620
So, this behaves very much like a different type of sequence exactly like strings and list we have seen so far.

16
00:01:41,719 --> 00:01:46,590
But, the difference between a tuple and a list is that a tuple is immutable.

17
00:01:46,590 --> 00:01:58,480
So, tuple behaves more like a string in this case, we cannot change for instance this date to eight by saying date at position 1 should be replaced by the value eight.

18
00:01:58,480 --> 00:02:01,299
This is possible in list, but not in a tuple.

19
00:02:01,299 --> 00:02:07,480
So, tuples are immutable sequences, and we will see in the minute. why this matters.

20
00:02:10,280 --> 00:02:12,370
Let us go back to lists.

21
00:02:12,500 --> 00:02:15,659
So, a list is a sequence of  values .

22
00:02:15,659 --> 00:02:22,419
And implicitly there are positions associated with these sequence starting at zero and going up with the length of the list minus 1.

23
00:02:23,539 --> 00:02:30,795
So, an alternative way of viewing a list is to say that it maps every positions to the value, in this case the  values  are integers.

24
00:02:30,820 --> 00:02:40,569
We can say that this list  L , we can say is a map or a function in the mathematical sense from the domain 0, 1, 2, 3, 4 to the range of integers.

25
00:02:40,569 --> 00:02:45,810
And in particular it assigns L0 to be 13, L4 to be 72 and so on.

26
00:02:45,810 --> 00:02:48,009
Where we are looking at this as a function value.

27
00:02:50,419 --> 00:02:57,598
So, the programming and  program language a way of thinking about this is that 0, 1, 2, 3, 4 are what are called keys.

28
00:02:57,623 --> 00:03:02,919
So, these are the  values  with which we have some items associated.

29
00:03:02,919 --> 00:03:07,409
So, we will search for the items associated with one and we get back fourty six.

30
00:03:07,409 --> 00:03:12,400
So, we have keys and the corresponding entries in the list are called  values .

31
00:03:12,439 --> 00:03:17,110
So, a list is one way of associating keys to  values .

32
00:03:20,360 --> 00:03:29,319
So, we can generalize this concept by allowing keys from a different set of things other than just the range of  values  from  zero to n minus one .

33
00:03:30,620 --> 00:03:33,520
So, the key for instance could be a string.

34
00:03:33,545 --> 00:03:38,370
So, we might want a list in which we index the  values  by the name of a player.

35
00:03:38,395 --> 00:03:45,665
For instance, we might keep track of the scores in a test match by saying that for each players name what is the value associated?

36
00:03:45,690 --> 00:03:53,199
So, Dhawan score is 84, Pujara score is 16, Kholi score is 200, we store these all in a more generic list.

37
00:03:53,199 --> 00:03:59,860
Where the list  values  are not indexed by position, but by some more abstract key in this case, the name of the player.

38
00:04:00,800 --> 00:04:07,863
So, this is what python calls a dictionary in some other programming languages , this is also called an  associative array .

39
00:04:07,888 --> 00:04:10,250
So, you might see this in the literature.

40
00:04:10,275 --> 00:04:20,439
So, here is a store of  values  which are accessed a key which is not just a position, but some arbitrary index.

41
00:04:20,569 --> 00:04:25,060
And pythons rule is that any immutable value can be a key.

42
00:04:25,730 --> 00:04:33,879
So, this means that you can use strings which are immutable and here for instance, we can use tuples, but you cannot use lists as we will see.

43
00:04:34,100 --> 00:04:38,560
The other feature of a dictionary is that like a list it is mutable.

44
00:04:38,560 --> 00:04:46,816
We can take a value with the key and replace it. So, we can change pujaara score if you want by an  assignment  to 72

45
00:04:46,841 --> 00:04:52,930
And this will just take the current dictionary and replace the value associated with the Pujara from 16 to 72.

46
00:04:52,930 --> 00:04:58,120
So, dictionaries can be updated in place and hence are mutable exactly like this.

47
00:05:00,500 --> 00:05:05,791
So, we have to tell python that some name is a dictionary and is not a list.

48
00:05:05,816 --> 00:05:11,529
So, we signify an empty dictionary by curly braces. So, remember we use square brackets for lists.

49
00:05:11,569 --> 00:05:20,915
So, if you want to initialize that dictionary that we saw earlier then we would first say test 1 is the empty dictionary by giving it a braces here

50
00:05:20,940 --> 00:05:28,663
And then we can start assigning  values  to all the players that we had before like dhawan and Pujara and so on.

51
00:05:29,484 --> 00:05:34,600
So, notice that all these three sequences and types of things that we have are different.

52
00:05:34,625 --> 00:05:44,459
So, for for strings of course, we use double quotes or single quote, for list we use square brackets For tuples use round brackets and for dictionaries we use braces.

53
00:05:44,459 --> 00:05:55,300
So, there is an unambiguous way of signaling to python, what type of a collection we are associated with the name, so that we can operate on it with the appropriate operations that are defined for that type of collection

54
00:05:58,670 --> 00:06:03,250
So, once again for a dictionary, the key can be any immutable value.

55
00:06:03,920 --> 00:06:11,050
That means, we keep an integer, it could be a float, it could be a bool it could be a string, it could be a tuple.

56
00:06:11,209 --> 00:06:15,310
What it cannot be is a list or a dictionary .

57
00:06:15,310 --> 00:06:19,180
So, we cannot have a value indexed by a list itself or by a dictionary .

58
00:06:22,100 --> 00:06:25,560
So, we can have multiple just like we have nested lists

59
00:06:25,560 --> 00:06:34,329
Where we can have a  list  containing  list  and we have two indices take the zeroth  list  and then the first position in the zeroth list we can have two levels of  keys .

60
00:06:34,329 --> 00:06:38,410
So, if you want to keep track of  scores  across multiple  test  matches.

61
00:06:38,410 --> 00:06:46,209
Instead of having two  dictionaries , we can have one  dictionary  where the first  key  is the  test  match,  test  one or test two and the second key is the player.

62
00:06:46,220 --> 00:06:58,300
So, with the different first key, for example, test 1 and test 2, we could keep track of two different scores for Dhawan score in test 1 and the score in test 2.

63
00:06:58,699 --> 00:07:03,536
And we can have more than one player in test two like we have here, both kholi and dhawan and so on

64
00:07:05,839 --> 00:07:14,983
So If you try to display a dictionary in python it will show it to you in this bracket, it will be in this kind of the curly bracket notation.

65
00:07:15,008 --> 00:07:22,001
So, each entry will be the key followed by the value separate by the colon and then this will be like a list separate by commas.

66
00:07:22,026 --> 00:07:28,829
And if you have multiple keys, then essentially this is one whole entry in this dictionary .

67
00:07:28,829 --> 00:07:36,009
It says for the key test one, I have these  values , for the key test 2 have these  values  and internally they are again dictionary , so they have their own key values .

68
00:07:38,089 --> 00:07:41,829
Let us see how it works, we start with an empty dictionary say score.

69
00:07:42,410 --> 00:07:47,379
Now, we want to create keys so supposing we say score s1

70
00:07:54,403 --> 00:08:04,003
Now, this is going to give us an error because we have not told it, that score test 1 is suppose to be a dictionary . So, it does not know that you can further index it

71
00:08:04,160 --> 00:08:13,389
So, we have to first tell it that not only score in dictionary , so is score test 1 and presumably since we will use it, so is score test 2.

72
00:08:14,509 --> 00:08:19,329
And now we can go back and set the dhawan score in the first test to 76.

73
00:08:19,329 --> 00:08:24,189
and maybe we can set it in the second test to 27.

74
00:08:24,230 --> 00:08:28,536
and maybe we can set kholi score in the first test to 200.

75
00:08:30,110 --> 00:08:37,029
Now, if we ask it to show us what score looks like, we see that it has an outer dictionary with two keys  s1 s2 .

76
00:08:37,029 --> 00:08:43,734
Each of which is a nested dictionary and the nested dictionaries we have two keys, dhawan and kholi who scored 76 and 200 as  values 

77
00:08:43,759 --> 00:08:48,490
And test 2 has one dictionary, one dictionary entry with the dhawan as the key and 27 as the value

78
00:08:53,269 --> 00:08:55,870
So, if you want to process a dictionary.

79
00:08:55,870 --> 00:08:58,330
then we would need to run through all the  values .

80
00:08:58,330 --> 00:09:03,940
and one way to run through with all the  values  is to extract the keys and extract each value by turn.

81
00:09:03,940 --> 00:09:08,529
So, there is a function d dot keys which it turns a sequence of keys of a  dictionary d

82
00:09:09,620 --> 00:09:14,950
And then typical thing we will do is for every key in  d dot keys is do something with  d  square bracket  k.

83
00:09:14,990 --> 00:09:20,679
So, pick up all the keys, so this is like saying for every position in a list do something with the value at that position.

84
00:09:20,840 --> 00:09:26,289
This is same for something for every key in a list do something with the value associated with that.

85
00:09:27,409 --> 00:09:40,679
Now, one thing we have to keep in mind which I will show you in a minute is that  d dot keys is not in any predictable order. So dictionary are optimized internally to return the value with the key quickly.

86
00:09:40,679 --> 00:09:47,323
So, it may not preserve the keys in the order in which they are inserted. So, we cannot predict anything about  d dot keys will be presented to us.

87
00:09:47,348 --> 00:09:50,950
So one way to do this is to use the  sorted function.

88
00:09:51,200 --> 00:10:00,789
So, we can say for k in  sorted d dot keys process d k and this will give us the keys in  sorted  order according to the sort function.

89
00:10:00,789 --> 00:10:04,016
So,  sorted L  is a function we have not seen so far.

90
00:10:04,041 --> 00:10:08,530
 Sorted L returns  a  sorted  copy of  L , it does not modify  L .

91
00:10:08,555 --> 00:10:15,002
What we have seen so far is  L dot sort  which is a function which takes a list and updates it in place.

92
00:10:15,029 --> 00:10:19,629
So,  sorted l  takes an <input> list, leaves it unchanged, but returns a  sorted version .

93
00:10:22,220 --> 00:10:31,908
The other thing to keep in mind is that though it is tempting to believe that  d dot keys it is not a list, it is like  range  and other things.

94
00:10:31,933 --> 00:10:35,590
It is just a sequence of  values  that you can use inside a  for .

95
00:10:35,590 --> 00:10:41,470
So, we must use the list property to actually create a list out of b dot c.

96
00:10:45,289 --> 00:10:49,600
Let us validate the claim that keys are not kept in any particular order.

97
00:10:49,600 --> 00:10:51,759
So, let us start with an  empty dictionary

98
00:10:51,950 --> 00:11:03,916
Now, let us create for each letter an entry which is the same as that type. So, we can say for < DNT> n < /DNT> in < DNT> A,B,C, D, E, F, G, H, I 

99
00:11:05,810 --> 00:11:11,409
 d l  is equal to  l . So, what is it saying?

100
00:11:11,409 --> 00:11:19,690
So, when you say for  l  in a string it goes through each letter in that string, it is going to say d with the key a is the value a, d with the key b is the value b and so on

101
00:11:19,690 --> 00:11:25,353
So, now if I ask you what is  d A  it will tell me it is  A . If I ask you for  d I  it will say  I 

102
00:11:25,460 --> 00:11:30,730
Now, notice that the keys were inserted it in the order a, b, c, d, e, f, g, h, i.

103
00:11:30,730 --> 00:11:33,250
But if I ask for  d dot keys.

104
00:11:35,480 --> 00:11:37,960
It produces it in some very random order.

105
00:11:38,389 --> 00:11:41,460
So, e is first and a is way down and so on.

106
00:11:41,460 --> 00:11:43,929
There is no specific order that you can discern functions.

107
00:11:44,090 --> 00:11:51,960
So, this is just to emphasize that the order in which keys are inserted into the dictionary is not going to be the order in which they are present it through the keys function.

108
00:11:51,960 --> 00:12:02,769
So, you should always ensure that if you want to process the keys in a particular order, make sure that you preserve that order during you extract the keys. We cannot assume that the keys will come up in any given order.

109
00:12:07,370 --> 00:12:18,129
So, another way to run through the  values  in the dictionary is to use  d dot  values . So,  d dot keys is returns the keys is in some order,   d dot values  gives you the  values  in some order.

110
00:12:18,129 --> 00:12:21,639
So, this is for example like saying.

111
00:12:22,100 --> 00:12:28,049
 For x in l . So, we just get the  values , we do not get their positions.

112
00:12:28,049 --> 00:12:30,389
So, here you just get the  values  we dont get the keys.

113
00:12:30,389 --> 00:12:39,220
So, if you want to add up all the  values  for instance from our dictionary you can start off by initializing total to 0 and then for each value you can just add it up

114
00:12:42,860 --> 00:12:47,409
So, we can pick up each  s  in  test one dot  values  and add it

115
00:12:50,450 --> 00:12:59,470
So, you can test for a key being in a dictionary by using the in operator just like list when you say  x  in L.

116
00:12:59,470 --> 00:13:06,009
For a list it tells you true if x belongs to l, the value x belongs to l, it tells you false otherwise same is true of keys.

117
00:13:06,500 --> 00:13:12,759
So, if I want to add up the scores for individual batsman, but I do not know that they batted in each test match

118
00:13:12,799 --> 00:13:25,059
So, I will say for each of the keys in this case, Dhawan and Kholi initialize a dictionary which I have already set up not here, I would have said that   total  is a dictionary

119
00:13:25,059 --> 00:13:28,830
So,  total  with key Dhawan is 0, total with key Kholi is 0.

120
00:13:28,830 --> 00:13:33,940
Now for each  match  in our nested dictionary

121
00:13:34,309 --> 00:13:41,769
If the dhawan is entered as a batsman in that match. So, if the name dhawan appears as a  key 

122
00:13:42,049 --> 00:13:49,899
So, the score for that match, then and only then you add a score because if it does not appear it is illegal to access that value.

123
00:13:50,509 --> 00:13:58,539
So, this will one way to make sure that when you access the value from a dictionary, the key actually exists, you can use the in function.

124
00:14:01,759 --> 00:14:06,930
So, here is a way of remembering that dictionary is different from a list.

125
00:14:06,955 --> 00:14:12,429
If I start with an empty dictionary, then I assign a key which has not been seen so far.

126
00:14:12,429 --> 00:14:18,340
in a dictionary there is no problem it is just a equivalent to inserting this key in the dictionary with that value.

127
00:14:18,340 --> 00:14:22,990
if  d 0  already existed, it will be updated. So, either you update or you insert.

128
00:14:23,480 --> 00:14:30,970
So, this is in contrast with the list where if you have an empty list and then try to insert it a position which does not exist, we get an index error

129
00:14:31,309 --> 00:14:39,700
So, in a dictionary it flexibly expands to a accomodate new keys or updates the key depending on whether the key already exists or not.

130
00:14:42,830 --> 00:14:49,419
So, to summarize, A dictionary is a more flexible association of  values  to keys, then you have in a list.

131
00:14:49,419 --> 00:14:54,865
The only constraint that python imposes is that all these must be immutable values .

132
00:14:54,890 --> 00:15:00,700
You cannot have keys which are mutable values . So, you cannot use dictionaries or lists themselves as these.

133
00:15:00,700 --> 00:15:04,090
But you can have nested dictionaries with multiple levels of things.

134
00:15:05,029 --> 00:15:17,250
The other thing is that we can use  d dot keys is to cycle through all keys in the dictionary and similarly, d dot values . But the order in which these keys emerge from  d dot keys  is not predictable.

135
00:15:17,250 --> 00:15:22,480
So, we need to sort it to do something else if we want to make sure that process them in a predictable order.

136
00:15:23,149 --> 00:15:29,490
So, it turns out that, you will see that dictionaries are actually something that make python really useful language

137
00:15:29,490 --> 00:15:41,453
For manipulating the information from text files or tables, if you have what are called  comma separated value tables. it is taken out of  spread sheets , because then we can use column headings acumulate  values  and so on.

138
00:15:41,478 --> 00:15:52,570
So, you should understand and assimilate  dictionaries  in pure programming skills, because this is what makes python really a very powerful language for writing scripts to manipulate data.

