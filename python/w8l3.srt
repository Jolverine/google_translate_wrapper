1
00:00:03,830 --> 00:00:07,290
So we are in the realm of inductive definitions

2
00:00:07,290 --> 00:00:13,419
recursive functions and efficient evaluation of these using memoization and dynamic programming.

3
00:00:13,419 --> 00:00:22,260
So we are looking at examples of problems where the main target is to ideno results
00:00:22,260 --> 00:00:24,969
then the recursive structure of the program becomes apparent

5
00:00:24,969 --> 00:00:27,420
from which you can extract the dependencies

6
00:00:27,420 --> 00:00:30,149
and figure out what kind of memo table you have

7
00:00:30,149 --> 00:00:34,530
and how you can fill it iterative using dynamic programming

8
00:00:34,530 --> 00:00:37,806
procedure will come clearer So this is something which comes with practice and by looking at more examples hopefully 

9
00:00:37,831 --> 00:00:44,159
but the key thing to dynamic programming is to be able to understand the inductive structure.

10
00:00:44,159 --> 00:00:53,729
So you need to be able to take a problem identify how the main problem depends on its subparts and using this come up with a nice inductive definition.

11
00:00:53,729 --> 00:00:57,909
which you can translate into a recursive program.

12
00:00:57,909 --> 00:01:02,950
Once you have the recursive program then the memo table and the dynamic programming almost comes out automatically from that.

13
00:01:03,560 --> 00:01:06,329
So this is a problem involving words.

14
00:01:06,329 --> 00:01:09,310
So what we want to do is take a pair of words

15
00:01:09,310 --> 00:01:13,090
and find the longest common subword for instance

16
00:01:13,090 --> 00:01:20,489
Here we have secret and secretary and secret is already also inside secretary

17
00:01:20,489 --> 00:01:23,500
clearly the longest word is secret itself.

18
00:01:23,500 --> 00:01:27,549
the longest subword that is common is the word secret and it has length 6.

19
00:01:27,680 --> 00:01:31,480
If we move to the next thing bisect and trisect then.

20
00:01:31,480 --> 00:01:36,579
Actually this should be Isect. Isect which has length 5.

21
00:01:38,719 --> 00:01:41,680
Similarly if we have bisect and secret.

22
00:01:41,900 --> 00:01:44,500
then s e c.

23
00:01:44,569 --> 00:01:52,200
So when we say subword of course we do not mean a word in the sense we just mean a sequence of letters. So s e c is the longest common subword

24
00:01:52,200 --> 00:01:53,200
this has length 3.

25
00:01:53,420 --> 00:01:56,760
If we have two very different words like director and secretary.

26
00:01:56,760 --> 00:02:00,489
And sometimes we might have only small things.

27
00:02:00,620 --> 00:02:02,200
For example here Re and ec.

28
00:02:02,359 --> 00:02:08,590
are examples of subwords but there are not really very long words which are common to this so that subword is only length 2.

29
00:02:11,780 --> 00:02:16,590
So here is a more formal description. supposing I have two words u and v

30
00:02:16,590 --> 00:02:18,849
so u is of length m

31
00:02:19,460 --> 00:02:24,310
and v is of length n and

32
00:02:24,310 --> 00:02:27,370
the number of the positions using python notation are number 0 to m - 1 0 to n - 1

33
00:02:27,370 --> 00:02:31,379
then what I want to do is to be able to start at i a i and go k steps.

34
00:02:31,379 --> 00:02:34,180
So i to i + k - 1.

35
00:02:34,180 --> 00:02:36,939
and b j to j + k - 1.

36
00:02:36,939 --> 00:02:39,729
such that these two segments are identical.

37
00:02:40,430 --> 00:02:42,240
So this is a common subword.

38
00:02:42,240 --> 00:02:45,969
and we want to find the longest such common subword what is the k?

39
00:02:45,969 --> 00:02:49,509
we do not even want the subword we will find that the subword will be a byproduct.

40
00:02:49,509 --> 00:02:54,280
we first need to just find k what is the length of the longest common subword of u and v

41
00:02:57,740 --> 00:03:01,030
so there is a brute force algorithm that you could use.

42
00:03:01,030 --> 00:03:04,110
which is to just start at i and j.

43
00:03:04,110 --> 00:03:08,639
in two words in each word you can start a position i u j and v.

44
00:03:08,639 --> 00:03:12,189
and see how far you can go before you find they are not common.

45
00:03:12,199 --> 00:03:14,469
So you match a i and b j.

46
00:03:14,569 --> 00:03:17,680
So if a i and b j work then it is fine.

47
00:03:19,669 --> 00:03:23,699
If a i and b j work then you go to a i + 1 b j + 1 and so on.

48
00:03:23,699 --> 00:03:26,490
And whenever we find two letters which differ

49
00:03:26,490 --> 00:03:30,629
then the common subword starting at i j has ended and you say ok

50
00:03:30,629 --> 00:03:35,460
from i j I have a common subword or some thing. Now among all the i j's you look for the longest one

51
00:03:35,460 --> 00:03:39,210
and that becomes your answer.

52
00:03:39,210 --> 00:03:43,229
Now this unfortunately is a effectively in order n cubed algorithm if we think of m and n has been equal.

53
00:03:43,229 --> 00:03:48,569
m n square because there are m times n different choices of i and j.

54
00:03:48,569 --> 00:03:53,020
And in general I started i j and then I have to go from i till the end.

55
00:03:53,240 --> 00:03:54,939
right and from j to the end.

56
00:03:55,400 --> 00:04:03,300
So we have to do a scan for each i j and this scan in general adds up to an order order n factor and so we have order m, n squared 

57
00:04:03,300 --> 00:04:06,099
or order n cubed if you would like to do.

58
00:04:07,849 --> 00:04:13,419
So our goal is to find some inductive structure which makes this thing computationally more efficient.

59
00:04:14,599 --> 00:04:18,759
So what is the inductive structure well we have already kind of seen it.

60
00:04:19,160 --> 00:04:25,899
When can we say that there is a common subword starting at i j of length k.

61
00:04:26,449 --> 00:04:32,529
Well the first thing is that we need this a i to be the same as b j. So I need this condition.

62
00:04:32,600 --> 00:04:37,079
And now if this is a common subword of length k at i j then what remains of

63
00:04:37,079 --> 00:04:41,040
subword namely this segment from i + 1 to this.

64
00:04:41,040 --> 00:04:45,449
j + 1 to this must also match and they must in turn be a

65
00:04:45,449 --> 00:04:48,279
k - 1 length subword from here to here.

66
00:04:48,290 --> 00:04:52,589
So we want to say that there is a k length sub word starting at i j.

67
00:04:52,589 --> 00:05:00,430
if a i is equal to b j and from i + 1 and j + 1 there is a k - 1 length subword.

68
00:05:01,910 --> 00:05:11,920
So in other words I can now write the following definition I can say that the length of the longest common subword lcw starting from i j.

69
00:05:12,350 --> 00:05:16,829
well if they two are not the same if a i is not the same there is no common sub word at all

70
00:05:16,829 --> 00:05:21,220
if I start from here I immediately have two different letters. So then the length is 0

71
00:05:21,220 --> 00:05:22,629
otherwise.

72
00:05:22,639 --> 00:05:26,470
I can inductively find out what is the longest common subword to my right.

73
00:05:26,470 --> 00:05:30,579
start from i + 1 start from j + 1 find out what I can do from there

74
00:05:30,579 --> 00:05:36,339
and to that word. I can add one letter because this current letter a i is equal to b j

75
00:05:37,550 --> 00:05:42,759
so I get 1 + and The base case or the boundary condition is when one of the two vertices empty.

76
00:05:42,759 --> 00:05:48,100
If I have no letters left I am looking at different combinations i and j.

77
00:05:48,100 --> 00:05:51,009
if either i or j has reached the end of the word.

78
00:05:51,009 --> 00:05:53,819
then there is no possibility of a common subword at that point.

79
00:05:53,819 --> 00:05:57,040
So when we have reached the end of one of the words the answer must be 0.

80
00:05:59,480 --> 00:06:01,779
So this gives us

81
00:06:03,529 --> 00:06:08,459
at the following definition. So remember that u is actually of length m

82
00:06:08,459 --> 00:06:16,209
so it has 0 to m - 1. So what we will do is we will add a position of m to indicate that we have crossed the last letter.

83
00:06:16,209 --> 00:06:20,370
Similarly we have 0 to n - 1 as valid positions. So we will use the indices 0 to n.

84
00:06:20,370 --> 00:06:28,600
So if i becomes m or j becomes n it means that that corresponding index has gone beyond the end of the word.

85
00:06:29,240 --> 00:06:35,649
So this should be n So we have that.

86
00:06:35,660 --> 00:06:44,579
If we reach m then lcw of m j is 0 because we have gone pass the length of u.

87
00:06:44,579 --> 00:06:48,610
Similarly if we reach n then lcw of i  n is 0 because we gone pass the length of v.

88
00:06:48,769 --> 00:06:54,009
and If we have not gone past the length if we are somewhere inside the word in a valid position.

89
00:06:54,110 --> 00:06:59,079
then the length is going to be 0 if the two positions are not the same.

90
00:06:59,079 --> 00:07:07,720
if a i is not equal to b j. Otherwise inductively I compute the length from i + 1 and j + 1 and I add 1 to it.

91
00:07:08,089 --> 00:07:11,910
So this is the case when a is equal to b j because that segment I can extend this.

92
00:07:11,910 --> 00:07:16,959
So this is just stating in an equation form the inductive definition that we proposed in the earlier thing.

93
00:07:20,420 --> 00:07:24,339
So here is for example  this bicect and secret.

94
00:07:24,680 --> 00:07:28,709
So we have position 0 to 5 and then we have the sixth position.

95
00:07:28,709 --> 00:07:32,189
indicating the end of the word and now remember that.

96
00:07:32,189 --> 00:07:34,689
The way our inductive definition was phrased.

97
00:07:34,689 --> 00:07:37,439
i j depends only on i + 1 j + 1.

98
00:07:37,439 --> 00:07:42,000
So actually the dependencies are this way. So the arrows are indicating that

99
00:07:42,000 --> 00:07:45,160
In order to solve this I need to solve this first.

100
00:07:45,259 --> 00:07:49,029
the value at 2 3 depends on the value at 3 4.

101
00:07:49,579 --> 00:07:53,430
So in order to solve this I do not need to solve anything.

102
00:07:53,430 --> 00:07:57,310
if was everything on the right. So in order to solve this I do not need to

103
00:07:57,310 --> 00:08:02,230
So we can basically we have this simple thing which says that the corner

104
00:08:02,230 --> 00:08:06,509
and the actually the right column and the bottom thing do not require anything and we know that because

105
00:08:06,509 --> 00:08:09,029
those are all zeros.

106
00:08:09,029 --> 00:08:12,420
So we can actually fill in those values as 0s because that is given to us by definition.

107
00:08:12,420 --> 00:08:16,769
And now we can start for instance we can start with this value because its value is known

108
00:08:16,769 --> 00:08:23,519
So we will look at whether this t matches that t it is that so we take 1 + the value to its bottom right.

109
00:08:23,519 --> 00:08:28,180
So we will get 1 and then we can walk up and do the same thing at every point

110
00:08:28,180 --> 00:08:35,620
if c is not the same as t so none of these letters. If you look at these letters here right none of these letters are t.

111
00:08:35,870 --> 00:08:41,950
So for all of these letters I will get 0 directly because it says that a i is not equal to b j.

112
00:08:42,350 --> 00:08:47,379
So I do not even have to look at i + 1 j + 1 directly says it is 0 because it is not there.

113
00:08:47,379 --> 00:08:52,080
In this way I can fill up this column. This is like our grid path thing I can fill up column by column

114
00:08:52,080 --> 00:08:57,370
Even though there the dependency was to the left and bottom and here the dependency is diagonally bottom right.

115
00:08:57,370 --> 00:09:00,580
i can fill up column by column and I can keep going.

116
00:09:00,580 --> 00:09:03,129
If I keep going I find an entry 3.

117
00:09:04,190 --> 00:09:09,789
So the entry 3 is the largest entry that I see and that is the actual answer this entry 3.

118
00:09:14,059 --> 00:09:23,789
And now We said earlier that we are focusing on the length of the longest common subword not the word itself and the reason we can afford to do that is because

119
00:09:23,789 --> 00:09:26,529
We can actually read off the answer once we have got the lengths.

120
00:09:26,529 --> 00:09:29,169
So we ask ourselves why did we get a 3 here.

121
00:09:29,269 --> 00:09:34,360
So we got a 3 here because we came as 1 + 2.

122
00:09:34,549 --> 00:09:38,919
So since we came as 1 + 2 it must mean that these two letters are the same.

123
00:09:39,019 --> 00:09:42,370
Similarly we got a 2 here because it is 1 + 1.

124
00:09:42,740 --> 00:09:45,340
So these two letters must also be the same.

125
00:09:45,470 --> 00:09:47,639
So these two letters are same these two letters.

126
00:09:47,639 --> 00:09:52,379
So finally we got 1 here because this is 1 + 0 so these two letters are same.

127
00:09:52,379 --> 00:09:54,629
So therefore these three letters must be the same.

128
00:09:54,629 --> 00:10:00,389
So if you walk down from that magical value the largest value and we follow the sequence.

129
00:10:00,389 --> 00:10:04,110
then we can read off on the corresponding row or column because they are the same

130
00:10:04,110 --> 00:10:09,399
the actual subword which is the longest common subword for these two words.

131
00:10:11,629 --> 00:10:15,330
Here is a very simple implementation in python.

132
00:10:15,330 --> 00:10:24,220
So all it says is that you start with two words u and v

133
00:10:24,220 --> 00:10:28,389
you initialize this lcw thing at the boundary

134
00:10:28,460 --> 00:10:31,830
at the nth row and the nth column

135
00:10:31,830 --> 00:10:36,100
and then you remember the maximum value. So you keep track by initializing the maximum value to 0

136
00:10:36,100 --> 00:10:39,669
and then you fill up in this particular case and call a model.

137
00:10:39,669 --> 00:10:43,210
So for each column then for each row and that column.

138
00:10:43,210 --> 00:10:46,409
you fill up the thing using the equation if it is equal.

139
00:10:46,409 --> 00:10:51,360
i do 1 + otherwise I say it 0 and if I see a new value this is the thing where I update

140
00:10:51,360 --> 00:10:55,590
If I see a new entry which is bigger than the entry which is currently the maximum

141
00:10:55,590 --> 00:10:58,889
I update the maximum. This allows me to quickly find out

142
00:10:58,889 --> 00:11:00,730
what is the maximum length overall.

143
00:11:00,730 --> 00:11:04,240
And finally when I have gone through this loop I would have filled up the entire table

144
00:11:04,240 --> 00:11:06,580
then I will return the maximum value I saw over there.

145
00:11:09,409 --> 00:11:13,600
So when we did it by brute force we had an order m n squared algorithm.

146
00:11:13,600 --> 00:11:16,740
Here we are filling up a table which is of size order n by n.

147
00:11:16,740 --> 00:11:22,990
and each entry only requires us to check the i th position in the word the jth position in the word.

148
00:11:22,990 --> 00:11:25,799
depending on that if necessary look up.

149
00:11:25,799 --> 00:11:29,320
one entry i + 1 j + 1. So it is a constant time update.

150
00:11:29,320 --> 00:11:33,750
So we need to fill up a table of size order m, n each update takes constant time.

151
00:11:33,750 --> 00:11:35,309
So this algorithm brings us.

152
00:11:35,309 --> 00:11:39,700
from m n squared in the brute force case to m n using dynamic programming.

153
00:11:44,840 --> 00:11:52,330
So a much more useful problem in practice than the longest common subword is called the longest common subsequence.

154
00:11:52,330 --> 00:11:55,559
So the difference between a subword and a subsequence.

155
00:11:55,559 --> 00:11:58,539
that we are allowed to drop some letters in between.

156
00:11:59,389 --> 00:12:04,120
So for instance if you go back to the earlier examples for secret

157
00:12:04,120 --> 00:12:12,220
And secretary there is no problem because the subword is actually the entire thing and again for bisect and trisect also it is the same thing.

158
00:12:12,220 --> 00:12:20,379
But if you have bisect and secret earlier if we did not allow ourselves to skip then We could only match sec with sec.

159
00:12:20,379 --> 00:12:24,720
Now we can take this extra t and we can skip these two.

160
00:12:24,720 --> 00:12:29,669
and match this t and say that s e c t is a subsequence.

161
00:12:29,669 --> 00:12:34,559
the right word which matches the corresponding subsequence which is also a sub word in the left

162
00:12:34,559 --> 00:12:38,860
the right is not a sub word. The subsequence I have to drop some letters to get a c c t.

163
00:12:38,860 --> 00:12:42,149
Similarly if I have secretary and director

164
00:12:42,149 --> 00:12:45,370
then I can find things like e c t r.

165
00:12:45,370 --> 00:12:49,840
e, c, t, r in both of them by skipping over

166
00:12:50,480 --> 00:12:54,970
Why is this a better problem well we will see that?

167
00:12:55,220 --> 00:13:03,730
So effectively what skipping means skipping means that I get segments which are connected by gaps.

168
00:13:03,830 --> 00:13:08,789
So I get this segment then I want to continue the segments. So I look for the next match.

169
00:13:08,789 --> 00:13:12,389
So I skipped but the next match must come to my right.

170
00:13:12,389 --> 00:13:14,759
So it must come to the right and below the current match

171
00:13:14,759 --> 00:13:18,840
because I cannot go backwards in a word and start a matching.

172
00:13:18,840 --> 00:13:20,279
So I cannot for instance go here and say that

173
00:13:20,279 --> 00:13:24,029
this is an extension because this requires them to go back and reuse

174
00:13:24,029 --> 00:13:28,600
the e that I have seen in sec to match the second e in secret which is not allowed.

175
00:13:28,600 --> 00:13:31,080
So I can keep going forwards.

176
00:13:31,080 --> 00:13:34,379
which in the table corresponds to going to the right and down.

177
00:13:34,379 --> 00:13:38,769
So I am going increasing the order of index in both words.

178
00:13:38,769 --> 00:13:43,470
and I can prove together these things and this is what the longest kind of subsequences.

179
00:13:43,470 --> 00:13:50,940
So we could in principle look at the longest common subword answer and look for these clever connections.

180
00:13:50,940 --> 00:13:54,850
but it turns out there is a much more direct way to do it in an inductive way.

181
00:13:57,529 --> 00:13:59,470
So looking at the motivations

182
00:13:59,870 --> 00:14:07,149
One of the big motivations for subsequence matching comes from things like genetics

183
00:14:07,149 --> 00:14:11,399
for instance when we compare the gene sequences of two organisms they are rarely equal.

184
00:14:11,399 --> 00:14:14,080
So what we are looking for is large matches.

185
00:14:14,080 --> 00:14:16,830
where there might be some junk genes in between.

186
00:14:16,830 --> 00:14:21,179
which we want to discard. So we want to say that two gene sequences or two organisms are similar.

187
00:14:21,179 --> 00:14:25,330
If there are large overlaps over the entire genome

188
00:14:25,330 --> 00:14:33,340
not just looking for individual segments along by just throwing away the minimal things on both sides you can make them align as such

189
00:14:33,529 --> 00:14:36,399
Another important example is something called a

190
00:14:36,399 --> 00:14:39,990
diff which is a UNIX command to compare 2 text files.

191
00:14:39,990 --> 00:14:44,159
This treats in fact line by line two files as a word.

192
00:14:44,159 --> 00:14:48,899
So each line is compared to each line in the other file.

193
00:14:48,899 --> 00:14:52,919
If The lines match they considered to be equal and this is the good way of comparing

194
00:14:52,919 --> 00:14:55,860
one version of a file with another version of file.

195
00:14:55,860 --> 00:15:03,659
So supposing you are collaborating on a document or a program with somebody else and you send it by email and send it back saying I made some changes

196
00:15:03,659 --> 00:15:08,620
then diff tells you quickly what are the differences between the file you sent and the file you got back

197
00:15:08,620 --> 00:15:12,990
It is essentially doing the same thing it is trying to find the longest match between the file

198
00:15:12,990 --> 00:15:14,830
that you sent and the file you got back.

199
00:15:14,830 --> 00:15:18,970
and the shortest way in which you can transform one to the other by changing the few lines.

200
00:15:18,970 --> 00:15:23,039
So these are some typical examples of this longest common subsequence problem.

201
00:15:23,039 --> 00:15:28,029
and therefore it is usually much more useful in practice than the longest common subword

202
00:15:30,980 --> 00:15:35,440
So what is the inductive structure of the longest common subsequence problem?

203
00:15:35,659 --> 00:15:40,450
So As before we have the words data dout.

204
00:15:40,450 --> 00:15:45,309
So we can say 0 a 0 to a m or a m - 1 it does not matter how you choose it

205
00:15:45,309 --> 00:15:50,139
but in the picture it says am but if you want you can ignore these last

206
00:15:52,429 --> 00:15:57,220
So a 0 to a m - 1 is the first word b 0 to b n - 1 is the second word then

207
00:15:57,740 --> 00:16:04,389
And now there are two cases. The first case is the easy case supposing I have these two things are equal.

208
00:16:05,149 --> 00:16:13,029
Then like before I can inductively solve the problem for A1 and B1 onwards and add this.

209
00:16:13,029 --> 00:16:16,379
I can extend that solution by saying a 0 matches b 0.

210
00:16:16,379 --> 00:16:20,740
and whatever match. So what is the subsequence? A subsequence actually is some kind of a a matching

211
00:16:20,740 --> 00:16:27,539
You know it will say that. This match is this

212
00:16:27,539 --> 00:16:31,620
then this match is this these are the same and this match is this and then this match is this and so on

213
00:16:31,620 --> 00:16:35,409
only thing is that these lines cannot overlap they must keep going from left to right without overlapping

214
00:16:35,409 --> 00:16:41,171
So this kind of pairing up of equal letters the maximum way which I can do this is a longest common subsequence.

215
00:16:41,196 --> 00:16:47,970
So Now what we are saying is that if I can actually match the first two things then I should match them

216
00:16:47,970 --> 00:16:50,529
and then I can go ahead and match the rest as I want.

217
00:16:51,870 --> 00:16:56,049
and the reason is very simple supposing the best solution did not match these.

218
00:16:56,049 --> 00:17:01,149
supposing you claim that the best solution actually requires me to match a 0 and b 1.

219
00:17:01,149 --> 00:17:05,309
Well if I could match a 0 and b 1 I can also undo it and match a 0 and b 0 and then

220
00:17:05,309 --> 00:17:09,279
continue because a 0 and b 1 if they are match and a 1 must match to its right.

221
00:17:09,279 --> 00:17:13,470
So I can take that solution and change it to a solution where a 0 matches b 0.

222
00:17:13,470 --> 00:17:22,210
So if the first two letters are the same I might as well go with that and say it is 1 + the result of optimally solving the rest.

223
00:17:22,460 --> 00:17:27,400
What if they are not the same this is the interesting case right so supposing these are not the same

224
00:17:27,619 --> 00:17:33,250
then what happens. So then can we just go ahead and ignore a 0 and b 0 no.

225
00:17:33,619 --> 00:17:37,799
So it could be that a0 actually matches b1.

226
00:17:37,799 --> 00:17:41,259
or it could be that b 0 matches a 1 we do not know.

227
00:17:41,259 --> 00:17:44,170
but we certainly know that a 0 does not match b 0.

228
00:17:44,170 --> 00:17:46,390
So we have to drop one of them.

229
00:17:46,390 --> 00:17:50,589
because we cannot make a solution with a 0 matching b 0 but we do not know which one.

230
00:17:50,589 --> 00:17:52,359
So what we do is we.

231
00:17:52,400 --> 00:17:57,369
Take two sub problems we say okay let us assume b 0 is not part of the solution.

232
00:17:57,369 --> 00:18:03,579
then the best solution must come out of a0 to a m - 1 and b 1 to b n.

233
00:18:03,579 --> 00:18:06,900
B0 is excluded because I cannot match it with A0.

234
00:18:06,900 --> 00:18:10,299
and whatever a 0 matches must match to the right so I must throw it away

235
00:18:10,299 --> 00:18:13,890
but may be this is the wrong choice. The other choice would be to keep

236
00:18:13,890 --> 00:18:18,700
b 0 and drop a 0 in which case I do a 1 to a m - 1 and b 0 to b n.

237
00:18:18,700 --> 00:18:22,480
So these are two different choices.

238
00:18:23,000 --> 00:18:27,250
which one do I choose well since we do not know.

239
00:18:27,250 --> 00:18:28,599
we solve them both.

240
00:18:29,089 --> 00:18:34,289
we solve If a i if a 0 is not b 0 we solve both these problems a 1

241
00:18:34,289 --> 00:18:39,269
a m - 1 b 0 and a 0 to a m - 1 b 1. solve both of them.

242
00:18:39,269 --> 00:18:42,670
take the maximum one whichever one is better is a 1

243
00:18:47,000 --> 00:18:51,779
So this in general will take us deeper in the word.

244
00:18:51,779 --> 00:18:55,750
a0 b0 will require to solve it for a1 and b0 or

245
00:18:55,750 --> 00:19:00,210
a b a 0 and b 1. So in general we have a i and b j right

246
00:19:00,210 --> 00:19:03,779
So Again since we have a i and b j then

247
00:19:03,779 --> 00:19:07,900
we will use the same logic if a i is equal to b j then it is 1 + the rest.

248
00:19:08,180 --> 00:19:12,069
Then this is the good case. If <DTN>a i is not equal to b j then right

249
00:19:13,609 --> 00:19:15,690
Then what we do is.

250
00:19:15,690 --> 00:19:19,779
we look at the same thing we drop b j and solve it

251
00:19:20,690 --> 00:19:25,359
and symmetrically we drop a i and solve it and take the better of the 2.

252
00:19:25,359 --> 00:19:32,259
We take max of the solution from i and the solution from j + 1.

253
00:19:32,450 --> 00:19:37,329
So if we say like we had before that LCS of ij.

254
00:19:37,329 --> 00:19:40,859
is the length of the longest common subword starting from i and j.

255
00:19:40,859 --> 00:19:44,819
if a i is equal to b j it will be 1 + the length starting from i + 1 j + 1.

256
00:19:44,819 --> 00:19:46,230
If it is not equal.

257
00:19:46,230 --> 00:19:50,619
It will be the maximum of the two sub problems where either increment i or increment j.

258
00:19:51,589 --> 00:19:54,730
and has with the longest common subword.

259
00:19:54,730 --> 00:20:00,039
okay When we go to the last position m and n we get 0.

260
00:20:02,359 --> 00:20:05,519
So here the dependency is slightly more complicated because

261
00:20:05,519 --> 00:20:12,509
depending on the case I either have to look at i + 1 j + 1 or I have to look at i + 1 j or i j + 1.

262
00:20:12,509 --> 00:20:17,319
this square I have to look at its right neighbor right diagonal neighbor and the bottom

263
00:20:17,660 --> 00:20:19,240
But once again

264
00:20:19,240 --> 00:20:22,920
the ones which have no dependency are clear.

265
00:20:22,920 --> 00:20:25,619
So earlier we had for longest common subword we had only this dependency.

266
00:20:25,619 --> 00:20:29,039
This meant that even a square like this had no dependencies because

267
00:20:29,039 --> 00:20:33,549
there is nothing to its bottom right. But now for instance if we look at this picture

268
00:20:33,549 --> 00:20:36,369
Since we are looking bottom right and left.

269
00:20:36,369 --> 00:20:40,119
If I look at this its dependencies are in three directions.

270
00:20:40,309 --> 00:20:42,180
Two of those directions are empty.

271
00:20:42,180 --> 00:20:45,490
but this direction there is dependence. So I cannot fill up this square directly.

272
00:20:45,490 --> 00:20:51,099
The only square I can fill up directly is this one because it has nothing to its right nothing in directly and nothing below.

273
00:20:51,289 --> 00:20:52,990
So I start from there.

274
00:20:53,299 --> 00:20:56,970
So I start from there and I put a 0 and as before.

275
00:20:56,970 --> 00:21:01,089
we can go down this because now once we have this we have everything to its left.

276
00:21:01,220 --> 00:21:06,700
and once we have this and because we have beyond the word we are at this dummy position.

277
00:21:06,700 --> 00:21:10,859
the row and column becomes 0 but the important thing to remember is the row and column have no dependency

278
00:21:10,859 --> 00:21:14,039
but because we can systematically fill it up.

279
00:21:14,039 --> 00:21:16,950
exactly like in the grid paths we can fill up the bottom row.

280
00:21:16,950 --> 00:21:20,250
and the left most column there here the right most column.

281
00:21:20,250 --> 00:21:22,089
Once we have this we can fill up this point.

282
00:21:23,390 --> 00:21:29,730
and then again we can once we have three entries we can fill this

283
00:21:29,730 --> 00:21:33,069
So we can fill up this column and we can do this column by column.

284
00:21:33,069 --> 00:21:36,869
and we propagate it and then finally the value that propagates here

285
00:21:36,869 --> 00:21:40,420
is our longest length of the longest common subsequence.

286
00:21:40,789 --> 00:21:42,910
You could also do this row by row.

287
00:21:44,420 --> 00:21:48,369
Now how do we trace out the actual solution?

288
00:21:48,369 --> 00:21:52,170
the solution grows whenever we increment the number.

289
00:21:52,170 --> 00:21:57,089
So we can ask why is this 4? So we say that this is 4 because

290
00:21:57,089 --> 00:22:02,650
we did 1 + 3 because s is not equal to b which is 4 because we got max value from here.

291
00:22:02,650 --> 00:22:06,309
Why is this 4 again i is not equal to s so we got the max value from here.

292
00:22:06,309 --> 00:22:10,359
Why is this 4? S is equal to S we must have got it by 3 + 1.

293
00:22:10,359 --> 00:22:14,829
why is this because e + c so we must have got it from here.

294
00:22:14,829 --> 00:22:18,779
So we follow the path according to the choices that we made in applying the inductive function.

295
00:22:18,779 --> 00:22:23,640
in order to generate the value at each point. In other words for each cell i j

296
00:22:23,640 --> 00:22:28,289
we remember whether we wrote it because it was 1 + the diagonal labour

297
00:22:28,289 --> 00:22:36,490
or the maximum of the left in the right in which case we record whether the left or the bottom was the maximum we picked.

298
00:22:36,490 --> 00:22:40,799
Now in this picture every time we take a diagonal step it means we actually had a match.

299
00:22:40,799 --> 00:22:45,569
This is a match the first one here is a match s equal to s.

300
00:22:45,569 --> 00:22:50,400
this is a match equal to e this is a match c equal to c. Now after this point

301
00:22:50,400 --> 00:22:54,390
we have flat and then at this point again we have a match. So we get s c c and t.

302
00:22:54,390 --> 00:23:02,730
So we can read off the diagonal steps along this explanation of the largest number we got.

303
00:23:02,730 --> 00:23:07,500
and each diagonal step will contribute to the final solution. Now there could be more than one

304
00:23:07,500 --> 00:23:14,109
we have not got an example in this case but sometimes the max could be one in both directions if I am taking the max of the left.

305
00:23:14,109 --> 00:23:16,720
the right neighbor and the bottom they could be the same.

306
00:23:17,000 --> 00:23:21,789
So I could have a situation like this supposing I landed up here thet I do not know whether i wanted from here or here

307
00:23:21,829 --> 00:23:25,470
So I might have two different extensions which lead me to a solution.

308
00:23:25,470 --> 00:23:31,000
The longest common subsequence need not be unique but you can recover at least one by following this path.

309
00:23:32,660 --> 00:23:34,140
So here is the  python code

310
00:23:34,140 --> 00:23:42,630
it is not very different from the earlier one. So we can see we have just initialized the last row and the bottom row and the last column and then as before.

311
00:23:42,630 --> 00:23:43,660
you walk up.

312
00:23:43,970 --> 00:23:47,650
row by row column by column and fill in using the equation.

313
00:23:47,650 --> 00:23:55,119
And in this case we do not have to keep track of the maximum value and keep updating because the maximum value automatically propagates to the 00 value.

314
00:23:57,619 --> 00:24:02,579
So Just like the longest common subword here once again.

315
00:24:02,579 --> 00:24:05,200
we are filling in a table of size m times n.

316
00:24:05,200 --> 00:24:11,109
each entry only requires look at at most three other entries so one to the right the one to the bottom right and the back.

317
00:24:11,599 --> 00:24:13,529
So it is a constant amount of work.

318
00:24:13,529 --> 00:24:19,390
So m n entries constant amount of a plapper entry so this takes time m times n.

