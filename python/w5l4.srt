1
00:00:03,589 --> 00:00:07,269
So the last lecture we saw how to read and write text files.

2
00:00:07,490 --> 00:00:13,330
And reading and writing text, invariably involves processing the strings that we read and write.

3
00:00:13,460 --> 00:00:20,053
And so, python has a number of string processing functions that make it easier to modify these contents.

4
00:00:20,078 --> 00:00:23,829
So usually you are reading and writing files in order to do something with these files.

5
00:00:23,829 --> 00:00:27,760
And to do something with these, you can use the built-in string functions, which are quite powerful.

6
00:00:27,760 --> 00:00:34,270
Among other things you can do with these string functions you can search for text or search and replace it.

7
00:00:34,789 --> 00:00:41,740
A typical use of string processing for a file is when we take something like a spreadsheet and export it as text.

8
00:00:41,740 --> 00:00:51,130
So there is something called a comma separated value format  CSV , where the columns are output separated by commas as text.

9
00:00:51,130 --> 00:01:00,738
So now, what we can do with the string file is to read such a file line by line, and in each line extract the columns from the text by reading between the commas.

10
00:01:00,763 --> 00:01:03,399
So we will see  all these in this lecture.

11
00:01:05,060 --> 00:01:11,903
So the first example of a string command that we already saw last time is the command to strip white space.

12
00:01:11,928 --> 00:01:18,989
So we have r strip, for example which we used to remove the trailing white space, the backslash ns in our lines.

13
00:01:18,989 --> 00:01:26,530
And we had l strip to remove leading white space and we had strip which removes it on both directions.

14
00:01:26,530 --> 00:01:28,900
So let's see how this works.

15
00:01:31,400 --> 00:01:39,640
Let's create a string which has whitespace before and afterwards and let us put some spaces, tab, then the word hello, then two tabs.

16
00:01:41,689 --> 00:01:47,769
So we have a string which has white space and you can see the tabs are indicated by backslash t and blanks.

17
00:01:47,769 --> 00:01:53,135
So now, if we want to just strip from the right we say t is equal to s dot r strip.

18
00:01:53,160 --> 00:01:57,420
So remember, because strings are immutable the strip command would not change s.

19
00:01:57,420 --> 00:01:58,420
It will just return a new string.

20
00:01:58,445 --> 00:02:03,180
So if I say, t is s dot r strip, it will strip the whitespace to the right, and give me t.

21
00:02:03,180 --> 00:02:07,530
If I look at t, it has everything up to hello, but not the tab and the space afterwards.

22
00:02:07,530 --> 00:02:17,050
Similarly if I say t is s dot l strip, it will remove the ones to the left, and now t will start with hello, but it will have the white space at the end.

23
00:02:17,121 --> 00:02:23,860
And finally, if I say t is s dot strip, then both sides are gone, and I will just get the word that I want.

24
00:02:23,860 --> 00:02:29,163
So this is useful, because when you ask people to type things and forms for example, usually they leave some blanks before and after.

25
00:02:29,190 --> 00:02:41,229
So if you want everything before the first blank and after the last blank to be lost , and only keep the text in between, you can use a combination of l strip, r strip or just strip to extract the actual data that you want from the file.

26
00:02:43,849 --> 00:02:48,310
So the next thing that we want to do is to look for text in a string.

27
00:02:48,310 --> 00:02:50,909
So there is a basic command called find.

28
00:02:50,909 --> 00:02:59,020
So if s is a string, and pattern is an other string that I am looking for in s, s dot find pattern will return the first position in s in which pattern occurs.

29
00:02:59,020 --> 00:03:04,900
So the position will there obviously be between 0 and the length of s minus 1.

30
00:03:04,900 --> 00:03:08,560
We already wrote some of our own implementations of this earlier.

31
00:03:08,873 --> 00:03:12,909
So if it doesn't occur, it will give you minus 1.

32
00:03:14,449 --> 00:03:17,109
Now, sometimes you may not want to search the entire string.

33
00:03:17,134 --> 00:03:29,949
So pattern takes an optional pair of arguments< start and end, ,and instead of looking for the pattern from the entire string, it looks at a slice from start to end with the usual convention that this is the position from start to n minus 1.

34
00:03:32,270 --> 00:03:41,439
And there is another version of this command called index, and the difference between find and index is what happens when the pattern is not found.

35
00:03:41,439 --> 00:03:44,500
In find, when the pattern is not found you get a minus 1.

36
00:03:44,500 --> 00:03:50,110
In index, when the pattern is not found, you get a special type of error, in this case a value error.

37
00:03:50,120 --> 00:03:53,169
So again let us just see how these things actually work.

38
00:03:56,150 --> 00:04:02,849
So we have a string here s which contains the words brown, fox, grey, dog, brown, fox.

39
00:04:02,849 --> 00:04:12,879
Now, if I ask it to look for the first occurrence of the word “Brown”, then it will return the position 0 because it is right there at the beginning of the string.

40
00:04:12,879 --> 00:04:21,069
If on the other hand I do not want this position, but I want to say starting from position 5 and going to the length of s for example.

41
00:04:21,069 --> 00:04:24,899
Then, it will say 19, and if you count you will find that it is correct.

42
00:04:24,899 --> 00:04:27,420
The second occurrence of Brown is at position 19.

43
00:04:27,445 --> 00:04:33,927
If on the other hand, I look for something which is not there like cat, then find will return  minus 1.

44
00:04:33,952 --> 00:04:39,519
So  minus 1 is the indication that the string was not found.

45
00:04:39,709 --> 00:04:48,000
Now, difference with index is that if I give index the same thing, instead of a minus 1 it gives me a value error saying the substring does not occur.

46
00:04:48,000 --> 00:04:50,259
So this is how find and index work.

47
00:04:54,199 --> 00:04:58,930
So the next natural thing after searching is searching and replacing.

48
00:04:58,930 --> 00:05:09,850
So if I want to replace something, I give it two strings: what I am searching for, and what I am replacing it with, and it will return a copy of s with each occurrence of the first string replaced by the second string.

49
00:05:11,779 --> 00:05:19,889
Now, this can be controlled in the following way, supposing I do not want each occurrence, but I only want the first occurrence or the first three occurrences.

50
00:05:19,889 --> 00:05:24,865
So I can give it an optional argument saying how many such occurrences starting from the beginning should be replaced.

51
00:05:24,890 --> 00:05:40,149
So it says replace at most the first n copies, and notice that like in strip, it's changing the string but because strings are immutable, it is going to return us the transformed string.

52
00:05:40,149 --> 00:05:41,949
So let's look at an example.

53
00:05:44,660 --> 00:05:49,319
Once again, let's use our old example, s is Brown, Fox, grey, Dog, brown, Fox.

54
00:05:49,319 --> 00:05:58,110
And now, supposing I want to replace brown by black, then I get black, fox, grey, dog, black, fox.

55
00:05:58,110 --> 00:06:06,100
If I say only want one to be replaced, then I get black, fox, grey, dog and the second brown is left unchanged.

56
00:06:06,379 --> 00:06:13,831
Now, if I ask what happens if I have this pattern, it does not neatly split up, if I the different copies of brown overlap.

57
00:06:13,856 --> 00:06:26,110
So supposing I have some stupid string like ababa, and now, I say replace all abas by say DD.

58
00:06:27,139 --> 00:06:33,540
So, now the question is, will it find two abas or one aba because there is an aba starting at position 0.

59
00:06:33,540 --> 00:06:37,240
There is also an aba in the second half of the string starting at position 2.

60
00:06:37,240 --> 00:06:41,240
The question is will it mark both of these and replace them by DD.

61
00:06:41,265 --> 00:06:47,220
Well, it doesn't because it does it sequentially so it first takes the first aba, replaces by DD.

62
00:06:47,245 --> 00:06:50,560
At this point, the second aba has been destroyed.

63
00:06:50,560 --> 00:06:52,000
So it will not find it.

64
00:06:52,189 --> 00:07:02,689
Whereas, if I had had, for instance, two copies of this disjoint, then it would have correctly found this, and given me DD followed by DD.

65
00:07:02,714 --> 00:07:13,660
So there is no problem about overlapping strings, it just does it from left to right, and it makes sure that the overlapped string is first written so the second copy will not get transferred.

66
00:07:19,310 --> 00:07:23,110
So the next thing that we want to look at is splitting a string.

67
00:07:23,110 --> 00:07:29,709
Now, when we take a spreadsheet and write it out as text, usually what happens is that we will have an output which looks like this.

68
00:07:33,259 --> 00:07:36,985
So, the first column would be written followed by comma then second column.

69
00:07:37,010 --> 00:07:46,079
So we had three columns and the first column said 6, second column said 7 ,and the third was a string hello, then when we write it out as text we will get 6, 7 and hello.

70
00:07:46,079 --> 00:07:50,529
So actually hello as a bit of a problem because it has double quotes so let us not use hello.

71
00:07:50,529 --> 00:07:57,670
Let us use something simpler so let's just say that we had three numbers 6, 7, and 8 for example.

72
00:07:58,879 --> 00:08:02,410
Now, what we want to do is we want to extract this information.

73
00:08:02,490 --> 00:08:08,980
So we want to extract the individual 6, 7 and 8 that we had as 3 values.

74
00:08:08,980 --> 00:08:12,000
So what we need to do is look for this text between the strings.

75
00:08:12,000 --> 00:08:17,846
So we want to split the column into chunks between the commas, and this is done using the split command.

76
00:08:17,871 --> 00:08:30,228
So split takes a string s, and takes a character or actually it could be any string, and it splits the columns, it gives you a list of values that come by taking the parts between the commas.

77
00:08:30,253 --> 00:08:35,340
So up to the first comma is the first thing so columns is just the name that we have used, it could be any list.

78
00:08:35,340 --> 00:08:42,399
So, the first item of the list will be up to the first comma then between the first and second comma, and so on, and finally after the last comma.

79
00:08:43,549 --> 00:08:58,840
So comma in this case is not a very special thing, you can split using any separator string, and again just like in replace we could control how many times we replace, here you can also control how many splits you make.

80
00:08:58,840 --> 00:09:06,750
So you can say split according to this string, so notice that this could be any string so here we are splitting it according to space colon space.

81
00:09:06,750 --> 00:09:09,196
But, we are saying do not make more than n chunks.

82
00:09:09,221 --> 00:09:18,580
So if you have more than n columns or whatever chunks which come like this, beyond a certain point we will just lump it as one remaining string, and keep it with us.

83
00:09:18,580 --> 00:09:21,039
So again let's see how this works.

84
00:09:23,509 --> 00:09:31,860
Suppose, this is our line of text which I call csvline, it is a sequence of values separated by commas, notice it is a string.

85
00:09:31,860 --> 00:09:42,850
So now if I say csvline dot split, using comma as a separator, then I get a list of values the string 6, the string 7, the string 8.

86
00:09:42,850 --> 00:09:45,690
Remember this is exactly like what we said about input.

87
00:09:45,690 --> 00:09:49,796
It doesn't get you the values in the form that you want, you then have to convert them using int or something.

88
00:09:49,821 --> 00:09:55,356
These are still strings so it just takes a long string, and splits it up into a list of smaller strings.

89
00:09:55,381 --> 00:10:01,783
Now, here there are three elements so, if I say for example, I only want position 0,1 and 2.

90
00:10:01,808 --> 00:10:08,919
So if I say I only want it to do it once, then I get the first 6, but then 7 and 8 doesn't get split because it only splits once.

91
00:10:09,740 --> 00:10:19,720
Now, if I change this to something more fancy like say hash question mark.

92
00:10:19,940 --> 00:10:28,090
So now, I have a different separator which is not a single character, but hash question mark, then I can say split according to hash question mark.

93
00:10:28,129 --> 00:10:30,949
And this will give me the same thing.

94
00:10:30,974 --> 00:10:34,750
So you can split according to any string, it's just a uniform string.

95
00:10:34,750 --> 00:10:39,918
There are more fancy things you can do with regular expressions and all that, but we won't be covering that for now.

96
00:10:39,943 --> 00:10:44,980
As long as you have a fixed string which separates your thing, you can split according to that fixed string.

97
00:10:49,159 --> 00:10:52,976
So the inverse operation of split would be to join strings.

98
00:10:53,001 --> 00:10:59,590
So supposing we have a collection of strings, and I want to combine it into single string and separate each of them by a given separator.

99
00:10:59,720 --> 00:11:15,120
So as an example, supposing we take s, which is some csv output, and we split it into columns on comma, then we can take join string, and set it to the value comma, and then use that to join the columns.

100
00:11:15,120 --> 00:11:19,539
So join is a function which is associated with the string.

101
00:11:19,549 --> 00:11:22,103
In this case, the string in concern is a comma.

102
00:11:22,128 --> 00:11:28,149
So more or less you are saying comma dot join columns, which is use comma to join columns.

103
00:11:28,149 --> 00:11:32,429
So you just given it a name here joinstring is equal to comma.

104
00:11:32,429 --> 00:11:35,630
And then, csvline is joinstring dot join columns.

105
00:11:35,655 --> 00:11:51,070
So what this says is, use comma to separate so if at the end of this I have got like last
time 6, 7 and 8, then this will now put them back as 6 comma 7 comma 8 in to a single string.

106
00:11:51,740 --> 00:12:05,250
Here is another example, here we have a date 16 a month 08 and a year 2016 given as strings, and I want to string it together into a date like we normally use with hyphens.

107
00:12:05,250 --> 00:12:11,885
So here instead of giving an intermediate name to the hyphen and then saying hyphen dot join, I directly use the string itself.

108
00:12:11,910 --> 00:12:23,110
Just want to illustrate that you can directly use the joining string itself as a constant string and say use this to join this list of values.

109
00:12:23,135 --> 00:12:32,679
So all you need to make sure is that what you have inside the argument of join is a list of strings, and what you applied to is the string which will be used to join them.

110
00:12:32,690 --> 00:12:36,970
So let's just check that this works the way we actually intended to do.

111
00:12:37,429 --> 00:12:51,129
So let's directly do the second example so supposing we say date is 16, remember these are all strings, month is 08, year is 2016.

112
00:12:51,129 --> 00:13:06,580
And now I want to say, what is the effect of joining these three things using dash as a separator, and I get 16 dash 08 dash 2016.

113
00:13:09,799 --> 00:13:15,354
So there are many other interesting things you can do with strings for example, you can manipulate upper case and lower case.

114
00:13:15,379 --> 00:13:22,620
If you say capitalize, what it will do is it will convert the first letter to upper case, and keep the rest as lower case.

115
00:13:22,620 --> 00:13:25,980
If you say s dot lower it will convert all upper case to lower case.

116
00:13:25,980 --> 00:13:29,830
If you say s dot upper it will convert all lower case to upper case and so on.

117
00:13:29,830 --> 00:13:34,409
There are other fancy things like s dot title so title will capitalize each word.

118
00:13:34,409 --> 00:13:37,870
So this is how it normally appears in the title of a book or a movie.

119
00:13:37,870 --> 00:13:43,450
s dot swapcase will invert lower case to upper case and upper case to lower case and so on.

120
00:13:43,475 --> 00:13:52,269
So there are whole collection of functions in the strings which deal with upper case, lower case and how to transform between these.

121
00:13:53,210 --> 00:13:58,690
The other thing that you can do with strings is to resize them to fit what you want.

122
00:13:58,759 --> 00:14:08,679
So if you want to have a string which is positioned as a column of certain width, then we can say that center it in a block of size n.

123
00:14:08,750 --> 00:14:14,830
So, what this will do is it will return a new string which is of length n with s centered in it.

124
00:14:15,740 --> 00:14:20,039
Now, by centering what we mean is that on either side there will be blanks.

125
00:14:20,039 --> 00:14:23,639
Instead of blanks, you can put anything you want like stars or minuses.

126
00:14:23,639 --> 00:14:29,710
So you can give a character which will be used to fill up the empty space on either side, rather than a blank.

127
00:14:29,735 --> 00:14:33,043
Now, you may not want it centered, you may want to the left or the right.

128
00:14:33,068 --> 00:14:40,809
So you can for example, left justify it using ljust or right justify it using rjust, and again you can give an optional character and so on.

129
00:14:41,179 --> 00:14:44,649
So we can just check one or two of these just to see how they work.

130
00:14:46,759 --> 00:14:53,980
Suppose we take a short string like hello, and now we center it in a large block of 50.

131
00:14:54,080 --> 00:14:56,169
So, we say s dot center.

132
00:14:57,740 --> 00:15:01,210
Then this gives us hello with a lot of blank spaces on either side.

133
00:15:01,210 --> 00:15:08,259
Now, we can replace those blank spaces by anything we want like minus sign, and we get a string of minus signs or hyphens before that.

134
00:15:08,299 --> 00:15:13,865
Now, we can also say that I want the thing left justified and not centered.

135
00:15:13,890 --> 00:15:20,139
So if I do that, then I will get hello at the beginning and a bunch of minus signs similarly with rjust and so on.

136
00:15:24,080 --> 00:15:29,750
Some of the other types of functions which you find associated with strings are to check the properties of strings.

137
00:15:29,775 --> 00:15:34,620
Does s consist only of the letters a to z and capital a to capital z.

138
00:15:34,620 --> 00:15:38,503
So that is what s dot is α says, is it an alphabetic string.

139
00:15:38,528 --> 00:15:42,390
If it is true it means it is, if it is not it has at least one non alphabetic character.

140
00:15:42,415 --> 00:15:46,950
Similarly, is it entirely digits is numeric will tell us if it is entirely digits.

141
00:15:46,950 --> 00:15:55,450
There is a huge number of string functions, and there is no point going through all of them in this lecture, if we need them as we go along, we will use them and explain them.

142
00:15:55,450 --> 00:16:03,570
But you can look at the python documentation, look under string functions, and you will find a whole host of useful utilities which allow you to easily manipulate strings.

143
00:16:03,570 --> 00:16:09,825
And this is one of the reasons that python is a popular language because you can do this kind of easy text processing.

144
00:16:09,850 --> 00:16:19,623
So you can use it to quickly transform data from one format to another, and to change the way it looks or to resize it and so on.

145
00:16:19,699 --> 00:16:28,539
So string functions are an extremely important part of python’s utility as a language for transforming things from one format to another.

