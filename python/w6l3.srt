1
00:00:03,470 --> 00:00:05,580
So, we will be looking at backtracking.

2
00:00:05,580 --> 00:00:09,449
So in backtracking we systematically search for a solution one step at a time.

3
00:00:09,449 --> 00:00:14,289
and when we hit a dead end we undo the last step and try the next option.

4
00:00:15,349 --> 00:00:21,960
Now, in the process of doing backtracking we need to generate all the possibilities.

5
00:00:21,960 --> 00:00:28,839
Remember when we try to print out all queens we ran through every possible position for every queen on every row

6
00:00:28,839 --> 00:00:31,042
and if it was free then we tried it out

7
00:00:31,067 --> 00:00:34,840
and if the solution extended to a final case then we printed it out.

8
00:00:34,909 --> 00:00:43,119
Now, if you look at the solutions that we get for eight queens each queen on a row is in a different column from every other queen.

9
00:00:43,119 --> 00:00:46,770
So, the column numbers if we read them row by row.

10
00:00:46,770 --> 00:00:50,356
The column numbers form a permutation of zero to n minus one

11
00:00:50,576 --> 00:00:56,560
So, each number zero to n minus one occurs exactly once as a column number for the n queens.

12
00:00:58,549 --> 00:01:08,769
So, one way of solving a problem like eight queens or similar problems is actually to generate all permutations and keep trying them one at a time.

13
00:01:08,870 --> 00:01:11,560
So, this gives rise to the following question.

14
00:01:11,560 --> 00:01:17,435
If we have a permutation of zero to n minus one, how do we generate the next permutation.

15
00:01:17,609 --> 00:01:25,829
So, this is like thinking of it as a next number, but this could be in a arbitrary number of symbols. So suppose for instance we have the

16
00:01:25,829 --> 00:01:29,909
letters a to m. So, these are the first thirteen letters of the alphabet.

17
00:01:29,909 --> 00:01:35,159
and we treat the dictionary order of words as the ordering on the numbers. So, we think of them as digits

18
00:01:35,159 --> 00:01:37,329
If you want you can think of it as base thirteen.

19
00:01:37,329 --> 00:01:45,462
So, here for instance is a number in base thirteen or alternatively a rearrangement of a to m in some order.

20
00:01:45,487 --> 00:01:52,629
Now what we want to know is the next immediate rearrangement after this in dictionary order.

21
00:01:55,730 --> 00:01:58,209
So, in order to solve this problem.

22
00:01:58,209 --> 00:02:00,939
The first observation we can make is that.

23
00:02:00,939 --> 00:02:04,379
If we have a sequence of such letters or digits.

24
00:02:04,379 --> 00:02:09,520
The smallest permutation is the one in which the elements are arranged in ascending order.

25
00:02:09,520 --> 00:02:13,110
So, we start with a which is smallest one then b and c and so on.

26
00:02:13,110 --> 00:02:15,699
and there is no smaller permutation than this one.

27
00:02:15,889 --> 00:02:24,360
Similarly, the largest permutation is one in which all the elements are in descending order. So, we start with the largest element m,

28
00:02:24,360 --> 00:02:26,560
and we work backwards down to a.

29
00:02:28,099 --> 00:02:37,599
So, if you want to find the next permutation we need to find as short a suffix as possible that can be incremented.

30
00:02:37,599 --> 00:02:43,863
It is probably easiest to do it in terms of numbers, but let us do it with letters.

31
00:02:44,170 --> 00:02:53,650
So, the shortest suffix that can be incremented consists of something followed by the longest suffix that cannot be incremented.

32
00:02:53,650 --> 00:02:56,830
So, this will become a little clear when we work through an example.

33
00:02:57,229 --> 00:03:01,300
So, we want to find the longest suffix that cannot be incremented.

34
00:03:01,300 --> 00:03:08,199
So, a suffix that cannot be incremented is one which is as large as it could possibly be, which means it is already in descending order.

35
00:03:08,199 --> 00:03:12,639
So if you look at the example that we had before for which we wanted to find the next permutation.

36
00:03:12,639 --> 00:03:16,889
we find that this suffix o n m j i

37
00:03:16,889 --> 00:03:24,509
these five letters are in descending order. So, I cannot make any larger permutation using this. So, if I fix the letters from d to k,

38
00:03:24,509 --> 00:03:28,479
then this is the largest permutation I can generate with d to k fixed.

39
00:03:28,479 --> 00:03:32,729
So, if I want to change it, I need to increment something

40
00:03:32,729 --> 00:03:42,520
But I cannot increment it within this red box. So, I must extend this. to find the shortest suffix namely the suffix starting with k where, something can be incremented.

41
00:03:42,889 --> 00:03:46,919
Now, how do we increment this?

42
00:03:46,919 --> 00:03:53,080
Because with k we cannot do any better, so we have to replace k by something bigger.

43
00:03:53,080 --> 00:03:56,729
and the something bigger has to be the smallest thing that we can replace it by.

44
00:03:56,729 --> 00:04:00,789
So, we will replace k by the next largest letter to its right namely m.

45
00:04:00,789 --> 00:04:03,750
Among these letters m, n and o are bigger than k.

46
00:04:03,750 --> 00:04:07,319
If I replace it by j or i, I will get a smaller permutation which I do not want.

47
00:04:07,319 --> 00:04:09,719
So, I will just replace it by m, n or o.

48
00:04:09,719 --> 00:04:17,829
But among these since m is the smallest I must now start a sequence where the suffix of length six begins with the letter m.

49
00:04:17,870 --> 00:04:27,209
And among suffixes which begin with the letter m I need the smallest one, that means I may still need to rearrange the remaining letters k o and j i.

50
00:04:27,209 --> 00:04:34,000
in ascending order to give me the smallest permutation which begins with m and has the letters k o and j i after it.

51
00:04:35,209 --> 00:04:39,339
So, this gives me this permutation so I have now moved this m.

52
00:04:39,800 --> 00:04:46,269
and now I have taken these letters and rearrange them in an ascending order to get i j k n.

53
00:04:46,490 --> 00:04:52,899
So, therefore, this means that for this permutation the next permutation is this one.

54
00:04:55,970 --> 00:04:58,800
So, algorithmically we can do it as follows.

55
00:04:58,800 --> 00:05:02,819
What we need to do is first identify the suffix that can be incremented.

56
00:05:02,819 --> 00:05:10,050
So, we begin by looking for the suffix that cannot be incremented namely we go backwards so long as it is in descending order.

57
00:05:10,075 --> 00:05:14,189
So, we keep looking for values as they increase. So, i is smaller than j, j is smaller than m,

58
00:05:14,189 --> 00:05:17,879
m is smaller than n, n is smaller than o, but o is bigger than k.

59
00:05:17,879 --> 00:05:22,860
So that means that up to here we have a suffix that cannot be incremented.

60
00:05:22,860 --> 00:05:25,509
and this is the first position where we can make an increment.

61
00:05:26,839 --> 00:05:34,240
Having done this we now need to replace k by the next biggest letter to its right.

62
00:05:34,240 --> 00:05:43,470
Now, this is a bit like insert. We go one by one we say that k is smaller than o so we continue, k smaller than n so we continue.

63
00:05:43,470 --> 00:05:45,209
k is smaller than m we will continue.

64
00:05:45,209 --> 00:05:52,290
And now we say that k is bigger than j, so we stop here. So, this tells us that the letter m is the one that we want.

65
00:05:52,290 --> 00:05:54,569
So, we can identify this in one scan.

66
00:05:54,569 --> 00:05:57,779
because remember this is in descending order so it is in sorted order.

67
00:05:57,779 --> 00:06:02,250
So, we can go through and find the first position where we crossed from something bigger than k

68
00:06:02,250 --> 00:06:09,490
to something smaller than k and that is the position of the letter that we need to change. So it is exactly like inserting something into a sorted list

69
00:06:09,769 --> 00:06:13,680
Now, having done this, so we have exchanged this m and k.

70
00:06:13,680 --> 00:06:20,050
Now, we need to put this in ascending order but remember it was in descending order

71
00:06:20,050 --> 00:06:25,029
and what we did to the descending order is we replaced m by k, but what is the property of k

72
00:06:25,029 --> 00:06:31,540
k was smaller than m but bigger than j, so o, n k j i remains in descending order.

73
00:06:31,540 --> 00:06:35,430
So if you want to convert it to ascending order we do not need to sort it we just need to reverse it

74
00:06:35,430 --> 00:06:39,839
So, we just read it backwards, so this is just a reversal of this.

75
00:06:39,839 --> 00:06:40,449
ok.

76
00:06:41,449 --> 00:06:45,160
So, this is a definite way to find the next permutation.

77
00:06:45,160 --> 00:06:50,610
walk backwards from the end and see when the order stops increasing.

78
00:06:50,610 --> 00:06:54,149
So wherever it first decreases that is the suffix that you want to increment.

79
00:06:54,149 --> 00:07:01,500
Of course, if we go all the way and we go back to the first letter and we have not found the solution then we have already reached the last permutation in the overall scheme of things.

80
00:07:01,500 --> 00:07:06,720
Once we find such a position, we find which letter to swap it with by doing an equivalent of a search

81
00:07:06,720 --> 00:07:14,490
that we do for insertion sort. So we do an insert kind of thing, find the position in this case m to swap with k, after swapping it,

82
00:07:14,490 --> 00:07:17,526
We take the suffix after the new letter we put namely m.

83
00:07:17,551 --> 00:07:21,970
and we reverse it to get the smallest permutation starting with that letter m.

