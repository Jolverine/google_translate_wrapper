1
00:00:02,569 --> 00:00:06,882
So in the last lecture we saw that in object oriented programming.

2
00:00:06,907 --> 00:00:11,172
we define a data type through a template called a class.

3
00:00:11,212 --> 00:00:17,500
which defines the internal data implementation and the functions that we use to manipulate the data type.

4
00:00:17,500 --> 00:00:21,850
and then we create instances of this data type as objects.

5
00:00:22,969 --> 00:00:26,170
So we saw skeleton implementation of a heap.

6
00:00:26,269 --> 00:00:31,239
So this had a special function called init which

7
00:00:31,239 --> 00:00:35,259
was used to create the heap and then we had functions insert and delete.

8
00:00:35,259 --> 00:00:41,660
Now one thing which we did not explain is this argument self that runs through this.

9
00:00:41,685 --> 00:00:43,990
This is the convention.

10
00:00:43,990 --> 00:00:52,853
which we have in python that every function defined inside a class should have as its first parameter the name self.

11
00:00:52,878 --> 00:00:56,759
Now it need not be called self but it is less confusing always call it self.

12
00:00:56,759 --> 00:01:01,299
Let us just assume that this parameter is always there and it is called self.

13
00:01:01,299 --> 00:01:02,392
Now what is self?

14
00:01:02,417 --> 00:01:08,730
Self is a name that  is used inside the class to refer to the object that we are currently looking at.

15
00:01:08,730 --> 00:01:12,459
For instance if we are dealing with this heap h.

16
00:01:12,459 --> 00:01:14,799
When we say h dot insert>.

17
00:01:14,824 --> 00:01:22,196
Then this insert> is using the value 17. So 17 is the value x which is being passed to insert> .

18
00:01:22,221 --> 00:01:25,342
and h is the value

19
00:01:25,367 --> 00:01:30,695
on which the 17 should be added and that is a name for self.

20
00:01:30,720 --> 00:01:34,140
So self in other words tells the function v

21
00:01:34,140 --> 00:01:37,000
which object it is operating on itself.

22
00:01:37,000 --> 00:01:41,819
So it is a name to itself because you are telling a function an object a heap h

23
00:01:41,819 --> 00:01:44,105
insert> 17 into yourself.

24
00:01:44,130 --> 00:01:48,340
In that sense my values are denoted by self

25
00:01:48,340 --> 00:01:51,569
And inside a heap we can may be refer to other heaps.

26
00:01:51,569 --> 00:01:56,620
we will see a little later that we can take one value and refer to another value.

27
00:01:56,620 --> 00:02:05,019
there will be my values and there will be other values. So my values are always implicitly called self because that is the one that is typically manipulated by a function.

28
00:02:05,359 --> 00:02:09,960
To make this little clearer let us look at a slightly simpler example than heaps.

29
00:02:09,960 --> 00:02:13,389
To get all the notation and the terminology correct for us.

30
00:02:14,719 --> 00:02:21,444
So our first example is just a representation of a point xy.

31
00:02:21,469 --> 00:02:26,770
We are just thinking about a normal co-ordinate system.

32
00:02:26,840 --> 00:02:30,419
where we have the x axis the y axis.

33
00:02:30,419 --> 00:02:34,960
therefore a given point is given some coordinate like a b.

34
00:02:34,960 --> 00:02:37,680
So this is the point whose x co-ordinate is a and y count is a.

35
00:02:37,680 --> 00:02:41,740
This is the familiar concept that all of you must have seen in mathematics at some point.

36
00:02:41,900 --> 00:02:48,340
So we want to associate with such an object two quantities the x coordinate and the y coordinate.

37
00:02:48,439 --> 00:02:51,990
and this is set up by the init function.

38
00:02:51,990 --> 00:02:55,236
by passing the values a and b that you want the point to have.

39
00:02:55,261 --> 00:03:04,330
and now we have within the point we have these two attributes x and y. So every point looks like this.

40
00:03:04,355 --> 00:03:10,569
It has x value and y value and this x value is something.

41
00:03:10,819 --> 00:03:17,740
and the y value is something and if we change this x and y value then the point shifts around from one place to another.

42
00:03:18,050 --> 00:03:23,250
Now in order to designate that the x and y belong to this point and no other.

43
00:03:23,250 --> 00:03:25,379
we prefix it by self.

44
00:03:25,379 --> 00:03:29,169
so self dot x refers to the x value within this point myself.

45
00:03:29,169 --> 00:03:33,270
So self dot y is y value within myself.

46
00:03:33,270 --> 00:03:38,009
If we have a generic point p then we will have p dot x p dot y

47
00:03:38,009 --> 00:03:43,990
these will refer to the values x and y the names x and y associated with the point p.

48
00:03:44,330 --> 00:03:54,009
So inside the class definition self refers to the value of the attribute or the name within this particular object.

49
00:03:54,009 --> 00:03:58,873
Now this particular object changes as we move from one object to another but for every object self is itself.

50
00:03:58,898 --> 00:04:03,759
So for  p 1 if I tell something of p 1 p where in the context of p 1 self is p 1.

51
00:04:03,759 --> 00:04:07,120
if I have a different point p 2 in the context of p 2 self is p 2.

52
00:04:08,300 --> 00:04:11,979
So this is an important thing.

53
00:04:11,979 --> 00:04:18,920
Just remember that every function inside a class definition should always have a first argument a self and then the actual arguments.

54
00:04:18,945 --> 00:04:21,180
init in this case takes two arguments

55
00:04:21,180 --> 00:04:22,959
but we always insert a third argument

56
00:04:24,680 --> 00:04:33,579
So let us look at how this works for instance if we say If p is equal to point 3 2 then 3 will be passed as a.

57
00:04:33,579 --> 00:04:37,810
and 2 will be passed as b and this will set up a point that we have drawn here.

58
00:04:37,939 --> 00:04:43,540
which internally is represented as self dot x is 3 and self dot y is 2.

59
00:04:43,730 --> 00:04:48,629
Now here is a slightly different function it takes a point and shifts it.

60
00:04:48,629 --> 00:04:54,699
So we want to shift this to 3 + deltax and 4 + delta y.

61
00:04:55,670 --> 00:04:59,939
So this function we call translate so it takes the amount of shift as the argument

62
00:04:59,939 --> 00:05:01,449
delta x and delta y.

63
00:05:01,550 --> 00:05:08,889
And as usual we are always providing self as the default first argument.

64
00:05:08,990 --> 00:05:13,300
So we keep this in mind every python function every python class.

65
00:05:13,300 --> 00:05:16,620
If you want to use a function in the object oriented style.

66
00:05:16,620 --> 00:05:20,670
The first part init must necessarily be self and then the real arguments.

67
00:05:20,670 --> 00:05:26,399
So what do we want to do when we want to translate a point we want to take self dot x

68
00:05:26,399 --> 00:05:31,259
and move it to self dot x + the value delta x.

69
00:05:31,259 --> 00:05:33,672
So you want self dot x + delta x.

70
00:05:33,697 --> 00:05:39,180
Now this is a very common paradigm in python and other programming languages where you want to take

71
00:05:39,180 --> 00:05:42,839
the name say z and then you want to shift it by some amount.

72
00:05:42,839 --> 00:05:47,009
say  z + 6 or z is equal to z - 6.

73
00:05:47,009 --> 00:05:54,910
So whenever we have this kind of a thing where the
same name is being updated. There is a short form.

74
00:05:54,910 --> 00:05:57,939
where we can combine the operation with the assignment.

75
00:05:57,949 --> 00:06:02,430
So self dot x + equal to delta x is just a shortcut in python.

76
00:06:02,430 --> 00:06:06,170
for self dot x equal to self dot x + delta x .

77
00:06:06,195 --> 00:06:13,689
It means implicitly the name on the left is the first argument to the operation mentioned along with the assignment operation.

78
00:06:13,689 --> 00:06:17,430
So this is a very convenient shortcut that allows us to save some typing.

79
00:06:17,430 --> 00:06:22,790
Instead of writing self dot x equal to self dot x with delta x we just write self dot + equal to delta x.

80
00:06:22,815 --> 00:06:27,759
So this shifts the x coordinate of the current point by the argument provided to translate.

81
00:06:27,759 --> 00:06:32,250
Similarly self dot y + equal to delta y  will shift the argument.

82
00:06:32,250 --> 00:06:36,509
by the amount provided by delta y.

83
00:06:36,509 --> 00:06:39,810
if we say p dot translate to 1.

84
00:06:39,810 --> 00:06:49,120
then we get a new point which is 3 + 2 5 for the x coordinate and 2 + 1 3. So this 3 + 2 gives us 5.

85
00:06:49,120 --> 00:06:53,829
and 2 + 1 gives us 3. So this shifts the point from 3 to 5 3.

86
00:06:54,139 --> 00:06:59,379
So this is how we define these internals.

87
00:06:59,379 --> 00:07:02,339
So implementation is defined inside the init function.

88
00:07:02,339 --> 00:07:04,829
This is the function that is called when the point is set up.

89
00:07:04,829 --> 00:07:08,610
and this associates these internal names x and y with the object.

90
00:07:08,610 --> 00:07:12,250
This is where the implementation really gets set up.

91
00:07:12,250 --> 00:07:18,149
And then the functions that we define externally translate manipulate this internal representation.

92
00:07:18,149 --> 00:07:22,839
in an appropriate way so that it changes consistently with what you expect the functions to do.

93
00:07:24,529 --> 00:07:32,730
So let us look at a different function. Supposing we want to compute the distance for a point from the origin. So we want to know what is this distance.

94
00:07:32,730 --> 00:07:42,990
So this distance by Pythagoras's theorem is nothing but the square root of x square + y square.

95
00:07:42,990 --> 00:07:49,025
This is like hypotenuse of a right angle triangle. So you take x square + y square root and you get d.

96
00:07:49,050 --> 00:07:52,319
So if you want the distance of a point

97
00:07:52,319 --> 00:07:56,800
we do not give it any arguments but we always have this default argument self.

98
00:07:56,800 --> 00:08:01,439
So we want to know what is the distance from 0 0 to the current point.

99
00:08:01,439 --> 00:08:13,899
So we would say something like in our earlier case p is equal to point say 3 4 and then we would say p dot 0 distance to get its distance.

100
00:08:14,089 --> 00:08:17,050
So maybe we will assign this to a name.

101
00:08:17,509 --> 00:08:19,180
Let us not call it.

102
00:08:21,920 --> 00:08:25,449
Say we might assign this to a name like n.

103
00:08:25,910 --> 00:08:32,970
So when we do this it will Look at the current value of self dot x the current value of self dot y.

104
00:08:32,970 --> 00:08:35,320
square them add them and take the square root 

105
00:08:35,320 --> 00:08:42,809
Now one thing to remember is that actually square root is not a function available by default and python. So you actually have to import the math library

106
00:08:42,809 --> 00:08:47,889
So at the top of your class definition you should have remembered to write from math import star.

107
00:08:47,889 --> 00:08:50,700
So assuming that we have done that then square root is defined.

108
00:08:50,700 --> 00:08:54,629
This is a typical function which returns some information about the point.

109
00:08:54,629 --> 00:08:57,820
The earlier function just translate it the point did not tell us anything.

110
00:08:57,820 --> 00:09:00,490
This is the function that returns some information about the point.

111
00:09:01,789 --> 00:09:11,230
Now if o distance is something that we need to do often then maybe it is useful to just keep the point in a different representation.

112
00:09:11,230 --> 00:09:20,409
So you may remember or you may have seen somewhere in a is a high school mathematics that if you take a point x y.

113
00:09:21,049 --> 00:09:26,615
Then an alternative way of representing where this point is to actually represent the this polar coordinate.

114
00:09:26,640 --> 00:09:30,759
So you can keep this distance and you can keep this angle.

115
00:09:30,769 --> 00:09:33,460
So if I have r and theta

116
00:09:33,460 --> 00:09:36,429
It is the same information as keeping x and y.

117
00:09:36,440 --> 00:09:43,929
So the connection between the two is that x is r cos theta¸ where cos is the trigonometric cosine function y is equal to r sin theta.

118
00:09:44,360 --> 00:09:52,360
and on the other hand if I have x and y then I can recover r as we just did for odd distance it is the square root of x square + y square.

119
00:09:52,360 --> 00:09:56,620
and for theta So y by x is actually if you divide y by x

120
00:09:56,960 --> 00:09:58,929
we get tan of theta.

121
00:09:59,870 --> 00:10:02,909
because it is sin divided by cos in the r cancels.

122
00:10:02,909 --> 00:10:08,169
So y by x is tan theta. So theta is the tan inverse of y by x.

123
00:10:08,750 --> 00:10:12,960
Now speaking of changing implementation we could change our implementation.

124
00:10:12,960 --> 00:10:17,159
so that we do not keep the internal representation in terms of x and y

125
00:10:17,159 --> 00:10:20,799
we actually keep in terms of r and theta but the functions remain the same.

126
00:10:21,019 --> 00:10:26,129
So for instance we could take the earlier definition and change it.

127
00:10:26,129 --> 00:10:29,710
So we again pass it x and y. So from the users perspective

128
00:10:30,019 --> 00:10:34,710
The user believes that the point is defined in terms of the x and y coordinate.

129
00:10:34,710 --> 00:10:38,879
But instead of using a and b as the argument directly to set up the point.

130
00:10:38,879 --> 00:10:46,269
we first set up the r the radius by taking square root of a square + b square.

131
00:10:46,269 --> 00:10:50,169
So we want to divide b by a.

132
00:10:50,330 --> 00:10:54,870
but if a is 0 then we have a special case b by a will give us an error.

133
00:10:54,870 --> 00:10:57,960
So if a is 0 we set the angle to be 0.

134
00:10:57,960 --> 00:11:02,190
This is the python function in the math library for tan inverse arc tan.

135
00:11:02,190 --> 00:11:07,149
we set theta to be the arc tan of b - a I mean divided by it.

136
00:11:07,279 --> 00:11:12,000
So we internally manipulate the xy version to r theta.

137
00:11:12,000 --> 00:11:18,940
using the same formula that we had shown before which is that r is square root of x square y square and theta is tan inverse of y by x.

138
00:11:19,519 --> 00:11:24,129
The only thing we have to take care is when x is equal to 0 we have to manually set theta to 0.

139
00:11:24,200 --> 00:11:27,779
Now internally we are now keeping self dot r and self dot theta.

140
00:11:27,779 --> 00:11:30,429
we are not keeping self dot x and self dot y.

141
00:11:30,620 --> 00:11:35,100
So this is useful because if you want the o-distance the origin distance then

142
00:11:35,100 --> 00:11:38,100
We just have to return the r value we do not do any computation.

143
00:11:38,100 --> 00:11:41,830
In other words if we are going to use 0 distance very often.

144
00:11:41,990 --> 00:11:47,889
then it is better to use the calculation square root a square + b square once at the beginning to setup the point.

145
00:11:47,889 --> 00:11:52,240
and just return the r value without any calculation whenever you want the distance from the origin

146
00:11:52,240 --> 00:11:54,990
So this might be a requirement depending on how you are using it

147
00:11:54,990 --> 00:12:01,720
and one implementation may be better than the other but from the users perspective the same function is there there is self.

148
00:12:01,789 --> 00:12:06,269
there is o-distance. So if I take a point and I ask 0 distance I get the distance of the origin.

149
00:12:06,269 --> 00:12:10,509
whether or not the point is represented using xy or r theta.

150
00:12:10,639 --> 00:12:15,064
Now of course using 0 distance as r theta  is good for 0-distance.

151
00:12:15,089 --> 00:12:16,590
it is not very good for translate.

152
00:12:16,590 --> 00:12:19,710
if I want to translate a point by delta x delta y.

153
00:12:19,710 --> 00:12:22,649
I have to convert the point back from r theta to x,y.

154
00:12:22,649 --> 00:12:26,799
using x equal to r cos theta¸ and y equal to r sin theta¸.

155
00:12:26,799 --> 00:12:31,299
then do x + delta x y + delta y and convert it back to the r theta.

156
00:12:31,299 --> 00:12:34,980
So you pay a price in one function or the other.

157
00:12:34,980 --> 00:12:37,679
with the x y representation translate is better.

158
00:12:37,679 --> 00:12:40,710
with the r theta¸ representation 0 distance is better.

159
00:12:40,710 --> 00:12:44,970
and this is a very typical case of the kind of compromise that you have to deal with.

160
00:12:44,970 --> 00:12:52,500
You have to decide which of these operations is likely to be more common and more useful for you to implement directly.

161
00:12:52,500 --> 00:12:56,490
If you think translate happens more often is probably better to use x and y

162
00:12:56,490 --> 00:13:00,870
if you think the origin distance is more important it is probably better to r and theta

163
00:13:00,870 --> 00:13:09,580
Often there is no one good answer it is not like saying that a heap implementation is always better than a sorted list implementation for a priority queue

164
00:13:09,580 --> 00:13:14,309
There may be trade offs which depend on the type of use you are going to put a data structure

165
00:13:14,309 --> 00:13:18,580
as to which internal implementation works best.

166
00:13:18,580 --> 00:13:25,029
what you have to always keep in mind is that the implementation should not be change the way the functions behave.

167
00:13:25,029 --> 00:13:28,389
To the external user the function must behave exactly the same way.

168
00:13:31,460 --> 00:13:35,710
So in this particular example just to illustrate what we have said again.

169
00:13:35,710 --> 00:13:39,990
We have changed the private implementation namely we have moved from xy to r theta.

170
00:13:39,990 --> 00:13:45,759
But the functionality of the public interface the functions 0 distance translate etc. remain exactly the same.

171
00:13:49,519 --> 00:13:57,820
Now we have seen earlier that in python functions we can provide the default arguments which make sometimes the arguments option.

172
00:13:57,820 --> 00:14:07,090
For instance if we want to say that if we do not specify the x and y coordinates over a point then by default the point will be created at the origin.

173
00:14:07,090 --> 00:14:10,950
So then we can use a equal to 0 and b equal to 0 as default argument

174
00:14:10,950 --> 00:14:14,309
So that if the user does not provide values for a and b

175
00:14:14,309 --> 00:14:16,840
then x will be set to 0 y will be set to 0.

176
00:14:18,289 --> 00:14:22,179
So for instance if you want a point at a specific place 3 4.

177
00:14:22,309 --> 00:14:28,269
We would invoke this function this class will create an object by passing the arguments 3 4.

178
00:14:28,269 --> 00:14:32,830
if you do not pass any arguments like p 2 then we get a point at the origin.

179
00:14:35,720 --> 00:14:46,419
So the function init clearly looks like a special function because of these underscore underscore on either side which we normally do not see or think of using while writing a python function.

180
00:14:46,419 --> 00:14:50,759
As we said before python interprets init as a constructor.

181
00:14:50,759 --> 00:14:54,429
call a object like p equal to 0.54.

182
00:14:54,429 --> 00:14:58,620
then this implicitly caused init and init is used to set up self dot take self dot y

183
00:14:58,620 --> 00:15:03,120
So the internal representation of the point is set up in the correct way by init.

184
00:15:03,120 --> 00:15:07,289
So init is a special function. Now python has other special functions.

185
00:15:07,289 --> 00:15:11,129
For instance one of the common things that we might want to do is to.

186
00:15:11,129 --> 00:15:14,740
print out the value of an object what an object contains.

187
00:15:14,750 --> 00:15:18,879
And for this the most convenient way is to convert the object to a string.

188
00:15:18,889 --> 00:15:23,519
So the function str  normally converts an object to a string but how do we convert

189
00:15:23,519 --> 00:15:26,039
describe how str  should behave on an object.

190
00:15:26,039 --> 00:15:29,860
there is a special function that we can write called underscore underscore str.

191
00:15:29,860 --> 00:15:33,629
So underscore underscore str is implicitly invoked when we write str  of o.

192
00:15:33,629 --> 00:15:39,309
So str  of an object o is nothing but o dot underscore underscore str.

193
00:15:39,559 --> 00:15:41,610
And for instance print.

194
00:15:41,610 --> 00:15:45,509
the function print implicitly takes any name you pass to print

195
00:15:45,509 --> 00:15:52,659
and converts it to a string representation. When I say print x and x is an integer then implicitly str  of x is what is displayed.

196
00:15:52,659 --> 00:15:58,899
So str is invoked and str in turn internally invokes the special function underscore underscore str.

197
00:15:59,720 --> 00:16:03,389
Let us see how this would work for instance for our point thing.

198
00:16:03,389 --> 00:16:07,667
If we want to represent the point so internally we have self dot x and self dot y.

199
00:16:10,399 --> 00:16:15,220
and we want to print this out in this form the value x and the value y.

200
00:16:15,220 --> 00:16:20,019
So what we do is we set up str  so remember that self is always a parameter

201
00:16:20,019 --> 00:16:22,710
So what it does is it first?

202
00:16:22,710 --> 00:16:28,289
creates a string with the open bracket and the close bracket at other end and a comma in between the two.

203
00:16:28,289 --> 00:16:32,549
and in between the open bracket and the comma it puts the string representation of self dot x

204
00:16:32,549 --> 00:16:37,419
and in between the comma and the closed bracket it produces the string representation of self

205
00:16:37,419 --> 00:16:41,100
So this creates a string from the value.

206
00:16:41,100 --> 00:16:52,570
It internally invokes str  on the values themselves self dot x and self dot y and then constructs these extra things the open close bracket and the comma to make it look like point as we would expect.

207
00:16:56,029 --> 00:17:00,909
Another interesting function that python has as a special function is add.

208
00:17:00,909 --> 00:17:04,839
So when we write + the function add is invoked.

209
00:17:04,940 --> 00:17:09,220
So in other words p 1 + p 2 if these are two points would.

210
00:17:09,220 --> 00:17:12,400
invoke p 1 underscore underscore add p 2.

211
00:17:12,400 --> 00:17:16,779
So what we would expect is that if I had p 1 and if I had p 2.

212
00:17:16,779 --> 00:17:24,269
then I would get something which gives me a point where I combine these two. So I get the x coordinate as x 1 to p 1 + p 2.

213
00:17:24,269 --> 00:17:29,250
and y coordinate p 1 + p 2. It is up to us I mean it does not mean it has to be this way

214
00:17:29,250 --> 00:17:32,369
But if I say p 1 + p 2 the function that is invoked is add.

215
00:17:32,369 --> 00:17:36,839
It is upto us to define what add means. Let us assume that we want to construct a new point

216
00:17:36,839 --> 00:17:40,630
whose x coordinate and y coordinate the sum of the two points given to us.

217
00:17:40,819 --> 00:17:44,430
So here is a way we would do it we would create a new point.

218
00:17:44,430 --> 00:17:53,099
whose x coordinate is self dot x + p dot x. Now notice that self is the function associated with p 1 in this case.

219
00:17:53,099 --> 00:17:55,509
add. So self refers to p 1.

220
00:17:55,509 --> 00:18:00,009
I say p 1 + p 2 and the other argument p 2 is the point p.

221
00:18:00,009 --> 00:18:02,440
So I can look at the values of p.

222
00:18:02,440 --> 00:18:06,750
and say p dot x p dot y I can look at my value will say self dot x self dot y.

223
00:18:06,750 --> 00:18:09,630
now I can combine these by creating a new point.

224
00:18:09,630 --> 00:18:14,319
self dot x + p dot x and self dot x + p dot y.

225
00:18:15,799 --> 00:18:20,740
So for instance if we have two points at 1 2 and 2 5 then.

226
00:18:20,740 --> 00:18:26,170
So this will return a point at t. 3 7 and I must store it in new points. So p 3 now becomes a new point.

227
00:18:26,170 --> 00:18:28,809
whose x coordinate is 3 and y coordinate is 7.

228
00:18:30,529 --> 00:18:37,329
So in the same way we could have a special way of defining multiplication.

229
00:18:37,329 --> 00:18:40,960
and in python the underscore underscore mult function.

230
00:18:40,960 --> 00:18:43,599
is the one that is implicitly called and multiply.

231
00:18:45,140 --> 00:18:48,299
Similarly we can define comparisons.

232
00:18:48,299 --> 00:18:51,539
what is to be done when we compare whether p 1 is less than p 2.

233
00:18:51,539 --> 00:18:55,049
we check both coordinates are less we check the distance in the original is less

234
00:18:55,049 --> 00:18:57,059
We have complete freedom how to define this.

235
00:18:57,059 --> 00:19:01,650
we just write p 1 less than p 2 for readability in our program.

236
00:19:01,650 --> 00:19:04,509
and internally it will call the function less than lt.

237
00:19:04,509 --> 00:19:07,019
Similarly greater than we will call the function gt.

238
00:19:07,019 --> 00:19:10,390
and there is something for less than equal to greater than equal to and so on.

239
00:19:10,390 --> 00:19:14,890
So these are all very convenient functions that python allows us to call implicitly

240
00:19:14,900 --> 00:19:18,900
And allows us to use conventional operators and conventional functions.

241
00:19:18,900 --> 00:19:24,700
So we do not really have to think of these objects in a different way when we are coding our programs.

242
00:19:25,549 --> 00:19:29,880
So there are several other such functions it is impossible to list all of them

243
00:19:29,880 --> 00:19:34,200
it is not useful to list all of them at this introductory stage when you are learning python

244
00:19:34,200 --> 00:19:36,539
but you can always look up the python documentation.

245
00:19:36,539 --> 00:19:40,329
and see all the special functions that are defined for objects and classes.

