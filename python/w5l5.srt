1
00:00:02,299 --> 00:00:12,119
So when we looked at input and print earlier in this week’s lectures, we said that we could control in a limited way how print displayed the output.

2
00:00:12,144 --> 00:00:18,770
Now by default, print takes a list of things to print, separates them by spaces, and puts a new line at the end.

3
00:00:18,795 --> 00:00:23,820
However, it takes an optional argument end equal to string, which changes what we put at the end.

4
00:00:23,820 --> 00:00:31,600
In particular, if we put an empty string, it means that it doesn't start a new line, and the next print will continue on the same line.

5
00:00:31,600 --> 00:00:35,429
Similarly, we can change the separator from a space to whatever we want.

6
00:00:35,429 --> 00:00:41,829
And in particular, if we do not want any spaces, we can put them ourselves, and just say the separator is nothing but the empty string.

7
00:00:42,770 --> 00:00:45,780
Now, sometimes you want to be a little bit more precise.

8
00:00:45,780 --> 00:00:51,640
So for this, we can use the format method, which is actually part of the string library.

9
00:00:51,640 --> 00:01:00,280
In the previous lecture we looked at other things we can do with strings like find, replace and all these things.

10
00:01:00,280 --> 00:01:07,480
So remember, when you are doing print, you are actually printing a string, and so anything you can do to modify a string will give you another string, and that's what you are going to print.

11
00:01:07,505 --> 00:01:18,075
So the string here is actually going to call a format method, and the easiest way to do this is by example.

12
00:01:18,100 --> 00:01:25,150
So we have a base string here, which is First, Second, and we have these two funny things in braces.

13
00:01:25,670 --> 00:01:35,620
So the funny things in braces are to be thought of as the equivalent of arguments in a function and these are things to be replaced by actual values.

14
00:01:35,659 --> 00:01:47,200
And then what happens is that, when you give this string, and you apply the format method, then the 0 refers to the first argument, and 1 refers to second argument.

15
00:01:47,629 --> 00:01:50,423
So what we are doing is we are replacing by position.

16
00:01:50,448 --> 00:02:06,790
So if I actually take this string and I pass it to python, the resulting thing is First colon 47 Second colon 11 because the brace 0, is replaced by the first argument to format 47, and the second replaces second.

17
00:02:07,340 --> 00:02:12,270
Now, the positions determine the names so they don't have to be used in the same order.

18
00:02:12,270 --> 00:02:17,770
So we could first print argument 1 and then print argument 0 as the second example shows.

19
00:02:18,349 --> 00:02:28,810
So essentially, this version of format allows us to pass things into a string by their position in the format thing.

20
00:02:28,810 --> 00:02:31,360
So we are replacing arguments by position.

21
00:02:34,250 --> 00:02:36,456
Now, we can do the same thing by name.

22
00:02:36,481 --> 00:02:43,876
So this is exactly like defining a function, where we could give function arguments and pass them by name.

23
00:02:43,901 --> 00:02:57,280
So in the same way here, we can specify names of the arguments to format and we can say f is equal to 47, s is equal to eleven, and now we can say One f, and two s.

24
00:02:57,620 --> 00:03:00,750
Now, here the advantages is not by position, but my name.

25
00:03:00,750 --> 00:03:06,456
So if I take these two things, and I exchange them, so if I make f the second argument, and s the first argument.

26
00:03:06,481 --> 00:03:11,969
When I pass it to the same string, then f will be correctly caught as 47, and s as 11.

27
00:03:11,969 --> 00:03:14,729
And so, here we are using the name and not the position.

28
00:03:14,729 --> 00:03:18,370
So the order in which we supply the things to format doesn't matter.

29
00:03:20,840 --> 00:03:23,313
So up to this point, we haven't done any formatting.

30
00:03:23,338 --> 00:03:30,819
All we have done is we have taken a string, and we have told how to replace values for placeholders in this string.

31
00:03:30,819 --> 00:03:37,244
There is no real formatting which has happened, because whatever we did with that, we could have already done using the existing print statement.

32
00:03:37,269 --> 00:03:44,789
Now, the real formatting comes by giving additional instructions on how to display each of these place holders.

33
00:03:44,789 --> 00:03:48,490
So here we have 0 followed by some funny thing.

34
00:03:48,490 --> 00:03:53,919
So we have this special colon, and what comes after the colon is the formatting instruction.

35
00:03:53,919 --> 00:03:55,780
So this has two parts.

36
00:03:55,780 --> 00:03:58,836
Here we see a 3 and a d, and they mean different things.

37
00:03:58,861 --> 00:04:05,319
So the 3d as a whole tells us how to display the value that is going to be passed here, that's the first thing.

38
00:04:05,870 --> 00:04:11,405
d is a code that specifies decimal.

39
00:04:11,430 --> 00:04:20,829
So d specifies that 4 should be treated as an integer value, so we should actually display it as a normal integer value, namely it's an a base 10 integer.

40
00:04:21,139 --> 00:04:27,269
And finally, 3 says that we must format 4 so that it takes 3 spaces.

41
00:04:27,269 --> 00:04:30,819
So it occupies the equivalent of three spaces though it's a single digit.

42
00:04:30,819 --> 00:04:37,589
So if I do all this, I get a value now, and notice that there is already one space here.

43
00:04:37,589 --> 00:04:42,149
And then its going to take 3 spaces so I am going to get 2 blank spaces and then a 4.

44
00:04:42,149 --> 00:04:46,509
So that's why we have this long space here so this is actually 3 blank spaces and then a 4.

45
00:04:46,639 --> 00:04:50,980
Ok this whole thing, this part of it, comes from the format.

46
00:04:51,005 --> 00:05:03,279
So I have a blank space and a 4, because I was told to put 4 in a width of 3 and think of it as a number, and since it's a number it goes to the right end.

47
00:05:05,629 --> 00:05:07,430
Let's look at another example.

48
00:05:07,455 --> 00:05:13,089
Supposing I had number which is not an integer, but a floating point number.

49
00:05:13,089 --> 00:05:15,579
Ok, it is 47.523.

50
00:05:15,579 --> 00:05:20,379
Now, here first thing is that instead of d, we have f for floating point.

51
00:05:20,379 --> 00:05:28,290
So 6.2 f this whole thing to the right of the colon tells me how to format the value that comes from here.

52
00:05:28,290 --> 00:05:30,592
Now, 6.2 f breaks up as follows.

53
00:05:30,617 --> 00:05:34,839
The f, the letter part of it always tells me what is the type of value.

54
00:05:34,839 --> 00:05:42,670
So it says that 47.523 should be treated as a floating point value.

55
00:05:42,670 --> 00:05:49,509
Now, the second thing this 6 tells me how much space I have to write.

56
00:05:49,509 --> 00:05:54,819
So it says that total value including the decimal point and everything is going to be 6 characters wide.

57
00:05:55,040 --> 00:06:00,250
Finally, the 2 says how many digits you show after the decimal point.

58
00:06:00,275 --> 00:06:06,910
So if I apply all this, then first of all, because I only have two digits after decimal point, this 3 gets knocked off.

59
00:06:06,910 --> 00:06:13,860
And then, because it's said to use 6 characters, now if I count from the right, this is 1, 2, 3, 4, 5 characters.

60
00:06:13,860 --> 00:06:16,360
It said to use 6 characters that is why there is an extra blank here.

61
00:06:16,385 --> 00:06:23,019
If you notice here, there is only one blank, but here there are two blanks, the second blank comes from the format statement.

62
00:06:27,259 --> 00:06:32,430
So unfortunately, this is not exactly user friendly or something that you can easily remember.

63
00:06:32,430 --> 00:06:40,319
But there are codes for other values, so we saw f and d, so there are codes like, s for string, and o for octal, and x for hexadecimal.

64
00:06:40,319 --> 00:06:44,790
So, all these values can be displayed too using this formatted thing.

65
00:06:44,790 --> 00:06:47,129
And you can also do other things.

66
00:06:47,129 --> 00:06:50,950
You can tell it not to put it on the right but to put on the left to left justify the value.

67
00:06:50,975 --> 00:06:57,519
So in a field of width 5, for example, if you want to put a string, you might say this string should come from the left, and not from the right.

68
00:06:58,339 --> 00:07:06,939
Then you can add leading zeros so you might want to display a number 4 in width 3 not as 4, but as 004, and you can do all these things.

69
00:07:06,939 --> 00:07:12,040
As I said, this is a whole zoo of formatting things that you can do with this.

70
00:07:12,050 --> 00:07:18,584
This all have their origin from the language C, and the statement called print f in C.

71
00:07:18,609 --> 00:07:34,926
So the exact meaning of the format statements 3d and 6.2f and all what they mean could be found in python documentation, and you may not need all the variations but is useful to know that this kind of formatting can be done.

