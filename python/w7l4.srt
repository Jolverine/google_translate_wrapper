1
00:00:03,649 --> 00:00:11,619
So as our final example of a user defined data structure we will look at binary search trees.

2
00:00:12,349 --> 00:00:18,100
So we are looking at a situation where we need to maintain dynamic data in a sorted manner.

3
00:00:18,199 --> 00:00:20,820
Remember that one of the byproducts of sorting

4
00:00:20,820 --> 00:00:24,399
is that we can use binary search to efficiently search for a value.

5
00:00:24,739 --> 00:00:27,750
But binary search can be used

6
00:00:27,750 --> 00:00:31,629
if we can sort data once and for all and keep it in sorted order.

7
00:00:31,789 --> 00:00:34,619
If the data is changing dynamically

8
00:00:34,619 --> 00:00:38,820
Then in order to exploit binary search we will have to keep resorting the data

9
00:00:38,820 --> 00:00:40,270
which is not efficient.

10
00:00:41,119 --> 00:00:44,640
So supposing we have a sequence of items

11
00:00:44,640 --> 00:00:47,740
and items are periodically being inserted and deleted.

12
00:00:47,750 --> 00:00:52,289
Now as we saw with heaps for instance if we try to maintain a sorted list.

13
00:00:52,289 --> 00:00:54,189
and then keep track of inserts

14
00:00:54,189 --> 00:00:59,079
then each insert or delete in this case would take order (n) time and that would also be expensive.

15
00:00:59,179 --> 00:01:03,659
However it turns out that we can move to a tree like structure or a tree structure

16
00:01:03,659 --> 00:01:05,950
like in a priority queue we move to a heap

17
00:01:05,950 --> 00:01:10,390
and then do insert and delete also efficiently alongside searching.

18
00:01:12,170 --> 00:01:16,239
So the data structure we have in mind is called a binary search tree.

19
00:01:16,340 --> 00:01:20,370
So in a binary search tree we keep exactly one copy of every value.

20
00:01:20,370 --> 00:01:23,079
So it is like a set we do not keep duplicates.

21
00:01:23,079 --> 00:01:27,040
and the values are organized as follows for every node

22
00:01:27,040 --> 00:01:31,150
all values which are smaller than the current nodes value are to the left.

23
00:01:31,150 --> 00:01:34,689
and all values that are bigger than the current node value are to the right.

24
00:01:34,730 --> 00:01:38,620
Here is an example of a binary search tree.

25
00:01:38,620 --> 00:01:42,540
So you can check for instance that to the left of the root 5

26
00:01:42,540 --> 00:01:45,879
We have all values 1 2 and 4 which are smaller than 5.

27
00:01:45,890 --> 00:01:49,450
to the right of 5 we have the values 8 and 9 which are bigger.

28
00:01:49,909 --> 00:01:57,480
Now this is a recursive property. If you go down for instance if you look at the node labelled 2 then below it it has values 1 and 4.

29
00:01:57,480 --> 00:02:03,640
Since 1 is smaller than 2 1 is to the left of 2 since 4 is bigger than 2 4 is to the right of 2.

30
00:02:04,250 --> 00:02:06,730
Similarly if you look at the node 8.

31
00:02:06,730 --> 00:02:11,289
It has only one value below it namely 9 and therefore it has no left child.

32
00:02:11,289 --> 00:02:13,629
but 9 is in the right sub tree of 8.

33
00:02:15,469 --> 00:02:22,240
So we can maintain a binary search tree using nodes exactly like we did for our user defined lists.

34
00:02:22,240 --> 00:02:26,289
except in a list we had a value and a next pointer.

35
00:02:26,289 --> 00:02:33,669
In a binary search tree we have two values below each node potentially a left child and a right child.

36
00:02:33,669 --> 00:02:39,789
So each node now consists of three items the value being stored the left child and the right child.

37
00:02:39,789 --> 00:02:43,599
So if we look at the same example that we had before on the right

38
00:02:43,599 --> 00:02:47,409
then the root node 5 will have a pointer to the nodes with 2 and 8.

39
00:02:47,409 --> 00:02:50,710
The node 2 will have a pointer to the nodes 1 and 4.

40
00:02:50,710 --> 00:02:54,120
These are now what are called leaf nodes so they have no children.

41
00:02:54,120 --> 00:02:58,509
So their left and right pointers will be None indicating there is nothing in that direction

42
00:02:58,520 --> 00:03:04,439
Similarly 8 has got None as its left pointer we could have no left child and the right pointer points to 9

43
00:03:04,439 --> 00:03:09,669
and the node with 9 again has two null pointers because it is a leaf node.

44
00:03:11,449 --> 00:03:15,849
Now it will turn out that we will want to expand this representation

45
00:03:15,849 --> 00:03:19,509
in order to exploit it better for recursive presentations.

46
00:03:19,509 --> 00:03:27,060
So what we will do is that we will not just terminate the tree with the value and two pointers none.

47
00:03:27,060 --> 00:03:31,090
you will actually add a layer of empty nodes with all fields none.

48
00:03:31,580 --> 00:03:35,349
With this the empty tree will be a single empty node

49
00:03:35,990 --> 00:03:41,580
and a leaf node that is not None will have a value and both its children will be empty node.

50
00:03:41,580 --> 00:03:44,699
So it won't directly have None as its left and right pointers.

51
00:03:44,699 --> 00:03:47,050
It will actually have children which are empty.

52
00:03:47,300 --> 00:03:50,830
So this makes it easier to write recursive functions.

53
00:03:50,830 --> 00:03:55,330
and if you don't do this then it is a bit harder to directly implement recursive functions.

54
00:03:55,819 --> 00:03:59,069
so Just to understand how our prestructure changes.

55
00:03:59,069 --> 00:04:02,050
This is the structure that we had before the same example.

56
00:04:02,050 --> 00:04:06,669
So here notice that in the leaf nodes the leaf nodes have a value.

57
00:04:06,800 --> 00:04:11,319
and both the child pointers left and right are directly null.

58
00:04:11,319 --> 00:04:16,500
So we want to change this what we want to do is we want to insert below this an empty node

59
00:04:16,500 --> 00:04:20,699
at wherever we see a none we want to insert an extra empty node.

60
00:04:20,699 --> 00:04:25,170
So this of course blows up the tree a little bit but then this cost is not that much

61
00:04:25,170 --> 00:04:29,790
So If we do this then we get a new tree which looks like this as you can calculate.

62
00:04:29,790 --> 00:04:36,029
So below every leaf node wherever we normally had a None pointer indicating that the path has ended

63
00:04:36,029 --> 00:04:40,329
We will explicitly add one extra node which has all three fields now.

64
00:04:40,329 --> 00:04:44,290
and it will turn out that this is very useful to clean up our programming later on.

65
00:04:44,290 --> 00:04:47,649
So this is a representation that we will use for a binary search tree

66
00:04:47,649 --> 00:04:49,480
each node has three pointers

67
00:04:49,480 --> 00:04:55,600
and at the leafs we have an extra layer of empty nodes with all three values none none none.

68
00:04:58,069 --> 00:05:01,449
So here is the basic class tree.

69
00:05:01,490 --> 00:05:08,350
So as before we have an init function which takes an initial value which is by default null.

70
00:05:08,449 --> 00:05:12,689
So the unit function works as follows.

71
00:05:12,689 --> 00:05:15,639
So we first set up the value to be needed val which would be none.

72
00:05:15,639 --> 00:05:19,709
If there is a value then we create an empty tree with one node.

73
00:05:19,709 --> 00:05:22,949
In which case we have to create these empty nodes.

74
00:05:22,949 --> 00:05:27,009
Notice if we go back and we go do not give a value so may be it is better to look at this case

75
00:05:27,290 --> 00:05:29,279
If we do not give a value

76
00:05:29,279 --> 00:05:33,040
then we end up with a tree which just has none none none  so this is our entity.

77
00:05:34,189 --> 00:05:38,670
So the initial value is None we get this tree. If the initial value is not None

78
00:05:38,670 --> 00:05:45,540
then we get a tree in which we put the value v and then we make the left and the right pointers both point to this and none.

79
00:05:45,540 --> 00:05:54,240
So this is a tree that will contain exactly one value. So depending on whether any value is None or not None

80
00:05:54,240 --> 00:05:57,509
We end up either a tree with three nodes with two dummy nodes below.

81
00:05:57,509 --> 00:06:00,069
or a single empty node denoting the tree.

82
00:06:00,769 --> 00:06:05,550
So given this as before we have the function is empty.

83
00:06:05,550 --> 00:06:07,740
which basically checks if the value is null.

84
00:06:07,740 --> 00:06:11,009
So if I start looking at the tree and the very first node says none

85
00:06:11,009 --> 00:06:13,269
then that tree is empty otherwise it is not empty.

86
00:06:16,910 --> 00:06:22,839
So let us first look at a way to systematically explore the values in a tree.

87
00:06:23,000 --> 00:06:27,000
So these are called traversals and one traversal which is

88
00:06:27,000 --> 00:06:31,839
very useful for a binary search tree is an in order traversal.

89
00:06:31,839 --> 00:06:39,759
So what an in order traversal does is that it first explores the left side so it will first explore this.

90
00:06:39,759 --> 00:06:42,449
recursively again using in order traversal.

91
00:06:42,449 --> 00:06:46,829
then it will display this and then it will explore the right. So if you see the code

92
00:06:46,829 --> 00:06:47,850
You can see that.

93
00:06:47,850 --> 00:06:51,990
If the tree is not empty you first do an in order to first look at the left sub tree

94
00:06:51,990 --> 00:06:54,879
then you pick up the value in the current node.

95
00:06:54,879 --> 00:06:59,069
and then you do an in ordered traversal of the right sub tree this produces a list

96
00:06:59,069 --> 00:07:02,550
So if we execute this step by step.

97
00:07:02,550 --> 00:07:07,529
if we reach 5 it says first do an in ordered traversal of the left so we come down to 2 this is again not a trivial tree.

98
00:07:07,529 --> 00:07:11,139
So again we have to do in order to do this so we go to its left.

99
00:07:11,139 --> 00:07:13,110
And now when we have one.

100
00:07:13,110 --> 00:07:16,709
And in order traversal of one consists of its left child which is empty.

101
00:07:16,709 --> 00:07:19,680
1 and then its right child is empty so this produces 1.

102
00:07:19,680 --> 00:07:22,360
Now I come back and I list out the node 2.

103
00:07:22,360 --> 00:07:26,250
Now I do an in order to traverse of its right so I have got 1 2 4.

104
00:07:26,250 --> 00:07:29,620
So this completes the in order traversal of the left sub tree of 5.

105
00:07:29,620 --> 00:07:31,839
Now I list out 5 itself.

106
00:07:31,839 --> 00:07:36,509
and then I do a in order traversal of 8 and 9 since 8 has no left child

107
00:07:36,509 --> 00:07:39,790
the next value that comes out is 8 itself and then 9.

108
00:07:39,980 --> 00:07:42,220
So what you can see is that.

109
00:07:42,220 --> 00:07:45,089
Since we print out the values on the left child.

110
00:07:45,089 --> 00:07:48,540
before the current value and the value of the right child after the current value

111
00:07:48,540 --> 00:07:53,220
with respect to the current value all these values are sorted because that is how a search tree is organized

112
00:07:53,220 --> 00:07:56,009
and since this is recursively done at every level down.

113
00:07:56,009 --> 00:08:02,879
final output of an in-order traversal of a search tree is always a sorted list.

114
00:08:02,879 --> 00:08:07,269
So this is one way to ensure that the tree that you have constructed is sorted you do an in orders of traversal

115
00:08:07,370 --> 00:08:10,199
that the tree I have constructed is a search tree.

116
00:08:10,199 --> 00:08:15,610
you do an in-order traversal and ensure that the output that you get from this reversal is a sorted list.

117
00:08:17,899 --> 00:08:21,269
So as we mentioned at the beginning of this lecture.

118
00:08:21,269 --> 00:08:27,339
One of the main reasons to construct a search tree is to be able to do something like binary search.

119
00:08:28,009 --> 00:08:34,960
and this is with dynamic data. So we would also have insert and
delete as operations but the more main fundamental operation is find.

120
00:08:34,960 --> 00:08:39,929
so Like in binary search we start at the root imagine that this is the middle of an list or an array

121
00:08:39,929 --> 00:08:44,529
for example we look at this element if we have found it its fine

122
00:08:44,529 --> 00:08:48,639
If we have not found it then we need to look in the appropriate subtree.

123
00:08:48,639 --> 00:08:53,080
Since a search tree is organized with the left value smaller and the right value is bigger

124
00:08:53,080 --> 00:08:57,070
we go left if the value we are searching for smaller than the current node.

125
00:08:57,070 --> 00:09:00,659
and we go right if it is larger than the current node.

126
00:09:00,659 --> 00:09:03,669
So this is very much a generalization of binary search in the tree.

127
00:09:04,519 --> 00:09:12,090
So here is the code it is very straightforward we want to find value of v and remember this is python syntax.

128
00:09:12,090 --> 00:09:15,779
so we always have self as the first parameter to our function.

129
00:09:15,779 --> 00:09:20,129
So if the current node is empty we cannot find it so we return false

130
00:09:20,129 --> 00:09:24,789
if we do find v then we return true and if we do not find v then if it is

131
00:09:24,860 --> 00:09:27,450
So this is not this should be self.

132
00:09:27,450 --> 00:09:31,289
If it is smaller than this current value then we go left and search for it.

133
00:09:31,289 --> 00:09:33,370
otherwise we go right and search

134
00:09:33,799 --> 00:09:38,039
So this is exactly a binary search and it is a very simple recursive thing.

135
00:09:38,039 --> 00:09:41,320
which exactly follows a structure of a search tree.

136
00:09:44,779 --> 00:09:49,289
it will be useful later on to be able to find the smallest and largest values in a search tree.

137
00:09:49,289 --> 00:09:54,029
So notice that as we keep going left we go smaller and smaller.

138
00:09:54,029 --> 00:09:56,279
So where is the minimum value in a tree?

139
00:09:56,279 --> 00:10:03,309
it is along the smaller left most path. SO if i have to go on the left most path and if I cannot go any further then I find it.

140
00:10:03,679 --> 00:10:07,379
So We will always supply this function only when tree is non empty.

141
00:10:07,379 --> 00:10:12,370
So let us assume that we are looking for the minimum value in a non empty tree

142
00:10:12,370 --> 00:10:15,820
Well we find the left most path. So if I cannot go further left

143
00:10:15,820 --> 00:10:23,379
then I have found it. So in this case if I reach one since I cannot go for the left one is the minimum value.

144
00:10:23,379 --> 00:10:27,720
Otherwise if I can go left then I will go one more step. So If we start this for instance set 5

145
00:10:27,720 --> 00:10:34,149
it will say that 5 has a left subtree so the minimum value below 5 is the minimum value below its left child so we go to 3.

146
00:10:34,149 --> 00:10:38,889
Now we say that the minimum value below 3 is the minimum value below left side. So we goto 1

147
00:10:38,889 --> 00:10:43,529
Then we say that the minimum value is the minimum value at 1 because there is no left child

148
00:10:43,529 --> 00:10:48,549
and therefore we get the answer 1 and it is indeed true that anything below that is only on the right is 2.

149
00:10:51,620 --> 00:10:55,720
So dually one can find the maximum value by following the right most path.

150
00:10:55,753 --> 00:10:59,370
So if the right is None then we return the current value.

151
00:10:59,370 --> 00:11:02,700
otherwise we recursively look for the maximum value to our right.

152
00:11:02,700 --> 00:11:06,340
So in this case we start at 5 we go down to 7.

153
00:11:06,340 --> 00:11:10,500
then we go down to 9 and since there is no further right path from 9.

154
00:11:10,500 --> 00:11:12,759
9 must be the maximum value in this tree.

155
00:11:13,399 --> 00:11:17,860
We will come back later on and see why we need this minimum and maximum value.

156
00:11:17,860 --> 00:11:20,220
But anyway it is a useful thing to be able to find.

157
00:11:20,220 --> 00:11:24,129
and it is very easy to do so again using the structure of the binary search.

158
00:11:26,960 --> 00:11:31,440
So one of the things we said is that we need to be able to dynamically add and remove elements from the tree.

159
00:11:31,440 --> 00:11:35,080
So the first function that we look for is insert.

160
00:11:35,080 --> 00:11:37,809
So how do we insert an element in the tree?

161
00:11:37,909 --> 00:11:41,049
Well it is quite easy we look for it.

162
00:11:41,149 --> 00:11:49,120
If we do not find it then the place where our search concludes is exactly where it should have been so we insert it at that point.

163
00:11:49,549 --> 00:11:53,379
So for example supposing we try to insert 21 in this tree.

164
00:11:53,379 --> 00:11:57,179
we look at 52 and we go left when we go left.

165
00:11:57,179 --> 00:12:01,320
we come to 16 again and we go right then we come to 21.

166
00:12:01,320 --> 00:12:08,009
28 and we find that we have exhausted this path and there is no possible 21 in this tree.

167
00:12:08,009 --> 00:12:11,470
But had we found 21 it should be to the left or 28 so we insert it there.

168
00:12:12,019 --> 00:12:14,740
So we look for where we should find it.

169
00:12:14,740 --> 00:12:19,469
and if you do not find it we insert it in the appropriate place

170
00:12:19,494 --> 00:12:24,370
look for 65. So 65 is bigger than 52 so we go right.

171
00:12:24,590 --> 00:12:28,059
Similarly we can start and it is smaller than 74 so we go left

172
00:12:28,059 --> 00:12:31,720
there is nothing to the left 74 so we put it to the left of 74.

173
00:12:32,149 --> 00:12:36,600
Now insert will not put in a value that is already there because we have no duplicates

174
00:12:36,600 --> 00:12:41,460
So if now we try to for instance insert 91 then we go right from 52.

175
00:12:41,460 --> 00:12:46,169
we go right from 74 and now we find that 91 is already present in the tree.

176
00:12:46,169 --> 00:12:48,879
So insert 91 has no effect on this tree.

177
00:12:50,870 --> 00:12:55,720
So This is a very simple modification of the fined algorithm.

178
00:12:55,720 --> 00:13:01,090
So we keep searching and if we reach the leaf node then we come to an empty node.

179
00:13:01,429 --> 00:13:04,299
So if we find that we have reached an empty node.

180
00:13:04,850 --> 00:13:09,370
then we do the equivalent of creating a new node here.

181
00:13:09,370 --> 00:13:12,240
we set this value to be v.

182
00:13:12,240 --> 00:13:17,799
and we create a new frontier below by adding two empty nodes in the left and right rather than just having none.

183
00:13:18,200 --> 00:13:22,269
On the other hand if we find the value in the tree we do nothing.

184
00:13:22,610 --> 00:13:26,429
and If we do not find the value then we just use the search tree property.

185
00:13:26,429 --> 00:13:30,759
and try to insert either on the left or on the right as appropriate.

186
00:13:34,009 --> 00:13:35,769
how about delete.

187
00:13:35,960 --> 00:13:39,340
So delete is a little bit more complicated than insert.

188
00:13:39,830 --> 00:13:43,990
So basically whenever we find v in the tree.

189
00:13:43,990 --> 00:13:48,929
and there can only be one v remember this is not like deleting from the list that we had before

190
00:13:48,929 --> 00:13:51,159
that we were removing the first copy of v.

191
00:13:51,159 --> 00:13:54,610
in a search tree we have only one copy of every value if at all.

192
00:13:54,610 --> 00:13:58,870
So if we find v we must delete it. So we search for v as usual.

193
00:13:59,179 --> 00:14:03,309
Now if the node that we are searching for is a leaf.

194
00:14:03,309 --> 00:14:04,750
Then we are done.

195
00:14:04,850 --> 00:14:07,720
we just delete it and nothing happens.

196
00:14:08,029 --> 00:14:11,919
If it has only one child then we can promote that child.

197
00:14:12,110 --> 00:14:15,570
If it has two children we have a hole.

198
00:14:15,570 --> 00:14:19,480
we have a node which we have to remove value but we have values on both sides below it.

199
00:14:19,480 --> 00:14:23,639
And now we will use this maximum function maxval or minval

200
00:14:23,639 --> 00:14:28,539
in order to do the work. so Let us just see how this works through some examples.

201
00:14:28,539 --> 00:14:31,899
So supposing we first perform delete 65.

202
00:14:31,899 --> 00:14:36,179
then we first search for 65 we find it since it is a leaf.

203
00:14:36,179 --> 00:14:41,019
Then we are in this case the first case we just have to remove this leaf and we are done

204
00:14:41,990 --> 00:14:44,769
Now we try to delete 74.

205
00:14:44,769 --> 00:14:48,519
So we find 74 and we find that it has only one child.

206
00:14:48,519 --> 00:14:54,330
So if it has only one child then we move this out then we can just effectively short circuit this link.

207
00:14:54,330 --> 00:14:58,950
and move this whole thing up and make 52 point to 91 directly.

208
00:14:58,950 --> 00:15:01,870
So we are in this second case this is what it means to promote the child.

209
00:15:01,870 --> 00:15:03,990
So the deleted node has only one child.

210
00:15:03,990 --> 00:15:12,250
we can bypass the child and directly connect the parent of the deleted node to the one child of the deleted node so this will result in 91 moving up there.

211
00:15:12,769 --> 00:15:20,110
Finally we have the difficult case which is we want to delete a node which is in the middle of the tree in case this case 37.

212
00:15:20,110 --> 00:15:24,690
So we come to 37 and we want to remove this now if you remove this there will be a vacancy

213
00:15:24,690 --> 00:15:28,269
Now what do we fill the vacancy in and how do we adjust the shape of the tree.

214
00:15:28,639 --> 00:15:32,159
So we look to the left and find the maximum value.

215
00:15:32,159 --> 00:15:36,450
 Remember that everything to the left is smaller than 37

216
00:15:36,450 --> 00:15:38,769
and everything to the right is bigger than 37.

217
00:15:38,769 --> 00:15:44,710
So among the left nodes we take the biggest one and move it there then everything to the left remain smaller than that node.

218
00:15:44,710 --> 00:15:51,759
We could also do it the other way and take the smallest value from the right but we stick to taking the maximum value in the left.

219
00:15:51,759 --> 00:15:56,830
So we go to the left and find the maximum value is 28. So basically we have taken this 28 and made a new setup.

220
00:15:57,110 --> 00:16:01,360
Now we should not have two copies of 28 so we need to remove this 28.

221
00:16:01,360 --> 00:16:05,889
So how do we do that? well within this subtree we delete 28.

222
00:16:06,019 --> 00:16:10,509
Now this looks like a problem because in order to delete a node we are again deleting it

223
00:16:10,509 --> 00:16:15,009
but remember that the way that the maximum value was defined it is along the rightmost path

224
00:16:15,009 --> 00:16:18,129
So the rightmost path will either end in the leaf.

225
00:16:18,129 --> 00:16:22,950
or it will end in the node like this which has only one child and we know that when we have a leaf or only one child

226
00:16:22,950 --> 00:16:28,509
we are in the first two cases which we can handle without doing this maximum value to the heap.

227
00:16:28,580 --> 00:16:33,039
So we can just walk down remove the 28 and promote the 21.

228
00:16:33,139 --> 00:16:37,870
So this is exactly how delete works.

229
00:16:37,870 --> 00:16:46,570
So we can now look at the function. So If there is no value v then we just return.

230
00:16:47,870 --> 00:16:54,490
easy cases are when we do not find the current thing. If it is less than the current value then we go to the left and delete.

231
00:16:54,679 --> 00:16:58,299
If it is bigger than the current value we go to the right and delete.

232
00:16:58,299 --> 00:17:01,990
So the hard work comes when we actually find v at the current value.

233
00:17:02,929 --> 00:17:07,349
So If this is a leaf now we have not shown how to write this function but if this is a leaf

234
00:17:07,349 --> 00:17:12,240
This means that it has left and right child both as empty nodes.

235
00:17:12,240 --> 00:17:16,470
If this is a leaf then we will make it empty we will see how we do this in a minute I will just show you code for this

236
00:17:16,470 --> 00:17:20,519
If this is a leaf we delete it this is the first case the simple case.

237
00:17:20,519 --> 00:17:24,160
This is a leaf we just delete it and we make this not empty.

238
00:17:24,920 --> 00:17:30,579
If on the other hand it has only one child.

239
00:17:30,890 --> 00:17:35,380
So actually in this case if the left is empty

240
00:17:35,380 --> 00:17:42,369
Then we just promote the right and if its left is not empty.

241
00:17:42,369 --> 00:17:45,990
then we will copy the maximum value from the left.

242
00:17:45,990 --> 00:17:47,799
and delete the maximum value on the left.

243
00:17:48,140 --> 00:17:51,369
So we need to just see these two functions here.

244
00:17:51,369 --> 00:17:53,410
make empty and copy right.

245
00:17:53,690 --> 00:17:57,750
So make empty just says convert this into an empty node.

246
00:17:57,750 --> 00:18:00,119
An empty node is one in which all three fields are none.

247
00:18:00,119 --> 00:18:03,869
So we will just say self dot value is None self dot left is None self dot right is None

248
00:18:03,869 --> 00:18:08,019
So If it had an empty node hanging off it those empty nodes are now disconnected from the tree

249
00:18:08,480 --> 00:18:11,230
So this is make empty function.

250
00:18:12,410 --> 00:18:17,019
and Now copy right function just takes everything from the right and moves it up.

251
00:18:17,210 --> 00:18:24,910
So it takes the right value and makes it the current value the left value the right dot left and makes it the left.

252
00:18:24,910 --> 00:18:29,579
So we just take basically this node and copy these values one by one.

253
00:18:29,579 --> 00:18:34,299
So we copy right dot value to the current value right dot left to the current left right dot right to the current right

254
00:18:38,299 --> 00:18:41,640
So How much time do all these take?

255
00:18:41,640 --> 00:18:45,180
Well If you examine the thing carefully you would realize that

256
00:18:45,180 --> 00:18:49,619
In every case we are just walking down one path searching for the value.

257
00:18:49,619 --> 00:18:53,380
and along that path either we find it or we go down to the end and then we stop.

258
00:18:53,569 --> 00:18:57,789
So the complexity of every operation is actually determined by the height of the tree.

259
00:18:58,640 --> 00:19:03,150
If we have a balanced tree a balanced tree is one where you can define

260
00:19:03,150 --> 00:19:07,690
that each time we come to a node the left and the right child roughly have the same size.

261
00:19:07,690 --> 00:19:10,869
If we have a balanced tree then it is not difficult to see.

262
00:19:10,869 --> 00:19:12,960
that we have height logarithm n.

263
00:19:12,960 --> 00:19:16,119
This is like a heap heap is an example of a balanced tree.

264
00:19:16,119 --> 00:19:20,519
A search tree will not be as nicely structured as the heap because we will have some holes

265
00:19:20,519 --> 00:19:29,170
but we will have a logarithmic height in general and we will not explain how to balance the tree in this particular course.

266
00:19:29,170 --> 00:19:31,289
you can look it up you can look for

267
00:19:31,289 --> 00:19:37,539
topic called AVLtrees which is one variety of balanced trees which are balanced by rotating subtrees.

268
00:19:37,539 --> 00:19:44,470
So it is possible while doing insert and delete to maintain balance at every node and ensure that all these operations are logarithm.

269
00:19:45,230 --> 00:19:48,509
So let us just look at the code directly and execute it

270
00:19:48,509 --> 00:19:52,509
and convince ourselves that all the things that we hope here actually work as intended.

271
00:19:56,269 --> 00:20:02,160
So here we have a python code for the class tree that we showed in the lectures.

272
00:20:02,160 --> 00:20:06,299
we have just added a comment about how the empty node is organized and the leaves are organized.

273
00:20:06,299 --> 00:20:09,730
So there is the constructor which sets up.

274
00:20:09,730 --> 00:20:15,279
either an empty node or a leaf node with two empty children.

275
00:20:15,410 --> 00:20:20,019
Then we have is empty and is leaf we check whether the current value is None

276
00:20:20,019 --> 00:20:24,130
or both the left and right children are empty respectively.

277
00:20:24,130 --> 00:20:28,089
then we have this function make empty which converts the leaf or empty node.

278
00:20:28,089 --> 00:20:32,049
copy right copies the right child values to the current node.

279
00:20:32,089 --> 00:20:36,609
and then we have the basic recursive functions. So we start with find.

280
00:20:36,650 --> 00:20:41,440
So find is the one which is equivalent to a binary search.

281
00:20:41,720 --> 00:20:46,509
then insert is like find and then where it does not find it tries to insert.

282
00:20:46,789 --> 00:20:51,039
Finally we have Maxval which we need for a delete.

283
00:20:51,039 --> 00:20:59,490
and delete now when we reach the situation where we have found a value that needs to be deleted.

284
00:20:59,490 --> 00:21:01,529
If it is a leaf then we remove the leaf and make it empty.

285
00:21:01,529 --> 00:21:05,170
if it is the left child is empty then we copy the right child up.

286
00:21:05,170 --> 00:21:09,940
otherwise we delete the maximum from the left and promote that maximum value to the current

287
00:21:10,130 --> 00:21:16,740
Finally we have this in order traversal which generates a sorted list from the tree values.

288
00:21:16,740 --> 00:21:20,259
and the str function just displace the in order troubles.

289
00:21:22,460 --> 00:21:25,779
So now if we load this.

290
00:21:28,670 --> 00:21:35,410
then we can for instance import this package and set up an empty tree.

291
00:21:38,299 --> 00:21:42,279
and then put in some random values.

292
00:21:46,400 --> 00:21:51,329
It is important to not put in sorted order otherwise in a sorted tree if we just insert one at a time

293
00:21:51,329 --> 00:21:53,140
it will just generate one long path.

294
00:21:53,210 --> 00:21:57,279
So I am just trying to put it in some random order.

295
00:22:02,809 --> 00:22:06,819
So we insert into the tree all these values.

296
00:22:08,150 --> 00:22:11,950
And now we are printing the name a sorted version of this

297
00:22:12,079 --> 00:22:18,099
So I can now insert more values so I can for example insert 17.

298
00:22:18,200 --> 00:22:20,099
and verify that now.

299
00:22:20,099 --> 00:22:24,069
17 is there between 14 and 18 and I can keep doing the second insert.

300
00:22:24,069 --> 00:22:28,990
I can even set values in between at 4.5 because I have not in specified the integers

301
00:22:28,990 --> 00:22:32,970
So it puts it between 4 and 5 and so on and now I can delete

302
00:22:32,970 --> 00:22:34,750
For example if I delete 3

303
00:22:36,890 --> 00:22:39,940
then I find that I have 1 2 4 if I delete.

304
00:22:40,339 --> 00:22:41,650
for example 14

305
00:22:43,430 --> 00:22:46,750
and I no longer have 14 between 7 and 17 and so on.

306
00:22:46,750 --> 00:22:50,519
So this implementation definitely works although we have not balanced it.

307
00:22:50,519 --> 00:22:56,460
So if we do not balance it then the danger is that if we keep inserting
in sorted sequence say we keep inserting larger values

308
00:22:56,460 --> 00:23:00,579
if we keep adding on the right side. So the tree actually looks like a long path

309
00:23:00,579 --> 00:23:04,809
So then it becomes like a sorted list and every insert will take order n time.

310
00:23:04,809 --> 00:23:09,700
But if we do have rotations built in as we do we could be using an avl tree.

311
00:23:09,700 --> 00:23:12,240
then we can ensure that the tree never grows to height more than log n

312
00:23:12,240 --> 00:23:20,529
So all the operations insert find and delete will always be logarithmic with respect to the number of values currently being maintained.

