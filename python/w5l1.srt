1
00:00:03,140 --> 00:00:06,969
Let us see what to do when things go wrong with our programs.

2
00:00:06,980 --> 00:00:10,029
Now, there are many different kinds of things that can go wrong.

3
00:00:10,099 --> 00:00:17,980
For instance, we might have an expression like x divided by z, and z has a value 0, so this expression value cannot be computed.

4
00:00:18,589 --> 00:00:26,140
Or we might be trying to convert something from a string to an integer, where the string s is not a valid representation of an integer.

5
00:00:27,140 --> 00:00:32,890
We could also be trying to compute an expression using a name whose value has not been defined.

6
00:00:33,350 --> 00:00:38,259
Or we could try to index a position in a list which does not exist.

7
00:00:39,109 --> 00:00:43,810
As we go forward we will be looking at how to read and write from files on the disk.

8
00:00:43,810 --> 00:00:47,770
So we may be trying to read from a file, but perhaps there is no such file.

9
00:00:48,259 --> 00:00:52,899
Or we may be trying to write to a file, but the disk is actually full.

10
00:00:53,030 --> 00:00:58,869
So there are many situations in which, while our program is running we might encounter an error.

11
00:01:00,590 --> 00:01:05,769
So some of these errors can be anticipated whereas others are unexpected.

12
00:01:06,260 --> 00:01:13,299
So if we can anticipate an error, we would prefer to think of it not as an error, but as an exception.

13
00:01:13,549 --> 00:01:16,019
So let us think of the word exceptional.

14
00:01:16,019 --> 00:01:21,519
So we encounter a normal situation where the program runs the way we would like.

15
00:01:21,519 --> 00:01:27,798
And then occasionally we might encounter an exceptional situation where something wrong happens.

16
00:01:27,823 --> 00:01:34,750
And what we would like to do is to provide a plan on how to deal with this exceptional situation.

17
00:01:34,750 --> 00:01:37,090
And this is called exception handling.

18
00:01:38,719 --> 00:01:45,310
So exception handling is, how do we provide corrective action when something goes wrong.

19
00:01:45,739 --> 00:01:50,823
Now, the type of corrective action could depend on what type of error it is.

20
00:01:50,848 --> 00:01:58,299
If for instance, we had asked the user to type a file name, and we are trying to read the file and the file does not exist.

21
00:01:58,299 --> 00:02:04,150
So you could display a message and ask the user to retype the file name saying the file asked for does not exist.

22
00:02:04,549 --> 00:02:11,349
On the other hand, if a list is being indexed out of bounds, there is probably an error in our program.

23
00:02:11,349 --> 00:02:17,979
We might want to print out this value of the index to try and diagnose what is going wrong with our program.

24
00:02:17,979 --> 00:02:23,530
Sometimes, the error handling might just be debugging the error prone program.

25
00:02:24,500 --> 00:02:33,610
For all this, what we require is a way of capturing these errors within the program as it is running without killing the program.

26
00:02:34,400 --> 00:02:47,460
So, when we have spotted errors while we are using the interpreter, if an error does happen, and we do not trap it in this way, then the program will actually abort and exit.

27
00:02:47,485 --> 00:02:52,780
So, we want a way to catch the error and deal with it without aborting the program.

28
00:02:54,830 --> 00:02:58,319
Now, there are many different types of errors.

29
00:02:58,319 --> 00:03:02,199
And some of these we have seen, but you may not have noticed the subtlety of these.

30
00:03:02,199 --> 00:03:16,389
For example, when we run python we type something which is wrong, then we get something called a syntax error and the message that python gives us is syntax error invalid syntax.

31
00:03:19,099 --> 00:03:34,719
For example, supposing we try to create a list and by mistake we use a semicolon instead of a comma, then immediately python points to that semicolon and says this is a syntax error, it is invalid python syntax.

32
00:03:38,449 --> 00:03:45,909
Of course, if we have invalid syntax that means the program is not going to run at all and there is not much we can do.

33
00:03:45,949 --> 00:03:51,155
So, what we are really interested in is errors that happen in valid programs.

34
00:03:51,180 --> 00:03:56,800
 So, the program is syntactically correct and it is something that the python interpreter can execute.

35
00:03:56,800 --> 00:04:00,610
But, while the code is being executed some error happens.

36
00:04:03,680 --> 00:04:06,759
So these are what are called run time errors.

37
00:04:06,889 --> 00:04:11,639
These are errors that happen while the program is running.

38
00:04:11,639 --> 00:04:15,759
We have seen these errors and they come with some diagnostic information.

39
00:04:16,009 --> 00:04:24,930
For instance, if we use a name whose value is undefined, then we get a message from python that the name is not defined.

40
00:04:24,930 --> 00:04:29,709
And we also get a code at the beginning of the line saying this is a name error.

41
00:04:29,709 --> 00:04:33,040
So this is python’s way of telling us what type of error it is.

42
00:04:33,040 --> 00:04:41,470
Similarly, if we have an arithmetic expression where we end up dividing by a value 0, then we will get something called a 0 division error.

43
00:04:41,959 --> 00:04:49,750
And finally, if we try to index a list outside its range then we get something called an index error.

44
00:04:53,329 --> 00:04:56,699
Let us look at all these errors just to be sure that we understand.

45
00:04:56,724 --> 00:05:08,259
So supposing we say y is equal to 5 times x and we have not defined anything for x, then it gives us a name error and it says clearly that the name x is not defined.

46
00:05:08,259 --> 00:05:16,959
On the other hand, if we say y is equal to 5 divided by 0, then we get a 0 division error and along with the message division by 0.

47
00:05:16,959 --> 00:05:29,199
Finally, if we have a list say 1 2 and then we ask for the position 3, then it will say that there is no position 3 in this list, so this is an index error.

48
00:05:29,199 --> 00:05:33,959
So these are three examples of the types of error that the python interpreter tells us.

49
00:05:33,959 --> 00:05:43,029
And notice that there is an index error name error, the 0 division error and a diagnostic explanation after that.

50
00:05:47,540 --> 00:05:51,850
So, let us first quickly settle on some terminology.

51
00:05:51,850 --> 00:05:58,389
So, usually the act of signalling an error is called raising an exception.

52
00:05:58,790 --> 00:06:07,470
So, when the python interpreter detects an error, it gives us information about this error and as we saw it comes in two parts.

53
00:06:07,470 --> 00:06:11,670
There is the type of the error, which defines what kind of error it is.

54
00:06:11,670 --> 00:06:15,639
So, it is name error or an index error or a 0 division error.

55
00:06:15,639 --> 00:06:20,525
And secondly, there is some diagnositic information telling us where this error occurs.

56
00:06:20,550 --> 00:06:24,089
It is not enough to just tell us, some value was not defined.

57
00:06:24,089 --> 00:06:27,339
It tells us specifically the name x is not defined.

58
00:06:27,339 --> 00:06:31,180
So this gives us some hint as to where the error might be.

59
00:06:31,730 --> 00:06:39,240
Now, when such an error is signaled by python, what we would like to do is to handle it within our program.

60
00:06:39,240 --> 00:06:44,186
So we would like to anticipate and take corrective action based on the error type.

61
00:06:44,211 --> 00:06:47,690
So we may not want to take the same type of action for every error type.

62
00:06:47,715 --> 00:06:52,628
That is why it is important to know whether it is a name error or an index error or something else.

63
00:06:52,653 --> 00:06:57,310
And depending on what the error is, we might take appropriate action for that type of error.

64
00:06:58,100 --> 00:07:08,500
Finally, if we do get an error or an exception, which we have not explicitly handled, then the python interpreter has no option, but to abort the program.

65
00:07:08,500 --> 00:07:14,980
So if we do not handle an exception, the execution will abort.

66
00:07:17,120 --> 00:07:23,079
So this is done using a new type of block which we have not seen before called try.

67
00:07:23,089 --> 00:07:26,649
So what we have is a try block.

68
00:07:26,649 --> 00:07:34,360
So when we have code here, and when we anticipate that there may be some error, we put it inside a try.

69
00:07:34,360 --> 00:07:40,060
So this is our usual block of code, where we anticipate that something may go wrong.

70
00:07:40,060 --> 00:07:49,629
And now we provide contingencies for all the things that could go wrong depending on the type of error and this is provided using this except statement.

71
00:07:49,629 --> 00:07:56,110
It says try this and if something goes wrong then go to the appropriate except one after the other.

72
00:07:56,110 --> 00:08:01,585
So the first one says what happens if an index error occurs.

73
00:08:01,610 --> 00:08:05,403
So this is the code that happens if an index error occurs.

74
00:08:05,428 --> 00:08:11,079
On the other hand maybe I could get a name error or a key error for both of which I do the same thing.

75
00:08:11,079 --> 00:08:12,796
So this is the next except block.

76
00:08:12,821 --> 00:08:20,350
So you could have as many except blocks as you have types of errors which you anticipate errors for.

77
00:08:20,350 --> 00:08:25,300
It is not obligatory to handle every kind of error but only those which you anticipate.

78
00:08:25,639 --> 00:08:31,029
And of course, now you might want to do something in general for all errors that you do not anticipate.

79
00:08:31,029 --> 00:08:37,062
So you can have a pure except block in which you do not specify the type of error.

80
00:08:37,087 --> 00:08:42,120
And by default such an except statement would catch all other exceptions.

81
00:08:42,120 --> 00:08:45,539
So, the important thing to remember is that this happens in sequence.

82
00:08:45,539 --> 00:08:56,040
So, if I have three errors for example, if I have an index error and a name error and a 0 division error, then what will happen is that it will first go here and find that there is an index error.

83
00:08:56,065 --> 00:08:59,649
This code will execute so the name error code will not execute.

84
00:08:59,649 --> 00:09:13,889
On the other hand, if I had only a name error and if I had a 0 division error for example because there is a name error first, it will come here and will find that there is no index error.

85
00:09:13,889 --> 00:09:17,039
Then it will come here and say there is a name error and will execute this code.

86
00:09:17,039 --> 00:09:21,250
So the 0 division error will not be explicitly handled.

87
00:09:21,250 --> 00:09:25,679
The program will not abort, but there will be no code executed for the 0 division error.

88
00:09:25,679 --> 00:09:32,649
It is not that it tries each one of these in turn, it will try whichever except matches the error and it will skip the rest.

89
00:09:32,649 --> 00:09:41,844
Finally, if I add only a 0 division error in this particular example, then since it is not an index error and it is not a name error it would try to go through these in turn.

90
00:09:41,869 --> 00:09:50,440
So it would come here and find this is not the type of error, this is not the type of error, and it will go to the default except statement and catch all other exceptions.

91
00:09:51,769 --> 00:09:58,463
Finally, python offers us a very useful alternative clause called else.

92
00:09:58,488 --> 00:10:03,472
So this else is in the same spirit as the else associated with the for or a while.

93
00:10:03,519 --> 00:10:12,730
Remember that a for or a while that does not break, terminates normally, and then executes the else, if there is a break the else is skipped.

94
00:10:12,730 --> 00:10:21,284
In the same way if the try executes normally, that is there is no error which is found, then the else will execute, otherwise the else is skipped.

95
00:10:21,309 --> 00:10:27,970
So we have an else block which will execute if the try terminates normally with no errors.

96
00:10:27,970 --> 00:10:31,389
So this is the overall structure of how we handle exceptions.

97
00:10:31,389 --> 00:10:39,591
We put the code that we want inside a try block then we have a sequence of except blocks which catch different types of exceptions.

98
00:10:39,616 --> 00:10:44,629
We can catch more than one type of exception by putting a sequence in a tuple of exceptions.

99
00:10:44,772 --> 00:10:52,229
We can have a default except with no name associated with it to catch all other exceptions which are not handled.

100
00:10:52,316 --> 00:10:57,129
And finally, we have an else which will execute if the try terminates normally.

101
00:10:59,480 --> 00:11:10,600
Now, while we normally use exception handling to deal with errors which we do not anticipate, we can actually use it to change our style of programming.

102
00:11:10,909 --> 00:11:13,269
Let us look at a typical example.

103
00:11:13,279 --> 00:11:17,490
We saw recently that we can use dictionaries in python.

104
00:11:17,490 --> 00:11:20,340
So dictionaries associate values with keys.

105
00:11:20,340 --> 00:11:27,759
Here we have two keys Dhawan and Kohli and with each key which is a name we have a list of scores.

106
00:11:28,039 --> 00:11:33,759
So this scores is a dictionary whose keys are strings and whose values are lists of numbers.

107
00:11:33,830 --> 00:11:38,190
Now, suppose we want to add a score to this.

108
00:11:38,190 --> 00:11:41,500
The score is associated with a particular batsman b.

109
00:11:41,500 --> 00:11:46,149
So we have a score s for a batsman b and we want to update this dictionary.

110
00:11:46,149 --> 00:11:47,940
Now, there are two situations.

111
00:11:47,940 --> 00:11:51,610
One is that we already have an entry for b in the dictionary.

112
00:11:51,610 --> 00:11:56,590
In which case we want to append s to the existing list scores of b.

113
00:11:56,720 --> 00:12:00,029
The other situation is that this is a new batsman.

114
00:12:00,029 --> 00:12:07,809
There is no key for b, in which case we have to create a key by setting scores of b equal to the list containing s.

115
00:12:07,809 --> 00:12:10,679
So, we have two alternative modes of operation.

116
00:12:10,679 --> 00:12:13,840
It is an error to try and append to a non-existent key.

117
00:12:13,840 --> 00:12:17,730
But if there is an existing key, we do not want to lose it by reassigning s.

118
00:12:17,730 --> 00:12:21,009
So we want to append it, and we want to distinguish these two cases.

119
00:12:22,700 --> 00:12:34,649
So, a standard way to do this using what we have already seen is to use the statement in to check whether the value b already occurs as a key in scores.

120
00:12:34,649 --> 00:12:45,519
If b is in the score dot keys, which means we have b as an existing key then we append this score, otherwise we create a new entry so this is fine.

121
00:12:45,799 --> 00:12:55,019
Now, we can actually do this using exception handling and try to append it.

122
00:12:55,019 --> 00:13:03,820
We assume by default that the batsman b already exists as a key in this dictionary scores.

123
00:13:03,820 --> 00:13:07,269
We try scores b dot append s.

124
00:13:07,269 --> 00:13:09,419
What would happen if b is not there.

125
00:13:09,419 --> 00:13:14,963
Well, python will signal an error saying that this is an invalid key and that is called a key error.

126
00:13:14,988 --> 00:13:24,908
So we can then revert to this except statement and say if there is a key error when I try to append s to scores of b, then create an entry scores of b equal to s.

127
00:13:24,933 --> 00:13:40,649
It is just a different style and it is not saying that one is better than the other, but it is just emphasising that once we have exception handling under our control, we may be able to do things differently from what we are used to and sometimes these may be more clear.

128
00:13:40,649 --> 00:13:44,080
It is a matter of style you might prefer the left or the right.

129
00:13:44,080 --> 00:13:46,659
But both are valid pieces of python code.

130
00:13:49,850 --> 00:13:53,740
Let us examine what actually happens when we error occurs.

131
00:13:53,740 --> 00:14:02,169
So suppose we start executing something and we have a function call to a function f with parameters y and z.

132
00:14:02,169 --> 00:14:08,100
This will go and look up a definition for the function.

133
00:14:08,100 --> 00:14:11,679
So this call results in executing this code.

134
00:14:11,679 --> 00:14:16,269
And this definition might have yet another function called in it called g.

135
00:14:16,269 --> 00:14:20,289
So, this will in turn transfer us to a new definition.

136
00:14:20,289 --> 00:14:22,330
Sorry, this should be on the same line.

137
00:14:22,549 --> 00:14:27,820
g might in turn have another function h.

138
00:14:27,830 --> 00:14:32,429
Finally, when we go to h perhaps this is where the problem happens.

139
00:14:32,429 --> 00:14:44,590
So, somewhere inside h perhaps there is an index error and where we used list for example in h, we did not put it in a try block and so the error is not handled.

140
00:14:44,779 --> 00:14:49,169
So when an error is not handled the program aborts.

141
00:14:49,169 --> 00:14:56,649
The program does not directly abort, this function will abort and it will transfer the error back to whatever called it.

142
00:14:56,649 --> 00:15:04,440
So what will happen here is that this index error will go back to the point where h was invoked in g.

143
00:15:04,440 --> 00:15:08,370
Now, it is as though g has generated an index error.

144
00:15:08,370 --> 00:15:12,759
Calling h has generated an index error, so the index error is actually now within g.

145
00:15:12,759 --> 00:15:17,110
Because, h did not do anything with that error, it just passed it back with the error.

146
00:15:17,110 --> 00:15:20,139
Now, we have two options either g has a try block.

147
00:15:20,299 --> 00:15:25,269
But, if g does not have a try block then this error will cause g to abort.

148
00:15:25,269 --> 00:15:33,070
So what will happen next is that if g does not handle it then this will go back to where g was called in f.

149
00:15:33,230 --> 00:15:39,903
And likewise, if now f does not handle it, then it will go back to where f was called in the main program.

150
00:15:39,928 --> 00:15:45,750
So, we keep going back across the sequence of function calls passing back the error.

151
00:15:45,750 --> 00:15:50,070
If we do not handle it in the function where we are right now, the error goes back.

152
00:15:50,070 --> 00:16:02,740
This function aborts, it goes back and finally when it reaches the main thread of control, the first function or the first python code that we are executing does not handle it, then definitely the overall python program aborts.

153
00:16:02,740 --> 00:16:07,049
It is not as though the very first time we find an error which is not handled it will abort.

154
00:16:07,049 --> 00:16:10,379
It will merely pass control back to where it was called from.

155
00:16:10,379 --> 00:16:15,360
And across the sequence of calls hierarchically we can catch it at any point.

156
00:16:15,360 --> 00:16:21,700
So we do not have to catch the error at the point where it is handled but we can catch it higher up from the point that is calling us.

157
00:16:24,980 --> 00:16:31,600
To summarize exception handling allows us to gracefully deal with runtime errors.

158
00:16:32,059 --> 00:16:37,899
So, python when it flags an error tells us the type of error and some diagnostic information.

159
00:16:37,899 --> 00:16:44,379
So, using a try and except block we can check the type of error and take appropriate action based on the type.

160
00:16:44,570 --> 00:16:53,379
We also saw with that inserting a value into a dictionary example that we can exploit exception handling to develop new styles of programming.

161
00:16:54,080 --> 00:17:07,559
And finally, as we go ahead and start dealing with input, output and files, exceptions will be rather more common as we saw earlier, and one of the examples we mentioned was a file is not found or a disk is full.

162
00:17:07,559 --> 00:17:14,549
So, input and output inherently involves a lot of interaction with things outside the program.

163
00:17:14,549 --> 00:17:21,460
And hence is much more prone to errors and therefore it is useful to be able to have this mechanism within our bag of tricks.

