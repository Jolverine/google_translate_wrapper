1
00:00:02,089 --> 00:00:06,879
So, in the last lecture we saw how to use the  input  and  print statements .

2
00:00:06,879 --> 00:00:13,630
To collect  input  from the  standard input , this is the  keyboard  and to display values on the screen using  print .

3
00:00:13,640 --> 00:00:19,445
Now, this is useful for small quantities of  data , but we want to  read  and write  the large quantities of  data .

4
00:00:19,470 --> 00:00:24,429
It is impractical to type them in by hand or to see them as a  scrollbars  in the screen.

5
00:00:24,429 --> 00:00:29,129
So, for large  data  we are forced to deal with  files  which reside on the  disk .

6
00:00:29,129 --> 00:00:33,850
So, we have to  read  a large volume of  data  which is already written on a  file  in the  disk 

7
00:00:33,850 --> 00:00:38,049
and the  output  we compute is typically written back into another  file  on the  disk .

8
00:00:38,299 --> 00:00:42,820
Now, one thing to keep in mind when dealing with  disks  is that.

9
00:00:42,820 --> 00:00:47,859
 disk read  and  write  is very much slower than  memory read  and  write .

10
00:00:50,210 --> 00:00:57,670
So, to get around this most systems will  read  and  write data  in large blocks.

11
00:00:57,670 --> 00:01:02,738
Imagine that you have a large storage facility in which you store things in big carton

12
00:01:02,763 --> 00:01:08,939
Now, when you go and fetch something, you bring a carton at a time even if you are only looking for say one book in the carton

13
00:01:08,939 --> 00:01:12,629
So, you do not go and fetch one book out of the Carton from the storage facility

14
00:01:12,629 --> 00:01:18,250
You bring the whole carton and then when you want to put things back again you assemble them in a Carton and then put them back.

15
00:01:18,250 --> 00:01:24,359
So, in the same way the way that  data  flows back and forth between  memory  and  disk  is in chunks called  blocks .

16
00:01:24,359 --> 00:01:27,700
Even if you want to  read  only one value or only one  line .

17
00:01:27,700 --> 00:01:35,909
It will actually fetch a large volume of  data  from the  disk  and store it in what is called a  buffer  and then you  read  whatever you need from the  buffer 

18
00:01:35,909 --> 00:01:47,650
Similarly, when you want to  write  to the  disk  you assemble your  data  in the  buffer  and when the  buffer  is enough quantity to be written on to the  disk  then one chunk of  data  and a  block  will be written back on a  disk .

19
00:01:50,120 --> 00:01:52,836
So, when we  read  and  write  from a  disk .

20
00:01:52,861 --> 00:01:58,709
The first thing we need to do is connect to this  buffer . So, this is called the opening a  file .

21
00:01:58,709 --> 00:02:02,469
So, when we open a  file  we create something called a  file handle .

22
00:02:02,469 --> 00:02:10,810
And you can imagine that this is like getting access to a  buffer  from which  data  from that  file  can be  read  into  memory  or written back.

23
00:02:11,539 --> 00:02:14,560
Now, having opened this  file handle  .

24
00:02:14,560 --> 00:02:18,300
Everything we do with the  file  is actually done with respect to the  file handle 

25
00:02:18,300 --> 00:02:21,780
So, we do not directly try to  read  and  write  from the  disk .

26
00:02:21,780 --> 00:02:26,289
we  write  and  read  from the  buffer  that we have opened using this  file handle 

27
00:02:27,439 --> 00:02:30,330
Finally, when we are done with our processing

28
00:02:30,330 --> 00:02:34,330
We need to make sure that all the  data  that we have written goes back.

29
00:02:34,330 --> 00:02:39,479
So, this is done by closing the  file . So closing the  file  has two effects.

30
00:02:39,479 --> 00:02:43,800
The first effect is to make sure that all changes that we intended to make with the  file 

31
00:02:43,800 --> 00:02:51,449
Any  data  we wanted to  write  to the  file  is actually taken out to the  buffer  and put on to the  disk  and this is technically called  flushing  the  buffer .

32
00:02:51,449 --> 00:02:57,268
So, closing a  file flushes  the  output buffer  make sure that all  writes  go back to the  file  and do not get lost.

33
00:02:57,293 --> 00:02:59,800
And the second thing it does is that it

34
00:02:59,800 --> 00:03:04,736
In some sense, makes this  buffer  go away. So it disconnects the  file handle  that we just set up.

35
00:03:04,761 --> 00:03:10,719
So now this  file  is no longer connected to us if we want to  read  or  write  to it again we have to again  open  it.

36
00:03:13,610 --> 00:03:16,659
So, the  command  to open a  file  is just  open .

37
00:03:16,659 --> 00:03:21,683
So, the first  argument  that you give  open  is the actual  file  name on your  disk .

38
00:03:21,708 --> 00:03:28,740
So now, this will depend a little bit on what  system  you are using, but usually it has a first part in an  extension .

39
00:03:28,740 --> 00:03:32,560
So, this  command for instance is to open the  file   gcd dot py .

40
00:03:32,560 --> 00:03:39,743
Now, implicitly if we just give a  file  name it will look for it in the current  folder  or a  directory  where you are  running  the  script.

41
00:03:39,768 --> 00:03:46,221
So you can give a  file name  which belongs to a different part of your  directory hierarchy  by giving a path

42
00:03:46,246 --> 00:03:51,520
And how you describe the path will depend on whether you are working on  windows  or  unix  or what  operating system  you are using.

43
00:03:51,710 --> 00:03:55,800
Now, you see that there is a second argument there which is the  letter R .

44
00:03:55,800 --> 00:03:59,590
So, this tells us how we want to open the  file .

45
00:03:59,590 --> 00:04:06,340
So, you can imagine that if you are making changes to a  file  by both  reading  it and  writing  it, this can create confusion.

46
00:04:06,340 --> 00:04:14,430
So, what we have to do is decide in advance whether we are going to  read  from a  file  or  write  to it we cannot do both.

47
00:04:14,430 --> 00:04:18,819
There is no way we can simultaneously  read  from a  file  and modify it while it is  opened .

48
00:04:18,819 --> 00:04:23,800
So,  read  is signified by  r , now  write  comes in two flavors.

49
00:04:23,800 --> 00:04:26,860
We might be wanting to create a new  file  from scratch.

50
00:04:26,860 --> 00:04:29,079
In this case we use the letter  w .

51
00:04:29,389 --> 00:04:32,860
So,  w  stands for  write  out to a new  file .

52
00:04:32,860 --> 00:04:41,980
We have to be a bit careful with this because if we  write  out to a  file  which already exists then opening it with more  w  will just overwrite the contents that we already had.

53
00:04:42,319 --> 00:04:48,770
The other thing which might be useful to do is to take a  file  that already exists and add something to it.

54
00:04:48,795 --> 00:04:54,823
So this is called  append , so we can have two writing modes  w for write  and  a for append .

55
00:04:54,848 --> 00:05:01,149
What append will do is it will take a  file  which already exists and add the new stuff that we are writing at the  end of the file .

56
00:05:03,980 --> 00:05:07,449
So, once we have a  file open .

57
00:05:07,699 --> 00:05:09,459
Let us see how to  read .

58
00:05:09,680 --> 00:05:13,420
So, we invoke the  read command  through the  file handle .

59
00:05:13,420 --> 00:05:20,699
So, this is like some of the other  functions  that we saw with  strings  and so on, where we attach the  function  to the  object .

60
00:05:20,699 --> 00:05:24,196
So,  F H  was the  file handle   we  opened , so we want to  read  from it.

61
00:05:24,221 --> 00:05:32,860
So we say  F H  dot read . What  F H  dot read  does is, it swallows the entire contents to the  file  as a single  string  and  returns it.

62
00:05:32,860 --> 00:05:36,569
And then we can assign it to any name, here we have used the name  contents .

63
00:05:36,569 --> 00:05:44,620
So,  contents  is now assigned the entire  data  which is in the  file handle  pointed to by  F H  in one  string  .

64
00:05:45,920 --> 00:05:50,279
Now, we can also consume a  file , so we are typically dealing with  text files .

65
00:05:50,279 --> 00:05:52,276
So,  text files  usually consist of  lines 

66
00:05:52,301 --> 00:05:55,916
Think of  python code for example, we have  lines  after  lines  after  lines .

67
00:05:55,941 --> 00:06:01,540
So natural unit is a bunch of  texts  which is ended by a  new line character  .

68
00:06:01,540 --> 00:06:08,670
If you remember this is what the  input command  does the input command waits for you to type something and then you press  return  which is a  new line .

69
00:06:08,670 --> 00:06:15,223
And whatever you typed up to that  return  is then transmitted by  input  as a  string  to the name that you have assigned to the  input .

70
00:06:15,248 --> 00:06:21,240
So,  readline  is like that but the difference between  readline  and  input  is that when you  read  a  line  , that

71
00:06:21,240 --> 00:06:24,623
You get the last  new  line  character  along with the  input string .

72
00:06:24,648 --> 00:06:31,019
So when you say  input  you only get the  characters  which come before the last  newline , the  newline  is not included

73
00:06:31,019 --> 00:06:33,329
but in  readline  you do get the  newline character  .

74
00:06:33,329 --> 00:06:37,449
So you have to remember that you have this extra  character floating  around at the end of your  string 

75
00:06:37,449 --> 00:06:41,459
So, this is conventionally denoted by this  backslash n .

76
00:06:41,459 --> 00:06:47,339
So,  backslash n  is a notation which denotes a single  character  even though it looks like two  characters .

77
00:06:47,339 --> 00:06:50,110
This is suppose to be the  new line character .

78
00:06:50,110 --> 00:06:55,083
Now, the actual  newline character  differs on  operating systems  from one to the other.

79
00:06:55,108 --> 00:07:00,189
But in  python  if you use  backslash n  it will be correctly translated in all the systems that you are using

80
00:07:01,670 --> 00:07:08,829
the third way that you can  read  from a  file  is to  read  all the  lines  one by one into a  list of strings .

81
00:07:08,829 --> 00:07:14,790
So, instead of  readline  if I say  readlines  then it  reads  the entire  file  as a  list  of strings.

82
00:07:14,815 --> 00:07:23,709
Each  string  is one item in the  list  and remember this again, each of these  lines  has the  backslash n  and included.

83
00:07:23,709 --> 00:07:31,079
So,  read readline  and  readlines  none of them will actually remove the  backslash n , they will remain faithfully as part of your  input 

84
00:07:31,079 --> 00:07:34,063
So in other words, if you are going to transfer this from one  file  to another.

85
00:07:34,088 --> 00:07:37,356
You do not have to worry about re-inserting  backslash n  because it is already there.

86
00:07:37,381 --> 00:07:40,070
So you can use this  input  to  output  directly.

87
00:07:40,095 --> 00:07:47,740
But on the other hand if you want to do some manipulation of this  string  then you must remember that it  backslash n  is there and you must deal with it appropriately.

88
00:07:50,660 --> 00:07:54,850
reading files  is inherently a sequential operation.

89
00:07:54,850 --> 00:07:59,100
Now, of course if we use the basic  command read , it  reads  the entire contents.

90
00:07:59,100 --> 00:08:08,198
So obviously, it  reads  it from beginning to end, but if we are reading one  line  at a time then the way it works is that when we  open  the  file  , we are initially at the beginning of the  file .

91
00:08:08,223 --> 00:08:13,810
So you can imagine a  pointer  like this  red arrow  which tells us where we are going to  read  next.

92
00:08:13,810 --> 00:08:16,439
So, initially when we open we are going to  read  from the beginning.

93
00:08:16,439 --> 00:08:23,098
Now each  readline  takes us forward, so if I do a  readline  at this point it will take me up to the next  backslash n .

94
00:08:23,123 --> 00:08:29,399
Remember a  line  is a quantity which is delimited by  backslash n .

95
00:08:29,399 --> 00:08:33,370
We could have a  line  which has hundred  characters , next  line  would have three  characters  and so on.

96
00:08:33,370 --> 00:08:37,590
So, it is from one  backslash n  to the next is what a line   is, so this is not a fixed length.

97
00:08:37,590 --> 00:08:41,669
So, we will move forward  reading  one  character  at a time time until we have  backslash n 

98
00:08:41,669 --> 00:08:48,423
Then everything up to that  backslash n  will be  returned  as the effect of the  string  returned  by the  readline  and the  pointer  will move to the next  character .

99
00:08:48,448 --> 00:08:55,179
Now we do another  readline  possibly of a different length again the  pointer  will move forward, so, in this way we go from beginning to end.

100
00:08:57,649 --> 00:09:02,049
In case we want to actually divert from the strategy.

101
00:09:02,049 --> 00:09:06,750
There is a  command seek  which takes a position an  integer n

102
00:09:06,750 --> 00:09:10,013
and moves directly to position  n  regardless of where you are.

103
00:09:10,038 --> 00:09:16,779
So, this is one way to move back or to jump around in a  file  other than by reading consecutively  line  by  line .

104
00:09:18,679 --> 00:09:25,829
Finally, we can modify the  read statement  to not  read  the entire  file , but to  read  a fixed number of  characters .

105
00:09:25,854 --> 00:09:31,720
Now, this may be useful, If your  file  actually consists of fixed  blocks of data .

106
00:09:31,720 --> 00:09:36,779
So, you might have, say for example  PAN  numbers, which are typically ten  characters  long

107
00:09:36,779 --> 00:09:42,960
and you might have just stored them as one long sequence of  text  without any  new lines , knowing that every  PAN  number is ten  characters .

108
00:09:42,960 --> 00:09:49,240
So, if you say  F H dot  read  ten, it will  read  the next  PAN  number and keep going and this will save you some space in the long run.

109
00:09:49,240 --> 00:09:53,830
So there are situations where you might exploit this, where you  read  a fixed number of  characters .

110
00:09:57,500 --> 00:10:00,129
Now, when we are reading a  file  incrementally.

111
00:10:00,139 --> 00:10:03,149
It is useful to know when the  file  is over.

112
00:10:03,149 --> 00:10:06,750
Because we may not know in advance how long the  file  is or how many  lines  the  file  is.

113
00:10:06,750 --> 00:10:12,669
If you are reading a  file   line  by  line  , then we may want to know, when the  file  is ended.

114
00:10:12,740 --> 00:10:17,938
So, there are two situations where we will know this

115
00:10:17,963 --> 00:10:25,051
So, one is if we try to  read  using the  read command  and we get nothing back, we get an empty  string  that means the  file  is over.

116
00:10:25,076 --> 00:10:26,518
We have reached the   end of file .

117
00:10:26,543 --> 00:10:31,565
Similarly, if we try to  read  a  line  and we get the empty  string  it means we have reached the  end of file .

118
00:10:31,590 --> 00:10:37,270
So,  read  or  readline , if they return the empty  string  it means that we have reached the  end of the  file 

119
00:10:37,295 --> 00:10:42,940
Remember we are going sequentially from beginning to end. So, we reached the  end of the file  and there is nothing further to  read  in this  file .

120
00:10:46,519 --> 00:10:48,940
So, having  read  from a  file .

121
00:10:49,399 --> 00:10:53,529
Then, the other thing that we would like to do is to  write  to a  file .

122
00:10:53,779 --> 00:10:56,399
So, here is how you  write  to a  file .

123
00:10:56,399 --> 00:10:59,019
Just like you have a  read command , you have a  write command .

124
00:10:59,019 --> 00:11:03,179
but now unlike  read  which implicitly takes something from the  file  and gives it to you

125
00:11:03,179 --> 00:11:08,139
Here you have to provide it something to provide put in the  file . So,  write  takes an argument which is a  string 

126
00:11:08,480 --> 00:11:12,549
So, when you say  write S  it says take the  string S   and  write  it to a  file .

127
00:11:12,549 --> 00:11:18,265
Now, there are two things: one is this  S  may or may not have a  backslash n  it may have more than one  backslash n .

128
00:11:18,290 --> 00:11:25,799
So, there is nothing that tells you that this is one  line , part of a  line  more than a  line , you have to  write S according to the way you wanted to be written on the  file 

129
00:11:25,799 --> 00:11:29,139
If you wanted to be one  line  you should make sure it ends with the  backslash n .

130
00:11:29,629 --> 00:11:33,399
And this  write  actually returns the number of  characters  written.

131
00:11:33,399 --> 00:11:36,929
Now, this may seem like a strange thing to do.

132
00:11:36,929 --> 00:11:44,580
I should tell you because you know from the length of  S  what is number of  characters  written but this is useful if for instance a  disk  is full.

133
00:11:46,340 --> 00:11:54,389
So, if you try to  write  a long  string  and you find out that only part of the  string  was written and this is an indication that there was a problem with the  write .

134
00:11:54,389 --> 00:11:59,620
It is useful sometimes to know how many  characters  actually got written out of the  characters  that you try to  write .

135
00:12:02,059 --> 00:12:06,916
so the otherthing which  writes  in bulk to a  file  is called  writelines .

136
00:12:06,941 --> 00:12:12,309
So this takes a  list  of strings  and  writes  them one by one into the  file .

137
00:12:12,309 --> 00:12:17,649
Now, though it says  writelines , these need not actually be  lines . So, it is a bit misleading the name.

138
00:12:18,083 --> 00:12:23,376
If you want them to be  lines , you must make sure that you have each of them terminated by  backslash n .

139
00:12:23,401 --> 00:12:27,536
If they are not then they will just cascade to form a long  line  in the thing.

140
00:12:27,561 --> 00:12:34,409
So though it says  writelines , It should be more like  write  a  list of strings  that is a more appropriate name for this  function .

141
00:12:34,409 --> 00:12:38,620
It just takes a  list  of  strings  and  writes  it to the  file  pointed to by the  file handle .

142
00:12:42,019 --> 00:12:53,230
And finally, as we said once we are done with the  file , we have to  close  it and make sure that the  buffers  that are associated with the  file , especially if you are writing to a  file  that they are  flushed 

143
00:12:53,230 --> 00:12:57,610
So,  F H dot   close  would  close  the  file  handle   F H .

144
00:12:57,610 --> 00:13:01,509
And all pending  writes  at this point are copied out to the  disk .

145
00:13:01,509 --> 00:13:06,269
It also now means that  F H  is no longer associated with the  file  we are dealing with.

146
00:13:06,269 --> 00:13:11,740
After this if we try to invoke operations on  F H  it is like having a  undefined name  in  python .

147
00:13:14,090 --> 00:13:20,049
Now, sometimes there are situations where we might want to  flush  the  buffer  without closing the  file .

148
00:13:20,049 --> 00:13:28,980
We might just want to make sure that all  writes  up to this point have been actually reflected on the  disk . So, there is a  command   flush  which does this.

149
00:13:28,980 --> 00:13:41,110
So, in case we say  flush  it just say if there are any pending  writes then please put them all onto the  disk  do not wait for the  writes  to accumulate until the  buffer  is full  and then  write  as you normally would to the  disk .

150
00:13:44,480 --> 00:13:50,620
So, here is a typical thing that you would like to do in python which is to process it  line  by  line .

151
00:13:50,690 --> 00:13:59,820
So, the natural way to do this, is to  read  the  lines  into a  list  and then process the  list  using a for.

152
00:13:59,820 --> 00:14:06,549
So, you say contents is  F H dot  readlines  and then  for  each  line  in contents  you do something with it.

153
00:14:08,330 --> 00:14:11,009
You can actually do this in a more compact way.

154
00:14:11,009 --> 00:14:21,220
Do away with the name  contents  and just read directly every  line  returned by the  function   F H dot  readlines , so this is an equivalent formulation of the same  loop .

155
00:14:27,679 --> 00:14:32,159
So, as an example of how to use this  line  by  line  processing.

156
00:14:32,159 --> 00:14:37,600
Let us imagine that we want to copy the contents of a  file input dot txt   to a  file output dot txt .

157
00:14:37,842 --> 00:14:44,734
So, the first thing that we need to do is to make sure that we  open  it correctly. So we should actually

158
00:14:47,783 --> 00:14:53,139
open the  out file  with mode  w and  in file  with mode  r .

159
00:14:53,240 --> 00:14:57,730
This says that I am going to  read  from  in file  and  write  to the  out  file .

160
00:14:57,769 --> 00:15:02,350
Now,  for  each  line  in  return  by  readlines  on the in  file .

161
00:15:02,350 --> 00:15:07,029
Remember that when I get a  line  from  readlines  the  backslash n  is already there.

162
00:15:07,029 --> 00:15:10,769
If I do not do anything to the  backslash n , I can  write  it out exactly the same way.

163
00:15:10,769 --> 00:15:15,149
So, for each  line  that I  read  from the  list   in file  dot  readlines .

164
00:15:15,149 --> 00:15:19,179
I just  write  it to  out  file  and finally, I  close  both the  files .

165
00:15:19,370 --> 00:15:23,379
So, this is one way to copy one  file  from  input  to  output .

166
00:15:28,190 --> 00:15:37,750
Of course, we can do it even in one shot becasue there is a  command  called  writelines  which takes a  list  of strings  and  writes  them in one shot

167
00:15:37,750 --> 00:15:41,460
So, instead of going  line  by  line  through the  list   readlines .

168
00:15:41,460 --> 00:15:46,049
We can take the entire  list   contents  and just  output  it directly through  writelines .

169
00:15:46,049 --> 00:15:52,509
So, this is an alternative way where I have replaced. So this basically replacing the  for .

170
00:15:53,850 --> 00:16:00,490
So, instead of saying for each  line  in in  files , I can just  write  it directly out.

171
00:16:03,740 --> 00:16:08,860
One of the things we have been talking about is this  newline character   which is a bit of a annoyance.

172
00:16:08,860 --> 00:16:16,450
So, if we want to get through a  new line character , remember this is only a  string  and the  new line character  is going to be the last  character  in the  string .

173
00:16:16,450 --> 00:16:22,010
So, one way to get it is just we take a slice of the  string  up to, but not including the last  character .

174
00:16:22,035 --> 00:16:29,190
Now, remember that we when we count backwards minus one is the last  character . So, if we take the slice from zero up to minus one

175
00:16:29,190 --> 00:16:31,926
Then it will correctly exclude the last  character  from the  string .

176
00:16:31,951 --> 00:16:40,960
So,  S  equal to  line  colon minus one will take  line  and  strip  off the last  character  which is typically the  backslash n  that we get when we do  readlines .

177
00:16:42,529 --> 00:16:47,300
Now, in general we may have other spaces.

178
00:16:47,325 --> 00:16:53,129
So remember when we  write  out text very often we cannot see the  spaces  at the end of the  line  because they are invisible to us

179
00:16:53,129 --> 00:16:55,139
So, these are what are called  white space.

180
00:16:55,139 --> 00:17:00,899
So,  spaces tabs ,  new lines  these are  characters  which do not display on the screen especially  spaces  and  tabs .

181
00:17:00,899 --> 00:17:05,289
And if there is the  end of the line  we dont know if line ends with the last  character  we see or there are  spaces  afterwards

182
00:17:05,299 --> 00:17:14,490
So,  R strip  is a  string   command  which actually takes a  string  and removes the trailing  white space  all the  white space  at the end of the  line .

183
00:17:14,490 --> 00:17:19,200
In paritcular if there is only a  backslash n  then it will  strip  the  backslash n , it will also strip other junk

184
00:17:19,200 --> 00:17:22,980
If there is some  spaces  and  tabs  before the  backslash n  and return  that.

185
00:17:22,980 --> 00:17:37,779
So,  s  equal to  line  dot  R Strip that is  strip   line  from the right of  white space , this is an equivalent thing to the previous  line  except is more general because it  strips  all the  white space  not just the last  backslash n  but all the  white space  at the end of the  line .

186
00:17:40,190 --> 00:17:48,343
So, we can also strip from the left using  L strip  or we can  strip  on both sides if we just say  strip  without any characterization of l or r.

187
00:17:48,368 --> 00:17:56,230
So these are  string  manipulation functions and we look at some more of them but this is just a useful one which has come up immediately in the context of  file handling .

188
00:17:56,630 --> 00:18:03,250
So, before we go ahead let us try and look at some examples. of all these things that we have seen so far.

189
00:18:06,859 --> 00:18:15,460
So, here we have created a  file  called  input dot txt  which consists of the  lines   the quick brown fox jumps over the lazy dog 

190
00:18:15,559 --> 00:18:20,818
Now, let us open the  python interpreter  and try to  read lines  from this  file  and  print  it up.

191
00:18:20,843 --> 00:18:28,690
So, we can say for instance that  F  is equal to  open    input dot txt  in  read  mode  .

192
00:18:30,200 --> 00:18:34,900
Now, I have opened the  file  and now I can say for instance.

193
00:18:34,900 --> 00:18:40,809
 for   line  in  F dot  readlines 

194
00:18:47,428 --> 00:18:50,655
 print line 

195
00:18:51,529 --> 00:19:01,240
Now, you will see something interesting happening here. You will see that we have now a blank  line  between every  line  in our  file .

196
00:19:01,240 --> 00:19:04,420
Now, why is there a blank  line  between every  line  in our  file 

197
00:19:04,420 --> 00:19:09,089
That is because when we  readlines  we get a  backslash n character  from the  line  itself.

198
00:19:09,089 --> 00:19:13,599
So,  the quick brown , the first  line  ends with  backslash n ,  fox  ends with  backslash n .

199
00:19:13,599 --> 00:19:17,789
And then over and above that if you remember the  print  statement adds a  backslash n  of its own

200
00:19:17,789 --> 00:19:22,420
So, actually  print  is putting out two blank  lines  for each of these.

201
00:19:22,519 --> 00:19:27,279
Now, let us try and do this again. Supposing I repeat this thing.

202
00:19:27,279 --> 00:19:29,710
And now I do this again.

203
00:19:31,670 --> 00:19:33,519
Now, nothing happens.

204
00:19:33,769 --> 00:19:38,799
Now, the reason nothing happens is because we have this sequential reading of the  file .

205
00:19:38,799 --> 00:19:46,299
So, the first time we did  f dot   readlines , It  read one line  at a time and now we are actually pointing to the end of the  file .

206
00:19:46,299 --> 00:20:03,339
So, if for instance at this point we were to say, say text equal to  F H  dot  read , sorry  F  dot  read , then text will be the empty  string .

207
00:20:03,339 --> 00:20:11,640
So this is an indication that we have actually reached the  end of the  file . Similarly, if we try to say  readline  again  text  will be the empty  string .

208
00:20:13,156 --> 00:20:16,815
Remember we said that if  read  or  readline   returns  the empty  string  then we have reached the end of the  file .

209
00:20:16,840 --> 00:20:21,678
So the only way that we can undo this is to  start  again by  closing  the  file .

210
00:20:21,703 --> 00:20:24,569
So, what we say is  f dot close , so this  closes  the  file .

211
00:20:24,569 --> 00:20:33,390
Now, if we try to do  f dot  read  then we will get an error saying that. This is not been defined, so we do not have  f  with us anymore.

212
00:20:33,390 --> 00:20:38,650
So, we again have to say  f  is  open input dot txt R 

213
00:20:39,890 --> 00:20:47,400
Now, we can say while  for   line  in  f  dot  readlines  for each  line .

214
00:20:47,400 --> 00:20:58,698
Now, we use that trick that we had last time which is to say end equal to empty  string  that says do not insert anything after each  print  statement.

215
00:20:58,723 --> 00:21:08,769
Now if we do this we see that we get back exactly the  input file  as it is without the extra blank  lines  because  print  is no longer creating these extra  lines .

216
00:21:10,490 --> 00:21:15,549
Let us say we want to copy  input dot txt  to a file   output dot txt 

217
00:21:15,829 --> 00:21:31,059
So, we say  f  is equal to  open input dot txt   r  as before and we say  g  is equal to  open output dot txt   w .

218
00:21:32,869 --> 00:21:40,796
And now we say for line in   f  dot read lines   G  dot writeline 

219
00:21:43,490 --> 00:21:52,119
Now notice you get a sequence of numbers. Now why do you get a sequence of numbers that is because each time we  write  something it returns a number of  characters  written.

220
00:21:52,119 --> 00:21:55,769
And it will turn out if you look at the  lines   the quick brown fox  etc.

221
00:21:55,769 --> 00:21:57,720
Now for example, the second  line  is just  Fox .

222
00:21:57,720 --> 00:22:01,769
 fox  has three letters, but if we include the  backslash n , it is wrote four letters.

223
00:22:01,769 --> 00:22:06,569
So, that is why the  quick brown  was fifteen letters plus a  backslash n .

224
00:22:06,569 --> 00:22:13,180
 fox  was three plus a  backslash n . So, this is  line  by  line . Now, if I correctly close these  files.

225
00:22:18,289 --> 00:22:25,480
And then come out of this. Then  output dot txt  is exactly the same as  input dot txt.

226
00:22:28,220 --> 00:22:31,569
So to summarize what we have seen is that

227
00:22:31,569 --> 00:22:43,589
If you want to interact with  files , we do it through  file handles  Which actually correspond to the  buffers  that we use to interact between  memory  and the  file  on the  disk , we can open a  file  in one of three modes  read ,  write  and  append .

228
00:22:43,589 --> 00:22:50,009
We did not actually do an example with  append , but if you do  append  What it will do is it will keep writing beyond where the  file  already existed.

229
00:22:50,009 --> 00:22:53,470
Otherwise  write  will erase the  file  and start with beginning.

230
00:22:53,509 --> 00:22:57,329
So, we saw that we have  read ,  readline  and  readlines .

231
00:22:57,329 --> 00:23:02,259
So, using this we can either  read  the entire  file  in one shot as a  string  or  read  it  line  by  line 

232
00:23:02,259 --> 00:23:06,750
Similarly, we can either  write  a  string  or we can  write  a  list  of  strings  too

233
00:23:06,750 --> 00:23:13,089
So, we have a  write   command  and a  writelines  and  writelines  is more correctly to be interpreted as  write list  of  strings 

234
00:23:13,089 --> 00:23:17,230
Finally, we can close the  handle  when we are done.

235
00:23:17,230 --> 00:23:20,680
And in between we can  flush  the  buffer  by using the  flush   command .

236
00:23:20,720 --> 00:23:25,049
And we also saw that there are some  string  operations to   strip white space .

237
00:23:25,049 --> 00:23:31,269
And this can be useful to remove. these trailing  backslash n  which come whenever you processing text  files .

