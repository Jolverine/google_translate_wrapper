1
00:00:03,589 --> 00:00:06,639
So, we were looking at the eight queens problem

2
00:00:06,639 --> 00:00:13,029
and our solution involved representing the board which squares are under attack and placing the queen one by one.

3
00:00:13,099 --> 00:00:17,010
One feature of this solution is that we have to keep

4
00:00:17,010 --> 00:00:23,469
passing the board through the functions in order to update them or to resize them have to initialize them and so on

5
00:00:23,469 --> 00:00:26,620
because the board had to be updated through each function.

6
00:00:26,620 --> 00:00:31,000
Now, question is can we avoid passing the board around all over the place.

7
00:00:31,640 --> 00:00:36,119
So, can we avoid passing this board explicitly or can we have a single global

8
00:00:36,119 --> 00:00:41,500
copy of the board that all the functions can update. which will save us passing this board back and forth.

9
00:00:43,310 --> 00:00:46,770
So, this brings us to a concept of scope.

10
00:00:46,770 --> 00:00:52,899
So, this is scope of a name. in python is the portion of the code where it is available to read an update.

11
00:00:53,000 --> 00:00:56,770
Now, by default in python scope is local to a function.

12
00:00:56,770 --> 00:00:59,460
we saw that if we use a name inside a function

13
00:00:59,460 --> 00:01:03,325
and that is different from using the same name outside the function.

14
00:01:03,350 --> 00:01:07,480
But actually this happens only when we update the name inside the function.

15
00:01:08,420 --> 00:01:10,959
So let's look at this particular code.

16
00:01:10,969 --> 00:01:13,349
Here we have a function f.

17
00:01:13,349 --> 00:01:17,290
which reads the value x and prints it by storing it in the name y.

18
00:01:17,290 --> 00:01:21,489
Now, the question is what is this x well there is an xhere. Ok

19
00:01:21,489 --> 00:01:27,519
So, will this x inside a function? correctly reflect the x outside the function or not.

20
00:01:32,095 --> 00:01:35,494
So, here we see that function we have written pa among

21
00:01:35,519 --> 00:01:39,892
file f1 dot py which contains exactly that code. So, we have a function f

22
00:01:39,917 --> 00:01:44,048
which reads an x from outside and tries to print it ok and if you run this

23
00:01:47,720 --> 00:01:54,659
then indeed it prints the value seven as we expect. So, y gets the value seven because x has the value seven outside

24
00:01:54,659 --> 00:02:02,200
and that x is inherited except the function from a as inside f.

25
00:02:04,430 --> 00:02:07,829
So, this works now what if we do this?

26
00:02:07,829 --> 00:02:12,370
This is exactly the same function except that after printing the value of y.

27
00:02:12,370 --> 00:02:17,710
It sets x equal to twenty two  inside f. Now, what happens?

28
00:02:19,129 --> 00:02:24,630
Right So, here is f two dot py the code at the middle of the screen.

29
00:02:24,655 --> 00:02:30,219
So, only difference with respect to f one dot py is extra assignment x equal to twenty two inside f.

30
00:02:30,219 --> 00:02:33,189
Now, if we try to run f two dot py.

31
00:02:33,214 --> 00:02:39,000
ok then it gives us an error saying that the original assignment y equal to x

32
00:02:39,000 --> 00:02:44,949
gives us an unbound local name, there is no x which is available at this point inside.

33
00:02:44,949 --> 00:02:52,439
So, somehow assigning x equal to twenty two inside f change the status of x it is no longer willing to look up the outside x.

34
00:02:52,439 --> 00:02:54,969
It insist that there is an inside x.

35
00:02:59,060 --> 00:03:04,229
ok So, this gives us an error. So, x is not found in f.

36
00:03:04,229 --> 00:03:08,289
python is willing to look at the enclosing function for a global x.

37
00:03:08,289 --> 00:03:14,500
However, if x is updated in f then it becomes a local name and then it gives an error.

38
00:03:16,460 --> 00:03:21,460
So, strictly speaking this applies only to immutable values.

39
00:03:21,460 --> 00:03:29,250
So, if we change this function as follows, we made x not a, a integer, but an list for example.

40
00:03:29,250 --> 00:03:32,129
and we ask why to pick up the zero th element in the list.

41
00:03:32,129 --> 00:03:36,520
and then later in f we change the zero th element of x to twenty two.

42
00:03:39,650 --> 00:03:44,439
So, here we have this function in which we now tent x from an integer to a list.

43
00:03:44,439 --> 00:03:49,539
and then we try to assign it in y but we update thatlist inside the function. and then.

44
00:03:50,180 --> 00:03:54,939
If we run it, then it does print the value seven as we expect.

45
00:03:56,419 --> 00:04:01,500
So, this works. So, we have an immutable we have a immutable value.

46
00:04:01,500 --> 00:04:06,689
I mean we have a mutable value sorry, then we can actually change it inside f and nothing will happen.

47
00:04:06,689 --> 00:04:10,870
So, global names at point to mutable values can be updated within a function.

48
00:04:10,870 --> 00:04:14,310
In fact, this means therefore, that the problem that we started out to solve

49
00:04:14,310 --> 00:04:20,459
namely how to avoid passing the board around which inside eight queens actually requires no further explanation.

50
00:04:20,459 --> 00:04:23,279
Since board is a dictionary it is a mutable value.

51
00:04:23,279 --> 00:04:27,660
And in fact, we can write eight queens in such a way that we just ignore

52
00:04:27,685 --> 00:04:32,075
passing the board around we change all the definitions so that board doesn't occur and works fine.

53
00:04:35,509 --> 00:04:40,424
Here we have rewritten the previous code just removing board from all the functions.

54
00:04:40,449 --> 00:04:45,689
So, initialize earlier to board and n now it just takes n print board does not take an argument at all.

55
00:04:45,689 --> 00:04:51,324
And all of them are just referring to this global value board which you can see everywhere right.

56
00:04:51,349 --> 00:04:54,310
So, we have this global value board here

57
00:04:54,500 --> 00:05:00,481
which is being referred to inside the functionand it is doesn't matter that is not being passed because this is a mutable value.

58
00:05:00,506 --> 00:05:06,250
So, It is going to look for the value which is defined outside namely this empty dictionary

59
00:05:06,250 --> 00:05:11,430
and then all these functions like place, queen, or undo queen or add queen just take

60
00:05:11,430 --> 00:05:15,639
the relevant parameters and implicitly refer to the global value of the code.

61
00:05:15,980 --> 00:05:19,300
So, if we run this now this global version.

62
00:05:19,300 --> 00:05:24,149
we get exactly the same output. So, as we said for our purpose which is to fix that

63
00:05:24,149 --> 00:05:26,939
eight queens problem without having to pass the board around.

64
00:05:26,939 --> 00:05:34,209
The fact that python implicitly treats mutable global names has updateable within a function is all that we need.

65
00:05:38,930 --> 00:05:41,920
But, what we actually want a global integer?

66
00:05:42,054 --> 00:05:46,474
Ok why would we want a global integer? Well, suppose for instance we want to count the number of

67
00:05:46,499 --> 00:05:49,529
times a function is called. So, every time a function is called

68
00:05:49,529 --> 00:05:53,399
we would like to update an integer inside this function, but that function

69
00:05:53,399 --> 00:05:57,029
that integer cannot be a local name to the function because that local value will be

70
00:05:57,029 --> 00:05:59,519
destroyed in the function ends, we wanted to persist.

71
00:05:59,519 --> 00:06:02,040
So, it must be a value which exists outside the function.

72
00:06:02,040 --> 00:06:07,019
But an integer it is an immutable value and therefore, if we try to update it inside the function

73
00:06:07,019 --> 00:06:10,839
it will treat it as a local value. So, how do we get around this? ok

74
00:06:10,839 --> 00:06:13,560
So, python has a declaration called global.

75
00:06:13,560 --> 00:06:17,949
it says a name is to be referred to globally and not taken as a local value.

76
00:06:17,949 --> 00:06:23,937
So, if we change our earlier definition of f so that we add this particular Tag global x

77
00:06:23,962 --> 00:06:28,899
then it says that this x and this x both referred to the same x outside.

78
00:06:29,120 --> 00:06:32,790
ok So, this is a way of telling python don't confuse

79
00:06:32,790 --> 00:06:36,959
is x  equal to twenty twenty two with creation of a new local name x.

80
00:06:36,959 --> 00:06:42,634
All x is referred to in f are actually the same as the x outside and to be treated as global values.

81
00:06:42,659 --> 00:06:48,670
So, this is one way in which We can make an immutable value accessible globally within a python program.

82
00:06:50,750 --> 00:06:54,839
So, here is that global code, so we have global x.

83
00:06:54,839 --> 00:07:00,060
And just to make sure that the x equal to twenty two inside is actually affecting the x outside.

84
00:07:00,060 --> 00:07:05,819
We have a print x now after the call to f right, so the bottom of the main program we have print x.

85
00:07:05,819 --> 00:07:10,170
Now x was seven before f was called, but x got set to twenty two inside f.

86
00:07:10,170 --> 00:07:12,990
So, we will expect the second print statement to give us twenty two.

87
00:07:12,990 --> 00:07:20,579
So, this statement should first print seven, right it should print a seven from the print y and then print twenty two from this print x.

88
00:07:20,579 --> 00:07:23,379
So, if we run this indeed this is what we see.

89
00:07:23,420 --> 00:07:30,579
Right We have two lines, the first seven comes from print y and the second level twenty two comes from print x outside.

90
00:07:35,389 --> 00:07:38,589
So While we are on the topic of local scope,

91
00:07:38,589 --> 00:07:41,910
python allows us to define functions within functions.

92
00:07:41,910 --> 00:07:46,110
So, here for instance the function f has defined functions g and h.

93
00:07:46,110 --> 00:07:49,816
g of a returns a plus one, h of b returns two times b.

94
00:07:49,896 --> 00:07:52,870
And now we can update x for instance

95
00:07:52,870 --> 00:07:58,131
y for instance by calling g of x plus h of x rather than just setting it to the value x.

96
00:07:58,156 --> 00:08:00,420
Now, the point to note in this is that

97
00:08:00,420 --> 00:08:04,379
these functions g and h are only visible to f. So, they are defined

98
00:08:04,379 --> 00:08:08,139
within the scope of f so they are inside f right

99
00:08:08,139 --> 00:08:12,660
So, there inside f and hence, they are not visible outside.

100
00:08:12,660 --> 00:08:15,870
So, from outside if I go if I ask g of x.

101
00:08:15,870 --> 00:08:19,420
at this point this will be an error because it will say there is no such g defined.

102
00:08:19,420 --> 00:08:24,420
So, if we can define so this is useful because now we can define local functions which we might want

103
00:08:24,420 --> 00:08:27,329
to performs one specialized task which is a relevance to f

104
00:08:27,329 --> 00:08:32,620
but it should not be a function which is exposed to everybody else and this is possible here.

105
00:08:35,480 --> 00:08:41,129
Now, of course, the same rules apply. So, if we look up x inside g or h.

106
00:08:41,129 --> 00:08:43,889
So, if you look up an x here, we will first try to look up an f.

107
00:08:43,889 --> 00:08:46,075
If it is not there in f it will go outside and so on.

108
00:08:46,100 --> 00:08:51,600
Ok so either we can declare it global in which case we can update it within g or h

109
00:08:51,600 --> 00:08:57,090
or it will use the same rule as before if we don't update immutable value it will look outside

110
00:08:57,090 --> 00:09:00,370
and if it is a mutable value it will allow us to update it from insert.

111
00:09:01,370 --> 00:09:08,549
Now, there are some further refinements. So, there python has anintermediate scope call non-local which says

112
00:09:08,549 --> 00:09:12,940
within g and h refer to the value inside f but not to the value outside f.

113
00:09:12,940 --> 00:09:16,379
So, this is a technicality and it will not be very relevant.

114
00:09:16,379 --> 00:09:22,169
we needed we will come back to it, but for the moment if you want to find out more about non-local declarations.

115
00:09:22,169 --> 00:09:25,649
please see the python documentation but global is the important one.

116
00:09:25,649 --> 00:09:34,980
The global allows us to transfer an immutable value from outside into the function and make it updateable within a functionto summarize.

117
00:09:34,980 --> 00:09:39,220
What we have seen is that python names are looked up inside out from within functions

118
00:09:39,220 --> 00:09:42,330
If we update an immutable value it creates a local copy.

119
00:09:42,330 --> 00:09:46,351
So, we need to have a global definition to update immutable values.

120
00:09:46,376 --> 00:09:50,580
On the other hand, if we have mutable values like lists and dictionaries there is no problem.

121
00:09:50,580 --> 00:09:54,480
within a function we can implicitly refer to the global one and update it.

122
00:09:54,480 --> 00:09:59,126
And this we saw in our eight queens solution we can make the board into a global value

123
00:09:59,151 --> 00:10:04,632
and just keep updating it within each function rather than passing it around explicitly as an argument.

124
00:10:04,659 --> 00:10:10,289
And finally, what we have seen is that we can nest functions,so we can create so called helper functions within functions.

125
00:10:10,289 --> 00:10:13,590
that are hidden to the outside, but can be used inside the function.

126
00:10:13,590 --> 00:10:17,080
the logically break up its activities into smaller units. S

