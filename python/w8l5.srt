1
00:00:03,350 --> 00:00:07,950
So we have come to the last lecture of this course.

2
00:00:07,950 --> 00:00:11,919
We shall look at more features of Python some of which haven't been described in the past lectures.

3
00:00:11,919 --> 00:00:15,750
Let us take some time instead to reflect about Python as a language.

4
00:00:15,750 --> 00:00:20,620
And compare it to some of the other languages which exist though you may not be familiar with them.

5
00:00:20,620 --> 00:00:27,460
i would like to highlight some aspects of Python which are different from other languages and also argue whether they are better or worse.

6
00:00:27,460 --> 00:00:32,340
So why did we choose Python to do this course?

7
00:00:32,340 --> 00:00:36,340
Python is a very good language to start programming for many reasons.

8
00:00:36,340 --> 00:00:40,020
One reason is that its syntax is very simple.

9
00:00:40,020 --> 00:00:49,000
Part of the simplicity comes in the fact. that you do not have to declare names in advance. So you don't have to start doing a lot of things before you write your code.

10
00:00:49,000 --> 00:00:56,280
So you can jump into the Python interpreter for example and keep writing statements without worrying about what x is and what y is.

11
00:00:56,280 --> 00:01:06,549
So that makes it very convenient and very nice to start programming because you do not have to learn a lot of things to write even basic programs.

12
00:01:08,000 --> 00:01:11,140
Python programs are well laid out.

13
00:01:11,140 --> 00:01:15,760
The indentation indicates when a loop begins and when an if block begins.

14
00:01:15,760 --> 00:01:24,180
We do not have to worry about a lot of punctuation such as Braces, brackets and semicolons which are a part of most conventional languages.

15
00:01:24,180 --> 00:01:30,549
So once again this makes a language a little easier to learn and a little less formidable for a new comer to look at.

16
00:01:31,909 --> 00:01:42,900
Also with respect to variables we do not have to worry about storage beyond understanding what is mutable and immutable.

17
00:01:42,900 --> 00:01:46,290
If we need to use a name x we use it if we need a list l we use it.

18
00:01:46,290 --> 00:01:48,790
if we need to add something to l we just say append.

19
00:01:48,790 --> 00:01:53,170
We never bother about where the space is coming from or where it is going.

20
00:01:55,189 --> 00:01:57,870
So these are all the plus points of python.

21
00:01:57,870 --> 00:02:02,079
So what are the minus points? What are the things that are bad about Python ?

22
00:02:05,689 --> 00:02:08,909
So the first thing is to do with is the lack of declarations.

23
00:02:08,909 --> 00:02:18,099
So the lack of declarations often a very good thing because you do not have to worry about writing a lot of stuff before you start programming.

24
00:02:18,099 --> 00:02:27,189
But it is also a bad thing because many programming errors come from simple typing mistakes. Very often you mean to write x and you write y.

25
00:02:27,189 --> 00:02:33,400
Now in Python if you write a y and you assign it a value somewhere where you meant an x Python will not know.

26
00:02:33,400 --> 00:02:40,553
It will not know that you are supposed to use x and not y because every new name that comes along is just added to the family of names which our program manipulates.

27
00:02:40,969 --> 00:02:44,560
So these kind of typos can be very hard to find.

28
00:02:44,560 --> 00:02:54,310
And because you have this kind of dynamic name introduction with no declarations Python makes it very difficult for you as a programmer to spot these errors.

29
00:02:54,310 --> 00:02:57,569
On the other hand if you declare these names in advance.

30
00:02:57,569 --> 00:03:01,090
This happens in other languages like C or C plus plus or Java.

31
00:03:01,090 --> 00:03:08,199
Then if you use a name which you have not declared then the compiler will tell you that this name has not been seen before and therefore something is wrong.

32
00:03:08,270 --> 00:03:15,069
So a mistyped name can be easily caught as an undeclared name if you have declarations.

33
00:03:15,069 --> 00:03:22,979
Whereas in Python it will just happily go ahead and create a new value for that name and pretend that there are two names now where you think there is only one.

34
00:03:22,979 --> 00:03:26,080
This creates all sorts of unpredictable errors in your code.

35
00:03:26,930 --> 00:03:29,979
The other side of this is typing.

36
00:03:29,979 --> 00:03:34,080
So in Python we have seen that names do not have types.

37
00:03:34,080 --> 00:03:36,490
They only inherit the type from the value that they have.

38
00:03:36,650 --> 00:03:39,930
So you could say at some point x equal to 5.

39
00:03:39,930 --> 00:03:50,710
Later on if you assign x to a string and then assign x to a list Python will not complain. At each point given the current value of x legal operations are defined as per that value.

40
00:03:50,780 --> 00:03:54,750
Now this is again nice and convenient but it can also lead to errors.

41
00:03:54,750 --> 00:04:04,150
For the same reason you might be thinking of x as an integer but somewhere half way through your program you forgot that it is an integer and start assigning at some different type of value.

42
00:04:04,150 --> 00:04:10,240
Now if you had announced to Python that x must always be an integer then this name must only store an integer value.

43
00:04:10,240 --> 00:04:13,030
Presumably as a compiler it could catch it and tell you.

44
00:04:13,099 --> 00:04:21,250
So a lot of errors are either typos and variable names or misguided usage of names from one type to another.

45
00:04:21,250 --> 00:04:26,649
Both of these can be caught. very easily by compilers if you have declarations of names.

46
00:04:26,649 --> 00:04:34,089
Both of these get uncaught or they are left uncaught by Python and it allows you to propagate errors and these errors can be very difficult to find

47
00:04:34,100 --> 00:04:40,689
So there is a downside of having this flexibility about adding names and and changing their types values as you go along.

48
00:04:40,689 --> 00:04:43,709
It is that debugging large programs requires a lot more care.

49
00:04:43,709 --> 00:04:48,519
Writing large programs requires care and debugging is very difficult.

50
00:04:50,540 --> 00:05:00,490
The other part has to do with the discussion that we had towards the second half of the course about user defined data types in terms of classes and objects.

51
00:05:00,860 --> 00:05:09,550
So the first thing that is a direct consequence of not having declarations is that we cannot assert that a name has a type.

52
00:05:09,649 --> 00:05:13,170
In particular we saw that if we want to use something as a list.

53
00:05:13,170 --> 00:05:19,319
We have to first declare it to be an empty list. So we have to write something like l is equal to this.

54
00:05:19,319 --> 00:05:27,240
That will ensure that henceforth i can append to it. So the first operation on the list that i do will have to be legal.

55
00:05:27,240 --> 00:05:31,345
This is more or less like a declaration except it is not quite a declarations.

56
00:05:31,370 --> 00:05:39,069
i am actually creating an empty object of that type. In the same way if i want a dictionary i have to give it a completely new name like this.

57
00:05:39,259 --> 00:05:49,269
So i have to say d is equal to an empty dictionary. There is no way to assign a type to a name without creating an object of that type

58
00:05:49,790 --> 00:05:55,500
So this is actually a problem with the kind of user defined types that we have.

59
00:05:55,500 --> 00:06:02,560
It is very convenient to be able to define an empty tree without having to create a tree node.

60
00:06:02,720 --> 00:06:10,860
So for instance if you had type declarations you can say that the name t is of type tree and then you can use this value like none.

61
00:06:10,860 --> 00:06:15,250
All programming languages have such a value which denotes something that does not exist

62
00:06:15,250 --> 00:06:23,563
So you can say that there is not one none but many nones. Here this none is none of type tree. There is only one value of none

63
00:06:23,588 --> 00:06:28,439
And you can use none for anything but when it has none it has no type.

64
00:06:28,439 --> 00:06:32,379
So if Python the name has the value none it has by definition no type.

65
00:06:32,379 --> 00:06:37,629
Whereas if you had declarations you can say that this name has a type it just does not happen to have a value.

66
00:06:37,629 --> 00:06:41,860
This is typically what you want for an empty node or an empty tree.

67
00:06:41,930 --> 00:06:49,809
We had a very cumbersome way of dealing with this in order to make recursive tree exploration work better.

68
00:06:49,834 --> 00:06:59,470
We added a layer of empty nodes at the frontier and extended our tree by one layer just so that we could easily detect when we reach the end of a path.

69
00:06:59,629 --> 00:07:05,189
Now this can be avoided if you have type declarations.

70
00:07:05,189 --> 00:07:07,330
So this is a problem.

71
00:07:07,355 --> 00:07:11,800
The lack of declarations makes things a little bit more complicated in python.

72
00:07:11,800 --> 00:07:15,040
One does not normally come across this in the beginning stages of programming.

73
00:07:16,850 --> 00:07:19,290
The other thing is much more serious.

74
00:07:19,290 --> 00:07:23,730
This is more to do with convenience and representation of empty objects.

75
00:07:23,730 --> 00:07:30,819
Without declarations you really cannot implement the the kind of separation of public and private that you want in an object.

76
00:07:30,819 --> 00:07:40,600
Remember our goal in an abstract type data type was to have a public interface or a set of functions which are legal for a data type while having a private implementation.

77
00:07:40,600 --> 00:07:44,160
For instance we would have a stack we might implement it as a list.

78
00:07:44,160 --> 00:07:47,970
But we would only like pop and push. Same way a queue may be also a list.

79
00:07:47,970 --> 00:07:51,540
But we only want to add and remove at the opposite ends.

80
00:07:51,540 --> 00:07:54,765
And we do not want to confuse this with the list operations.

81
00:07:54,790 --> 00:07:57,339
we do not want things like append and extend to be used indiscriminately.

82
00:07:58,610 --> 00:08:02,920
The other part of it is that we do not want the data itself to be accessible.

83
00:08:02,945 --> 00:08:09,670
We do not want to say that given a point pi can use p dot x and p dot y from outside and directly update the values.

84
00:08:09,670 --> 00:08:15,750
Because this is sensitive to the fact that tomorrow i might change from x and y representation to r and theta.

85
00:08:15,750 --> 00:08:21,399
We said that we might have situations where we might prefer to represent our point using r and theta.

86
00:08:21,410 --> 00:08:31,740
Now we don't want the programmer to start using dot when we are using x and y and manipulate p dot x and p dot y directly outside the code.

87
00:08:31,740 --> 00:08:42,370
If it is inside the class code then as maintainers of the class we would make sure that wherever we used to use p dot x and p dot y we now use p dot r and p dot theta.

88
00:08:42,370 --> 00:08:51,059
So it is an internal thing we change from x y to r theta and then we update all the code within the class to be in terms of r theta¸ and not x y.

89
00:08:51,059 --> 00:08:56,259
But if somebody outside is using x and y and these values no longer exist.

90
00:08:56,259 --> 00:09:00,429
Then what happens is that for the outside person their code stops working.

91
00:09:00,429 --> 00:09:04,720
This is because we have changed an internal implementation of dot.

92
00:09:04,759 --> 00:09:09,480
And this is a very dangerous situation which happens quite often.

93
00:09:09,480 --> 00:09:15,610
So it is important to separate public from private if they do not have any access to the private implementation of the dot.

94
00:09:15,610 --> 00:09:19,570
Then they cannot use p dot x and p dot y outside.

95
00:09:19,789 --> 00:09:24,789
So just to reiterate supposing we have a stack and we may implement it as a list

96
00:09:24,789 --> 00:09:27,970
We only allow public methods push and pop.

97
00:09:27,970 --> 00:09:34,779
A person who is exploiting this fact that it is a list. could directly add something to the end and violate the stack property.

98
00:09:34,789 --> 00:09:38,309
So we could get into such situations.

99
00:09:38,309 --> 00:09:48,279
The data structure is compromised because the person is using operations which are legal for the private implementation but illegal for the abstract data type which we are trying to present to the user.

100
00:09:50,029 --> 00:09:59,200
So for this the only way we can get around this is to actually have some way of saying that these names are private and these names are public.

101
00:09:59,200 --> 00:10:02,940
Now it is not that Python does not have declarations.

102
00:10:02,940 --> 00:10:10,509
Python has a global declaration which allows you to take an immutable value inside a function and refer to the same immutable value outside.

103
00:10:10,509 --> 00:10:18,659
There are situations in which Python allows declarations but there are many other places where it could allow declarations and make things more usable.

104
00:10:18,659 --> 00:10:26,370
But it does not and this is one example. So in languages like Java you will find a lot of declarations saying private and public.

105
00:10:26,370 --> 00:10:33,129
This looks very bureaucratic but it is really required in order to separate out the implementation from the interface.

106
00:10:34,370 --> 00:10:38,470
So actually in an ideal world the implementation must be completely private.

107
00:10:38,470 --> 00:10:41,939
So you should never be able to look at x and y directly.

108
00:10:41,964 --> 00:10:46,690
If you want x there should be a function called get x which gives you the x value.

109
00:10:46,690 --> 00:10:49,590
A function called set x we sets the x value.

110
00:10:49,590 --> 00:10:54,460
Now this may look superficially the same as saying p dot x and p dot x equal to v.

111
00:10:54,460 --> 00:10:58,679
But the difference is that we do not know actually this an x x is an abstract concept for us.

112
00:10:58,679 --> 00:11:06,269
So x coordinate is just a property of the point which we can set now how we set it is through this function

113
00:11:06,269 --> 00:11:16,809
So if inside the function we start setting r theta as the representation then changing the x value will correspondingly change r and theta indirectly.

114
00:11:16,809 --> 00:11:22,240
So when we say get x it does not actually read the x value it gets r cos theta.

115
00:11:22,240 --> 00:11:27,610
When we say set x it will change the r to account for the a new x and recompute the theta.

116
00:11:27,799 --> 00:11:35,039
So in this way if we have only these functions to access the thing then we have only conceptual values inside.

117
00:11:35,039 --> 00:11:38,220
These conceptual values are manipulated through these functions.

118
00:11:38,220 --> 00:11:42,190
We do not know the actual representation. This is the ideal world where everything is hidden.

119
00:11:42,190 --> 00:11:48,100
We only have these functions but this is cumbersome because for every part of the data type we have to use these functions.

120
00:11:48,100 --> 00:12:00,240
Partly the reason to have this private public declaration is that we programmers are not happy with having to always invoke a functions.

121
00:12:00,240 --> 00:12:02,726
So they would like some parts of the data type to be public.

122
00:12:02,751 --> 00:12:05,409
So they can say p dot x equal to 7.

123
00:12:05,659 --> 00:12:11,279
So this is the style that one would ideally advocate for object oriented programming.

124
00:12:11,279 --> 00:12:16,980
Make all the internal names and variables private and only allow restricted access.

125
00:12:16,980 --> 00:12:24,413
So that they are used in an appropriate way and the use does not get compromised if the internal representation changes.

126
00:12:24,438 --> 00:12:31,539
If we move x y to r theta get x and set x will still work whereas p dot x may not be meaningful there.

127
00:12:33,769 --> 00:12:41,203
Another reason to have this style of accessing values is sometimes you do not want individual values to be actually accessed individually.

128
00:12:41,228 --> 00:12:46,289
For instance a date is typically a three component field.

129
00:12:46,289 --> 00:12:48,399
It has the day month and year.

130
00:12:48,649 --> 00:12:56,019
These have their own ranges the valid days for a month range from 1 to 31 and the months range from 1 to 12.

131
00:12:56,019 --> 00:12:58,600
But not every month has 31 days.

132
00:12:59,149 --> 00:13:04,440
So the valid combination for a day and month depends on both these quantities.

133
00:13:04,440 --> 00:13:10,720
In fact it depends on the year because we cannot have twenty nineth of February unless it is a leap year.

134
00:13:10,789 --> 00:13:15,725
So if we update day or month we have to be careful that we are updating it legally.

135
00:13:15,750 --> 00:13:22,000
We can start with the month February with a legal date like 15 and then change the date from 15 to 31.

136
00:13:22,000 --> 00:13:25,120
We then end up with the illegal date which is thrity first of February.

137
00:13:25,490 --> 00:13:35,139
So what we need actually is to have a composite update operation which sets the date by providing all three values rather than supplying three separate operations.

138
00:13:35,139 --> 00:13:48,580
So even if you have these functions you do not always want to give these functions individually because there might be some constraints between the values which have to be preserved.

139
00:13:48,580 --> 00:13:51,090
You can preserve those by controlling access to them.

140
00:13:51,090 --> 00:13:55,090
So you can say you cannot set the day separately and the month separately you must set them together.

141
00:13:55,730 --> 00:14:01,690
So this is one other reason why it is good to keep all the implementation private.

142
00:14:01,690 --> 00:14:06,159
Do not allow direct update and then control the way in which you update the values.

143
00:14:09,620 --> 00:14:19,090
Now let us come to storage allocation. How the names and values we use in our program are actually allocated and stored in memory so that we can look them up.

144
00:14:19,549 --> 00:14:25,513
Because in Python we use names on the fly as we keep coming up with names and the values keep changing.

145
00:14:25,538 --> 00:14:30,029
Python cannot decide in advance that it needs a space for x for one integer.

146
00:14:30,029 --> 00:14:34,179
This is because later this x might be a list and it does not even know which names are coming.

147
00:14:34,179 --> 00:14:39,079
So Python has to allocate space always in a dynamic manner.

148
00:14:39,104 --> 00:14:42,639
As you use a name it has to find space for it.

149
00:14:42,639 --> 00:14:46,570
The space requirement may change if the value changes its type.

150
00:14:47,389 --> 00:14:50,440
On the other hand let us discuss static declaration.

151
00:14:50,480 --> 00:15:02,289
If i say that i is an integer and j is an integer and k is an integer then the compiler can directly declare in advance some space in the memory to be reserved for i j and k.

152
00:15:02,570 --> 00:15:05,289
Now this is particularly useful for arrays.

153
00:15:05,289 --> 00:15:09,000
We mentioned in an earlier lecture the difference between arrays and lists.

154
00:15:09,000 --> 00:15:20,080
So in a statically declared situation an array of size 100 will actually be allocated as a block of 100 contiguous values without gaps.

155
00:15:20,080 --> 00:15:25,740
This means that i can get to any entry in the array by knowing the first value and then how many values to skip.

156
00:15:25,740 --> 00:15:34,929
So if i want the seventy fifth value i can go to the first value and calculate where the seventy fifth value will be if i just jump over that many values and get there directly.

157
00:15:34,929 --> 00:15:37,809
So this gives us what is called a random access array.

158
00:15:37,809 --> 00:15:41,139
It does not take any more time to get to the first element and the last element.

159
00:15:41,139 --> 00:15:51,940
In a list as we saw even in our object based implementation that to get to the i th element we have to start with the head and go to the first second third so it takes time proportional to the position.

160
00:15:51,940 --> 00:16:00,399
So if we have static allocation we can also exploit the fact that we can get the truly random access arrays which Python actually does not have.

161
00:16:00,559 --> 00:16:05,440
Now just because we have declaration does not mean everything is declared statically.

162
00:16:05,440 --> 00:16:13,029
Even in languages like Java and C++ we would do this object oriented style where we have a template for a class.

163
00:16:13,029 --> 00:16:19,389
Then we create object of this class dynamically as a tree or list grows and shrinks will create more objects or remove them.

164
00:16:19,389 --> 00:16:25,059
So there is a dynamic part also but it is exclusively for this kind of user defined data types.

165
00:16:25,059 --> 00:16:29,340
The static part takes care of all the standard data types which we use for counting

166
00:16:29,340 --> 00:16:38,710
If you know an advance that you need a block of data of a particular size arrays are much more efficient than this.

167
00:16:40,789 --> 00:16:44,159
So one part of storage allocation is getting it.

168
00:16:44,159 --> 00:16:46,960
The other part is deallocation or giving it up.

169
00:16:46,960 --> 00:16:52,029
So we saw that in Python you can remove an element from the list by saying del l zero.

170
00:16:52,429 --> 00:16:56,820
In general you can unallocate any name's value by saying del x.

171
00:16:56,820 --> 00:16:59,830
Does this give up the storage?

172
00:16:59,830 --> 00:17:07,960
When we say del x we are saying that the space that x is currently using for its value is no longer needed. So what happens to that space? Who takes care of it?

173
00:17:08,599 --> 00:17:13,840
Similarly when we had a list if you remember when we wanted to delete a list if we had a sequence of things.

174
00:17:13,865 --> 00:17:17,593
We want to delete a list we did not actually delete it we just bypassed it.

175
00:17:17,618 --> 00:17:22,920
We said that first element points to the third. Now logically this thing is no longer part of my list.

176
00:17:22,920 --> 00:17:28,180
But where has it gone has it gone anywhere or it is still sitting there in my memory blocking space.

177
00:17:29,509 --> 00:17:38,680
So what happens to the dead values which we have unset by using del or we have bypassed by manipulating the dot links inside an object and so on.

178
00:17:41,000 --> 00:17:45,210
It turns out that languages like Python and also other languages like Java

179
00:17:45,210 --> 00:17:49,599
There is nothing to do with declaring variables it has to do with how space is allocated

180
00:17:50,059 --> 00:17:53,099
They use something called garbage collection.

181
00:17:53,099 --> 00:18:03,464
So garbage in the programming language sense is memory which has been allocated once but it is not been used anymore and therefore is not useful anymore.

182
00:18:03,489 --> 00:18:05,142
We cannot reach it in some sense.

183
00:18:05,167 --> 00:18:11,490
It is like that list example there was an element in our list which we could reach from the head and at some point we deleted it.

184
00:18:11,490 --> 00:18:18,130
Now we can no longer reach that value nor can we reuse it because it is being declared to be allocated so it is garbage.

185
00:18:18,200 --> 00:18:21,190
So roughly speaking how does garbage collection work?

186
00:18:21,190 --> 00:18:25,000
Well what you do is you imagine that you have your memory.

187
00:18:26,329 --> 00:18:33,759
And then you have some names n m x l and so on in your program.

188
00:18:33,759 --> 00:18:38,732
You know where these things point this is somewhere here this is somewhere here this is somewhere here.

189
00:18:39,619 --> 00:18:46,509
So you start with the names that you have and you go on you mark this thing you say this is mine this is mine this is mine and this is mine.

190
00:18:46,579 --> 00:18:55,599
Now this could be a list l could be a list so it could be that this in turn points to the next element in the list so it goes here. So then you point to this and say this is also mine.

191
00:18:55,599 --> 00:19:05,079
So it is what the names point to plus what their pointed values point to. You keep following this until you mark all the memory that you can actually reach from your name.

192
00:19:05,079 --> 00:19:09,309
So the names in your program and all the memory that they can indirectly reach.

193
00:19:09,309 --> 00:19:18,339
Now i can go through and say that everything which has not been marked. is not in use. So i can go and explicitly free it.

194
00:19:18,339 --> 00:19:20,250
So this is the second phase.

195
00:19:20,250 --> 00:19:24,309
So you collect all the unmarked memory locations and make them free and proceed.

196
00:19:24,619 --> 00:19:28,380
This is a process which runs in parallel with your program.

197
00:19:28,380 --> 00:19:37,000
At some point logically speaking you have to stop your program and mark the memory release space and then resume your program. So there is an overhead.

198
00:19:37,460 --> 00:19:41,460
So some languages like C do not have this garbage collection built in.

199
00:19:41,460 --> 00:19:44,712
So in C if you need dynamic memory remember that.

200
00:19:44,737 --> 00:19:52,480
All the things that you declare in advance are allocated statically you cannot say henceforth i do not need them.

201
00:19:52,480 --> 00:20:01,119
But if you have something like a list or a tree where you are growing and shrinking you will ask for a memory saying i want memory worth one node.

202
00:20:01,579 --> 00:20:03,240
Then you will populate it.

203
00:20:03,240 --> 00:20:11,410
When you delete something it is your responsibility as a programmer to say this was given to me sometime back please take it back.

204
00:20:11,410 --> 00:20:14,309
As a programmer you ask for dynamic memory.

205
00:20:14,309 --> 00:20:22,569
More importantly when you are no longer using it return it back. So C has this kind of programmer driven manual memory allocation.

206
00:20:23,900 --> 00:20:33,039
So this is quite error prone as you can imagine there might be just a simple case where a programmer writes a delete operation in a tree or a list and forgets to give the memory back.

207
00:20:33,039 --> 00:20:40,960
So this means that every time something is added and removed from a list or a tree in such a program that thing will be residing in memory.

208
00:20:41,059 --> 00:20:50,475
Leaving it to be used when it is actually not used. So over at a period of time this thing will keep filling up.

209
00:20:50,500 --> 00:20:54,549
Think of the free memory as a fluid then the free memory is shrinking.

210
00:20:54,549 --> 00:20:56,430
So this is called a memory leak.

211
00:20:56,430 --> 00:21:01,029
The memory is leaking out of your system it is not really leaking out it is getting filled up.

212
00:21:01,029 --> 00:21:04,650
But this is the terminology.

213
00:21:04,650 --> 00:21:17,309
The word memory leak is just referring to the fact that memory that has been allocated to a program is not being properly deallocated when it is no longer in use.

214
00:21:17,309 --> 00:21:24,595
So as the program runs longer and longer. it will start taking up more and more memory and making the whole program very much more sluggish.

215
00:21:24,620 --> 00:21:29,319
So the performance will suffer over time as the space shrinks.

216
00:21:30,019 --> 00:21:39,970
So virtually speaking all modern languages use the garbage collection because it is so much simpler though there is a runtime overhead with this mark and release kind of mechanism.

217
00:21:39,970 --> 00:21:50,380
The advantages that you get from not having to worry about it and not trusting the programmer is that you avoid the memory leaks by making sure that your garbage collection works.

218
00:21:50,405 --> 00:21:56,079
Rather than relying on the good sense of the programmer to make sure that. all the memory allocated is actually deallocated.

219
00:21:59,089 --> 00:22:07,599
So for the final point of this last lecture let us look at a completely different style of programming called functional programming.

220
00:22:07,789 --> 00:22:13,310
Python and the other languages we have talked about are what are called imperative programming languages.

221
00:22:13,335 --> 00:22:18,359
So in an imperative programming language you actually give a step by step process to compute things.

222
00:22:18,359 --> 00:22:26,640
So you assign names to keep track of intermediate values you put things in lists and then you have to basically do the mechanical computation.

223
00:22:26,640 --> 00:22:29,700
You have to simulate it more or less through a sequence of instructions.

224
00:22:29,700 --> 00:22:34,869
So you have to know a mechanical process for computing something more or less before you can implement it.

225
00:22:35,269 --> 00:22:42,359
functional programming is something which tries to take the kind of inductive definitions that we have seen directly at heart.

226
00:22:42,359 --> 00:22:45,759
It just uses these as the basis for computation.

227
00:22:45,865 --> 00:22:50,759
So you will directly specify inductive functions in a declarative way.

228
00:22:50,759 --> 00:22:56,939
So here is a typical declaration. So for example this is a Haskell style declaration.

229
00:22:56,964 --> 00:23:05,559
The first line with this double colon says that factorial is a function and this is its type it takes an integer as input and produces an integer as output.

230
00:23:05,559 --> 00:23:10,363
So this is saying that factorial is a box that looks like this it takes integer and produces integer.

231
00:23:10,388 --> 00:23:13,210
So this is the thing that you have.

232
00:23:13,210 --> 00:23:17,079
And then it gives you the rule to compute it it says factorial of zero is one.

233
00:23:17,079 --> 00:23:23,799
Then the rules are processed in order. So it comes to the second rule at this point that n is not zero.

234
00:23:24,468 --> 00:23:28,299
So if n is not zero then n times factorial n minus one is the answer.

235
00:23:28,819 --> 00:23:33,160
So the actual way that Haskell works is by rewriting.

236
00:23:33,185 --> 00:23:40,720
But the main point is that there is no mention here about the intermediate names.

237
00:23:40,962 --> 00:23:46,450
It is just taking the parameters passed to the function and inductive or recursive calls on how to combine them.

238
00:23:47,809 --> 00:23:56,829
Here is another example. So if you want to add up the elements in a list this is is a Haskell style for a list of integer it takes a list of integer and produces an integer

239
00:23:56,829 --> 00:24:01,720
So you would say that for a list which is empty the sum is 0.

240
00:24:01,720 --> 00:24:03,150
And again coming here.

241
00:24:03,150 --> 00:24:07,462
If this is not empty if it comes here because we go in order.

242
00:24:07,487 --> 00:24:10,602
The first list does not match second list must be not empty.

243
00:24:10,627 --> 00:24:16,680
Then it has functions such as head or tail to take the first element and the last element.

244
00:24:16,680 --> 00:24:24,009
So if you have a non empty list the sum is given by taking the first element and then inductively adding it to the inductive result of computing the rest.

245
00:24:24,009 --> 00:24:30,519
So this is a. completely different style of programming which is called declarative programming.

246
00:24:30,519 --> 00:24:35,109
You can look it up it is very elegant and it has its own uses and features.

247
00:24:35,720 --> 00:24:41,950
So Python has actually borrowed some of its nice features from functional programming.

248
00:24:41,950 --> 00:24:48,569
And one of them in particular that we have seen is this use of map and filter.

249
00:24:48,569 --> 00:24:51,930
In general the idea that you can pass functions to other function.

250
00:24:51,930 --> 00:24:54,640
So these are all naturally pumped from functional programming.

251
00:24:54,640 --> 00:25:06,420
Here map and filter which allows to take a function and apply it to every element of the list and then resulting from this is very compact way of writing lists called list comprehensions

252
00:25:06,420 --> 00:25:10,329
So these are features of functional programming which are integrated into python.

253
00:25:13,910 --> 00:25:21,318
So to conclude one can never say that one programming language is the best programming language.

254
00:25:21,343 --> 00:25:26,440
Because if there were a best programming language then everybody would be using that language.

255
00:25:26,440 --> 00:25:34,960
So the very fact that there are so many programming languages around it is obvious that no programming language is universally better than every other.

256
00:25:35,930 --> 00:25:40,210
So what happens is that you choose the language which is best for you.

257
00:25:40,210 --> 00:25:45,918
In particular here we were trying to learn programming and various aspects of programming data structures algorithms.

258
00:25:45,943 --> 00:25:51,160
Python is a simple language to start working with and to use.

259
00:25:51,160 --> 00:25:59,289
It is nice built with things like dictionaries and stuff like exception handling are done in a very nice way so it makes it very attractive to learn.

260
00:25:59,779 --> 00:26:11,440
But as we saw the same things that make it attractive to learn the lack of declarations also limit its expressiveness we cannot talk about privacy in terms of implementations of objects and so on.

261
00:26:13,369 --> 00:26:18,749
So the moral of the story is that when you have a programming task you know what you want to do.

262
00:26:18,774 --> 00:26:22,390
Look around for the language that suits your task the best.

263
00:26:22,730 --> 00:26:26,970
Do not be afraid of learning programming languages to do the task.

264
00:26:26,970 --> 00:26:33,579
Because once you have learnt one programming language there is not that much difference between languages.

265
00:26:33,579 --> 00:26:37,289
Of course there are different styles like functional programming which look very different.

266
00:26:37,289 --> 00:26:45,339
But more or less if you have a little bit of background you can switch programs from one language to another by just looking up the reference manual and working with it.

267
00:26:45,339 --> 00:26:49,720
Of course if you use a programming language long enough then we should be careful to learn it well.

268
00:26:49,720 --> 00:26:54,160
But for many things you can just get by on the fly by just translating one language to another.

269
00:26:54,619 --> 00:26:59,230
So the main message is that we should focus on learning programming.

270
00:26:59,329 --> 00:27:04,019
Learning how to translate first of all coming up with good algorithms.

271
00:27:04,019 --> 00:27:08,700
How to translate algorithms into effective implementations what are the good data structures.

272
00:27:08,700 --> 00:27:17,279
So your focus should be on algorithms data structures the most elegant way in which you can phrase your instructions and then worry about the programming language.

273
00:27:17,279 --> 00:27:21,990
So it is a mistake to sit and learn a programming language nobody learns a programming language.

274
00:27:21,990 --> 00:27:26,789
You learn features of a programming language and put them to use as you come up with good programs.

275
00:27:26,789 --> 00:27:32,680
So with this i wish you all the best and i hope that you have a fruitful carrier ahead in programming.
