1
00:00:03,485 --> 00:00:10,000
इसलिए, हमने देखा है कि हम उन तर्कों के लिए मूल्यों को प्रतिस्थापित करके कार्यों को मान देते हैं जिन्हें मैंने कार्यों को परिभाषित किया था।

2
00:00:10,840 --> 00:00:14,949
और यह प्रभावी रूप से एक निहित असाइनमेंट के समान ही है।

3
00:00:14,949 --> 00:00:22,047
इसलिए, जब हम घात x n कहते हैं और हम इसे मान 3 और 5 कहते हैं, तो हमारे पास यह नियत कार्य x बराबर 3 और n 5 के बराबर होता है।

4
00:00:22,072 --> 00:00:27,911
यह वास्तव में वहां नहीं है, लेकिन ऐसा लगता है कि इस कोड को इस असाइनमेंट से पहले निष्पादित किया गया है।

5
00:00:27,935 --> 00:00:35,229
और निश्चित रूप से, इसे एक फ़ंक्शन के रूप में कॉल करने का लाभ यह है कि हमें कॉल के साथ आने वाली फ़ंक्शन परिभाषा में x और n निर्दिष्ट करने की आवश्यकता नहीं है।

6
00:00:35,229 --> 00:00:38,200
तो, x और n के विभिन्न मानों के लिए, हम एक ही कोड निष्पादित करेंगे।

7
00:00:41,529 --> 00:00:46,340
तो, पहली चीज जो पायथन हमें लचीले ढंग से करने की अनुमति देती है, वह है आदेश से नहीं जाना।

8
00:00:46,364 --> 00:00:49,299
ऐसा नहीं है कि पहला x है और दूसरा n है।

9
00:00:49,299 --> 00:00:59,146
हम कर सकते हैं, अगर हमें आदेश याद नहीं है, लेकिन हम मूल्यों या उन्हें सौंपे गए नामों को जानते हैं, तो हम वास्तव में तर्क के नाम का उपयोग करके उन्हें कॉल कर सकते हैं।

10
00:00:59,171 --> 00:01:09,099
तो, हम चीज़ को उल्टा भी कर सकते हैं और कॉल पावर कह सकते हैं मुझे पता है कि x निचला मान है, मुझे पता है कि यह x से घात n है, लेकिन मुझे याद नहीं है कि x पहले आता है या n पहले आता है।

11
00:01:09,099 --> 00:01:14,650
तो, मैं कह सकता हूं कि ठीक है, चलो बस सुरक्षित खेलें और कहें कि n बराबर 5, x बराबर 4 पर पावर है।

12
00:01:14,650 --> 00:01:22,090
और यह मान को तर्क के नाम के अनुसार सही ढंग से संबद्ध करेगा न कि स्थिति के अनुसार।

13
00:01:26,162 --> 00:01:33,489
पायथन की एक और अच्छी विशेषता यह है कि अनुमति देता है, यह कुछ तर्कों को छोड़ने की अनुमति देता है और निहित रूप से डिफ़ॉल्ट मान होते हैं।

14
00:01:34,222 --> 00:01:43,840
याद रखें कि हमने इस प्रकार के रूपांतरण फ़ंक्शन को s के int में परिभाषित किया है जो एक स्ट्रिंग लेगा और इसे एक पूर्णांक के रूप में प्रस्तुत करने का प्रयास करेगा यदि s एक पूर्णांक का वैध प्रतिनिधित्व है।

15
00:01:44,965 --> 00:01:51,129
तो, हमने कहा कि अगर हम इसे स्ट्रिंग 7 6 देते हैं तो int इसे 76 नंबर में बदल देगा।

16
00:01:51,683 --> 00:01:59,049
यदि दूसरी ओर, हम इसे ए 5 की तरह एक स्ट्रिंग देते हैं, क्योंकि ए एक वैध संख्या नहीं है, ए 5 वास्तव में एक त्रुटि उत्पन्न करेगा।

17
00:02:01,542 --> 00:02:09,550
अब, यह पता चला है कि int वास्तव में एक तर्क का कार्य नहीं है बल्कि दो तर्क है और दूसरा तर्क आधार है।

18
00:02:10,260 --> 00:02:14,139
तो, हम इसे एक स्ट्रिंग देते हैं और इसे आधार बी में एक संख्या में परिवर्तित करते हैं।

19
00:02:14,696 --> 00:02:18,789
और अगर हम b प्रदान नहीं करते हैं, तो डिफ़ॉल्ट रूप से b का मान 10 होता है।

20
00:02:19,515 --> 00:02:36,189
तो, पहले के int रूपांतरण में जो हो रहा है वह यह है कि जैसे हम int 76 को आधार 10 के साथ कह रहे हैं, लेकिन चूंकि हम 10 प्रदान नहीं करते हैं, पायथन के पास वह मान लेने के लिए एक तंत्र है जो प्रदान नहीं किया गया है और इसके साथ स्थानापन्न करता है डिफ़ॉल्ट मान 10.

21
00:02:36,792 --> 00:02:41,560
अब, यदि हम इसे एक मान प्रदान करते हैं, तो उदाहरण के लिए, हम A 5 का भी अर्थ निकाल सकते हैं।

22
00:02:41,810 --> 00:02:51,077
यदि आपके पास आधार 16 है, यदि आपने कभी स्कूल में आधार 16 का अध्ययन किया है, तो आपको पता होगा कि आपके पास अंक 0 से 9 है लेकिन आधार 16 की संख्या 15 तक है।

23
00:02:51,101 --> 00:02:57,500
तो, 9 से आगे की संख्या आमतौर पर ए, बी, सी, डी, ई, एफ . का उपयोग करके लिखी जाती है

24
00:02:57,525 --> 00:03:02,650
तो, A उस बात से मेल खाता है जिसके बारे में हम सोचेंगे कि आधार 10 में संख्या 10 है।

25
00:03:02,659 --> 00:03:08,603
इसलिए, यदि हम आधार 16 में A 5 लिखते हैं, तो यह 16वां स्थान है और यह 1 का स्थान है।

26
00:03:08,628 --> 00:03:12,755
तो, हमारे पास 16 गुना 10 है, क्योंकि ए 10 है, + 5।

27
00:03:12,902 --> 00:03:17,050
तो, संख्यात्मक शब्दों में, यह 165 सही ढंग से वापस आ जाएगा।

28
00:03:18,480 --> 00:03:20,169
तो, यह पायथन में कैसे काम करता है?

29
00:03:22,830 --> 00:03:27,425
तो, यह होगा कि आंतरिक रूप से यदि आप एक समान फ़ंक्शन लिखना चाहते हैं तो आप लिखेंगे।

30
00:03:27,852 --> 00:03:29,789
तो, आप तर्क प्रदान करते हैं।

31
00:03:29,789 --> 00:03:35,199
और उस तर्क के लिए जिसके लिए आप एक विकल्प और डिफ़ॉल्ट तर्क चाहते हैं, आप फ़ंक्शन परिभाषा में मान प्रदान करते हैं।

32
00:03:35,666 --> 00:03:39,669
तो, यह परिभाषा क्या कहती है कि int दो तर्क s और b लेता है।

33
00:03:39,822 --> 00:03:43,780
और b को 10 माना जाता है और इसलिए वैकल्पिक है।

34
00:03:43,780 --> 00:03:48,550
यदि व्यक्ति दूसरे तर्क को छोड़ देता है तो यह स्वतः ही मान 10 ले लेगा।

35
00:03:48,550 --> 00:03:52,449
अन्यथा, यह फ़ंक्शन कॉलम द्वारा प्रदान किया गया मान लेगा।

36
00:03:54,002 --> 00:03:57,159
तो, फ़ंक्शन परिभाषा में डिफ़ॉल्ट मान प्रदान किया जाता है।

37
00:03:57,990 --> 00:04:01,810
और अगर उस पैरामीटर को छोड़ दिया जाता है, तो इसके बजाय डिफ़ॉल्ट मान का उपयोग किया जाता है।

38
00:04:02,779 --> 00:04:10,195
लेकिन, याद रखने वाली एक बात यह है कि यह डिफ़ॉल्ट मान कुछ ऐसा है जो फ़ंक्शन परिभाषित होने पर उपलब्ध होना चाहिए।

39
00:04:10,219 --> 00:04:14,139
यह ऐसा कुछ नहीं हो सकता है जिसे फ़ंक्शन कहा जाता है जब गणना की जाती है।

40
00:04:14,882 --> 00:04:26,740
इसलिए, हमने त्वरित सॉर्ट और मर्ज सॉर्ट और बाइनरी सर्च जैसे विभिन्न कार्यों को देखा जहां हमें सरणी के साथ प्रारंभिक स्थिति और समाप्ति स्थिति के साथ पारित करने के लिए मजबूर किया गया था।

41
00:04:27,465 --> 00:04:35,646
अब, यह मध्यवर्ती लागत के लिए ठीक है, लेकिन जब हम वास्तव में एक सूची को पहले छोर पर क्रमबद्ध करना चाहते हैं, तो हमें इसे हमेशा 0 और सूची की लंबाई के साथ कॉल करना याद रखना होगा।

42
00:04:35,670 --> 00:04:48,642
इसलिए, यह कहना लुभावना होगा कि हम फ़ंक्शन को किसी ऐसी चीज़ के रूप में परिभाषित करते हैं जो एक प्रारंभिक सरणी A को पहले तर्क के रूप में लेती है और फिर डिफ़ॉल्ट रूप से एक बाईं सीमा 0 हो जाती है जो कि ठीक है और सही सीमा A की लंबाई है।

43
00:04:48,808 --> 00:04:54,189
अब, समस्या यह है कि यह मात्रा, A की लंबाई, A पर ही निर्भर करती है।

44
00:04:54,592 --> 00:05:04,273
इसलिए, जब फ़ंक्शन को परिभाषित किया जाता है, तो A के लिए कोई मान होगा या नहीं भी हो सकता है और आपने A के लिए जो भी मान चुना है, यदि कोई है तो लंबाई को डिफ़ॉल्ट के रूप में लिया जाएगा।

45
00:05:04,298 --> 00:05:07,067
हर बार जब हम इसे कॉल करते हैं तो इसकी गतिशील रूप से गणना नहीं की जाएगी।

46
00:05:07,435 --> 00:05:09,283
तो, यह काम नहीं करता है, है ना?

47
00:05:09,308 --> 00:05:18,430
इसलिए, जब आपके पास डिफ़ॉल्ट मान होते हैं, तो डिफ़ॉल्ट मान एक स्थिर मान होना चाहिए, जिसे निर्धारित किया जा सकता है जब परिभाषा पहली बार पढ़ी जाती है, न कि जब इसे निष्पादित किया जाता है।

48
00:05:21,725 --> 00:05:24,118
तो, यहाँ एक सरल प्रोटोटाइप है।

49
00:05:24,143 --> 00:05:27,189
तो, मान लीजिए कि हमारे पास 4 तर्कों ए, बी, सी, डी के साथ एक फ़ंक्शन है।

50
00:05:27,375 --> 00:05:44,740
और हमारे पास सी डिफ़ॉल्ट मान 40 और डी डिफ़ॉल्ट मान 22 के रूप में है, तो यदि आपके पास केवल दो तर्कों के साथ कॉल है तो यह ए और बी से जुड़ा होगा और इसलिए इसे एफ 13 12 के रूप में व्याख्या किया जाएगा और लापता के लिए तर्क सी और डी आपको डिफ़ॉल्ट मिलते हैं, 14 और 22।

51
00:05:45,942 --> 00:05:57,420
दूसरी ओर, आप तीन तर्क प्रदान कर सकते हैं जिसमें ए 13 हो जाता है, बी पहले की तरह 12 हो जाता है और सी 16 हो जाता है, लेकिन डी को अनिर्दिष्ट छोड़ दिया जाता है, इसलिए यह डिफ़ॉल्ट मान उठाता है।

52
00:05:57,420 --> 00:06:02,319
तो, इसकी व्याख्या 13, 12, 16 के f और डिफ़ॉल्ट मान 22 के रूप में की जाती है।

53
00:06:04,549 --> 00:06:08,759
तो, ध्यान रखने वाली बात यह है कि डिफ़ॉल्ट मान स्थिति द्वारा दिए जाते हैं।

54
00:06:08,759 --> 00:06:14,889
इस फ़ंक्शन में यह कहने का कोई तरीका नहीं है कि डी के लिए 16 दिया जाना चाहिए और मुझे सी के लिए डिफ़ॉल्ट मान चाहिए।

55
00:06:15,299 --> 00:06:18,360
हम केवल अंत से स्थिति के आधार पर मूल्यों को छोड़ सकते हैं।

56
00:06:18,360 --> 00:06:23,910
इसलिए, यदि मेरे पास दो डिफ़ॉल्ट मान हैं और यदि मैं उनमें से केवल दूसरा निर्दिष्ट करना चाहता हूं, तो यह संभव नहीं है।

57
00:06:23,910 --> 00:06:26,470
मुझे इसे पुन: व्यवस्थित करने के लिए फ़ंक्शन को फिर से परिभाषित करना होगा।

58
00:06:27,450 --> 00:06:38,680
इसलिए, इसलिए, हमें यह सुनिश्चित करना चाहिए कि जब आप इन डिफ़ॉल्ट मानों का उपयोग करते हैं, तो वे अंत में आते हैं और उन्हें स्थिति से पहचाना जाता है और आप इसे मिश्रित नहीं करते हैं और इन चीजों को यादृच्छिक तरीके से जोड़कर स्वयं को भ्रमित नहीं करते हैं।

59
00:06:39,899 --> 00:06:41,980
इसलिए, तर्कों का क्रम महत्वपूर्ण है।

60
00:06:45,319 --> 00:06:49,779
एक फ़ंक्शन परिभाषा एक फ़ंक्शन बॉडी को एक नाम से जोड़ती है।

61
00:06:49,779 --> 00:06:55,509
तो, यह कहता है कि f नाम की व्याख्या एक फ़ंक्शन के रूप में की जाएगी जो कुछ तर्क लेता है और कुछ करता है।

62
00:06:57,303 --> 00:07:02,529
तो, कई मायनों में, पायथन इसे किसी नाम के मूल्य के किसी अन्य असाइनमेंट की तरह व्याख्या करता है।

63
00:07:04,279 --> 00:07:10,913
इसलिए, उदाहरण के लिए, इस मान को अलग-अलग तरीकों से, कई तरीकों से, सशर्त तरीकों से परिभाषित किया जा सकता है।

64
00:07:10,937 --> 00:07:17,529
इसलिए, जैसा कि आप एक फ़ंक्शन के साथ जाते हैं, इसे फिर से परिभाषित किया जा सकता है या इसे अलग-अलग तरीकों से परिभाषित किया जा सकता है, यह इस बात पर निर्भर करता है कि गणना कैसे आगे बढ़ती है।

65
00:07:17,949 --> 00:07:21,029
यहाँ एक सशर्त परिभाषा का एक उदाहरण है।

66
00:07:21,029 --> 00:07:25,060
यदि यह सत्य है, तो आप f को एक तरह से परिभाषित करते हैं अन्यथा आप f को दूसरे तरीके से परिभाषित करते हैं।

67
00:07:25,060 --> 00:07:31,240
तो, इस परिभाषा को निष्पादित करते समय इनमें से कौन सी स्थिति आयोजित की गई थी, इसके आधार पर, बाद में, f का मान भिन्न होगा।

68
00:07:31,519 --> 00:07:38,079
अब, इसका मतलब यह नहीं है कि यह करना वांछनीय है क्योंकि आप भ्रमित हो सकते हैं कि f क्या कर रहा है।

69
00:07:38,180 --> 00:07:45,939
लेकिन, ऐसी स्थितियां हैं जहां आप एक तरह से या किसी अन्य तरीके से एफ लिखना चाहते हैं, हम इस पर निर्भर करते हैं कि आवेदन कैसे आगे बढ़ रहा है और पायथन आपको ऐसा करने की अनुमति देता है।

70
00:07:45,939 --> 00:07:52,540
संभवतः पायथन के लिए एक परिचयात्मक दिन पर यह बहुत उपयोगी नहीं है लेकिन यह जानना उपयोगी है कि ऐसी संभावना मौजूद है।

71
00:07:52,540 --> 00:07:55,779
और विशेष रूप से आप आगे बढ़ सकते हैं और f को फिर से परिभाषित कर सकते हैं जैसे आप आगे बढ़ते हैं।

72
00:07:59,380 --> 00:08:06,189
एक और चीज जो आप पायथन में कर सकते हैं, जो आपको थोड़ा अजीब लग सकता है, वह यह है कि आप एक मौजूदा फ़ंक्शन ले सकते हैं और इसे एक नए नाम पर मैप कर सकते हैं।

73
00:08:06,739 --> 00:08:12,639
तो, हम एक फ़ंक्शन f को परिभाषित कर सकते हैं, जैसा कि हमने कहा कि f नाम के साथ संबद्ध है, इस फ़ंक्शन का शरीर।

74
00:08:12,639 --> 00:08:15,639
बाद के चरण में, हम j,g बराबर f कह सकते हैं।

75
00:08:16,659 --> 00:08:29,800
और इसका मतलब यह है कि अब हम ए, बी, सी के जी का भी उपयोग कर सकते हैं और इसका मतलब ए, बी, सी के एफ के समान होगा। इसलिए यदि हम फ़ंक्शन में g का उपयोग करते हैं तो यह ठीक उसी फ़ंक्शन का उपयोग करेगा, जैसा कि एक सूची को दूसरी या एक शब्दकोश को दूसरे को निर्दिष्ट करने जैसा है

76
00:08:31,202 --> 00:08:33,009
अब, आप ऐसा क्यों करना चाहेंगे?

77
00:08:35,473 --> 00:08:42,159
तो, एक उपयोगी तरीका जिसमें आप यह कर सकते हैं, इस सुविधा का उपयोग किसी अन्य फ़ंक्शन को फ़ंक्शन पास करना है।

78
00:08:43,206 --> 00:08:47,649
तो, मान लीजिए कि हम दिए गए फ़ंक्शन f को उसके तर्क n बार लागू करना चाहते हैं।

79
00:08:47,949 --> 00:08:55,682
फिर, हम इस तरह एक सामान्य कार्य लिख सकते हैं जिसे लागू कहा जाता है जो तीन तर्क लेता है पहला फ़ंक्शन ठीक है?

80
00:08:55,949 --> 00:09:01,210
दूसरा तर्क है और तीसरा बारंबारता की संख्या है, दोहराव।

81
00:09:04,759 --> 00:09:11,740
इसलिए, हम उस मान से शुरू करते हैं जो आप इसे प्रदान करते हैं और जितनी बार आपसे कहा जाता है, आप फ़ंक्शन f को पुनरावृत्त करते रहते हैं

82
00:09:12,820 --> 00:09:14,559
तो, आइए एक ठोस उदाहरण देखें।

83
00:09:14,965 --> 00:09:18,970
तो, मान लीजिए कि हमने x के एक फ़ंक्शन वर्ग को परिभाषित किया है, जो x गुणा x है।

84
00:09:19,599 --> 00:09:25,090
और अब हम कह सकते हैं, वर्ग को मान 5 पर दो बार लागू करें।

85
00:09:26,478 --> 00:09:33,656
तो इसका मतलब यह है कि 5 का वर्ग और फिर उसका वर्ग लगाएं, तो वर्ग को दो बार करें।

86
00:09:33,680 --> 00:09:37,389
और इसलिए, आपको 5 वर्ग 25 25 वर्ग 625 मिलता है।

87
00:09:37,935 --> 00:09:43,203
तो, यहाँ क्या हो रहा है कि वर्ग f को सौंपा जा रहा है।

88
00:09:43,915 --> 00:09:47,744
5 को x और 2 को n को सौंपा जा रहा है।

89
00:09:47,817 --> 00:09:52,149
तो, यह ठीक वैसा ही है जैसा हमने पहले कहा था जैसे कि f बराबर वर्ग के बराबर है।

90
00:09:52,715 --> 00:10:07,570
इसलिए, इस अर्थ में, किसी फ़ंक्शन का नाम लेने और उसे किसी अन्य नाम पर असाइन करने में सक्षम होना बहुत उपयोगी है क्योंकि यह हमें फ़ंक्शन को एक स्थान से दूसरे स्थान पर पास करने और उस फ़ंक्शन को दूसरे फ़ंक्शन के अंदर निष्पादित करने की अनुमति देता है बिना यह जाने कि वह फ़ंक्शन क्या है है।

91
00:10:10,413 --> 00:10:14,919
इसका एक व्यावहारिक उपयोग क्रमबद्ध कार्यों को अनुकूलित करना है।

92
00:10:16,059 --> 00:10:19,840
अब, कभी-कभी हमें विभिन्न मानदंडों के आधार पर मूल्यों को क्रमबद्ध करने की आवश्यकता होती है।

93
00:10:20,552 --> 00:10:31,570
तो, हमारे पास एक अमूर्त तुलना फ़ंक्शन हो सकता है जो देता है - 1 यदि पहला तर्क छोटा है, 0 यदि दो तर्क बराबर हैं और + 1 यदि पहला तर्क दूसरे से बड़ा है।

94
00:10:33,243 --> 00:10:41,597
इसलिए, स्ट्रिंग्स की तुलना करने में हमारे दिमाग में स्ट्रिंग्स की तुलना करने के दो अलग-अलग तरीके हो सकते हैं और जब हम इन दो अलग-अलग तरीकों से सॉर्ट करते हैं तो हम अंतर की जांच करना चाहते हैं।

95
00:10:41,731 --> 00:10:46,269
तो, हमारे पास एक प्रकार हो सकता है जिसमें हम स्ट्रिंग की तुलना शब्दकोश क्रम में करते हैं।

96
00:10:46,562 --> 00:10:52,059
तो, aab जैसी स्ट्रिंग ab से पहले आएगी क्योंकि दूसरी स्थिति a, b से छोटी है।

97
00:10:53,045 --> 00:10:57,129
तो, इसका परिणाम -1 होगा, क्योंकि पहला तर्क दूसरे तर्क से छोटा है।

98
00:10:57,982 --> 00:11:05,950
यदि, दूसरी ओर, हम लंबाई से तार की तुलना करना चाहते हैं, तो वही तर्क हमें + 1 देगा क्योंकि aab लंबाई 3 है और ab से अधिक है।

99
00:11:07,163 --> 00:11:14,130
इसलिए, हम एक सॉर्ट फ़ंक्शन लिख सकते हैं जो सूची लेता है और दूसरा तर्क लेता है कि सूची में तत्वों की तुलना कैसे करें।

100
00:11:14,416 --> 00:11:22,450
इसलिए, सॉर्ट फ़ंक्शन को स्वयं यह जानने की आवश्यकता नहीं है कि सूची में तत्व क्या हैं, जब भी इसे मनमाने मूल्यों की सूची दी जाती है, तो यह भी बताया जाता है कि उनकी तुलना कैसे करें।

101
00:11:22,596 --> 00:11:30,519
तो, बस इतना करना है कि इस फ़ंक्शन को दो मानों पर लागू करें और जांचें कि क्या उत्तर - 1, 0 या + 1 है और इसकी व्याख्या इससे कम, इसके बराबर या इससे अधिक के रूप में करें।

102
00:11:31,368 --> 00:11:36,755
और आप चाहें तो इसे पहले वाले फीचर के साथ जोड़ सकते हैं, यानी आप इसे डिफॉल्ट फंक्शन दे सकते हैं।

103
00:11:36,779 --> 00:11:41,200
इसलिए, यदि आप सॉर्ट फ़ंक्शन निर्दिष्ट नहीं करते हैं, तो एक अंतर्निहित फ़ंक्शन हो सकता है जो सॉर्ट फ़ंक्शन का उपयोग करता है।

104
00:11:41,781 --> 00:11:44,679
अन्यथा, यह आपके द्वारा लिखे गए तुलना फ़ंक्शन का उपयोग करेगा।

105
00:11:49,197 --> 00:11:55,179
इसलिए, फ़ंक्शन परिभाषाओं को सारांशित करने के लिए नामों के मानों के अन्य असाइनमेंट की तरह ही व्यवहार करें।

106
00:11:55,179 --> 00:11:57,370
हम किसी फ़ंक्शन को एक नई परिभाषा पुन: असाइन कर सकते हैं।

107
00:11:57,440 --> 00:11:59,919
हम इसे सशर्त रूप से परिभाषित कर सकते हैं और इसी तरह।

108
00:12:00,811 --> 00:12:07,330
महत्वपूर्ण रूप से, आप एक फ़ंक्शन का उपयोग कर सकते हैं और इसे दूसरे फ़ंक्शन पर नाम बिंदु बना सकते हैं।

109
00:12:07,330 --> 00:12:11,169
और यह निहित रूप से उपयोग किया जाता है जब हम अन्य फ़ंक्शन को फ़ंक्शन पास करते हैं।

110
00:12:11,826 --> 00:12:20,139
और छँटाई जैसी स्थितियों में, आप एक तुलना फ़ंक्शन पास करके अपनी छँटाई को और अधिक लचीला बना सकते हैं जो हमारे द्वारा छांटे गए मानों के लिए उपयुक्त है।

