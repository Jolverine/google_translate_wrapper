1
00:00:02,870 --> 00:00:09,490
Till now all the  programs  that you have been asked to write in your assignments have actually been just  functions  .

2
00:00:09,490 --> 00:00:14,589
So, these are  functions  which are called from other pieces of  python code  and  return values  to them.

3
00:00:14,689 --> 00:00:21,940
Now, when you have a standalone  python program , it must interact with the user in order to derive  inputs  and produce  outputs .

4
00:00:21,940 --> 00:00:26,050
Let us see how  python  interacts with its  environment .

5
00:00:27,769 --> 00:00:37,000
The most basic way of interacting with the  environment  is to take  input  from the  keyboard  and display  output  to the screen.

6
00:00:37,340 --> 00:00:42,579
Traditionally these modes are called  standard input  and  standard output .

7
00:00:42,579 --> 00:00:49,390
So,  standard input  just means take the  input  from the  keyboard  or any  standard input  device like that.

8
00:00:49,390 --> 00:00:53,649
And  standard output  just means display the  output  directly on the screen.

9
00:00:56,840 --> 00:01:03,039
So, the basic  command in python  to  read  from the  keyboard  is  input .

10
00:01:03,140 --> 00:01:18,459
So, if we invoke the  function input  with no arguments and assign it to a name then the name  user data  will get the  value  that is typed in by the user at the  input command .

11
00:01:18,459 --> 00:01:20,796
So, remember that it reads a line of input .

12
00:01:20,821 --> 00:01:27,189
So the way that the user signals that the  input  is over is by hitting the  return  on the  keyboard .

13
00:01:27,189 --> 00:01:34,569
And the entire sequence of  characters  up to the  return , but not including the  return  transmitted as a  string  to  user data .

14
00:01:35,930 --> 00:01:41,379
Now, of course, if the  Program  is just waiting for you for  input , it can be very confusing.

15
00:01:41,379 --> 00:01:48,879
So, you might want to provide a  prompt , which is a message to the user telling the user what is expected.

16
00:01:48,879 --> 00:02:01,540
So, you can provide such a thing by adding a  string  as an argument to  inputs , If you put an argument to  input  like this, then it is a  string  which is displayed when the user is suppose to  input data .

17
00:02:03,349 --> 00:02:14,370
Now, this  string  is displayed as it is, so you can make appropriate adaptations to make a little more user friendly, we will see an example in a minute

18
00:02:14,370 --> 00:02:19,110
But you might want to leave a  space  or you might want to insert a  new line .

19
00:02:19,110 --> 00:02:28,449
So, basically you use the  input command to read one line  of  input from the user  and you can display a message to tell the user what is expected of it.

20
00:02:31,849 --> 00:02:36,490
So, here is what happens if I just say  user data  is equal to  input .

21
00:02:36,500 --> 00:02:39,419
The  python program  will just wait.

22
00:02:39,419 --> 00:02:45,504
Now, as a user who does not know what is expected we do not know whether it is processing somethingor is waiting for  input .

23
00:02:45,529 --> 00:02:56,110
Now, it is a turns out that If we type something and press  enter  it will come back and now if I ask for the contents of the name  user data , it will in fact be the  string  of things that I typed.

24
00:02:56,330 --> 00:03:00,990
So, providing an  input   prompt  without a message can be confusing for the user.

25
00:03:00,990 --> 00:03:12,539
So, what we said was we might want to say something like  provide an input  like this. Now it provides us with a message

26
00:03:12,539 --> 00:03:18,090
but the number that we type for instance is stuck to the message. So, it is not very readable.

27
00:03:18,090 --> 00:03:22,210
So, user data  is indeed not a number now it is a  string  as we will see in a minute.

28
00:03:22,210 --> 00:03:28,250
But, the fact is that we did not get any  space  or anything else. So, it looks a bit ugly.

29
00:03:28,275 --> 00:03:38,291
so what as we said is that you can actually for instance put a  colon  and a  space  so that the message comes like this and now this is a slightly nicer prompt

30
00:03:38,316 --> 00:03:45,643
And you could also put a new line  if you want which is signaled by this special  character backslash n 

31
00:03:45,668 --> 00:03:50,083
So now the message comes and then you type on a new line .

32
00:03:50,569 --> 00:03:59,159
And in all cases the outcome is the same  user data  the name to which you are reading the  input  becomes set to the  string  that is typed in by the user.

33
00:03:59,159 --> 00:04:08,510
So, if I do it again and if I type in something else like nine ninety three for example. then  user data  becomes a  string  nine nine three.

34
00:04:08,535 --> 00:04:13,539
So, you can use  input  with a message and make the message as readable as you can.

35
00:04:17,629 --> 00:04:23,910
So, as we saw when we were playing with the  interpreter  the  input  that is  read  by the  function   input  is always a  string .

36
00:04:23,910 --> 00:04:27,420
Even if you say  enter a number  and the  user types  in a number.

37
00:04:27,420 --> 00:04:35,129
This is not actually a number. If you want to use it as a number you have to use this type conversion remember we have these  functions   int   str  and so on.

38
00:04:35,129 --> 00:04:40,540
So, we have to use the  int function  to type convert whatever the user is typed to an  integer .

39
00:04:40,550 --> 00:04:49,709
Now, of course, remember that if the user type some garbage then you get an  error . So, the user does not type something valid then you will get an  error .

40
00:04:49,709 --> 00:04:54,670
So, what we can do is we can use  exception handling  to deal with this  error 

41
00:04:55,250 --> 00:05:02,550
So, what we can say is  try user data . So, this is the code that we had before those two lines.

42
00:05:02,550 --> 00:05:09,939
So, what we want to say is we will try these lines, but if the user type something which is not a number then we are going to ask him to type it again

43
00:05:09,939 --> 00:05:14,759
And it will turn out that type of  error in python  is called a  value error .

44
00:05:14,759 --> 00:05:18,610
So, you can verify this by going to the  python interpreter  and checking.

45
00:05:19,850 --> 00:05:26,191
So, in the  python interpreter  if we try to apply  int  to some nonsensical thing then we get a  value error .

46
00:05:26,216 --> 00:05:31,629
So, what we are trying to say is that if we get a  value error  we want to take appropriate action.

47
00:05:34,220 --> 00:05:38,550
So, we have this  try block  and if we see a  value error .

48
00:05:38,550 --> 00:05:42,329
What we do is we  print  a message now we are going to see  print  just after this.

49
00:05:42,329 --> 00:05:47,800
But we  print  a message to the user saying this is not a number try again.

50
00:05:47,800 --> 00:05:55,671
This is now the whole thing is enclosed inside a  while  loop  and this  while  loop  has the condition  true 

51
00:05:55,696 --> 00:06:00,490
In other words the condition is never going to become  false , this  while loop  is going to keep on asking for a number

52
00:06:00,490 --> 00:06:04,620
So, how do we get out of this? Well, if I come here.

53
00:06:04,620 --> 00:06:10,899
And I get a  value  error  it will go back and the  while  will execute again.

54
00:06:10,899 --> 00:06:15,978
But, if there is no  error  remember, if there is no  error  here then it will come to the  else .

55
00:06:16,003 --> 00:06:19,110
So, this  else  is executed if there is no  error 

56
00:06:19,110 --> 00:06:23,259
And what the  else  does is it gets us out of this vicious  cycle .

57
00:06:23,259 --> 00:06:25,990
So, in other words we are in  infinite loop  .

58
00:06:25,990 --> 00:06:33,469
where we keep on trying to get one more piece of  data  from the user until we get something that we like, and when we get that we break out of the  loop .

59
00:06:33,494 --> 00:06:39,876
This is an another idiomatic kind of way to use  exceptions  in the context of  input  and  output 

60
00:06:39,901 --> 00:06:50,339
As we said in the last lecture  input and output  is inherently  error  prone because you are dealing with an uncertain interacting  environment  which can do things which you cannot anticipate or control

61
00:06:50,339 --> 00:06:55,990
So, you must take appropriate action to make sure that the interaction goes in the direction that you expect.

62
00:06:58,550 --> 00:07:05,259
So, the other part of interaction is displaying messages. which we call  standard output  or  printing  to the screen.

63
00:07:05,259 --> 00:07:12,779
And this is achieved using the  print statement  which we have seen occasionally informally without formally having defined it.

64
00:07:12,779 --> 00:07:17,910
The basic form of the  print statement  is to give the a sequence of  values  separated by commas.

65
00:07:17,910 --> 00:07:22,750
So,  print x y  will display the  value of x  then a space  then the  value of y .

66
00:07:22,750 --> 00:07:28,569
 print a b c  will display three  values , the  values of a b and c  separated by  spaces .

67
00:07:29,240 --> 00:07:34,529
Now, the other thing that we can do is directly  print  a  string  or a message.

68
00:07:34,529 --> 00:07:37,290
So, like we saw in the previous example we can say.

69
00:07:37,290 --> 00:07:42,310
 print  the  string   not a number try again  . So, this will display this  string  on the screen.

70
00:07:44,360 --> 00:07:47,730
Now, you can combine these two things in interesting ways.

71
00:07:47,730 --> 00:07:52,050
So  print  takes in general a  sequence  of things of arbitrary length separated by commas

72
00:07:52,050 --> 00:07:55,316
So, these things could be either messages or names.

73
00:07:55,341 --> 00:08:08,319
So we can say things supposing we want to  print  the  value of x and y , but we want to indicate to the  output  which is  x which is y . So, instead of saying just  print x comma y . Which produces two  values  on the screen with no indication as to which is  x  and which is  y .

74
00:08:08,319 --> 00:08:12,060
We could have this more elaborate  print statement  which  prints four things.

75
00:08:12,060 --> 00:08:19,170
The first thing it  prints  is a message saying the  values are x colon  So, this will  print x colon  and leave a  space .

76
00:08:19,170 --> 00:08:26,860
then it will  print the current value of x , then it will  print the value of y colon  after a  space  and then it will  print the current value of y .

77
00:08:26,860 --> 00:08:34,929
So, we can intersperse messages with  values  with names to produce meaningful  output  that is more readable.

78
00:08:39,590 --> 00:08:45,469
By default  print  appends a new line  whenever it is executed.

79
00:08:45,494 --> 00:08:49,860
In other words, every  print statement  we just  print  the way we have done so far.

80
00:08:49,860 --> 00:08:55,929
It appears on a  new line  because the previous  print statement  implicitly moves the  output to a new line .

81
00:08:56,779 --> 00:09:01,544
Now, if you want to control this, We can use an optional argument called   end  .

82
00:09:01,569 --> 00:09:07,080
So, we can provide a  string  saying This is what should we put at the end  of the  print statement .

83
00:09:07,080 --> 00:09:10,600
By default the  value  here is this new line character 

84
00:09:10,600 --> 00:09:14,850
So, we can replace this by something else. Here is an example.

85
00:09:14,850 --> 00:09:17,100
Supposing, we write these three  statements .

86
00:09:17,100 --> 00:09:23,919
So, the first  statement  says  print continue on the  this is just a  string  but set  end  to a  space .

87
00:09:23,919 --> 00:09:33,429
The second line says  prints same line  and then it says set  end  to  full stop  and a  new line  and then the third  statement  says  print next line .

88
00:09:33,666 --> 00:09:38,636
So, what we are doing is in the first two  statements  we are changing the default.

89
00:09:38,661 --> 00:09:43,470
The default would have been to  print  a  new line , but the first  statement  is not  printing  a  new line .

90
00:09:43,470 --> 00:09:53,740
So, if you  print  this what we see is that the first two  statements   continue on the  and  same line  come on a single  line  because we have disabled the default  print new line .

91
00:09:53,740 --> 00:10:00,820
And we have explicitly put a  new line  here and this is forced. the next one to come on the next  line .

92
00:10:00,820 --> 00:10:03,990
So, if you break this up and see what happens here.

93
00:10:03,990 --> 00:10:08,909
We see that in the first  statement  we had this  end   which says insert a  space .

94
00:10:08,909 --> 00:10:14,805
And this is why we get a  space  between the word  the  in the first  line  and the word  same  coming from the second  line .

95
00:10:14,830 --> 00:10:19,029
Otherwise,  the  and  same  would have been fused together as a single word.

96
00:10:19,190 --> 00:10:24,850
So,  end  equal to  space  is effectively separating this  print  from the next  print by a space .

97
00:10:25,070 --> 00:10:30,151
The next  print statement  inserts a  full stop  and a  new line .

98
00:10:30,176 --> 00:10:37,320
So, implicitly although the the word same  line  ends without a  full stop , we produce a  full stop 

99
00:10:37,320 --> 00:10:49,419
And after I produce a  full stop  it produces a  new line  and finally, after this  new line  the next line comes in  new line  and of course here we did not say anything if we  print  after that we implicitly would  print  on a  new line .

100
00:10:55,100 --> 00:11:01,189
The other thing that we might want to control is how the items are separated on a  line .

101
00:11:01,214 --> 00:11:07,330
We said that, If we say  print x comma y x and y  will be separated by a  space  by default.

102
00:11:07,730 --> 00:11:15,480
So, if we do this  print x y  we set  x equal to seven  y equal to ten and we say x is  x and y  is y.

103
00:11:15,480 --> 00:11:24,990
And then we want to  end  with a  full stop . So, this is what we want to write the  string  x is then the  value of x and y  is then the  value  of  y  and then a  full stop .

104
00:11:24,990 --> 00:11:33,509
Now because everything is separated by  space , what we find is that we find a  space  over here. Do you see this? This is fine.

105
00:11:33,509 --> 00:11:37,000
So, we get a  space  here because that is from this comma.

106
00:11:37,250 --> 00:11:40,450
We get a  space  here just from this comma.

107
00:11:40,460 --> 00:11:48,669
We get a  space  here which is from this comma and then we get an unwanted  space  between ten and the  full stop .

108
00:11:48,669 --> 00:11:55,000
So, how do we get rid of this  space , the second  space  right, we do not want a  full stop  to come after a  space .

109
00:11:55,250 --> 00:11:58,600
Just like we have the  optional argument end .

110
00:11:58,600 --> 00:12:05,178
We have an  optional argument sep   which we are going to specify what  string  should be used to separate.

111
00:12:05,203 --> 00:12:07,590
For example, we take the earlier thing

112
00:12:07,590 --> 00:12:10,049
We can say dont separate it with anything.

113
00:12:10,049 --> 00:12:19,659
Now of course do not separate with anything changes because then this  x  is seven will get fused and this  handle  gets fused. So what we do instead is we put the  space  explicitly here.

114
00:12:20,059 --> 00:12:23,710
So, earlier we had no  space  here.

115
00:12:26,570 --> 00:12:29,759
At the end just around the  quotes.

116
00:12:29,759 --> 00:12:36,029
Now, we put  spaces  where we want them and we say do not put any other spaces. So, what this will say is that?

117
00:12:36,029 --> 00:12:38,220
 x  is  space  I give the  space .

118
00:12:38,220 --> 00:12:48,283
Dont put a  space , put the  value of x  , do not put a  space , now I give a  space  and  y  is give a  space , now dont put a a  space  here. So, these commas do not contribute any  space 

119
00:12:48,308 --> 00:12:57,690
Because I have said separator should be empty and in particular what this means is that this last comma , the comma between the  y  and the  full stop  will not generate a  space .

120
00:12:57,690 --> 00:13:03,570
And in fact, if you execute this, then you will get the  output x  is seven and  y  is ten.

121
00:13:03,570 --> 00:13:06,549
And the way it works is that this is the first block.

122
00:13:06,549 --> 00:13:12,759
So, this is everything up to here, then this is the second block, this is this then this is the third block.

123
00:13:13,789 --> 00:13:17,289
Which is this whole thing with the spaces given.

124
00:13:17,289 --> 00:13:21,460
And then this is the  value of y  and finally, this is the  full stop .

125
00:13:21,740 --> 00:13:26,350
So, this is one way to control the  output  of a  print statement .

126
00:13:31,279 --> 00:13:44,110
So, with the  optional arguments end  and  sep  we can control when. successive  prints  continue on the same  line  and we can control to some. limited extent how these  values  are separated on a  line .

127
00:13:44,110 --> 00:13:46,200
but we may actually want to do a lot more.

128
00:13:46,200 --> 00:13:49,720
We might want to put a sequence of things so that they all  line  up.

129
00:13:49,720 --> 00:13:53,129
So, supposing we want to  print  out a table using a  print statement .

130
00:13:53,129 --> 00:13:55,320
We want to make sure that the columns line up.

131
00:13:55,320 --> 00:14:01,289
So, we might want to say that each item that we want to  print  like we have  printing  along sequence of numbers  line by line .

132
00:14:01,289 --> 00:14:05,009
So, the numbers may have different widths, some may be three digits, may be five digits

133
00:14:05,009 --> 00:14:08,769
We might say  print  them all to occupy seven  characters  width.

134
00:14:08,769 --> 00:14:12,323
So, this is a thing that we might want to do,  align  text .

135
00:14:12,348 --> 00:14:17,700
Now within this alignment, might want to  align  things left or right. So, if we have a default width say ten  characters 

136
00:14:17,700 --> 00:14:22,559
If there are numbers we might want and  right align  so that the units digit is  aligned 

137
00:14:22,559 --> 00:14:29,429
If there are names we might want them  left align  so that we can  read  them from left to right without looking rag it.

138
00:14:29,429 --> 00:14:33,120
if we are doing things like calculating averages or something.

139
00:14:33,120 --> 00:14:39,490
We may not want the entire thing to be displayed might want to truncated to two decimal points say its currency or something

140
00:14:39,500 --> 00:14:48,556
So, these are all more intricate ways of formatting the  output  and we will see in the next lecture how to do this.

141
00:14:48,581 --> 00:14:52,539
But right now you can use  end  and  sep  to do minimal formatting.

142
00:14:54,590 --> 00:15:01,389
So, to summarize you can use the  input statement  with an optional message in order to  read  from the  keyboard .

143
00:15:01,399 --> 00:15:04,659
You can  print  to the screen using a  print statement .

144
00:15:04,659 --> 00:15:09,796
Now, we mentioned at the beginning that there are some differences between  python two  and  three .

145
00:15:09,821 --> 00:15:17,295
And here is one of the more obvious differences that you will see if you look at  python two  code which is available from various sources.

146
00:15:17,320 --> 00:15:24,570
In  python two , you can say  print   space  and then give what is given as the arguments to  print  in  python three .

147
00:15:24,570 --> 00:15:30,179
So,  python three  insists on it being called like a  function  with  brackets  in  python two  the  brackets  are optional.

148
00:15:30,179 --> 00:15:40,440
So, you will very often see in  python two  code something that looks like   print x y  give without any  brackets .

149
00:15:40,440 --> 00:15:45,220
This is legal in  python two , this is not legal in  python three . So, just be careful about this.

150
00:15:45,220 --> 00:15:50,736
And what we saw is that with the limited amount of control we can make  print  behave as we want.

151
00:15:50,761 --> 00:15:58,710
So we can specify what to put at the end in particular we can tell it not to. put a  new line , so continue  printing  on the same line.

152
00:15:58,710 --> 00:16:03,100
And we can separate the  values  by something other than default  space  correct.

