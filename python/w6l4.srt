1
00:00:02,060 --> 00:00:12,939
In the 1970s Niklaus Wirth, the inventor of the programming language Pascal, wrote a very influential book called algorithms plus data structures equals programs.

2
00:00:12,939 --> 00:00:20,289
The title emphasizes the importance of both algorithms and data structures as components of effective programs.

3
00:00:20,480 --> 00:00:23,530
So far we have seen algorithms in some detail.

4
00:00:23,530 --> 00:00:27,460
So now, let's take a closer look at some specialized data structures.

5
00:00:28,190 --> 00:00:35,320
The data structures that we have seen that are built into python began with arrays and lists which are just sequences of values.

6
00:00:35,450 --> 00:00:42,250
We also saw dictionaries which are key value pairs, and which are very useful for maintaining various types of information.

7
00:00:43,640 --> 00:00:47,859
Another built-in data type that is available in python is the set.

8
00:00:49,795 --> 00:00:54,735
A set is like a list except that you do not have duplicates.

9
00:00:54,760 --> 00:01:00,203
In python one way of writing a set is to write a list with braces like this.

10
00:01:00,228 --> 00:01:06,400
Here, we have associated with the name colors, a list of values red, black, red and green.

11
00:01:06,400 --> 00:01:14,070
Notice that in setting it up we have repeated red, but because this is a set the duplicate red will be automatically removed.

12
00:01:14,095 --> 00:01:18,519
So if we print the name colors, we just get the list black, red and green.

13
00:01:19,640 --> 00:01:27,983
Now since the empty brace notation is already used for the empty dictionary, if you want to create an empty set, we have to call this set function as follows.

14
00:01:28,008 --> 00:01:32,439
So we say colors equal to set with with no arguments.

15
00:01:34,609 --> 00:01:39,879
Like lists and other data structures we can test membership using in.

16
00:01:39,879 --> 00:01:50,079
So if in the previous set colors which had red, black and green, we ask whether black is in colors by using the word in, then the return value is True.

17
00:01:51,769 --> 00:01:56,079
In general, we can convert any list to a set using the set function.

18
00:01:56,079 --> 00:01:59,560
We saw that if you give no arguments to set, you get an empty set.

19
00:01:59,560 --> 00:02:14,529
But if we give a list such as this 1, 3, 2, 1, 4 with duplicates, and assign it to the name numbers then, because it is a set the duplicate ones will be removed, and we will get a set of numbers as 0, 1, 2, 3, 4.

20
00:02:14,560 --> 00:02:21,810
Notice again that the order in which the set is printed need not be the order in which we provided it, and this is very much like a dictionary.

21
00:02:21,810 --> 00:02:30,520
Sets are optimized for internal storage to make sure there are no duplicates so we should not assume anything about the order of elements in the set.

22
00:02:31,819 --> 00:02:42,330
And the interesting feature is that a string itself is essentially a list of characters, and so if we give a string to a set, then it produces the set function.

23
00:02:42,330 --> 00:02:46,419
Then it produces a set which consists of individual letters from this set.

24
00:02:46,419 --> 00:02:55,120
So if we give this string banana to the set function, then we get the three individual letters a, n and b without duplicates in the set.

25
00:02:57,889 --> 00:03:03,984
As you would expect, sets support basic operations like their counterpart in mathematics.

26
00:03:04,009 --> 00:03:17,199
Suppose we set up the odd numbers to be the set of all odd numbers between 1 and 11, and the prime numbers to be the set of all prime numbers between 2 and 11 using these set function as we saw before.

27
00:03:18,409 --> 00:03:23,183
So if we write this vertical bar, then we can get the union of the two sets.

28
00:03:23,208 --> 00:03:28,439
So odd union prime will be those elements which are either in odd or in prime.

29
00:03:28,439 --> 00:03:31,530
And so we get one from the top, two from the bottom.

30
00:03:31,530 --> 00:03:33,969
So we get 3, 5, 7, 9 and 11.

31
00:03:33,969 --> 00:03:37,719
All the elements in both the sets, but without any duplicates.

32
00:03:38,449 --> 00:03:42,960
If we ask for the intersection of two sets, we use ampersand to denote it.

33
00:03:42,960 --> 00:03:50,663
We get those which occur in both sets, those numbers which are both odd and prime, in this case 3, 5, 11 and 7.

34
00:03:50,688 --> 00:03:54,789
Notice again that the order in which these numbers are printed may be arbitrary.

35
00:03:56,120 --> 00:04:02,550
Set difference ask for those elements that are in odd, but not in prime, in other words odd numbers that are not prime.

36
00:04:02,550 --> 00:04:07,030
In this particular collection, 1 and 9 are examples of odd numbers that are not prime.

37
00:04:07,789 --> 00:04:17,399
And finally, unlike union which collects elements which are in both sets, we can do an exclusive OR which takes elements which are exactly in one of the two sets.

38
00:04:17,399 --> 00:04:28,480
So if we use this caret symbol, then we will get one from the first set, 9 from the first set and 2 from the second set, because 3, 5, 7 and 11 occur in both sets.

39
00:04:28,759 --> 00:04:39,430
So we will not talk much more about sets, but you can use them in various context in order to keep track of a collection of values without duplicates using these built-in operations.

40
00:04:41,509 --> 00:04:45,360
Let's look at different ways in which we can manipulate sequences.

41
00:04:45,360 --> 00:04:51,339
So a list as we saw, is a sequence in which we can freely insert and delete values all over the place.

42
00:04:51,339 --> 00:04:56,889
Now, if we impose some restriction on this, we get specialized data structures, one of which is a stack.

43
00:04:56,889 --> 00:05:00,329
A stack is a last in first out list.

44
00:05:00,329 --> 00:05:04,509
So we can only remove from a stack, the element that we last added to it.

45
00:05:04,910 --> 00:05:13,689
Usually this is denoted by giving two operations, when we push an element on to a stack, we add it to the end of the stack.

46
00:05:13,689 --> 00:05:17,889
And when we pop a stack, we implicitly get the last value that was added.

47
00:05:18,470 --> 00:05:23,079
Now, this is easy to implement using built-in python list.

48
00:05:23,079 --> 00:05:25,709
We can assume that stacks grow to the right.

49
00:05:25,709 --> 00:05:29,090
We push to the right, and we pop from the right.

50
00:05:29,115 --> 00:05:34,093
So push s x would just be append x to s.

51
00:05:34,118 --> 00:05:41,019
So we can use the built-in append function that is available for list, and say s dot append x when we want to push.

52
00:05:41,420 --> 00:05:49,319
And it turns out that python’s list actually have a built-in function called pop, which removes the last element and returns it to us.

53
00:05:49,319 --> 00:05:55,779
So we just have to say s dot pop where s is a list, and we get exactly the behavior that we expect of our stack.

54
00:05:58,490 --> 00:06:09,339
Stacks are typically used to keep track of recursive function calls, where we want to keep going through a sequence of functions, and then returning to the last function that was called before this.

55
00:06:09,709 --> 00:06:13,660
In particular when we do backtracking, we have a stack like behavior.

56
00:06:13,660 --> 00:06:25,029
Because as we add queens and remove them, what we need to do effectively is to push the latest queen on to the stack so that when we back track we can pop it, and undo the last move.

57
00:06:29,180 --> 00:06:32,649
Another disciplined way of using a list is a queue.

58
00:06:32,660 --> 00:06:38,649
Unlike a stack which is last in first out, a queue is a first in first out sequence.

59
00:06:38,649 --> 00:06:42,660
In other words, we add at one end and we remove at another end.

60
00:06:42,660 --> 00:06:50,769
This is exactly like a queue that you see in real life where you join the queue at the back, and when your turn comes you are at the head of the queue, and then you get served.

61
00:06:50,810 --> 00:06:57,759
So add q will add x to the rear of the queue, and remove q will remove the element which is at the head of the queue.

62
00:06:59,029 --> 00:07:15,730
Once again, we can use python lists, and it turns out that it is convenient to assume that a list that represents a queue has its head at the right end and rear at the left.

63
00:07:16,250 --> 00:07:19,345
This is because we can use pop as before.

64
00:07:19,370 --> 00:07:28,889
But now when we want to insert into a queue, we can use the insert function that is provided, we have not seen this explicitly, but if you have gone through the documentation, you will find it.

65
00:07:28,889 --> 00:07:40,750
So if I have a list l, and if I insert with two arguments ‘j' and ‘x’, what it means is to put the value x before position j.

66
00:07:40,750 --> 00:07:46,396
In particular, if I insert at position 0, this has the effect of putting something before every element in the list.

67
00:07:46,421 --> 00:07:52,450
So addq q, x is just the same as q dot insert 0, x.

68
00:07:52,475 --> 00:08:01,180
In other words, push an x to the beginning, and so if I have a queue at this form, which has some values v 1, v 2 and so on.

69
00:08:02,779 --> 00:08:07,689
Then, this insert function will just put an x at the beginning.

70
00:08:08,269 --> 00:08:16,589
And as we said before, the reason we have chosen to use this notation is that we can then use pop to just remove the last element of the list.

71
00:08:16,589 --> 00:08:21,550
So queues and stacks can both be easily implemented using built-in lists.

72
00:08:23,990 --> 00:08:30,579
So one typical use of a queue is to systematically explore through a search space.

73
00:08:30,649 --> 00:08:40,950
So imagine that we have a rectangular m cross n grid, and we have a knight as a chess piece starting at a position s x, s y.

74
00:08:40,950 --> 00:08:45,700
In this case, the knight is denoted by this red circle, and this is our knight.

75
00:08:46,279 --> 00:08:53,289
Now, the knight move, if you are familiar with chess, is to move two squares up and 1 square left.

76
00:08:53,389 --> 00:08:57,690
Similarly, this is a knight move, similarly, this is a knight move and so on.

77
00:08:57,690 --> 00:09:02,450
So a knight move consists of moving 2 squares in one direction then 1 square across.

78
00:09:02,475 --> 00:09:08,919
So these are all the positions that are reachable from this initial position by the knight and there are 8 of them.

79
00:09:09,139 --> 00:09:16,440
So our question is that we have this red starting circle, and we have a green diamond indicating a target square.

80
00:09:16,440 --> 00:09:21,909
Can I hop using a sequence of knight moves from the red circle to the green diamond.

81
00:09:22,909 --> 00:09:28,090
One way to do this is to just keep growing the list of squares one can reach.

82
00:09:28,090 --> 00:09:40,330
So in the first step, we examine these 8 squares that we can reach, as we said using one move from the starting position, and we mark them as squares that are available to us to reach in one step.

83
00:09:40,549 --> 00:09:46,018
Now, we can pick one of them for instance one at the top left, and explore what we can reach from there.

84
00:09:46,043 --> 00:09:52,570
If we start in this square for instance, and now we explore its neighbors, some of its neighbors are outside the grid, so we throw them away.

85
00:09:52,570 --> 00:09:58,539
We keep only those neighbors inside the grid, and notice that one of them brings us back to the place where we started from.

86
00:09:58,940 --> 00:10:03,039
Now, we could pick another square for example, we could pick this square over here.

87
00:10:03,039 --> 00:10:11,370
And if we explore that, it will again in turn produce 8 neighbors, and some of these neighbors overlap with the yellow neighbors, and so I have indicated by joint shading of yellow and green.

88
00:10:11,370 --> 00:10:20,559
And in particular because both of them were originally reached from the starting point, of course the starting point is reached from both of them, so the starting point is both colored yellow and green.

89
00:10:21,500 --> 00:10:33,820
So as you can see, in the process of marking these squares, sometimes we mark a square twice, and we have to have a systematic way of making sure that we do this correctly, and don't get into a loop.

90
00:10:35,929 --> 00:10:44,860
So what we are trying to do is the following in the first step, we are trying to mark all squares reachable in one move from the starting point s x comma s y.

91
00:10:44,960 --> 00:10:50,281
Then, we try to mark all squares reachable from x 1 in one move and call this x2.

92
00:10:50,306 --> 00:10:56,049
Then, we will explore all squares reachable from x 2 in one move call this x 3 and so on.

93
00:10:57,409 --> 00:11:06,490
Now, one of the problems is that since we could reach x 2 from x 1 in one move, then the squares that you can reach from x 2 will include squares in x 1.

94
00:11:06,740 --> 00:11:13,480
So how do we ensure that we don't keep exploring an already marked square, and go around and round in circles.

95
00:11:13,820 --> 00:11:17,769
And related to this question is how do we know when to stop.

96
00:11:18,799 --> 00:11:24,940
Of course since we know that we are looking for the target square, if ever we mark the target square we can stop.

97
00:11:25,190 --> 00:11:29,409
On the other hand, it is possible that the target square is not reachable.

98
00:11:29,409 --> 00:11:37,785
In this case, we may keep going on exploring without ever realizing that we are fruitlessly going ahead, and we are never going to reach the target square.

99
00:11:37,810 --> 00:11:40,120
So how do we know when to stop.

100
00:11:42,049 --> 00:11:45,159
So queue is very useful for this.

101
00:11:45,159 --> 00:11:50,889
What we do is we maintain at any point a queue of cells, which remain to be explored.

102
00:11:51,350 --> 00:11:56,440
Initially the queue contains only the start node, which is s x, s y.

103
00:11:57,620 --> 00:12:03,909
At each point we remove the head of the queue, and we explore its neighbors.

104
00:12:03,909 --> 00:12:09,429
But when we explore its neighbors, we mark these neighbors, some of them may already be marked.

105
00:12:09,440 --> 00:12:16,230
So we look at a x, a y, the element we remove from the head of the queue, and we look at all the squares reachable at one step.

106
00:12:16,230 --> 00:12:22,840
So reachable means I can take one knight move and go there, and the result of this knight move doesn't take me off the board.

107
00:12:22,840 --> 00:12:30,820
So I mark all these squares which are reachable from a x and a y, some of which were already marked, some of which are marked just now.

108
00:12:30,860 --> 00:12:37,506
So what I do is, I take the ones which I had newly marked, and add them to the queue, saying that these have been newly marked.

109
00:12:37,531 --> 00:12:41,980
So now, I need to also explore these squares for what I can reach from there.

110
00:12:42,409 --> 00:12:48,340
So this guarantees that a square which has been reached once will never be reintroduced into the queue.

111
00:12:49,250 --> 00:12:52,683
And finally, we keep going until the queue is empty.

112
00:12:52,708 --> 00:12:57,658
When the queue is empty there have been no new squares added which are unmarked before they were added.

113
00:12:57,683 --> 00:13:02,590
So there is nothing more to explore, and we have gone to every square we can possibly visit.

114
00:13:04,639 --> 00:13:08,049
Here is some python pseudo code for this.

115
00:13:08,049 --> 00:13:13,750
So we are going to explore from s x, s y to t x, t y.

116
00:13:13,750 --> 00:13:21,858
We assume that we have given to us the values m and n indicating the number of rows and columns in our grid.

117
00:13:21,883 --> 00:13:28,230
So what we do is, initially we set the marked array to be 0, remember this list comprehension notation.

118
00:13:28,230 --> 00:13:35,725
So it says, 0 for i in range n gives us a list of n zeros, and we do this m times for j in range m.

119
00:13:35,756 --> 00:13:41,950
So we will get a list consisting of m blocks each block having n zeros.

120
00:13:42,200 --> 00:13:44,830
This says that initial nothing is marked.

121
00:13:44,830 --> 00:13:52,120
Now, we set up the thing by saying that we mark the starting node, and we insert the starting node in the queue.

122
00:13:52,820 --> 00:14:00,220
Now, so long as the queue is not empty, we pop one element from the queue, in this case s x, s y will come out.

123
00:14:00,220 --> 00:14:10,649
Now, there is a function which we have not written, but which will examine all the neighbors that I can reach from a x, a y, and give me a list of such pairs of nodes I can reach.

124
00:14:10,649 --> 00:14:18,879
So for each neighbor n x n y, if it is not marked, then I will mark it, and I will insert it into the queue.

125
00:14:19,460 --> 00:14:28,135
So I pull out an element from the queue to explore, look at all its neighbors, those which are not marked, I mark and put them back in the queue.

126
00:14:28,160 --> 00:14:34,269
And finally, in this case, I am not even going to check whether I mark t x or t y in the middle.

127
00:14:34,269 --> 00:14:38,610
I know that if I have a finite set of squares, at some point this process has to stop.

128
00:14:38,610 --> 00:14:51,370
So at the end I will return the value of marked at the target node t x t y, and if I have reached it, this will return one, which is true, if it is not reached, it will return 0 which is false.

129
00:14:53,750 --> 00:14:56,411
Let us look at an example of how this works.

130
00:14:56,435 --> 00:14:58,203
So here we have a 3 by 3 grid.

131
00:14:58,228 --> 00:15:02,950
Remember that the cells are 0, 1, 2 and 0, 1, 2 by our naming convention.

132
00:15:02,950 --> 00:15:05,705
We want to start from the top center square.

133
00:15:05,730 --> 00:15:11,511
So this is our source, and this here in the center is our target.

134
00:15:11,535 --> 00:15:16,351
So let's erase all these marks, and set up this thing as we would expect.

135
00:15:16,375 --> 00:15:26,579
So we say that initially the queue that we want, to have as the source node and we mark the source mode in the vertex in the grid.

136
00:15:26,604 --> 00:15:30,070
So the marking is indicated by red marks, and this is how we start.

137
00:15:30,070 --> 00:15:35,911
So our first step is to remove this from the queue, and explore its neighbors.

138
00:15:35,935 --> 00:15:40,480
Now, its neighbors are 2 0 and 2 2.

139
00:15:40,480 --> 00:15:47,220
So this means, so we will henceforth remove these brackets because it's more annoying.

140
00:15:47,220 --> 00:15:54,548
So we will just grow it like this and we say that my queue consists of 2 comma 0 and 2 comma 2.

141
00:15:54,573 --> 00:15:56,980
So this is my queue of vertices to be explored.

142
00:15:56,980 --> 00:16:02,039
So at each step, I will now remove the first element of the queue and explore its neighbors.

143
00:16:02,039 --> 00:16:08,655
When I explore the neighbors of 2 0, I will find one of them is of course is where I started from so, I only look at unmarked neighbors.

144
00:16:08,680 --> 00:16:14,500
So an unmarked neighbor is 1, 2 so I will add that back at the end of the queue.

145
00:16:15,230 --> 00:16:20,496
Now proceeding, I will take the next element of the queue which is 2 2, and look at its neighbors.

146
00:16:20,521 --> 00:16:26,559
So 2 2 can go back again to the original thing and it also has a new thing here which is 1 0.

147
00:16:27,950 --> 00:16:34,845
So continuing like this, I remove 1, 2, and I look at its neighbors.

148
00:16:34,870 --> 00:16:37,919
One of its neighbors is 2 0, but one of them is 0 0.

149
00:16:37,919 --> 00:16:40,899
So I get a new neighbor 0 0 here.

150
00:16:41,120 --> 00:16:45,129
Then, I continue taking 1 0 of the queue.

151
00:16:45,129 --> 00:16:49,620
So 1 0 is this one, and it has one new neighbor unexplored which is that one.

152
00:16:49,620 --> 00:16:54,070
So my queue now has 0, 0 followed by 0 2.

153
00:16:54,830 --> 00:17:05,650
Then, when I explore 0, 0, I get this neighbor at the bottom, which is 2, 1.

154
00:17:06,259 --> 00:17:14,289
Now, when I remove 0 2, which is this one, I find that I have explored both these neighbors, so I add nothing.

155
00:17:14,569 --> 00:17:19,259
I continue and take 2, 1, again I find both its neighbors have been explored and do nothing.

156
00:17:19,259 --> 00:17:21,339
So now, at this point the queue is empty.

157
00:17:22,220 --> 00:17:29,099
and since the queue is empty, I stop, and I find that my square of interest, namely 1 1, was not marked.

158
00:17:29,099 --> 00:17:33,039
Therefore in this case, the target is not reachable from the source node.

159
00:17:34,970 --> 00:17:47,259
So this is actually an example of breadth first search, which you will study if you look at graphs, but it just illustrates that a queue is a nice way to systematically keep track of how you explore through a search space.

160
00:17:48,230 --> 00:17:55,869
So to summarize data structures are ways of organizing information that allow efficient processing in certain contexts.

161
00:17:56,029 --> 00:18:01,269
So we saw that python has a built-in implementation of sets.

162
00:18:01,609 --> 00:18:06,240
We also saw that we can take sequences and use them in two structured ways.

163
00:18:06,240 --> 00:18:16,390
So a stack is a last in first out list, and we can use this to keep track of recursive computations, and queues are first in first out list that are useful for breadth first exploration.

