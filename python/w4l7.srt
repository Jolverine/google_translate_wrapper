1
00:00:02,400 --> 00:00:06,009
So, quite often we want to do something to an entire list.

2
00:00:06,460 --> 00:00:12,981
For instance, we might want to replace every item in the list by some derived value f of x.

3
00:00:13,341 --> 00:00:19,005
So, we would write a loop as follows: for every x in l, replace x by f of x.

4
00:00:21,620 --> 00:00:26,452
Now, we could write a function to do this which does is for different lists and different values of l.

5
00:00:27,418 --> 00:00:43,149
So, we could say define apply list which takes a function f and a list l, and for every x in l, it just replaces x by f of x and since l, a list is a mutable item, this will update list in the calling function as well.

6
00:00:45,990 --> 00:00:49,810
So, Python has a built in function map which does precisely this.

7
00:00:49,810 --> 00:00:55,179
So, map f l applies f in turn each element of l.

8
00:00:57,530 --> 00:01:11,750
Now, although you would think that if you take a list say x 1, x 2 and you apply map and you get f of x 1, f of x 2, the output of map should be another list.

9
00:01:11,774 --> 00:01:19,090
Unfortunately in Python 3, and this is another difference between Python 3 and Python 2, the output of map is not a list.

10
00:01:20,685 --> 00:01:24,040
So, you need to use the list function like we did before.

11
00:01:24,040 --> 00:01:27,159
We need to say list of map f l  to get a list.

12
00:01:28,819 --> 00:01:37,930
And you can however use the output of map directly in a for loop by saying for i in list map f l or you can even say for i in map f l, this will work.

13
00:01:38,480 --> 00:01:47,049
So, you don't need to use the list notation if you just wanted to index in a loop but if you want to use it as the list you must use the list function to convert it.

14
00:01:47,049 --> 00:01:52,417
And this is pretty much what happens with functions like range and d dot keys and so on.

15
00:01:52,441 --> 00:01:55,569
So these are all things which gives us sequences of values.

16
00:01:55,569 --> 00:01:58,500
These sequences are not absolutely list.

17
00:01:58,500 --> 00:02:01,360
They can be used in for functions , okay?

18
00:02:01,740 --> 00:02:09,699
But, if you want to use them as list and manipulate them as list you must use list to convert them from their sequences to the list format.

19
00:02:13,650 --> 00:02:20,020
Another thing that we typically want to do is to take a list and extract values that satisfy a certain property.

20
00:02:20,405 --> 00:02:28,141
So, we might have a list of integers called number list and from this we might want to extract the list of primes.

21
00:02:28,165 --> 00:02:38,680
So, we said start off by saying that the list of primes we want is empty and now we run through the number list and for each number in that list we apply the test: is it a primes?

22
00:02:38,680 --> 00:02:42,189
If it is a prime then we append the list to our output.

23
00:02:42,785 --> 00:02:46,150
So, we start with a list x 1, x 2 and so on.

24
00:02:46,150 --> 00:02:53,289
And then we apply the test and some of them will pass and some of them will succeed, some of them will fail, okay?

25
00:02:53,349 --> 00:03:00,009
And at the end wherever the things pass, those items will emerge in the output.

26
00:03:02,960 --> 00:03:19,043
So, in general, we could write a select function which takes a property and a list and it creates a sub list by going through every element to list checking if the property holds and for those element which the property holds, appending it to the sub list.

27
00:03:20,493 --> 00:03:31,800
So, the difference between select and our earlier map function is that property is not an arbitrary function it doesn't manipulate l all it does is it checks whether a property is true or not.

28
00:03:31,800 --> 00:03:36,535
So, property will be a function which takes an element in the list and tells us true or false.

29
00:03:36,589 --> 00:03:39,390
If it is true, it gets copied to the output.

30
00:03:39,414 --> 00:03:41,680
If it is false, it gets has discarded.

31
00:03:45,390 --> 00:03:49,240
So, there is a built in function for this as well, it's called filter.

32
00:03:49,240 --> 00:03:53,469
So, filter takes a function p, it returns true or false for every element.

33
00:03:53,728 --> 00:04:01,210
And it pulls out precisely that sub list of l for which every item in l which falls into the sub list satisfies p.

34
00:04:04,650 --> 00:04:06,550
So let's look at a concrete example.

35
00:04:06,550 --> 00:04:14,830
Supposing, we have the list of numbers from 0 to 99, we want to first pull out only the even numbers in the list so that's the filter operation.

36
00:04:15,256 --> 00:04:19,042
And then for each of these even numbers, we want to square them, okay?

37
00:04:19,595 --> 00:04:27,820
So, here we take the even numbers by using the filter.

38
00:04:29,732 --> 00:04:31,805
And then we map square.

39
00:04:31,830 --> 00:04:33,519
So, then we get a list.

40
00:04:33,732 --> 00:04:36,628
And then of course, having got this list then we can add it up, right?

41
00:04:36,653 --> 00:04:38,889
So, the sum is not part of this function.

42
00:04:38,889 --> 00:04:46,614
If we want to first extract the squares of the even numbers and that can be done using a combination of filter and then map.

43
00:04:47,051 --> 00:04:54,430
So, filter first gives us the even numbers and then map gives us the squares and the square is defined here and is even is defined here.

44
00:04:58,859 --> 00:05:03,373
So, there is a very neat way of combining map and filter without using that notation.

45
00:05:04,080 --> 00:05:07,233
So, let's get to it through a more simple mathematical example.

46
00:05:08,513 --> 00:05:23,749
So, you might have studied in school from right hand, right angle triangles that by Pythagoras’ theorem, we know that if x,y and z are the lengths of the two sides and the hypotenuse respect to v, then x squared + y squared will be z square.

47
00:05:24,066 --> 00:05:32,649
So, Pythagorean triple is a set of integers say 3,4 and 5 for example, such that x squared plus y squared is z squared.

48
00:05:32,649 --> 00:05:35,889
3 squared is 9, 4 squared is 16, 5 squared is 25.

49
00:05:39,136 --> 00:05:47,740
So, let us say we want to know all the integer values of x,y and z, whose values are below n such that x,y and z form a Pythagorean triplet.

50
00:05:50,515 --> 00:05:55,368
In conventional mathematics notation, we might see this kind of expression.

51
00:05:55,561 --> 00:06:06,220
It says give me all triples x,y and z, such that, this bar stands for such that, such that x,y and z all lie between 1 and n.

52
00:06:07,129 --> 00:06:10,329
And in addition x squared + y squared is equal to z squared .

53
00:06:11,529 --> 00:06:15,251
So, this is, in some sense, where we get the values from.

54
00:06:15,275 --> 00:06:19,389
This is an existing set we have x ranging from 1 to n, y ranging from 1 to n, z ranging from

55
00:06:19,952 --> 00:06:22,329
And we put together all possible combinations.

56
00:06:22,329 --> 00:06:27,234
Then, we take out those combinations to satisfy a given property, x squared + y squared equals z squared.

57
00:06:27,585 --> 00:06:29,439
And those are the ones that we extract.

58
00:06:31,952 --> 00:06:35,335
So, in set theory this is called set comprehension okay

59
00:06:35,360 --> 00:06:41,766
This is a way of building a new set by applying some conditional things to an old set.

60
00:06:41,791 --> 00:06:45,301
This is also implicitly applying a kind of a tripling operator.

61
00:06:45,326 --> 00:06:51,293
It takes 3 separate sets, x from 1 to n, y from 1 to n, z from 1 to n, and combines them into triples.

62
00:06:51,486 --> 00:07:02,925
So, there is a filtering process by which you only pull out those triples where x squared + y squared is z squared and then there is a a manipulating step where you combine them into a single triple x,y,z.

63
00:07:03,579 --> 00:07:07,720
But, in general the main point is that you are building a new set from existing sets.

64
00:07:08,595 --> 00:07:15,461
So, what Python does, and many other languages also from which Python is inspired to, is allow us to extend this notation to lists.

65
00:07:15,485 --> 00:07:25,663
So, this actually comes from a style of programming called functional programming which, for where this kind of notation is there and Python has borrowed it and it works quite well.

66
00:07:28,582 --> 00:07:34,436
So, here is how we will write our earlier thing which we had said, the squares of the even numbers below 100.

67
00:07:34,676 --> 00:07:38,232
So, earlier we had given a, map filter thing

68
00:07:38,420 --> 00:07:44,829
So, we had said we would take a range and we would filter it with is even then we would do a map of square.

69
00:07:45,256 --> 00:07:53,589
So, in Python there is an implicit perpendicular line below, before the for from the set notation.

70
00:07:53,589 --> 00:07:59,290
So, it says take square of x for i in range 100 such that is even.

71
00:08:00,363 --> 00:08:08,139
So, we have here three parts. So, we have a generator which tells us where to get the values from.

72
00:08:08,139 --> 00:08:12,990
List comprehension or set comprehension pulls out values from an existing set of list.

73
00:08:12,990 --> 00:08:16,835
So, we first generate a list in this case, the list range 100

74
00:08:16,860 --> 00:08:20,560
use another list we could use i for in any l, just like a for.

75
00:08:21,203 --> 00:08:25,480
Then we apply a filter to it, which are the values from this list which we are going retain.

76
00:08:25,916 --> 00:08:28,523
And then for each of those values we can do something here.

77
00:08:28,770 --> 00:08:31,750
In this case we square and that will be our output,

78
00:08:31,790 --> 00:08:38,338
So this is how we will generate a list using map and filter without using the words map and filter in the thing.

79
00:08:38,362 --> 00:08:46,330
We just use the for, for the generator if for the filter and the map is implicit by just applying a function to the output of the generator input .

80
00:08:50,029 --> 00:08:52,870
So let 's go back to the Pythagorean triple example.

81
00:08:53,163 --> 00:08:56,529
We want all Pythagorean triples with x,y,z below 100.

82
00:08:56,923 --> 00:09:02,655
So, this as we said requires us to cycle through all values of x, y and z in that range.

83
00:09:02,808 --> 00:09:10,162
So, it's a little bit more complicated than do one rated before, where we have to add a single generator all the values in range 0 to 1.

84
00:09:12,939 --> 00:09:13,783
So, it is simple enough.

85
00:09:13,837 --> 00:09:16,289
You write it with multiple forms.

86
00:09:16,289 --> 00:09:27,490
So, we say i want x,y,z for x in range 100, for y in range 100, for z in range 100, provided x squared + y squared equal to z squared so that is written with the if.

87
00:09:27,490 --> 00:09:34,960
Now just to fit on the slide i have split it up into multiple lines, but actually this will be a single line of Python.

88
00:09:38,282 --> 00:09:43,087
So, in what order will these be generated, well it will be exactly like a nested loop.

89
00:09:43,111 --> 00:09:48,549
Imagine we had written a loop which we said for x in range 100, for y in range 100, for z in range 100.

90
00:09:49,502 --> 00:09:56,442
So, what happens here is that first a value 0 will be fixed for x then the value 0 would be y,

91
00:09:56,509 --> 00:09:59,083
So, the first pair triple that comes out to be 0,0,0.

92
00:09:59,890 --> 00:10:02,372
Then the value of z will change.

93
00:10:02,396 --> 00:10:04,090
The inner most loop changes next.

94
00:10:04,090 --> 00:10:05,969
So, the next one will be 001.

95
00:10:05,993 --> 00:10:08,394
So, this is x, this is y, this is z.

96
00:10:08,779 --> 00:10:12,700
So, in this way we keep going until we do 0,0,99.

97
00:10:12,700 --> 00:10:18,935
So, when this reach 99, then this for loop will exit and we will go to 1.

98
00:10:18,960 --> 00:10:26,377
So i will get 0,1,0, to 01,0,1, 99 and so on.

99
00:10:26,401 --> 00:10:33,519
So the inner most for, so z will cycle first then y and then x will cycle so just remember that.

100
00:10:35,213 --> 00:10:36,870
So let's see how this works in Python.

101
00:10:36,894 --> 00:10:39,518
So let's first begin by defining square 

102
00:10:39,545 --> 00:10:40,990
square of x.

103
00:10:41,972 --> 00:10:44,200
return x times x

104
00:10:47,006 --> 00:10:50,049
We can define iseven of x 

105
00:10:50,543 --> 00:10:55,419
Check that the remainder of x divided by 2 is 0.

106
00:10:56,750 --> 00:11:00,460
So, we have square 8, 64

107
00:11:00,485 --> 00:11:06,940
iseven 67 should be false. iseven 68 should be true and so on.

108
00:11:07,253 --> 00:11:08,856
So, now we have list comprehension.

109
00:11:08,881 --> 00:11:18,730
So let us look at the set of square x, for x in range 100 such that x is even.

110
00:11:21,905 --> 00:11:26,440
So, we see now that 0 is there, so 0 square,2 square,4 square,6 square and so on.

111
00:11:27,330 --> 00:11:29,318
So, this is our list comprehension.

112
00:11:29,342 --> 00:11:31,330
Now, let's do the Pythagorean triple.

113
00:11:31,330 --> 00:11:47,649
So, we say we want x,y,z, for x in range 100, y in range, 100 for z in range 100.

114
00:11:48,350 --> 00:11:56,559
So, this is our 3 generators with the condition that x times x plus y times y is equal to z times z.

115
00:12:00,646 --> 00:12:03,125
Now, you see a lot of have come.

116
00:12:03,149 --> 00:12:06,220
In particular, you should see in the early stages somewhere

117
00:12:07,199 --> 00:12:10,059
which we are familiar with like 3,4,5 and so on.

118
00:12:10,059 --> 00:12:12,576
But you also see some nonsensical like 4,0,4.

119
00:12:12,601 --> 00:12:20,545
So, we should probably have done this better but we won't worry about it, but what i want to emphasize is that you see things like 

120
00:12:21,080 --> 00:12:28,000
Say you see 0,77,77,1 but let us see for instance you say, you see 3,4,5, right?

121
00:12:28,370 --> 00:12:30,309
So, we saw 3,4,5 somewhere.

122
00:12:30,320 --> 00:12:31,355
So, 3,4,5.

123
00:12:31,379 --> 00:12:34,509
But you will also see later on 4,3,5.

124
00:12:34,729 --> 00:12:38,289
Now, one might argue that 3,4,5 and 4,3,5 are the same triple.

125
00:12:38,662 --> 00:12:40,990
So, how do we eliminate this duplicate?

126
00:12:45,682 --> 00:12:52,169
So, we can have a situation just like could have in a for loop where the later loop can depend on inner loop.

127
00:12:52,169 --> 00:12:55,120
The outer loop says i to some, i goes from something to something.

128
00:12:55,393 --> 00:12:58,029
The later loop can say that j starts from i and goes 

129
00:12:58,730 --> 00:13:05,422
So, for instance, we can now rewrite our Pythagorean triples to say that x is in the range 100 but y doesn't start at 0.

130
00:13:05,447 --> 00:13:08,710
It starts from x onwards. So, y is never smaller than x.

131
00:13:08,710 --> 00:13:13,675
And z is never smaller than y, so z is also never smaller than x because y itself is never smaller than x.

132
00:13:14,142 --> 00:13:17,980
And this version will actually eliminate duplicates.

133
00:13:22,289 --> 00:13:28,353
So, here is our earlier definition of Pythagoras where we had x,y and z unconstrained.

134
00:13:28,750 --> 00:13:34,450
So, what i do is i go back and i say that y is not in range 100 but y is in range x to 100.

135
00:13:35,156 --> 00:13:37,809
And z is in range y to 100 

136
00:13:38,543 --> 00:13:45,967
And now, you will see a much smaller list and in particular you will see that in every sequence it is generated x is less than equal to y is less than equal to z.

137
00:13:46,380 --> 00:13:48,546
And you only get one copy of things like 3,4,5.

138
00:13:48,820 --> 00:13:51,329
So, you see 3,4,5  but you don't see 4,3,5.

139
00:13:51,355 --> 00:13:52,432
3,4,5 is here.

140
00:13:52,655 --> 00:13:54,009
Next one is 5,12,30

141
00:13:54,130 --> 00:13:55,419
4,3,5 is eliminated.

142
00:13:55,519 --> 00:14:01,339
So, the key thing is that generators can be dependent on outer generators.

143
00:14:01,363 --> 00:14:04,659
Inner generators can be dependent on the outer generators.

144
00:14:08,049 --> 00:14:12,809
So, this List comprehension notation is particularly useful for initializing this.

145
00:14:12,809 --> 00:14:17,129
For example, for initializing matrices when you are doing matrix like operations.

146
00:14:17,154 --> 00:14:21,104
So, supposing i want to initialize a 4 by 3 matrix to all 0s.

147
00:14:21,355 --> 00:14:26,928
So, 4 by 3 matrix has 4 rows in 3 columns and i am using the convention that i store it row wise.

148
00:14:26,953 --> 00:14:31,870
So, store the first row so it will be three entries for the first row then three entries for second row and so on.

149
00:14:32,875 --> 00:14:41,649
So, here is an initialization, it says l consists of something for, the outer thing says for, this is for each row.

150
00:14:44,796 --> 00:14:48,013
So it is something for each row.

151
00:14:48,037 --> 00:14:52,314
So, for 4 rows 0,1,2,3,i do something and what is that something?

152
00:14:52,394 --> 00:14:55,108
I create a list of 0s of size 3.

153
00:14:55,330 --> 00:15:03,580
So, each row j from 0 to 3 consists of columns  0,1,2 which are 0s.

154
00:15:04,785 --> 00:15:11,379
So, this will actually generate the correct sequence that we saw, that we would need to initialize such a matrix.

155
00:15:15,453 --> 00:15:18,856
So, here is that List comprehension notation for initializing the matrix.

156
00:15:19,056 --> 00:15:22,911
So, it says for every j in range 4, 

157
00:15:23,051 --> 00:15:26,309
We create a list and that list itself has 0 for i

158
00:15:26,309 --> 00:15:31,823
And if you do this and look at n, then correctly it has three zeros and three zeros and three zeros 4 times.

159
00:15:31,848 --> 00:15:37,545
So, these are 4 rows .

160
00:15:39,662 --> 00:15:44,470
So, suppose instead you split this initialization into two steps.

161
00:15:44,470 --> 00:15:49,765
We first create a list of three zeros called zero list which says 0 for i in range 3.

162
00:15:49,789 --> 00:15:51,970
So, this creates a list of 3 zeros.

163
00:15:51,970 --> 00:15:54,855
And then we copy this list 4 times in the 4 rows.

164
00:15:54,880 --> 00:15:59,799
So, we say the actual matrix L has 4 copies  of zero list.

165
00:16:00,392 --> 00:16:06,779
Now, we go and change one entry, say we change entry 1 in row 1.

166
00:16:07,029 --> 00:16:08,933
So, from the top it is actually second row.

167
00:16:08,957 --> 00:16:12,652
So, it's the second row second column if you want to think in normal terms.

168
00:16:12,832 --> 00:16:16,389
So, we take the list one which is the second list.

169
00:16:16,389 --> 00:16:19,750
Now, what do you expect is the output of this?

170
00:16:29,029 --> 00:16:36,399
So, here we have the zerolist and then we say l is 4 copies  of zerolist for j in range 4.

171
00:16:39,216 --> 00:16:41,575
So, superficially l looks exactly the same.

172
00:16:42,097 --> 00:16:46,600
Now, we say l 1 1 is equal to 7.

173
00:16:48,433 --> 00:16:53,289
And if you look at L now, we will find that we have not one 7, but four copies  of 7

174
00:16:54,619 --> 00:16:57,909
So, this is apparently something which we did not expect.

175
00:16:59,613 --> 00:17:06,564
So, the output after l 1 1  is equal to 7 is 0,7,0,0,7,0,0,7,0,0,7,0

176
00:17:06,915 --> 00:17:07,922
And why is this happening?

177
00:17:07,956 --> 00:17:15,750
Well, that's because by making a single zerolist and then making 4 copies  of it, we have effectively created four names of the same list.

178
00:17:15,750 --> 00:17:26,049
So, whether we access it through l 0 or l 1 or l 2 or l 3, all four of them are pointing to the same zerolist, so any one of those updates would actually update all 4 rows.

179
00:17:26,049 --> 00:17:43,930
So, if you want to create a two dimensional matrix and initialize it, make sure we initialize it in one shot using a nested range and not in two copies like this because these two copies  will unintentially combine two rows into copies  of the same thing and updates to one row will also update another.

180
00:17:47,472 --> 00:18:00,220
So, to summarize map and filter are very useful functions to manipulating lists and Python provides, like many other programming languages based on function programming a notation for List comprehension to combine map and filter.

181
00:18:00,938 --> 00:18:08,950
And one of the uses that we saw for List comprehension is to correctly initialize two dimensional or a multidimensional list some default values.

