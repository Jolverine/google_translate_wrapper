1
00:00:03,319 --> 00:00:11,439
We have seen how to implement data structures such as stacks queues and heaps using the built in list type of python.

2
00:00:11,869 --> 00:00:17,280
It turns out that we can go beyond the built in types in python and create our own data types

3
00:00:17,280 --> 00:00:21,460
So, we will look at this in more detail in this week's lectures.

4
00:00:21,679 --> 00:00:25,260
So, let us revisit what we mean by a data structure.

5
00:00:25,260 --> 00:00:33,420
So, a data structure is basically an organization of information whose behavior is defined through an interface.

6
00:00:33,420 --> 00:00:42,416
So, an interface is nothing but the allowed set of operations. For instance, for a stack the allowed set of operations are push and pop.

7
00:00:42,441 --> 00:00:45,820
And of course, we can also query whether a stack is empty or not.

8
00:00:45,820 --> 00:00:53,600
Likewise, for a queue the only way we can modify a queue is to add something to the tail of the queue using the function add queue

9
00:00:53,625 --> 00:00:57,789
and remove the element at the head of the queue using the function remove queue.

10
00:00:58,280 --> 00:01:06,700
And for a max< heap for instance, we have the functions insert to add an element, and delete max which removes the largest element from the heap.

11
00:01:07,670 --> 00:01:18,239
Now, just because we implement a heap as a list it does not mean that the functions that are defined for lists are actually legal for the heap. So, if we have a heap

12
00:01:18,264 --> 00:01:21,262
h which is implemented as a python list.

13
00:01:21,287 --> 00:01:28,689
though the list will allow an append function the append function on its own does not insert a value and maintain the heap property.

14
00:01:28,689 --> 00:01:33,250
So, in general the call such as h dot append seven would not be legal.

15
00:01:35,750 --> 00:01:42,700
So, we want to define new abstract data types in terms of the operations allowed.

16
00:01:43,549 --> 00:01:48,215
We do not want to look at the implementation and ask whether it is a list or not,

17
00:01:48,240 --> 00:01:57,159
because we do not want the implementation to determine what is allowed. We only want the actual operations that we define as the abstract interface to be permitted.

18
00:01:57,500 --> 00:02:03,909
So for instance, if we have a stack S and we push a value v then.

19
00:02:03,909 --> 00:02:12,550
The property of a stack guarantees that if we immediately apply pop the value we get back is the last value pushed and therefore we should get back v.

20
00:02:12,550 --> 00:02:17,296
In other words, if we execute this sequence we first do S dot push.

21
00:02:17,321 --> 00:02:22,569
and then we do a pop then the value that we pushed must be the value that we get back.

22
00:02:22,569 --> 00:02:26,642
So, this is an abstract way of defining the property of a stack.

23
00:02:26,667 --> 00:02:32,919
and how push and pop interact, without actually telling us anything about how the internal values are represented.

24
00:02:33,620 --> 00:02:39,400
In the same way if we have an empty queue and we add to it

25
00:02:39,560 --> 00:02:44,229
two elements u and v and then we remove the head of the queue.

26
00:02:44,229 --> 00:02:51,550
Then we expect that we started with an empty queue and we put in u from this end and then we put in a v.

27
00:02:51,550 --> 00:02:57,759
then the element that comes out should be the first element namely u. In other words assuming that this is empty.

28
00:02:58,099 --> 00:03:02,132
Then, if we add u and add v and then remove the head.

29
00:03:02,157 --> 00:03:05,469
We should get back the first element that we put in namely u.

30
00:03:07,939 --> 00:03:15,266
So, the important thing is that we would like to define the behavior of a data type without reference to the implementation.

31
00:03:15,291 --> 00:03:20,759
Now, this can be very tedious because you have to make sure that you capture all the properties between functions.

32
00:03:20,759 --> 00:03:25,150
but this can be done and this is technically how an abstract data type is defined.

33
00:03:25,150 --> 00:03:28,949
Now, in our purposes we will normally define it more informally.

34
00:03:28,949 --> 00:03:37,599
And we will make a reference to the implementation, but we definitely do not want the implementation to determine how these functions work.

35
00:03:37,599 --> 00:03:42,430
In other words, we should be able to change one implementation to another one.

36
00:03:42,590 --> 00:03:49,900
Such that the functions behave the same way, and the outside user has no idea which implementation is used.

37
00:03:49,900 --> 00:04:00,039
Now, this is often the case when we need to optimize the implementation. We might come up with an inefficient implementation, and then optimize it. For instance, we saw that for a priority queue

38
00:04:00,039 --> 00:04:05,460
We could actually implement it as a sorted list and we could implement insert.

39
00:04:05,460 --> 00:04:09,250
The insert operation in a sorted list will take order n time.

40
00:04:09,250 --> 00:04:12,789
but delete max would just remove the head of the list.

41
00:04:12,789 --> 00:04:19,600
Now, this is not optimal because over a sequence of n inserts and deletes this takes time order n square.

42
00:04:19,600 --> 00:04:24,579
So if we replace the internal implementation from a sorted list to a heap we get better behavior

43
00:04:24,579 --> 00:04:35,519
But in terms of the actual behavior, the user does not see any difference in values after a sequence of inserts and delete max between the sorted list implementation.

44
00:04:35,519 --> 00:04:46,500
and the heap implementation. Perhaps there is a perception that one is faster than the other but the actual correctness of the implementation should not be affected by how you choose to represent the data.

45
00:04:46,500 --> 00:04:49,660
So, this is the essence of defining an abstract data type.

46
00:04:50,810 --> 00:04:58,329
So, good way to think of an abstract data type is as a black box which allows limited interaction.

47
00:04:58,329 --> 00:05:01,060
Imagine something like an ATM machine.

48
00:05:01,060 --> 00:05:04,060
So, we have the data structure as a black box.

49
00:05:04,060 --> 00:05:10,149
and we have certain buttons, which are the public interface. These are the functions that we are allowed to use.

50
00:05:10,149 --> 00:05:21,490
So, in this picture imagine this is a stack and the buttons we are allowed to use are pop and push. We are allowed to remove the top element from the stack in order to put an element into the stack.

51
00:05:21,500 --> 00:05:25,980
Now, this requires us to also add and view things from the stack.

52
00:05:25,980 --> 00:05:31,600
So, we also have a slot for input which is shown as a kind of a thing at the bottom here.

53
00:05:32,029 --> 00:05:33,649
We have a slot for input.

54
00:05:33,674 --> 00:05:38,129
And we have a way to receive information about the state of the stack.

55
00:05:38,129 --> 00:05:40,930
So, we can imagine that we have some kind of a display.

56
00:05:40,930 --> 00:05:45,996
So, this is typically how we would like to think of a

57
00:05:46,021 --> 00:05:50,500
data structure we do not want to know what is inside the black box we just want to specify

58
00:05:50,500 --> 00:05:56,189
that if we do a sequence of button pushes and we start supplying input through the input box.

59
00:05:56,189 --> 00:05:58,420
What do we expect to see in the display?

60
00:05:58,879 --> 00:06:01,920
So, other than this no other manipulation should be allowed.

61
00:06:01,920 --> 00:06:09,850
So we are not allowed to exploit what is inside the box in order to quickly get access say to the middle of a stack or the middle of a queue.

62
00:06:10,275 --> 00:06:20,800
So, we do not want such operations we only want those operations which the externally visible interface or the buttons, in this case of a black box picture allow us to use.

63
00:06:22,639 --> 00:06:28,660
So, in a sense this is already implemented when we use the built in data types of python.

64
00:06:28,790 --> 00:06:34,779
If we announce that the name l is of type list by setting l to the empty list.

65
00:06:34,779 --> 00:06:41,892
then immediately python will allow us to use operations like append and extend on this list.

66
00:06:41,917 --> 00:06:45,220
because it is of list type and not dictionary type.

67
00:06:45,220 --> 00:06:50,740
We would not be able to execute an operation such as keys which is defined for dictionaries are not lists.

68
00:06:51,050 --> 00:06:59,350
Likewise, if we define d to be an empty dictionary then we can use a function such as d dot values to get the list of values currently stored

69
00:06:59,350 --> 00:07:04,120
but we cannot manipulate d as a list. So, we cannot say d dot append it will give us an error

70
00:07:04,120 --> 00:07:12,449
So, python uses the type information that it has about the value we have assigned to a name to determine what functions are legal.

71
00:07:12,449 --> 00:07:14,579
This is exactly what we are trying to do.

72
00:07:14,579 --> 00:07:26,670
So, we are trying to say that the data type on its own should allow only certain limited types of access whose behavior is specified without telling us anything about the internal implementation.

73
00:07:26,695 --> 00:07:29,100
Remember for instance we saw that in the dictionary.

74
00:07:29,100 --> 00:07:32,129
We add a sequence of values in a particular order.

75
00:07:32,129 --> 00:07:40,600
When we ask for the values after sometime they may not return in the same order because internally there is some optimization to make it fast to get values.

76
00:07:40,600 --> 00:07:44,850
So, we have no idea actually how a dictionary is implemented inside.

77
00:07:44,850 --> 00:07:52,649
But what we do know is that if we provide a valid key then we will get the value associated with that key. We do not ask how this is done.

78
00:07:52,649 --> 00:07:58,600
And we do not know whether from one version of python to the next, the way in which this is implemented changes.

79
00:07:59,959 --> 00:08:05,399
So, our question is that, instead of using the built in list.

80
00:08:05,399 --> 00:08:08,740
For stacks queues and heaps and other data structures.

81
00:08:08,740 --> 00:08:16,240
Can we also define a data type in which certain operations are permitted according to the type that we start with.

82
00:08:19,009 --> 00:08:26,470
So, this is one of the main things which is associated with a style of programming called object oriented programming.

83
00:08:26,569 --> 00:08:37,120
So, in object oriented programming we can provide data type definitions for new data types in which we do precisely what we have been talking about.

84
00:08:37,120 --> 00:08:42,730
We describe the public interface that is the operations that are allowed on the data.

85
00:08:43,129 --> 00:08:49,500
and separately we provide an implementation which should be private.

86
00:08:49,500 --> 00:08:55,992
We will discuss later that in python we do not actually have full notion of privacy because of the nature of the language.

87
00:08:56,017 --> 00:09:04,179
But ideally the implementation should not be visible outside, only the interface should allow the user to interact with the implemented data.

88
00:09:04,309 --> 00:09:11,289
And of course, the implementation must be such that the functions do not have such that are visible to the user behave correctly.

89
00:09:11,289 --> 00:09:16,740
So, here for instance if we had a heap the public interface would say insert and delete max.

90
00:09:16,740 --> 00:09:21,129
The private implementation may be a sorted list or it may be a heap.

91
00:09:21,139 --> 00:09:22,450
and then.

92
00:09:22,519 --> 00:09:28,679
we would then have to ensure that. that if we are using a sorted list we implement delete max and insert in the correct way

93
00:09:28,679 --> 00:09:32,740
and if we switch from that to a heat, the priority queue operations remain the same.

94
00:09:34,850 --> 00:09:42,549
So, in the terminology of object oriented programming there are two important concepts classes and objects.

95
00:09:42,549 --> 00:09:47,460
So a class is a template very much like a function definition is a template.

96
00:09:47,460 --> 00:09:51,789
when we say def and define a function the function does not execute.

97
00:09:51,789 --> 00:09:58,500
it just gives us a blue print saying that, this is what would happen if this function were called with a particular argument

98
00:09:58,500 --> 00:10:02,190
And that argument we substituted for the formal parameter in the function.

99
00:10:02,190 --> 00:10:05,500
and the code in the function will be executed to the corresponding value.

100
00:10:05,500 --> 00:10:10,600
In the same way a class sets up a blue print or a template for a data type.

101
00:10:10,600 --> 00:10:14,850
It tells us two things. It tells us what is the internal implementation

102
00:10:14,850 --> 00:10:22,470
How is data stored and it gives us the functions that are used to implement the actual public interface.

103
00:10:22,470 --> 00:10:29,620
So, how you manipulate the internal data in order to effect the operations that the public interface allows.

104
00:10:30,889 --> 00:10:40,922
And now once we have this template we can construct many instances of it. So, you have a blue print for a stack you can construct many independent stacks.

105
00:10:40,947 --> 00:10:45,399
each independent stack has its own data that stacks do not interfere with each other

106
00:10:45,399 --> 00:10:50,350
Each of them has a copy of the function that we have defined associated with it.

107
00:10:50,350 --> 00:11:02,289
So, the main difference from classical programming is that in classical programming you would have for instance a function like say push.

108
00:11:02,539 --> 00:11:07,740
defined and it will have two parameters typically a stack and a value.

109
00:11:07,740 --> 00:11:12,509
So, you have one function and then you provided the stack that you want to manipulate.

110
00:11:12,509 --> 00:11:16,809
On the other hand, now we have several stacks S one

111
00:11:16,809 --> 00:11:21,210
S two, S three which are created as instances of the class.

112
00:11:21,210 --> 00:11:24,879
and logically each of them has its own push function.

113
00:11:24,879 --> 00:11:31,049
There is a push associated with S one there is a push associated S two, there is push associated with S three and so on.

114
00:11:31,049 --> 00:11:37,929
and each of them is a copy of the same function derived from this template but implicitly attached to a single object.

115
00:11:37,929 --> 00:11:41,309
So, this is just a slight difference in perspective.

116
00:11:41,309 --> 00:11:49,000
Instead of having a function to which you pass the object that you want to manipulate, you have the object and you tell it what function to apply to itself.

117
00:11:51,379 --> 00:12:03,460
So, let us look at a kind of example, this would not be a detailed example, it will just give you a flavor of what we are talking about. So, here is a skeleton of a definition of a class heap.

118
00:12:03,460 --> 00:12:07,840
So now, instead of using a built in list we want to define our own data type heap.

119
00:12:08,210 --> 00:12:11,262
So there are some function definitions

120
00:12:11,287 --> 00:12:23,500
these things are these depth statements and these correspond to definition of the functions and what we will see is that inside these definitions we will have values which are stored in each copy of the heap.

121
00:12:25,070 --> 00:12:28,539
So, just to get a little bit of an idea about how this would work.

122
00:12:29,240 --> 00:12:33,870
When we create an object of type heap we call it like a function.

123
00:12:33,870 --> 00:12:37,240
So, we say h is equal to heap l.

124
00:12:37,240 --> 00:12:47,500
So, this implicitly says give me an instance of the class heap with the initial value l passed to it.

125
00:12:47,539 --> 00:12:50,669
This calls this function init which is why it is called init.

126
00:12:50,669 --> 00:12:53,195
So, in it is what is called a constructor.

127
00:12:53,220 --> 00:12:58,509
The constructor is a function that is called when the object is created and sets it up initially.

128
00:12:58,509 --> 00:13:06,509
In this particular case our constructor is presumed to take an arbitrary list of values say 14, 32, 15 and heapify it.

129
00:13:06,509 --> 00:13:12,700
So, somewhere inside the code of init there will be a heapification operation which we have not actually shown in this slide.

130
00:13:13,129 --> 00:13:16,979
So, this is how you create objects. You first define a class

131
00:13:17,004 --> 00:13:19,269
We will look at a comprehensive example soon.

132
00:13:19,269 --> 00:13:23,769
we define a class and then you call the class sort of like a function.

133
00:13:23,769 --> 00:13:30,105
and the name that is attached to this function call or this class call becomes a new object.

134
00:13:30,130 --> 00:13:35,190
Now, as we said we have functions like insert and delete max defined for heaps.

135
00:13:35,190 --> 00:13:39,029
but it is like we have a separate copy of this function for each individual heap.

136
00:13:39,029 --> 00:13:43,090
So, in this case we have created a heap h, so we want to tell h.

137
00:13:43,090 --> 00:13:52,659
to insert in yourself the value 17. So, we write that as insert with respect to h. So, h dot insert 17 as opposed to insert h

138
00:13:53,179 --> 00:13:57,759
17 which would be the normal functional style of writing this.

139
00:13:57,759 --> 00:14:07,360
We will normally pass it the heap and the value. Here, instead we say given the heap h applied to the heap h the function insert with the argument 17.

140
00:14:07,669 --> 00:14:13,179
The next line says apply to the heap h the function insert with the value 28.

141
00:14:13,549 --> 00:14:21,909
And then for instance we can now ask h to return the maximum value by saying h dot delete max and store the returned value in the name v.

142
00:14:24,889 --> 00:14:32,230
So, what we would like to emphasize is that an abstract data type should be seen as a black box.

143
00:14:32,230 --> 00:14:37,980
A black box has a public interface the buttons that you can use to update and query the data type.

144
00:14:37,980 --> 00:14:46,570
to add things delete things and find out whether the data type is empty and so on and inside we have a private implementation.

145
00:14:46,570 --> 00:14:53,409
This actually stores the data in some particular way to make the public functions work correctly.

146
00:14:53,409 --> 00:15:00,850
But the important thing is changing the private implementation should not affect how the functions behave in terms of input and output

147
00:15:00,850 --> 00:15:03,659
they may behave differently in terms of efficiency.

148
00:15:03,659 --> 00:15:08,049
You might see that one version is faster than the other

149
00:15:08,179 --> 00:15:11,810
But, this is not the same as saying that the functions change.

150
00:15:11,835 --> 00:15:18,450
So, we do not want the values to change, if we have a priority queue and we insert a set of values and then delete max.

151
00:15:18,450 --> 00:15:24,460
No matter how the priority queue is actually implemented internally, the delete max should give us the same value at the end.

152
00:15:24,769 --> 00:15:28,799
We saw that python supports object oriented programming.

153
00:15:28,799 --> 00:15:36,009
We shall look at it in more detail in the next couple of lectures in this week's part of the course.

154
00:15:36,009 --> 00:15:44,309
concepts associated with this object oriented programming are classes and objects. classes are templates for data types.

155
00:15:44,309 --> 00:15:50,259
and objects are instances of these classes they are comprehensive data types which we use in our program.

