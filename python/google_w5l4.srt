1
00:00:03,589 --> 00:00:07,269
इसलिए पिछले लेक्चर में हमने देखा कि टेक्स्ट फाइल्स को कैसे पढ़ना और लिखना है।

2
00:00:07,490 --> 00:00:13,330
और पाठ को पढ़ना और लिखना, हमेशा उन स्ट्रिंग्स को संसाधित करना शामिल है जिन्हें हम पढ़ते और लिखते हैं।

3
00:00:13,460 --> 00:00:20,053
और इसलिए, पायथन में कई स्ट्रिंग प्रोसेसिंग फ़ंक्शन हैं जो इन सामग्रियों को संशोधित करना आसान बनाते हैं।

4
00:00:20,078 --> 00:00:23,829
तो आमतौर पर आप इन फाइलों के साथ कुछ करने के लिए फाइलें पढ़ और लिख रहे हैं।

5
00:00:23,829 --> 00:00:27,760
और इनके साथ कुछ करने के लिए, आप बिल्ट-इन स्ट्रिंग फ़ंक्शंस का उपयोग कर सकते हैं, जो काफी शक्तिशाली हैं।

6
00:00:27,760 --> 00:00:34,270
अन्य चीजों के अलावा आप इन स्ट्रिंग फ़ंक्शंस के साथ कर सकते हैं, आप टेक्स्ट खोज सकते हैं या खोज सकते हैं और इसे बदल सकते हैं।

7
00:00:34,789 --> 00:00:41,740
किसी फ़ाइल के लिए स्ट्रिंग प्रोसेसिंग का एक विशिष्ट उपयोग तब होता है जब हम स्प्रेडशीट की तरह कुछ लेते हैं और इसे टेक्स्ट के रूप में निर्यात करते हैं।

8
00:00:41,740 --> 00:00:51,130
तो कॉमा सेपरेटेड वैल्यू फॉर्मेट CSV नाम की कोई चीज होती है, जहां कॉलम टेक्स्ट के रूप में कॉमा द्वारा अलग किए गए आउटपुट होते हैं।

9
00:00:51,130 --> 00:01:00,738
तो अब, हम स्ट्रिंग फ़ाइल के साथ क्या कर सकते हैं कि इस तरह की फ़ाइल लाइन को लाइन से पढ़ें, और प्रत्येक पंक्ति में कॉमा के बीच पढ़कर टेक्स्ट से कॉलम निकालें।

10
00:01:00,763 --> 00:01:03,399
तो हम इस व्याख्यान में इन सभी को देखेंगे।

11
00:01:05,060 --> 00:01:11,903
तो एक स्ट्रिंग कमांड का पहला उदाहरण जो हमने पिछली बार देखा था, वह है व्हाइट स्पेस को स्ट्रिप करने की कमांड।

12
00:01:11,928 --> 00:01:18,989
तो हमारे पास r पट्टी है, उदाहरण के लिए, जिसका उपयोग हम अपनी पंक्तियों में पीछे के सफेद स्थान, बैकस्लैश ns को हटाने के लिए करते थे।

13
00:01:18,989 --> 00:01:26,530
और हमारे पास प्रमुख सफेद जगह को हटाने के लिए एल पट्टी थी और हमारे पास पट्टी थी जो इसे दोनों दिशाओं में हटा देती थी।

14
00:01:26,530 --> 00:01:28,900
तो आइए देखें कि यह कैसे काम करता है।

15
00:01:31,400 --> 00:01:39,640
आइए एक स्ट्रिंग बनाते हैं जिसमें पहले और बाद में व्हाइटस्पेस होता है और हम कुछ रिक्त स्थान, टैब, फिर हैलो शब्द, फिर दो टैब डालते हैं।

16
00:01:41,689 --> 00:01:47,769
तो हमारे पास एक स्ट्रिंग है जिसमें सफेद स्थान है और आप देख सकते हैं कि टैब बैकस्लैश टी और ब्लैंक द्वारा इंगित किए गए हैं।

17
00:01:47,769 --> 00:01:53,135
तो अब, अगर हम सिर्फ दाईं ओर से पट्टी करना चाहते हैं तो हम कहते हैं कि t बराबर s dot r पट्टी है।

18
00:01:53,160 --> 00:01:57,420
तो याद रखें, क्योंकि तार अपरिवर्तनीय हैं, स्ट्रिप कमांड एस नहीं बदलेगा।

19
00:01:57,420 --> 00:01:58,420
यह सिर्फ एक नई स्ट्रिंग लौटाएगा।

20
00:01:58,445 --> 00:02:03,180
तो अगर मैं कहता हूं, टी एस डॉट आर स्ट्रिप है, तो यह व्हाइटस्पेस को दाईं ओर ले जाएगा, और मुझे टी देगा।

21
00:02:03,180 --> 00:02:07,530
अगर मैं टी को देखता हूं, तो इसमें सब कुछ हैलो तक है, लेकिन बाद में टैब और स्पेस नहीं है।

22
00:02:07,530 --> 00:02:17,050
इसी तरह अगर मैं कहता हूं कि टी एस डॉट एल स्ट्रिप है, तो यह बाईं ओर के लोगों को हटा देगा, और अब टी हैलो से शुरू होगा, लेकिन इसके अंत में सफेद जगह होगी।

23
00:02:17,121 --> 00:02:23,860
और अंत में, अगर मैं कहता हूं कि t एक बिंदु पट्टी है, तो दोनों पक्ष चले गए हैं, और मुझे बस वह शब्द मिलेगा जो मुझे चाहिए।

24
00:02:23,860 --> 00:02:29,163
तो यह उपयोगी है, क्योंकि जब आप लोगों को उदाहरण के लिए चीजों और रूपों को टाइप करने के लिए कहते हैं, तो आमतौर पर वे पहले और बाद में कुछ खाली छोड़ देते हैं।

25
00:02:29,190 --> 00:02:41,229
इसलिए यदि आप चाहते हैं कि पहले ब्लैंक से पहले और आखिरी ब्लैंक के बाद सब कुछ खो जाए, और केवल टेक्स्ट को बीच में रखें, तो आप वास्तविक डेटा निकालने के लिए l स्ट्रिप, r स्ट्रिप या केवल स्ट्रिप के संयोजन का उपयोग कर सकते हैं जो आप चाहते हैं। फ़ाइल।

26
00:02:43,849 --> 00:02:48,310
तो अगली चीज़ जो हम करना चाहते हैं वह है एक स्ट्रिंग में टेक्स्ट की तलाश करना।

27
00:02:48,310 --> 00:02:50,909
तो एक बेसिक कमांड है जिसे फाइंड कहा जाता है।

28
00:02:50,909 --> 00:02:59,020
तो यदि s एक स्ट्रिंग है, और पैटर्न एक अन्य स्ट्रिंग है जिसे मैं s में ढूंढ रहा हूं, s dot find pattern s में पहली स्थिति लौटाएगा जिसमें पैटर्न होता है।

29
00:02:59,020 --> 00:03:04,900
तो स्थिति स्पष्ट रूप से 0 और s माइनस 1 की लंबाई के बीच होगी।

30
00:03:04,900 --> 00:03:08,560
हमने पहले ही इसके अपने कुछ कार्यान्वयन पहले ही लिख दिए थे।

31
00:03:08,873 --> 00:03:12,909
तो अगर ऐसा नहीं होता है, तो यह आपको माइनस 1 देगा।

32
00:03:14,449 --> 00:03:17,109
अब, कभी-कभी आप पूरी स्ट्रिंग को खोजना नहीं चाहेंगे।

33
00:03:17,134 --> 00:03:29,949
तो पैटर्न तर्कों की एक वैकल्पिक जोड़ी लेता है <प्रारंभ और अंत, और पूरे स्ट्रिंग से पैटर्न की तलाश करने के बजाय, यह सामान्य सम्मेलन के साथ शुरू से अंत तक एक टुकड़ा देखता है कि यह शुरुआत से n शून्य से 1 की स्थिति है .

34
00:03:32,270 --> 00:03:41,439
और इस कमांड का एक और संस्करण है जिसे इंडेक्स कहा जाता है, और खोज और इंडेक्स के बीच का अंतर तब होता है जब पैटर्न नहीं मिलता है।

35
00:03:41,439 --> 00:03:44,500
फाइंड में, जब पैटर्न नहीं मिलता है तो आपको माइनस 1 मिलता है।

36
00:03:44,500 --> 00:03:50,110
इंडेक्स में, जब पैटर्न नहीं मिलता है, तो आपको एक विशेष प्रकार की त्रुटि मिलती है, इस मामले में एक वैल्यू एरर।

37
00:03:50,120 --> 00:03:53,169
तो चलिए फिर से देखते हैं कि ये चीजें वास्तव में कैसे काम करती हैं।

38
00:03:56,150 --> 00:04:02,849
तो हमारे यहां एक स्ट्रिंग है जिसमें भूरा, लोमड़ी, भूरा, कुत्ता, भूरा, लोमड़ी शब्द शामिल हैं।

39
00:04:02,849 --> 00:04:12,879
अब, अगर मैं इसे "ब्राउन" शब्द की पहली घटना को देखने के लिए कहता हूं, तो यह स्थिति 0 वापस कर देगा क्योंकि यह स्ट्रिंग की शुरुआत में वहीं है।

40
00:04:12,879 --> 00:04:21,069
अगर दूसरी तरफ मुझे यह स्थिति नहीं चाहिए, लेकिन मैं कहना चाहता हूं कि स्थिति 5 से शुरू हो रहा है और उदाहरण के लिए एस की लंबाई तक जा रहा है।

41
00:04:21,069 --> 00:04:24,899
फिर, यह 19 कहेगा, और यदि आप गिनेंगे तो आप पाएंगे कि यह सही है।

42
00:04:24,899 --> 00:04:27,420
ब्राउन की दूसरी घटना 19 की स्थिति में है।

43
00:04:27,445 --> 00:04:33,927
अगर दूसरी तरफ, मैं कुछ ऐसा ढूंढता हूं जो बिल्ली की तरह नहीं है, तो ढूंढें शून्य से 1 वापस आ जाएगा।

44
00:04:33,952 --> 00:04:39,519
तो माइनस 1 संकेत है कि स्ट्रिंग नहीं मिली थी।

45
00:04:39,709 --> 00:04:48,000
अब, इंडेक्स के साथ अंतर यह है कि अगर मैं इंडेक्स को एक ही चीज़ देता हूं, तो माइनस 1 के बजाय यह मुझे एक वैल्यू एरर देता है जिसमें कहा गया है कि सबस्ट्रिंग नहीं होती है।

46
00:04:48,000 --> 00:04:50,259
तो यह है कि खोज और अनुक्रमणिका कैसे काम करती है।

47
00:04:54,199 --> 00:04:58,930
तो खोज के बाद अगली प्राकृतिक चीज खोज और प्रतिस्थापित कर रही है।

48
00:04:58,930 --> 00:05:09,850
तो अगर मैं कुछ बदलना चाहता हूं, तो मैं इसे दो तार देता हूं: जो मैं खोज रहा हूं, और जो मैं इसे बदल रहा हूं, और यह दूसरी स्ट्रिंग द्वारा प्रतिस्थापित पहली स्ट्रिंग की प्रत्येक घटना के साथ एस की एक प्रति वापस कर देगा।

49
00:05:11,779 --> 00:05:19,889
अब, इसे निम्नलिखित तरीके से नियंत्रित किया जा सकता है, मान लीजिए कि मैं प्रत्येक घटना नहीं चाहता, लेकिन मुझे केवल पहली घटना या पहली तीन घटनाएं चाहिए।

50
00:05:19,889 --> 00:05:24,865
तो मैं इसे एक वैकल्पिक तर्क दे सकता हूं कि शुरुआत से शुरू होने वाली ऐसी कितनी घटनाओं को प्रतिस्थापित किया जाना चाहिए।

51
00:05:24,890 --> 00:05:40,149
तो यह कहता है कि सबसे पहले n प्रतियों को बदलें, और ध्यान दें कि पट्टी की तरह, यह स्ट्रिंग को बदल रहा है, लेकिन क्योंकि तार अपरिवर्तनीय हैं, यह हमें रूपांतरित स्ट्रिंग वापस करने जा रहा है।

52
00:05:40,149 --> 00:05:41,949
तो आइए एक उदाहरण देखें।

53
00:05:44,660 --> 00:05:49,319
एक बार फिर, आइए अपने पुराने उदाहरण का उपयोग करें, s ब्राउन, फॉक्स, ग्रे, डॉग, ब्राउन, फॉक्स है।

54
00:05:49,319 --> 00:05:58,110
और अब, मान लीजिए कि मैं भूरे रंग को काले रंग से बदलना चाहता हूं, तो मुझे काला, लोमड़ी, भूरा, कुत्ता, काला, लोमड़ी मिलता है।

55
00:05:58,110 --> 00:06:06,100
अगर मैं कहता हूं कि केवल एक को बदला जाना है, तो मुझे काला, लोमड़ी, भूरा, कुत्ता मिलता है और दूसरा भूरा अपरिवर्तित रहता है।

56
00:06:06,379 --> 00:06:13,831
अब, अगर मैं पूछता हूं कि क्या होता है यदि मेरे पास यह पैटर्न है, तो यह बड़े करीने से विभाजित नहीं होता है, अगर मैं भूरे रंग की अलग-अलग प्रतियां ओवरलैप करता हूं।

57
00:06:13,856 --> 00:06:26,110
तो मान लीजिए कि मेरे पास अबाबा की तरह कुछ बेवकूफी भरा तार है, और अब, मैं कहता हूं कि सभी अबास को डीडी से बदलें।

58
00:06:27,139 --> 00:06:33,540
तो, अब सवाल यह है कि क्या यह दो अबास या एक अबा खोजेगा क्योंकि स्थिति 0 से शुरू होने वाला अबा है।

59
00:06:33,540 --> 00:06:37,240
स्थिति 2 से शुरू होने वाली स्ट्रिंग के दूसरे भाग में एक अबा भी है।

60
00:06:37,240 --> 00:06:41,240
सवाल यह है कि क्या यह इन दोनों को चिह्नित करेगा और इन्हें डीडी से बदल देगा।

61
00:06:41,265 --> 00:06:47,220
ठीक है, ऐसा नहीं है क्योंकि यह क्रमिक रूप से करता है इसलिए यह पहले ABA लेता है, DD द्वारा प्रतिस्थापित करता है।

62
00:06:47,245 --> 00:06:50,560
इस बिंदु पर, दूसरा अबा नष्ट हो गया है।

63
00:06:50,560 --> 00:06:52,000
तो यह नहीं मिलेगा।

64
00:06:52,189 --> 00:07:02,689
जबकि, उदाहरण के लिए, अगर मेरे पास इस विच्छेदन की दो प्रतियां होतीं, तो यह सही ढंग से मिल जाती, और मुझे डीडी के बाद डीडी दिया जाता।

65
00:07:02,714 --> 00:07:13,660
इसलिए ओवरलैपिंग स्ट्रिंग्स के बारे में कोई समस्या नहीं है, यह बस इसे बाएं से दाएं करता है, और यह सुनिश्चित करता है कि ओवरलैप्ड स्ट्रिंग पहले लिखी जाए ताकि दूसरी कॉपी ट्रांसफर न हो।

66
00:07:19,310 --> 00:07:23,110
तो अगली चीज़ जो हम देखना चाहते हैं वह है एक स्ट्रिंग को विभाजित करना।

67
00:07:23,110 --> 00:07:29,709
अब, जब हम एक स्प्रेडशीट लेते हैं और उसे टेक्स्ट के रूप में लिखते हैं, तो आमतौर पर ऐसा होता है कि हमारे पास एक आउटपुट होगा जो इस तरह दिखता है।

68
00:07:33,259 --> 00:07:36,985
तो, पहले कॉलम के बाद कॉमा और फिर दूसरा कॉलम लिखा जाएगा।

69
00:07:37,010 --> 00:07:46,079
तो हमारे पास तीन कॉलम थे और पहले कॉलम ने कहा 6, दूसरे कॉलम ने 7 कहा, और तीसरा एक स्ट्रिंग हैलो था, फिर जब हम इसे टेक्स्ट के रूप में लिखते हैं तो हमें 6, 7 और हैलो मिलेगा।

70
00:07:46,079 --> 00:07:50,529
तो वास्तव में हैलो एक समस्या के रूप में है क्योंकि इसमें दोहरे उद्धरण हैं इसलिए आइए हम हैलो का उपयोग न करें।

71
00:07:50,529 --> 00:07:57,670
आइए हम कुछ सरल का उपयोग करें तो मान लें कि उदाहरण के लिए हमारे पास तीन संख्याएं 6, 7, और 8 थीं।

72
00:07:58,879 --> 00:08:02,410
अब, हम क्या करना चाहते हैं कि हम इस जानकारी को निकालना चाहते हैं।

73
00:08:02,490 --> 00:08:08,980
इसलिए हम व्यक्तिगत 6, 7 और 8 को निकालना चाहते हैं जो हमारे पास 3 मान थे।

74
00:08:08,980 --> 00:08:12,000
तो हमें स्ट्रिंग्स के बीच इस टेक्स्ट को देखने की जरूरत है।

75
00:08:12,000 --> 00:08:17,846
इसलिए हम कॉलम को कॉमा के बीच में विभाजित करना चाहते हैं, और यह स्प्लिट कमांड का उपयोग करके किया जाता है।

76
00:08:17,871 --> 00:08:30,228
तो विभाजन एक स्ट्रिंग लेता है, और एक चरित्र लेता है या वास्तव में यह कोई भी स्ट्रिंग हो सकता है, और यह कॉलम को विभाजित करता है, यह आपको उन मूल्यों की एक सूची देता है जो अल्पविराम के बीच के हिस्सों को लेकर आते हैं।

77
00:08:30,253 --> 00:08:35,340
तो पहले अल्पविराम तक पहली बात है इसलिए कॉलम सिर्फ वह नाम है जिसका हमने उपयोग किया है, यह कोई भी सूची हो सकती है।

78
00:08:35,340 --> 00:08:42,399
तो, सूची का पहला आइटम पहले अल्पविराम तक होगा, फिर पहले और दूसरे अल्पविराम के बीच, और इसी तरह, और अंत में अंतिम अल्पविराम के बाद।

79
00:08:43,549 --> 00:08:58,840
तो इस मामले में अल्पविराम कोई विशेष बात नहीं है, आप किसी भी विभाजक स्ट्रिंग का उपयोग करके विभाजित कर सकते हैं, और फिर से प्रतिस्थापित की तरह हम नियंत्रित कर सकते हैं कि हम कितनी बार प्रतिस्थापित करते हैं, यहां आप यह भी नियंत्रित कर सकते हैं कि आप कितने विभाजन करते हैं।

80
00:08:58,840 --> 00:09:06,750
तो आप इस स्ट्रिंग के अनुसार विभाजित कह सकते हैं, इसलिए ध्यान दें कि यह कोई भी स्ट्रिंग हो सकती है इसलिए यहां हम इसे स्पेस कोलन स्पेस के अनुसार विभाजित कर रहे हैं।

81
00:09:06,750 --> 00:09:09,196
लेकिन, हम कह रहे हैं कि n से ज्यादा टुकड़े न बनाएं।

82
00:09:09,221 --> 00:09:18,580
इसलिए यदि आपके पास n से अधिक कॉलम हैं या जो भी भाग इस तरह आते हैं, एक निश्चित बिंदु से आगे हम इसे केवल एक शेष स्ट्रिंग के रूप में गांठ कर देंगे, और इसे अपने पास रखेंगे।

83
00:09:18,580 --> 00:09:21,039
तो चलिए फिर से देखते हैं कि यह कैसे काम करता है।

84
00:09:23,509 --> 00:09:31,860
मान लीजिए, यह हमारी टेक्स्ट लाइन है जिसे मैं csvline कहता हूं, यह कॉमा द्वारा अलग किए गए मानों का एक क्रम है, ध्यान दें कि यह एक स्ट्रिंग है।

85
00:09:31,860 --> 00:09:42,850
तो अब अगर मैं एक विभाजक के रूप में अल्पविराम का उपयोग करके सीएसवीलाइन डॉट स्प्लिट कहता हूं, तो मुझे स्ट्रिंग 6, स्ट्रिंग 7, स्ट्रिंग 8 के मानों की एक सूची मिलती है।

86
00:09:42,850 --> 00:09:45,690
याद रखें कि यह ठीक वैसा ही है जैसा हमने इनपुट के बारे में कहा था।

87
00:09:45,690 --> 00:09:49,796
यह आपको उस रूप में मान नहीं देता है जो आप चाहते हैं, फिर आपको उन्हें int या कुछ का उपयोग करके परिवर्तित करना होगा।

88
00:09:49,821 --> 00:09:55,356
ये अभी भी तार हैं इसलिए यह सिर्फ एक लंबी स्ट्रिंग लेता है, और इसे छोटे तारों की सूची में विभाजित करता है।

89
00:09:55,381 --> 00:10:01,783
अब, यहां तीन तत्व हैं, उदाहरण के लिए, अगर मैं कहता हूं, मुझे केवल स्थिति 0,1 और 2 चाहिए।

90
00:10:01,808 --> 00:10:08,919
तो अगर मैं कहता हूं कि मैं इसे केवल एक बार करना चाहता हूं, तो मुझे पहले 6 मिलते हैं, लेकिन फिर 7 और 8 विभाजित नहीं होते क्योंकि यह केवल एक बार विभाजित होता है।

91
00:10:09,740 --> 00:10:19,720
अब, अगर मैं इसे कुछ और फैंसी में बदलता हूं जैसे हैश प्रश्न चिह्न कहें।

92
00:10:19,940 --> 00:10:28,090
तो अब, मेरे पास एक अलग विभाजक है जो एक वर्ण नहीं है, लेकिन हैश प्रश्न चिह्न है, तो मैं हैश प्रश्न चिह्न के अनुसार विभाजित कह सकता हूं।

93
00:10:28,129 --> 00:10:30,949
और यह मुझे वही देगा।

94
00:10:30,974 --> 00:10:34,750
तो आप किसी भी स्ट्रिंग के अनुसार विभाजित कर सकते हैं, यह सिर्फ एक समान स्ट्रिंग है।

95
00:10:34,750 --> 00:10:39,918
रेगुलर एक्सप्रेशन और वह सब करके आप और भी फैंसी चीज़ें कर सकते हैं, लेकिन अभी हम उसे कवर नहीं करेंगे।

96
00:10:39,943 --> 00:10:44,980
जब तक आपके पास एक निश्चित स्ट्रिंग है जो आपकी चीज़ को अलग करती है, आप उस निश्चित स्ट्रिंग के अनुसार विभाजित कर सकते हैं।

97
00:10:49,159 --> 00:10:52,976
तो विभाजन का उलटा संचालन तारों में शामिल होना होगा।

98
00:10:53,001 --> 00:10:59,590
तो मान लीजिए कि हमारे पास तारों का संग्रह है, और मैं इसे एकल स्ट्रिंग में जोड़ना चाहता हूं और उनमें से प्रत्येक को दिए गए विभाजक द्वारा अलग करना चाहता हूं।

99
00:10:59,720 --> 00:11:15,120
तो एक उदाहरण के रूप में, मान लें कि हम s लेते हैं, जो कि कुछ csv आउटपुट है, और हम इसे कॉमा पर कॉलम में विभाजित करते हैं, तो हम जॉइन स्ट्रिंग ले सकते हैं, और इसे वैल्यू कॉमा पर सेट कर सकते हैं, और फिर कॉलम में शामिल होने के लिए इसका उपयोग कर सकते हैं।

100
00:11:15,120 --> 00:11:19,539
तो ज्वाइन एक फंक्शन है जो स्ट्रिंग से जुड़ा होता है।

101
00:11:19,549 --> 00:11:22,103
इस मामले में, चिंता का विषय अल्पविराम है।

102
00:11:22,128 --> 00:11:28,149
तो कमोबेश आप कॉमा डॉट जॉइन कॉलम कह रहे हैं, जो कॉलम में शामिल होने के लिए कॉमा का उपयोग करता है।

103
00:11:28,149 --> 00:11:32,429
तो आपने इसे यहाँ एक नाम दिया है joinstring अल्पविराम के बराबर है।

104
00:11:32,429 --> 00:11:35,630
और फिर, csvline जॉइनस्ट्रिंग डॉट जॉइन कॉलम है।

105
00:11:35,655 --> 00:11:51,070
तो यह क्या कहता है, अलग करने के लिए अल्पविराम का उपयोग करें, इसलिए यदि इसके अंत में मुझे पिछली बार 6, 7 और 8 की तरह मिला है, तो यह अब उन्हें 6 कॉमा 7 कॉमा 8 के रूप में एक ही स्ट्रिंग में वापस रख देगा।

106
00:11:51,740 --> 00:12:05,250
यहां एक और उदाहरण दिया गया है, यहां हमारे पास एक तारीख 16 एक महीने 08 और एक वर्ष 2016 है जो स्ट्रिंग के रूप में दी गई है, और मैं इसे एक तारीख में एक साथ स्ट्रिंग करना चाहता हूं जैसे हम आमतौर पर हाइफ़न के साथ उपयोग करते हैं।

107
00:12:05,250 --> 00:12:11,885
तो यहां हाइफ़न को मध्यवर्ती नाम देने और फिर हाइफ़न डॉट जॉइन कहने के बजाय, मैं सीधे स्ट्रिंग का उपयोग करता हूं।

108
00:12:11,910 --> 00:12:23,110
बस यह स्पष्ट करना चाहते हैं कि आप सीधे जुड़ने वाली स्ट्रिंग का उपयोग एक स्थिर स्ट्रिंग के रूप में कर सकते हैं और कह सकते हैं कि मूल्यों की इस सूची में शामिल होने के लिए इसका उपयोग करें।

109
00:12:23,135 --> 00:12:32,679
तो आपको केवल यह सुनिश्चित करने की ज़रूरत है कि आपके पास शामिल होने के तर्क के अंदर जो कुछ है वह तारों की एक सूची है, और जो आपने लागू किया है वह वह स्ट्रिंग है जिसका उपयोग उनसे जुड़ने के लिए किया जाएगा।

110
00:12:32,690 --> 00:12:36,970
तो चलिए बस जाँचते हैं कि यह वैसे ही काम करता है जैसे हम वास्तव में करना चाहते थे।

111
00:12:37,429 --> 00:12:51,129
तो चलिए सीधे दूसरा उदाहरण करते हैं इसलिए मान लीजिए कि हम कहते हैं कि तारीख 16 है, याद रखें कि ये सभी तार हैं, महीना 08 है, साल 2016 है।

112
00:12:51,129 --> 00:13:06,580
और अब मैं कहना चाहता हूं कि डैश को विभाजक के रूप में उपयोग करके इन तीन चीजों को जोड़ने का क्या प्रभाव है, और मुझे 16 डैश 08 डैश 2016 मिलता है।

113
00:13:09,799 --> 00:13:15,354
तो कई अन्य दिलचस्प चीजें हैं जो आप स्ट्रिंग्स के साथ कर सकते हैं उदाहरण के लिए, आप अपर केस और लोअर केस में हेरफेर कर सकते हैं।

114
00:13:15,379 --> 00:13:22,620
यदि आप कहते हैं कि कैपिटलाइज़ करें, तो यह क्या करेगा कि यह पहले अक्षर को अपर केस में बदल देगा, और बाकी को लोअर केस के रूप में रखेगा।

115
00:13:22,620 --> 00:13:25,980
यदि आप एस डॉट लोअर कहते हैं तो यह सभी अपर केस को लोअर केस में बदल देगा।

116
00:13:25,980 --> 00:13:29,830
यदि आप एस डॉट अपर कहते हैं तो यह सभी लोअर केस को अपर केस में बदल देगा और इसी तरह।

117
00:13:29,830 --> 00:13:34,409
एस डॉट शीर्षक जैसी अन्य फैंसी चीजें हैं, इसलिए शीर्षक प्रत्येक शब्द को बड़ा कर देगा।

118
00:13:34,409 --> 00:13:37,870
तो यह सामान्य रूप से किसी पुस्तक या फिल्म के शीर्षक में दिखाई देता है।

119
00:13:37,870 --> 00:13:43,450
एस डॉट स्वैपकेस लोअर केस को अपर केस में और अपर केस को लोअर केस वगैरह में बदल देगा।

120
00:13:43,475 --> 00:13:52,269
तो स्ट्रिंग्स में फ़ंक्शंस का पूरा संग्रह होता है जो अपर केस, लोअर केस और इनके बीच ट्रांसफॉर्म करने के तरीके से निपटता है।

121
00:13:53,210 --> 00:13:58,690
दूसरी चीज जो आप स्ट्रिंग्स के साथ कर सकते हैं, वह यह है कि आप जो चाहते हैं उसे फिट करने के लिए उनका आकार बदलें।

122
00:13:58,759 --> 00:14:08,679
तो यदि आप एक स्ट्रिंग रखना चाहते हैं जो निश्चित चौड़ाई के कॉलम के रूप में स्थित है, तो हम कह सकते हैं कि इसे आकार n के ब्लॉक में केंद्रित करें।

123
00:14:08,750 --> 00:14:14,830
तो, यह क्या करेगा कि यह एक नई स्ट्रिंग लौटाएगा जो कि लंबाई में n है जिसमें s केंद्रित है।

124
00:14:15,740 --> 00:14:20,039
अब, केन्द्रित करने से हमारा तात्पर्य यह है कि दोनों ओर रिक्त स्थान होंगे।

125
00:14:20,039 --> 00:14:23,639
रिक्त स्थान के बजाय, आप अपनी इच्छानुसार कुछ भी डाल सकते हैं जैसे सितारे या मिन्यूज़।

126
00:14:23,639 --> 00:14:29,710
तो आप एक ऐसा कैरेक्टर दे सकते हैं जिसका इस्तेमाल दोनों तरफ की खाली जगह को भरने के लिए किया जाएगा, न कि किसी खाली जगह को।

127
00:14:29,735 --> 00:14:33,043
अब, हो सकता है कि आप इसे केंद्रित न करना चाहें, हो सकता है कि आप बाईं या दाईं ओर चाहते हों।

128
00:14:33,068 --> 00:14:40,809
तो आप उदाहरण के लिए, ljust का उपयोग करके इसे जस्टिफाई कर सकते हैं या rjust का उपयोग करके इसे राइट जस्टिफाई कर सकते हैं, और फिर से आप एक वैकल्पिक कैरेक्टर वगैरह दे सकते हैं।

129
00:14:41,179 --> 00:14:44,649
इसलिए हम इनमें से केवल एक या दो की जांच कर सकते हैं कि वे कैसे काम करते हैं।

130
00:14:46,759 --> 00:14:53,980
मान लीजिए कि हम हैलो की तरह एक छोटी स्ट्रिंग लेते हैं, और अब हम इसे 50 के बड़े ब्लॉक में केंद्रित करते हैं।

131
00:14:54,080 --> 00:14:56,169
तो, हम कहते हैं एस डॉट सेंटर।

132
00:14:57,740 --> 00:15:01,210
फिर यह हमें दोनों तरफ बहुत सारे रिक्त स्थान के साथ हैलो देता है।

133
00:15:01,210 --> 00:15:08,259
अब, हम उन रिक्त स्थानों को किसी भी चीज़ से बदल सकते हैं जो हम चाहते हैं जैसे माइनस साइन, और इससे पहले हमें माइनस साइन या हाइफ़न की एक स्ट्रिंग मिलती है।

134
00:15:08,299 --> 00:15:13,865
अब, हम यह भी कह सकते हैं कि मैं चाहता हूं कि चीज को उचित छोड़ दिया जाए और केंद्रित न हो।

135
00:15:13,890 --> 00:15:20,139
तो अगर मैं ऐसा करता हूं, तो मुझे शुरुआत में हैलो मिलेगा और माइनस साइन्स का एक गुच्छा इसी तरह rjust वगैरह के साथ होगा।

136
00:15:24,080 --> 00:15:29,750
कुछ अन्य प्रकार के फंक्शन जो आपको स्ट्रिंग्स से जुड़े हुए लगते हैं, वे हैं स्ट्रिंग्स के गुणों की जाँच करना।

137
00:15:29,775 --> 00:15:34,620
क्या s में केवल a से z तक के अक्षर और a से राजधानी z तक के अक्षर शामिल हैं।

138
00:15:34,620 --> 00:15:38,503
तो यही है कि एस डॉट α कहता है, क्या यह एक अल्फाबेटिक स्ट्रिंग है।

139
00:15:38,528 --> 00:15:42,390
यदि यह सत्य है तो इसका अर्थ है कि यह है, यदि यह नहीं है तो इसमें कम से कम एक गैर वर्णमाला वर्ण है।

140
00:15:42,415 --> 00:15:46,950
इसी तरह, क्या यह पूरी तरह से अंक है, यह हमें बताएगा कि क्या यह पूरी तरह से अंक है।

141
00:15:46,950 --> 00:15:55,450
स्ट्रिंग फ़ंक्शंस की एक बड़ी संख्या है, और इस व्याख्यान में उन सभी के माध्यम से जाने का कोई मतलब नहीं है, अगर हमें उनकी आवश्यकता होगी जैसे हम आगे बढ़ते हैं, तो हम उनका उपयोग करेंगे और उन्हें समझाएंगे।

142
00:15:55,450 --> 00:16:03,570
लेकिन आप पाइथन दस्तावेज़ देख सकते हैं, स्ट्रिंग फ़ंक्शंस के अंतर्गत देख सकते हैं, और आपको उपयोगी उपयोगिताओं की एक पूरी मेजबानी मिल जाएगी जो आपको आसानी से तारों में हेरफेर करने की अनुमति देती है।

143
00:16:03,570 --> 00:16:09,825
और यह एक कारण है कि पायथन एक लोकप्रिय भाषा है क्योंकि आप इस तरह की आसान टेक्स्ट प्रोसेसिंग कर सकते हैं।

144
00:16:09,850 --> 00:16:19,623
तो आप इसका उपयोग डेटा को एक प्रारूप से दूसरे प्रारूप में त्वरित रूप से बदलने के लिए, और इसके दिखने के तरीके को बदलने या इसका आकार बदलने आदि के लिए कर सकते हैं।

145
00:16:19,699 --> 00:16:28,539
इसलिए स्ट्रिंग फ़ंक्शन चीजों को एक प्रारूप से दूसरे प्रारूप में बदलने के लिए एक भाषा के रूप में अजगर की उपयोगिता का एक अत्यंत महत्वपूर्ण हिस्सा हैं।

