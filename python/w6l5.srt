1
00:00:03,950 --> 00:00:08,529
So, let us look at a data structure problem involving job schedulers.

2
00:00:08,630 --> 00:00:13,150
So, job scheduler maintains a list of pending jobs with priorities.

3
00:00:13,220 --> 00:00:18,360
Now the job scheduler has to choose the next job to execute at any point.

4
00:00:18,385 --> 00:00:23,298
So, whenever the processor is free it picks the job not the job which arrived earliest

5
00:00:23,323 --> 00:00:26,769
but the one with maximum priority in the list and then schedules it.

6
00:00:26,899 --> 00:00:31,163
So, new jobs keep joining the list each with its own priority

7
00:00:31,188 --> 00:00:35,649
and according to their priority they get promoted ahead of other jobs which may have joined earlier

8
00:00:35,689 --> 00:00:44,579
So, our question is how should this scheduler maintain the list of printed jobs pending jobs and their priorities So that it can always pull out very quickly

9
00:00:44,579 --> 00:00:47,380
the one with the highest priority to schedule text.

10
00:00:49,070 --> 00:00:56,590
So, this is like a queue, but a queue in which items are priority based on some other characteristic not on when they arrived

11
00:00:56,590 --> 00:01:02,049
So, we saw a normal queue is a first in first out object. The once it arrive first leave first.

12
00:01:02,049 --> 00:01:05,260
In a priority queue they leave according to their priority.

13
00:01:05,659 --> 00:01:09,879
So, there are two operations associated with the priority queue.

14
00:01:10,010 --> 00:01:16,120
One is delete max. In delete queue we just said we take the element at the head of the queue.

15
00:01:16,120 --> 00:01:21,819
And delete max we have to look through the queue and identify and remove the job with the highest priority.

16
00:01:21,829 --> 00:01:27,310
Note ofcourse that the priorities of two different jobs may be the same in which case we can pick anyone.

17
00:01:27,980 --> 00:01:33,549
And the other operation which we normally call add to the queue we will call insert.

18
00:01:33,549 --> 00:01:39,280
and insert just adds a new job to the list and each job when it is added comes with its own priority.

19
00:01:41,090 --> 00:01:49,450
So, based on linear structures that we already studied We can think of maintaining these jobs just as a list.

20
00:01:49,849 --> 00:01:58,540
Now, if its an unsorted list when we add something to the queue. We can just add it to the list append it in any position, so this takes constant time

21
00:01:58,939 --> 00:02:05,272
However, to do a delete max we have to scan through the list and search for the maximum element

22
00:02:05,297 --> 00:02:13,270
and as we have seen In an unsorted list it will take us order n time to find the maximum element in list because we have to scan all the items.

23
00:02:13,819 --> 00:02:16,539
The other option is to keep the list sorted.

24
00:02:16,819 --> 00:02:18,900
This helps to delete max

25
00:02:18,900 --> 00:02:24,945
for instance, if we keep it sorted in descending order, the first element of the list is always the largest element

26
00:02:24,970 --> 00:02:33,400
However, the price we pay is for inserting because to maintain the sorted order. when we insert the element we have to put it in the right position

27
00:02:33,400 --> 00:02:37,960
and as we saw in insertion sort insert will take linear time.

28
00:02:38,389 --> 00:02:45,099
So, as a trade off we either take linear time or delete max or linear time for insert.

29
00:02:45,099 --> 00:02:54,969
So, if we think of n jobs entering and removing leaving the queue one way or another we end up spending n squared time processing these n jobs

30
00:02:57,020 --> 00:03:01,810
So, this is a fundamental limitation of keeping the data in a one dimensional structure.

31
00:03:01,849 --> 00:03:04,900
So, let us look at two dimensional structures.

32
00:03:04,969 --> 00:03:10,000
The most basic two dimensional structure that we can think of is a binary tree.

33
00:03:10,490 --> 00:03:23,009
So, a binary tree consists of nodes and each node has a value stored in it and it has possibly a left and a right child. So, we start at the root.

34
00:03:23,009 --> 00:03:24,669
which is the top of the tree

35
00:03:24,669 --> 00:03:28,060
and then each node will have one or two children.

36
00:03:28,189 --> 00:03:30,939
A node which has no children is called a leaf.

37
00:03:31,669 --> 00:03:36,430
And then with respect to a child we call the parent node the node above it.

38
00:03:36,430 --> 00:03:39,580
And we refer to the children as a left child and the right child.

39
00:03:41,569 --> 00:03:48,819
So, our goal is to maintain a priority queue as a special kind of binary tree which we will call a heap.

40
00:03:49,219 --> 00:03:51,879
So, this tree will be balanced.

41
00:03:51,879 --> 00:03:59,710
A balanced tree is one in which roughly speaking at each point the left and right side are almost the same size.

42
00:04:00,169 --> 00:04:08,096
Because of this it turns out that in a balanced tree If we have n nodes, then the height of the tree will be logarithm

43
00:04:08,121 --> 00:04:16,269
because remember that the height doubles with each level and as a result of which we can fit n nodes in log n levels provided its balanced.

44
00:04:16,519 --> 00:04:24,279
And because it is a height log n We will achieve both insert and delete max in order log n time.

45
00:04:24,740 --> 00:04:32,350
This means that if we have to add and remove n elements from the queue overall we will go from n square to n log n.

46
00:04:33,139 --> 00:04:37,860
And another nice feature about a heap is that we do not have to fix n in advance

47
00:04:37,860 --> 00:04:40,860
This will work as the heap grows and shrinks.

48
00:04:40,860 --> 00:04:43,000
So, we do not need to know what n is.

49
00:04:44,839 --> 00:04:46,480
So, what is the heap?

50
00:04:46,879 --> 00:04:51,009
So, heap is a binary tree with two properties.

51
00:04:51,009 --> 00:04:56,352
The first property is structural which are the values which are stored in the tree.

52
00:04:56,377 --> 00:05:01,197
remember that leaves nodes in a binary tree may have zero children, one children or two children

53
00:05:01,222 --> 00:05:05,589
So, we could have in general a very uneven structure

54
00:05:05,589 --> 00:05:09,622
heaps have a very regular structure when we have a heap.

55
00:05:09,647 --> 00:05:15,129
We have a binary tree in which we fill each level from top to bottom left to right.

56
00:05:15,350 --> 00:05:19,823
So, in other words at any point a heap consists of a number of filled levels

57
00:05:19,848 --> 00:05:25,329
and then in the bottom level we have nodes filled from left to right and then possibly some unfilled nodes.

58
00:05:26,029 --> 00:05:32,740
The other property with the heap, the first property is structurally just tells us that how the values look in the heap.

59
00:05:32,779 --> 00:05:37,569
So, in this node for example, in this picture the blue nodes are those which have values

60
00:05:37,569 --> 00:05:41,980
and the empty nodes are indicated with open circles at the bottom.

61
00:05:42,259 --> 00:05:46,240
The second property about heap is the values themselves.

62
00:05:46,240 --> 00:05:52,149
So, the heap property in this case what we call the max heap property because we are interested in maximum values

63
00:05:52,149 --> 00:05:56,259
says that every value is bigger than the values of its two children.

64
00:05:56,259 --> 00:06:04,600
So, at every node if we look at the value and we look at the value in the left child right child then the parent will have a larger value than the both the children.

65
00:06:05,779 --> 00:06:09,819
So Let us see some examples. So, here is a four node heap.

66
00:06:09,819 --> 00:06:13,319
So, because it has four nodes we fill the root in the first level.

67
00:06:13,319 --> 00:06:16,639
And finally, the second level we have only one node which is the left most.

68
00:06:16,664 --> 00:06:20,970
And notice that the values are correctly ordered, twenty four is bigger than eleven and seven

69
00:06:20,970 --> 00:06:25,899
eleven is bigger than its only child ten seven has no children, so there are no constraints.

70
00:06:26,509 --> 00:06:31,110
Here is another example where the bottom level is actually full, here we have seven nodes

71
00:06:31,135 --> 00:06:35,490
and once again at every point we see that eleven is bigger than ten and five

72
00:06:35,490 --> 00:06:41,649
seven is bigger than six and five So, we have the value property the max heap property along with the structural property

73
00:06:43,939 --> 00:06:47,500
Here is an example of something which is structurally not a heap.

74
00:06:47,500 --> 00:06:51,420
So, because it is a heap we should have it filled from top to bottom left to right

75
00:06:51,420 --> 00:06:57,790
So, we should have a node here to the left of seven before we add a right child therefore this is not a heap.

76
00:06:59,689 --> 00:07:06,209
Similarly, here the node eight could not have been added here before we filled in the right child of seven

77
00:07:06,209 --> 00:07:11,709
So, once again this is not been filled correctly left to right top to bottom and therefore, this is not a heap.

78
00:07:13,970 --> 00:07:20,290
This particular tree satisfies the structural property of a heap in the sense that it is filled from top to bottom

79
00:07:20,290 --> 00:07:28,509
but we have here a violation of the heap property because seven has a child which has a larger value than it namely eight

80
00:07:33,800 --> 00:07:40,240
So, our first job is to insert a value into a heap while maintaining the heap property.

81
00:07:41,060 --> 00:07:46,750
So, the first thing to note is that we have no choice about where to put the new node.

82
00:07:46,750 --> 00:07:50,585
Remember that heap nodes are constructed top to bottom left to right

83
00:07:50,610 --> 00:07:58,360
So, if we want to insert twelve it must come with below the ten to the left because we have to start a new level since the previous level is full

84
00:07:58,550 --> 00:08:05,056
The problem is that this may not satisfy the heap property in this case twelve is bigger than its parent ten

85
00:08:05,081 --> 00:08:09,819
so although this is now structurally correct it doesn't have the right value distribution.

86
00:08:09,819 --> 00:08:13,600
So, we have to restore the heap property in some way.

87
00:08:14,180 --> 00:08:16,990
So, this is what we do we first create the node.

88
00:08:17,149 --> 00:08:24,662
we put the new value into that node and then We start looking at violations with respect to its parent

89
00:08:24,687 --> 00:08:29,637
So, we notice that twelve and ten are not correctly ordered so we exchange them.

90
00:08:30,733 --> 00:08:33,210
right Now, this is a new node.

91
00:08:33,210 --> 00:08:37,120
So, we have to check whether it is correctly ordered with respect to its current parent

92
00:08:37,120 --> 00:08:41,019
So, we look and we find that it is not, so again we exchange these.

93
00:08:41,019 --> 00:08:48,205
Now, notice that because eleven was already bigger than five twelve will remain bigger than five. So, there is no need to check

94
00:08:48,230 --> 00:08:54,463
anything down from where we got we only have to look up Now we have to check whether there is still a problem above.

95
00:08:54,488 --> 00:08:58,269
In this case there is no problem twelve is smaller than twenty four so we stop

96
00:09:00,019 --> 00:09:01,614
So, let us add another node.

97
00:09:01,710 --> 00:09:05,279
So, supposing we add thirty three now to the heap that we just created.

98
00:09:05,279 --> 00:09:08,289
So, thirty three again creates a new node at this point.

99
00:09:08,289 --> 00:09:12,340
Now, thirty three being bigger than eleven we have to walk up and swap it.

100
00:09:13,039 --> 00:09:16,450
Then again we compare thirty three and its parent twelve

101
00:09:16,450 --> 00:09:19,629
I mean notice that thirty three is bigger than twelve so we swap it again.

102
00:09:19,970 --> 00:09:26,320
Then we look at the root in this case twenty four and we find that thirty three is bigger than twenty four so I swap it again.

103
00:09:26,320 --> 00:09:31,600
And now thirty three has no parents and it is definitely bigger than its two children. So, we can stop.

104
00:09:33,830 --> 00:09:36,279
So, how much time does insert take?

105
00:09:36,409 --> 00:09:44,343
At each time we insert a node we have to check with its parent swap, check with its parent swap and so on.

106
00:09:44,368 --> 00:09:49,210
but the good thing is we only walk up a path we never walk down a path.

107
00:09:49,250 --> 00:09:55,029
So, the number of steps we walk up will become bounded by the height of the tree.

108
00:09:55,700 --> 00:10:02,019
Now, we argued before or we mentioned before that a bound up balanced tree will have height log n.

109
00:10:02,019 --> 00:10:08,905
So, you can actually measure it correctly by saying that the number of nodes at level i is two to the i, so initially we have one node

110
00:10:08,930 --> 00:10:14,919
two to the zero then at the first level we have two nodes two to the one second level we have four nodes two to the two and so on.

111
00:10:15,396 --> 00:10:19,750
right So, if we do it this way then we find that when k levels are filled.

112
00:10:19,750 --> 00:10:31,450
we will have two to the k minus one nodes and therefore turning this around we will find that if we have n nodes then the number of levels must be log n.

113
00:10:31,730 --> 00:10:39,210
So, therefore, insert walks up a path. the path is equal to the height of the tree, the height of the tree is order of log n

114
00:10:39,210 --> 00:10:41,559
So, insert takes time order log n.

115
00:10:44,210 --> 00:10:48,549
The other operation we need to implement in a heap is delete max.

116
00:10:48,889 --> 00:10:52,962
Now, one thing about a heap is that the maximum value is always at the root.

117
00:10:52,987 --> 00:10:59,710
This is because of the heap property, you can inductively see that because each node is bigger than each children.

118
00:10:59,710 --> 00:11:03,399
the maximum value in the entire tree must be at the root.

119
00:11:03,919 --> 00:11:08,710
So, we know where the root is, now the question is how do we remove it efficiently.

120
00:11:10,639 --> 00:11:12,909
So, if we remove this node.

121
00:11:13,669 --> 00:11:19,779
First of all we cant remove the node because it is a root, if we remove this value then we have to put some value there.

122
00:11:19,970 --> 00:11:25,003
On the other hand, the number of values in the node in the heap as now shrunk.

123
00:11:25,028 --> 00:11:33,789
So, this node in the bottom right must be deleted because the structural property of the heap says that we must fill the tree left to right top to bottom.

124
00:11:33,860 --> 00:11:41,230
So, we are going top to bottom and we have run out of a value, the last node that we added was the one at the right most end of the bottom row and that must go.

125
00:11:41,480 --> 00:11:49,299
So, we have a value which is missing at the top and we have a value at the bottom namely eleven whose node is going to be deleted.

126
00:11:49,429 --> 00:11:54,879
So, the strategy now is to move this value to eleven and then fix things.

127
00:11:55,700 --> 00:11:58,450
So, we first remove the thirty three from the root.

128
00:11:58,519 --> 00:12:04,389
We remove the node containing eleven and we move the eleven to the position of the root.

129
00:12:04,610 --> 00:12:09,210
Now, the problem with this is we have moved an arbitrary value not the maximum value to the top

130
00:12:09,210 --> 00:12:12,970
So obviously, there is going to be some problem with respect to its children.

131
00:12:13,159 --> 00:12:19,269
So, here it turns out that eleven is bigger than seven which is correct but unfortunately smaller than twenty four

132
00:12:20,629 --> 00:12:25,330
So, to restore the property what we do is we look at both directions.

133
00:12:25,355 --> 00:12:29,350
right and we exchange it with the largest child.

134
00:12:29,600 --> 00:12:36,880
So, suppose this has been seventeen here, then we could have swapped eleven with seventeen or eleven in twenty four both violations are there.

135
00:12:36,905 --> 00:12:40,649
But if we move the smaller child up then seventeen will not be bigger than twenty four

136
00:12:40,649 --> 00:12:44,620
So, we move the largest one So, in this case we move twenty four up.

137
00:12:45,200 --> 00:12:47,250
And now we have a eleven again.

138
00:12:47,250 --> 00:12:50,799
And now we have to again check whether it is correct with respect to its two children.

139
00:12:50,990 --> 00:12:54,669
So, again it is not so we move the largest to an up namely twelve

140
00:12:55,789 --> 00:12:59,309
then we see now whether it is correct with respect to its children.

141
00:12:59,309 --> 00:13:02,110
at this point eleven is bigger than ten so we stop.

142
00:13:04,610 --> 00:13:16,720
So, just as insert followed a single path from the new node at the leaf up to the root delete max will follow a single path from the root down to a leaf.

143
00:13:16,730 --> 00:13:24,100
So, once again the cost of delete max will be proportional to the height of the tree which as we said earlier is log n.

144
00:13:25,549 --> 00:13:30,399
Let us do another delete. So, we delete in this case twenty four.

145
00:13:30,399 --> 00:13:34,240
Now, we remove the node for ten, ten goes to the root.

146
00:13:34,490 --> 00:13:42,429
We compare ten with its two children twelve and seven and find that it is not satisfying the heap property, so we move the larger of the two up namely twelve

147
00:13:42,429 --> 00:13:51,480
Now, we look at it is children new children here eleven and five and again we see its not satisfying the property so the larger one moves up and once it reaches the leaf

148
00:13:51,480 --> 00:13:55,149
there are no properties to be solved satisfied anymore, so we stop.

149
00:13:58,279 --> 00:14:06,129
So, one very attractive feature of heaps is that we can implement this tree directly in a list or in an array.

150
00:14:06,289 --> 00:14:12,909
So, we have an n node heap we can represent it as a list or an array with position zero to n minus one.

151
00:14:13,639 --> 00:14:19,360
So, the position zero represents a root then in order one and two represent the children.

152
00:14:19,360 --> 00:14:22,379
then three, four, five, six are nodes at the next level and so on.

153
00:14:22,379 --> 00:14:27,187
so Just as we said we filled up this heap left to right top to bottom right

154
00:14:27,212 --> 00:14:35,710
In the same way we number the nodes also top to bottom left to right so we start with zero at the root then one on the left two on the right then three four five six seven eight nine ten and so on.

155
00:14:36,230 --> 00:14:45,759
So, from this you can see that if I have a position labelled i then the two children are at two i plus one and two i plus two

156
00:14:46,490 --> 00:14:50,063
So, the children of one are two into one plus one which is three

157
00:14:50,088 --> 00:14:57,909
and two into one plus two which is four Similarly, children of five are two into five plus one which is eleven and two into five plus two which is twelve

158
00:14:57,919 --> 00:15:05,019
So, just by doing index calculations for a position in the heap we can figure out where it is children are

159
00:15:05,120 --> 00:15:09,850
and by reversing this calculation we can also find the index of the parent.

160
00:15:09,850 --> 00:15:13,773
the parent of j is at j minus one by two

161
00:15:13,798 --> 00:15:17,250
Now j minus one by two may not be integer, so we take the floor.

162
00:15:17,250 --> 00:15:30,370
So, if we take a eleven for example, eleven minus one is ten ten by two is five, if we take fourteen for example fourteen minus one is thirteen thirteen by two is six point five we take the floor and we get six

163
00:15:32,720 --> 00:15:38,933
So, this allows us to manipulate parent and children nodes by just doing index arithmetic.

164
00:15:38,958 --> 00:15:47,409
we go from i to two i plus one, two i plus two to go to the children and we go from j to the to j minus one by two floor to go to the parent.

165
00:15:48,950 --> 00:15:55,069
How do we build a heap? A naive way to build a heap is just to take a list of values

166
00:15:55,094 --> 00:15:59,559
and insert them one by one using the heap operation into the heap.

167
00:15:59,559 --> 00:16:08,769
So, we start with an empty heap. We insert x one create a new heap containing x one we insert x two creating a heap of x one, x two and so on.

168
00:16:08,960 --> 00:16:12,029
Each operation takes log n time.

169
00:16:12,029 --> 00:16:16,419
Of course, n will be growing, but it doesnt matter if we take the final n as an upper bound

170
00:16:16,419 --> 00:16:22,090
we do n inserts each log n and we can build this heap in order n log n time.

171
00:16:24,289 --> 00:16:28,090
There is a better way to do this heap building.

172
00:16:28,789 --> 00:16:37,149
If we have the array as x one to x n then the last half of the nodes correspond to the leaves of the tree.

173
00:16:37,909 --> 00:16:42,549
Now, a leaf node has no properties to satisfy because it has no children.

174
00:16:42,549 --> 00:16:46,600
So, we do not need to do anything, we can just leave the leaves as they are.

175
00:16:46,600 --> 00:16:53,409
So, we go one level above and then we can fix all heap errors at one level above.

176
00:16:54,380 --> 00:17:05,440
right and then again we move one level above and so on. So, we do the kind of top to bottom, heap fixing that we did with the delete max while we are building the heap.

177
00:17:06,079 --> 00:17:14,470
So, as we are going up the number of steps that we need to propagate this error goes higher and higher because we need to start at a higher point.

178
00:17:14,470 --> 00:17:17,890
On the other hand, the number of nodes for which this happens is smaller.

179
00:17:18,380 --> 00:17:27,519
So, let us look at this here. So, what we are saying is that if we start with the original list of say elements zero to fourteen

180
00:17:27,873 --> 00:17:35,440
okay then the first level the number seven to fourteen already satisfied the heap properties, so whatever values they have we dont have to worry.

181
00:17:36,019 --> 00:17:41,910
So, then we go up. And we may have to swap this with its children, we may have to swap this with the children and so on

182
00:17:41,910 --> 00:17:48,849
So, for four nodes we have to do one level of shifting perhaps to repair the heap property.

183
00:17:49,730 --> 00:17:54,029
Then we go up now one and two are the original values so they may be wrong.

184
00:17:54,029 --> 00:18:04,599
So, again we may have to shift it down one value and then another value. So, now we need two levels of shifting, but we have only two nodes for this. so the number of nodes for which this required is shrinking.

185
00:18:05,450 --> 00:18:10,990
It is halving actually and the number of steps for which you have to do it is increasing by one

186
00:18:11,690 --> 00:18:19,263
So we will not do a careful calculation here, but it turns out that as a result of this this particular way of doing an heapify

187
00:18:19,288 --> 00:18:28,059
by starting from the bottom of the heap and working upwards rather than inserting one at a time into an empty heap actually takes us only linear time order l.

188
00:18:32,269 --> 00:18:36,490
A final use of heap is to actually sort.

189
00:18:37,490 --> 00:18:42,611
So, we are taking out one element at a time starting with the maximum one.

190
00:18:42,636 --> 00:18:53,259
So, it is natural that if we start with a list build a heap and then do n times delete max we will get the list of values in descending order.

191
00:18:53,960 --> 00:19:04,029
So, we build a heap in order n time called delete max n times and extract the elements in descending order, so we get an order n log n algorithm for heap

192
00:19:04,190 --> 00:19:09,470
Now, the question is where do we keep these values well remember that a heap is an array.

193
00:19:09,495 --> 00:19:16,269
So, initially we have a heap. which has n elements ok So, we build this heap.

194
00:19:16,910 --> 00:19:22,599
Now, we said that delete max will remove the element at the top because thats the root.

195
00:19:22,599 --> 00:19:26,940
But it will also create a vacancy here, this is the value that will go to the top

196
00:19:26,940 --> 00:19:30,866
This is the last leaf which will go to the top when we fix the delete max.

197
00:19:30,891 --> 00:19:38,645
So, since there is a vacancy here we can actually move this to this position So, the maximum value will now go to the end of the heap

198
00:19:38,670 --> 00:19:41,849
But the next time we process the heap there will be only n minus one values.

199
00:19:41,849 --> 00:19:45,749
So, we will not look at that value, we will just use from zero to n minus two

200
00:19:46,916 --> 00:19:51,275
right So, again this value will come here, this value will go there.

201
00:19:51,300 --> 00:19:54,599
And now we will have two elements fixed and so on.

202
00:19:54,599 --> 00:20:01,375
So, one by one the maximum value, second maximum value and so on will get into the same list or array in which we are storing the heap.

203
00:20:01,400 --> 00:20:03,970
and eventually the heap will come out in ascending order.

204
00:20:04,279 --> 00:20:07,329
So, this is actually an n log n sort.

205
00:20:07,329 --> 00:20:11,500
So, it has the same asymptotic complexity as merge sort we saw before.

206
00:20:11,500 --> 00:20:19,180
And unlike merge sort which forced us to create a new array everytime we merged two lists this is actually creating a list inplace.

207
00:20:21,740 --> 00:20:31,180
So, to summarize heaps are a tree based implementation of priority queues in which both insert and delete max can be done in log n time.

208
00:20:31,220 --> 00:20:36,430
We can do a bottom up heapify to build a heap in order n time

209
00:20:36,950 --> 00:20:41,740
and these are trees, but they can be manipulated very easily using an array.

210
00:20:42,319 --> 00:20:45,490
Now, in this case we were looking at max heaps.

211
00:20:45,490 --> 00:20:52,059
We can also do a dual construction where we change the heap condition to say that each element must be smaller than its children

212
00:20:52,059 --> 00:20:54,730
In which case we have what is called a min heap

213
00:20:54,730 --> 00:20:58,690
and then instead of delete max the operation we perform is delete min.

214
00:20:58,690 --> 00:21:01,316
Everything is exactly symmetric to the other case.

215
00:21:01,341 --> 00:21:05,550
when we move something up, we have to move it down according to the min condition

216
00:21:05,575 --> 00:21:08,288
and when we insert something at the bottom we have to move it up.

217
00:21:08,313 --> 00:21:12,022
right So, the insert and delete min work exactly like insert and delete max.

218
00:21:12,047 --> 00:21:17,980
except with the comparisons are reversed because we now have a min condition rather than a max condition locally.

219
00:21:18,200 --> 00:21:24,400
And finally, we saw that with a heap we can do sorting in order n log time inplace.

