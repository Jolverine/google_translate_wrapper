1
00:00:03,485 --> 00:00:10,000
So, we have seen that we pass values to functions by substituting values for the arguments that I defined the functions.

2
00:00:10,840 --> 00:00:14,949
And this is effectively the same as having an implicit assignment.

3
00:00:14,949 --> 00:00:22,047
So, when we say power x n and we call it values 3 and 5, then we have this assignment  x equal to 3 and n equal to 5.

4
00:00:22,072 --> 00:00:27,911
It's not really there, but it is as though this code is executed by preceding this assignment there.

5
00:00:27,935 --> 00:00:35,229
And of course, the advantage of calling it as a function is that we don't have to specify x and n in the function definition it comes with the call.

6
00:00:35,229 --> 00:00:38,200
So, for different values of x and n, we will execute the same code.

7
00:00:41,529 --> 00:00:46,340
So, the first thing that Python allows us to do flexibly is to not go by the order.

8
00:00:46,364 --> 00:00:49,299
It's not that the first is x and the second is n.

9
00:00:49,299 --> 00:00:59,146
We can, if we don't remember the order, but we do know the values or the names assigned to them, we can actually call them by using the name of the argument.

10
00:00:59,171 --> 00:01:09,099
So, we can even reverse the thing and say call power I know that x is the bottom value, I know it is x to the power n, but I don't remember whether x comes first or n comes first.

11
00:01:09,099 --> 00:01:14,650
So, I can say okay let's just play safe and say power at n equal to 5, x equal to 4.

12
00:01:14,650 --> 00:01:22,090
And this will correctly associate the value according to the name of the argument and not according to the position.

13
00:01:26,162 --> 00:01:33,489
Another nice feature of Python is that allow, it allows some arguments to be left out and implicitly have default values.

14
00:01:34,222 --> 00:01:43,840
Recall that we have defined this type conversion function int of s which will take a string and try to represent it as an integer if s is a valid representation of an integer.

15
00:01:44,965 --> 00:01:51,129
So, we said that if we give it the string 7 6 then int would convert it to the number 76.

16
00:01:51,683 --> 00:01:59,049
If on the other hand, we give it a string like A 5, since A is not a valid number, A 5 would actually generate an error.

17
00:02:01,542 --> 00:02:09,550
Now, it turns out that int is actually not a function of one argument but two arguments and the second argument is the base.

18
00:02:10,260 --> 00:02:14,139
So, we give it a string and convert it to a number in base b.

19
00:02:14,696 --> 00:02:18,789
And if we don't provide b, by default b has value 10.

20
00:02:19,515 --> 00:02:36,189
So, what is happening in the earlier int conversion is that it's as though we are saying int 76 with base 10, but since we don't provide the 10, Python has a mechanism to take the value that is not provided and substitute with the default value 10.

21
00:02:36,792 --> 00:02:41,560
Now, if we do provide it a value, then for instance, we can even make sense of A 5.

22
00:02:41,810 --> 00:02:51,077
If you have base 16, if you have studied base 16 ever in school, you would know that you have the digit 0 to 9 but base 16 has numbers up to 15.

23
00:02:51,101 --> 00:02:57,500
So, the numbers beyond 9 are usually written using A,B,C,D,E,F

24
00:02:57,525 --> 00:03:02,650
So, A corresponds to what we would think of is number 10 in base 10.

25
00:03:02,659 --> 00:03:08,603
So, if we write A 5 in base 16, then this is the 16th position and this is the 1’s position.

26
00:03:08,628 --> 00:03:12,755
So, we have 16 times 10, because A is 10, + 5.

27
00:03:12,902 --> 00:03:17,050
So, in numeric terms, this will return 165 correctly.

28
00:03:18,480 --> 00:03:20,169
So, how does this work in Python?

29
00:03:22,830 --> 00:03:27,425
So, this would be how internally if you are to write a similar function you would write.

30
00:03:27,852 --> 00:03:29,789
So, you provide the argument.

31
00:03:29,789 --> 00:03:35,199
And for the argument for which you want an option and default argument you provide the value in the function definition.

32
00:03:35,666 --> 00:03:39,669
So, what this definition says is that int takes two arguments s and b.

33
00:03:39,822 --> 00:03:43,780
And b is assumed to be 10 and is hence optional.

34
00:03:43,780 --> 00:03:48,550
If the person omits the second argument then it will automatically take the value 10.

35
00:03:48,550 --> 00:03:52,449
Otherwise, it will take the value provided by the function column.

36
00:03:54,002 --> 00:03:57,159
So, the default value is provided in the function definition.

37
00:03:57,990 --> 00:04:01,810
And if that parameter is omitted, then the default value is used instead.

38
00:04:02,779 --> 00:04:10,195
But, one thing to remember is that this default value is something that is supposed to be available when the function is defined.

39
00:04:10,219 --> 00:04:14,139
It cannot be something which is calculated when the function is called.

40
00:04:14,882 --> 00:04:26,740
So, we saw various functions like quick sort and merge sort and binary search where we were forced to pass along with the array the starting position and the ending position.

41
00:04:27,465 --> 00:04:35,646
Now, this is fine for the intermediate cost but when we want to actually sort a list the first end, we have to always remember to call it with 0 and the length of the list.

42
00:04:35,670 --> 00:04:48,642
So, it would be tempting to say that we define the function as something which takes an initial array A as the first argument and then by default takes a left boundary to be 0 which is fine and the right boundary to be the length of A.

43
00:04:48,808 --> 00:04:54,189
Now, the problem is that this quantity, the length of A depends on A itself.

44
00:04:54,592 --> 00:05:04,273
So, when the function is defined, there will be or may not be a value for A and whatever value you have chosen for A if there is one that length will be taken as a default.

45
00:05:04,298 --> 00:05:07,067
It won't be dynamically computed each time we call it.

46
00:05:07,435 --> 00:05:09,283
So, this does not work, right?

47
00:05:09,308 --> 00:05:18,430
So, when you have default values, the default value has to be a static value which can be determined when the definition is read for the first time, not when it is executed.

48
00:05:21,725 --> 00:05:24,118
So, here is a simple prototype.

49
00:05:24,143 --> 00:05:27,189
So, suppose we have a function with 4 arguments A,B,C,D.

50
00:05:27,375 --> 00:05:44,740
And we have C as a default value 40 and D as a default value 22, then if you have a call with just two arguments then this will be associated with A and B and so this will be interpreted as f 13 12 and for the missing arguments C and D you get the defaults, 14 and 22.

51
00:05:45,942 --> 00:05:57,420
On the other hand, you might provide three arguments in which case A becomes 13, B becomes 12 as before and C becomes 16, but D is left unspecified, so it pick up the default value.

52
00:05:57,420 --> 00:06:02,319
So, this is interpreted as f of 13, 12, 16 and the default value 22.

53
00:06:04,549 --> 00:06:08,759
So, the thing to keep in mind is that the default values are given by position.

54
00:06:08,759 --> 00:06:14,889
There is no way in this function to say that 16 should be given for D and I want the default value for C.

55
00:06:15,299 --> 00:06:18,360
We can only drop values by position from the end.

56
00:06:18,360 --> 00:06:23,910
So, if I have two default values and if I want to only specify the second of them, it is not possible.

57
00:06:23,910 --> 00:06:26,470
I will have to redefine the function to reorder it.

58
00:06:27,450 --> 00:06:38,680
So, therefore, we must make sure that when you use these default values, they come at the end and they are identified by position and you don't mix it up and don't confuse yourself by combining these things in a random manner.

59
00:06:39,899 --> 00:06:41,980
So, the order of the arguments is important.

60
00:06:45,319 --> 00:06:49,779
A function definition associates a function body with a name.

61
00:06:49,779 --> 00:06:55,509
So, it says the name f will be interpreted as a function which takes some arguments and does something.

62
00:06:57,303 --> 00:07:02,529
So, in many ways, Python interprets this like any other assignment of the value to a name.

63
00:07:04,279 --> 00:07:10,913
So, for instance, this value could be defined in different ways, multiple ways, in conditional ways.

64
00:07:10,937 --> 00:07:17,529
So, as you go along a function can be redefined or it can be defined in different ways depending on how the computation proceeds.

65
00:07:17,949 --> 00:07:21,029
Here is an example of a conditional definition.

66
00:07:21,029 --> 00:07:25,060
If it is true, you define f one way otherwise you define f another way.

67
00:07:25,060 --> 00:07:31,240
So, depending on which of these conditions held when this definition was executed, later on, value of f will be different.

68
00:07:31,519 --> 00:07:38,079
Now, this is not to say that this is a desirable thing to do because you might be confused as to what f is doing.

69
00:07:38,180 --> 00:07:45,939
But, there are situations where you might want to write f in one way or in other way we depending on how the application is proceeding and Python does allow you to do it.

70
00:07:45,939 --> 00:07:52,540
Probably at an introductory day to Python this is not very useful but this is useful to know that such a possibility exists.

71
00:07:52,540 --> 00:07:55,779
And in particular you can go on and redefine f as you go on.

72
00:07:59,380 --> 00:08:06,189
Another thing you can do in Python which may seem a bit strange to you is you can take an existing function and map it to a new name.

73
00:08:06,739 --> 00:08:12,639
So, we can define a function f which as we said associates with the name f, the body of this function.

74
00:08:12,639 --> 00:08:15,639
At a later stage, we can say j,g equal to f.

75
00:08:16,659 --> 00:08:29,800
And what this means is now that we can also use g of a,b,c and it will mean the same as f of a,b,c. So if we use g in the function it will use exactly the same function as, is exactly like assigning one list to another or one dictionary to another 

76
00:08:31,202 --> 00:08:33,009
Now, why would you want to do this?

77
00:08:35,473 --> 00:08:42,159
So, one useful way in which you can do this, use this facility is to pass the function to another function.

78
00:08:43,206 --> 00:08:47,649
So, suppose we want to apply a given function f to its argument n times.

79
00:08:47,949 --> 00:08:55,682
Then, we can write a generic function like this called apply which takes three arguments the first is a function okay?

80
00:08:55,949 --> 00:09:01,210
The second is the argument and the third is the number of times, the repetition.

81
00:09:04,759 --> 00:09:11,740
So, we start with the value that you provide it and as many times as you are asked to, you keep iterating the function f of 

82
00:09:12,820 --> 00:09:14,559
So, let's look at a concrete example.

83
00:09:14,965 --> 00:09:18,970
So, supposing we have defined a function square of x, which is returns x times x.

84
00:09:19,599 --> 00:09:25,090
And now we can say, apply square to the value 5 twice.

85
00:09:26,478 --> 00:09:33,656
So, what this means is, apply square of 5 and then square of that, so do square twice.

86
00:09:33,680 --> 00:09:37,389
And therefore, you get 5 square 25 25 square 625.

87
00:09:37,935 --> 00:09:43,203
So, what is happening here is that square is being assigned to f.

88
00:09:43,915 --> 00:09:47,744
5 is being assigned to x and 2 is being assigned to n.

89
00:09:47,817 --> 00:09:52,149
So, this is exactly as we said like before like saying f is equal to square.

90
00:09:52,715 --> 00:10:07,570
So, in this sense, being able to take a function name and assign it to another name is very useful because it allows us to pass functions from one place to another place and execute that function inside the another function without knowing in advance what that function is.

91
00:10:10,413 --> 00:10:14,919
One practical use of this is to customize functions that are sort.

92
00:10:16,059 --> 00:10:19,840
Now, sometimes we need to sort values based on different criteria.

93
00:10:20,552 --> 00:10:31,570
So, we might have an abstract compare function which returns - 1 if the first argument is smaller, 0 if the two arguments are equal and + 1 if the first argument is bigger than second.

94
00:10:33,243 --> 00:10:41,597
So, in comparing strings we may have two different ways of comparing strings in mind and we might want to check the difference when we sort by these two different ways.

95
00:10:41,731 --> 00:10:46,269
So, we might have one sort in which we compare strings in dictionary order .

96
00:10:46,562 --> 00:10:52,059
So, a string like aab will come before ab because the second position a is smaller than b.

97
00:10:53,045 --> 00:10:57,129
So, this will result in - 1, because the first argument is smaller than the second argument.

98
00:10:57,982 --> 00:11:05,950
If, on the other hand, we want to compare the strings by length, then the same argument will give us + 1 because aab as length 3 and is longer than ab.

99
00:11:07,163 --> 00:11:14,130
So, we could write a sort function which takes the list and takes a second argument which is how to compare elements in the list.

100
00:11:14,416 --> 00:11:22,450
So, the sort function itself does not need to know what the elements in the list are, whenever it's given a list of arbitrary values, it's also told how to compare them.

101
00:11:22,596 --> 00:11:30,519
So, all it needs to do is apply this function to two values and check if the answer is - 1, 0 or + 1 and interpret it as less than, equal to or greater than.

102
00:11:31,368 --> 00:11:36,755
And if you want, you can combine it with the earlier feature, which is you can give it a default function.

103
00:11:36,779 --> 00:11:41,200
So, if you don't specify a sort function there might be an implicit function that the sort function uses.

104
00:11:41,781 --> 00:11:44,679
Otherwise, it will use the comparison function that you wrote.

105
00:11:49,197 --> 00:11:55,179
So, to summarize function definitions behave just like other assignments of values to names.

106
00:11:55,179 --> 00:11:57,370
We can reassign a new definition to a function.

107
00:11:57,440 --> 00:11:59,919
We can define it conditionally and so on.

108
00:12:00,811 --> 00:12:07,330
Crucially, you can use one function and make it point, name point to another function.

109
00:12:07,330 --> 00:12:11,169
And this is implicitly used when we pass functions to other function.

110
00:12:11,826 --> 00:12:20,139
And in situations like sorting, you can make your sorting more flexible by passing a comparison function which is appropriate to the values we sorted.

