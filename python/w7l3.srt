1
00:00:02,270 --> 00:00:07,349
So, now that we have seen the basics about how to define classes and objects.

2
00:00:07,349 --> 00:00:09,730
Let's define an interesting data structure.

3
00:00:09,730 --> 00:00:13,599
Suppose, we want to implement our own version of a list.

4
00:00:13,849 --> 00:00:17,320
So, a list is basically a sequence of nodes.

5
00:00:17,390 --> 00:00:21,969
Each node in the sequence stores a value and points to the next node.

6
00:00:21,969 --> 00:00:29,170
So, in order to go through the list we have to start at the beginning and walk following these pointers till we reach the last pointer which points to nothing.

7
00:00:29,170 --> 00:00:36,421
So, we have a list of the form  v 1, v 2, v 3, v 4 in python notation.

8
00:00:36,446 --> 00:00:41,015
Then this is how we would imagine it is actually represented.

9
00:00:41,040 --> 00:00:42,659
There are four nodes.

10
00:00:42,659 --> 00:00:46,451
The list l itself which we have set up points to the first node in the list.

11
00:00:46,476 --> 00:00:50,254
v 1 points to v 2, v 2 points to v 3 and v 3 points to v 4.

12
00:00:50,279 --> 00:00:52,770
The final node points to nothing.

13
00:00:52,795 --> 00:00:55,479
That indicates we reach the end of the list.

14
00:00:56,149 --> 00:01:00,820
So, in this representation what would the empty list look like?

15
00:01:00,950 --> 00:01:14,017
It's natural to assume that the empty list will consist of a single node which has both the value and the next node pointer set to None.

16
00:01:14,041 --> 00:01:24,598
Whereas for instance a singleton would be a single node in which we have the value v 1 and the next set to None. So, this is the convention that we shall follow for our representation of a list.

17
00:01:24,623 --> 00:01:33,489
So, notice that unless we have an empty list with a single node None None no other node in a list can have value None.

18
00:01:33,489 --> 00:01:37,510
So, this is something that we will implicitly assume and use that.

19
00:01:37,510 --> 00:01:43,500
Checking for the value None will tell us it's an empty list and we will never find a None in the middle of a list.

20
00:01:43,500 --> 00:01:47,519
So, we distinguish between a singleton and an empty list purely based on the value.

21
00:01:47,519 --> 00:01:49,510
Both of them consists of a single node.

22
00:01:50,209 --> 00:01:55,901
Now, the reason that we have to do this is because actually python doesn't allow us to create an empty list.

23
00:01:55,925 --> 00:02:05,535
If we say something like L is equal to None and we want this to denote the empty list. The problem is that None doesn't have a type as far as python’s value system is concerned.

24
00:02:05,560 --> 00:02:11,393
So, once we have None we cannot apply the object functions, we are going to create for this list type.

25
00:02:11,417 --> 00:02:15,863
So, we need to create something which is empty of the correct type.

26
00:02:15,887 --> 00:02:18,189
So, we need to create at least one node.

27
00:02:18,189 --> 00:02:24,219
And that is why we need to use this kind of a representation in order to denote an empty list.

28
00:02:25,819 --> 00:02:28,637
So, here is the basic class that we are going to use.

29
00:02:28,661 --> 00:02:30,280
So, it's a class node.

30
00:02:30,280 --> 00:02:35,447
So, inside each node we have two attributes value and next as we said.

31
00:02:35,472 --> 00:02:41,469
Remember that self is always used with every function to denote the object under consideration.

32
00:02:41,469 --> 00:02:45,780
We will use this default scheme that if we don't say anything we create an empty list.

33
00:02:45,805 --> 00:02:48,400
So, the init value, so this should be.

34
00:02:49,699 --> 00:02:56,740
So, the initial value is by default None unless I provide you an initial value in which case you create a list with that value.

35
00:02:57,349 --> 00:03:07,090
Because of our assumption about empty list all we need to do to check whether a list is empty is to <NC>?</NC> check whether the value at the initial node is None or not.

36
00:03:07,090 --> 00:03:16,504
So, we take the list we are pointing to and look at the very first value which will be self dot value and ask whether it's None. If it is None it's empty, if it is not None it's not empty.

37
00:03:17,930 --> 00:03:20,620
So, here is a typical thing.

38
00:03:20,620 --> 00:03:24,370
So, we say l1 is equal to node. This creates an empty list.

39
00:03:24,370 --> 00:03:28,509
Because it is not provided any value, so the default initial value is going to be None

40
00:03:28,699 --> 00:03:38,740
If I say l2 is equal to node 5, this will create a node with the value 5. So, it will create the singleton list that we would normally write in python like this.

41
00:03:38,990 --> 00:03:42,789
So, if I ask whether l1 is empty, the answer will be true.

42
00:03:42,789 --> 00:03:47,469
If I ask whether l2 is empty, the answer will be false because self dot value is not None.

43
00:03:49,639 --> 00:03:57,759
Now, once we have a list what we would like to do is manipulate it. So, the first thing that we might want to do is add a value at the end of the list.

44
00:03:58,250 --> 00:04:05,830
So, if the list is already empty then we have a single node which has value None and we want to make it a singleton node.

45
00:04:05,830 --> 00:04:11,620
A singleton list with value v. So, we want to go from this to this.

46
00:04:12,199 --> 00:04:20,571
So, remember that in a singleton node instead of None we have the value v over here.

47
00:04:20,595 --> 00:04:24,490
So, that's all we need to do. We need to just replace the None by v.

48
00:04:25,730 --> 00:04:32,259
If we are at the last element of the list, and we know that we are at the last element of the list because the next value is None

49
00:04:32,259 --> 00:04:36,730
Then what we need to do is create a new value, so we walk to the end of the list.

50
00:04:37,759 --> 00:04:40,861
Then we reach None.

51
00:04:40,885 --> 00:04:51,759
So, now we create a new element here with the value v and we make this element point to it. So, we create a new node v and set the next field of the last node to point to the new node.

52
00:04:53,000 --> 00:05:04,750
If this is not the last value then we can just recursively say to the rest of the list treat this as the new list starting at the next element and take the next element and recursively append v to that.

53
00:05:04,750 --> 00:05:08,889
So, this gives us a very simple recursive definition of append.

54
00:05:08,889 --> 00:05:19,240
So, we take append and we want to append v to this list. So, if it is empty, then we just set the value to v. So, this just converts the single node with value None to the single node with value v.

55
00:05:19,240 --> 00:05:27,180
Otherwise, if we are at the last node that is self dot next is None then we create a new node with the value v.

56
00:05:27,180 --> 00:05:29,454
And we set our next pointer to new node.

57
00:05:29,479 --> 00:05:34,828
Remember when we create a new node the new node automatically is created by our init function with the next None.

58
00:05:34,852 --> 00:05:41,500
So, we would now create a new node which looks like v and None and we will set our next pointer to point to it.

59
00:05:41,839 --> 00:05:46,476
And the final thing is that if it is not None then we have something else after us.

60
00:05:46,501 --> 00:05:54,029
So, we go to the next element self dot next and with respect to that next element we reapply the append function with the value v.

61
00:05:54,053 --> 00:05:55,779
So, this is the recursive call.

62
00:05:58,160 --> 00:06:07,996
So, we have been abundantly careful in making sure that this is parsable. So we have put this bracket saying that we take the objects self dot next and apply append to that.

63
00:06:08,021 --> 00:06:09,597
Actually python will do this correctly.

64
00:06:09,622 --> 00:06:14,054
So, we need not actually put the bracket. So, we can just write self dot next dot append v.

65
00:06:14,079 --> 00:06:20,050
python will correctly bracket this as self dot next dot append. So, this dot is taken from the right.

66
00:06:24,050 --> 00:06:30,969
Now, instead of recursively going to the end of the list, we can also scan the end of the list till the end iteratively.

67
00:06:30,994 --> 00:06:37,060
So, we can write a loop which keeps traversing these pointers until we reach a node whose next is None.

68
00:06:37,060 --> 00:06:40,750
If the list is empty as before we replace the value None by v.

69
00:06:40,750 --> 00:06:43,990
Otherwise, we scan the list till we reach the last element.

70
00:06:43,990 --> 00:06:49,870
And then once we reach the last element as in the earlier case we create a new node and make the last element point to it.

71
00:06:51,470 --> 00:06:57,629
So, this gives us an append which is iterative, so we call it append i just to indicate that's iterative.

72
00:06:57,629 --> 00:06:58,925
So, the first part is the same.

73
00:06:58,949 --> 00:07:03,705
If the current list is empty, then we just set the value to be v and we return.

74
00:07:03,729 --> 00:07:07,720
Otherwise, we want to walk down the list.

75
00:07:08,000 --> 00:07:13,982
So, we set up a temporary pointer to point to the current node that we are at.

76
00:07:14,007 --> 00:07:18,600
So long as the next is None we keep shifting temp to the next value.

77
00:07:18,600 --> 00:07:25,379
So, we just write a loop which says while temp dot next is not None just keep going from temp to temp dot next.

78
00:07:25,404 --> 00:07:26,862
So, just keep shifting temp.

79
00:07:26,887 --> 00:07:33,100
Finally, when we come out of this loop at this point, we know that temp dot next is None.

80
00:07:33,439 --> 00:07:37,681
So, this is the condition under which we exit the loop.

81
00:07:37,706 --> 00:07:41,212
The node temp is now pointing to the last node in the current list.

82
00:07:41,236 --> 00:07:50,098
At this point, we do exactly what we did in the recursive case, we create a new node with the value v and we make this last node point to this new node.

83
00:07:50,122 --> 00:07:54,009
So, we reset the next of temp from None to the new node.

84
00:07:56,990 --> 00:08:00,471
So, what if we don't want to append, but we want to insert.

85
00:08:00,553 --> 00:08:06,089
Now it looks normally that insert should be easier than append, but actually insert is a bit tricky.

86
00:08:06,089 --> 00:08:09,450
So, by insert we mean that we want to put a value at the beginning.

87
00:08:09,475 --> 00:08:14,724
So, we want to put a node here which has v and make this point here.

88
00:08:14,749 --> 00:08:16,149
So, this is what we want to do.

89
00:08:16,939 --> 00:08:25,913
Now, the problem with this is that after we create a new node we cannot make this point here and this point here.

90
00:08:25,938 --> 00:08:28,480
There is no problem in making the new node point to v 1.

91
00:08:28,480 --> 00:08:37,094
But if we reassign the value of l or inside an object if we reassign the value of self then this creates a completely different object.

92
00:08:37,118 --> 00:08:45,169
So, we saw this when we were looking at how parameters are passed and mutable values are passed we said that if we pass a mutable value to a function.

93
00:08:45,194 --> 00:08:51,891
So long as we don't reassign that thing, any mutation inside the function will be reflected outside the function.

94
00:08:51,915 --> 00:08:57,730
But if we reassign to the <FW>funct</FW> to the list or dictionary inside the function we get a new copy.

95
00:08:57,730 --> 00:08:59,515
After that any change we make is off.

96
00:08:59,540 --> 00:09:10,169
So, the same way if we reassign x L or self to point to a new node then we will lose the connection between the parameter we pass to the function and the parameter we get back.

97
00:09:10,169 --> 00:09:13,620
So, we must be careful not to make l point to this thing.

98
00:09:13,644 --> 00:09:15,940
So, we cannot change where l points to.

99
00:09:16,669 --> 00:09:18,388
So, how do we get around this problem?

100
00:09:18,413 --> 00:09:26,230
We have created a new node; we want to make l point to it but we are not allowed to do so because if we do so, then python will disconnect the new l from the old l.

101
00:09:26,450 --> 00:09:28,534
So, there is a very simple trick.

102
00:09:28,558 --> 00:09:34,240
What we do is we don't change the identity of the node we change what it contains.

103
00:09:34,265 --> 00:09:38,559
So, we know now that v1 is the old first node and v is the new first node.

104
00:09:38,600 --> 00:09:43,120
But we can't make l point to the new first node so we exchange the values.

105
00:09:43,120 --> 00:09:46,713
So, what we do is we replace v1 by v and v by v1.

106
00:09:46,738 --> 00:09:48,549
So, now the values are swapped.

107
00:09:48,549 --> 00:09:52,289
And we also have to do similar thing further what is pointing where.

108
00:09:52,289 --> 00:09:57,404
So, l is now pointing to v as the first node but now we have bypassed v1 which is a mistake.

109
00:09:57,428 --> 00:10:03,309
So, we must now make the first node point to the new node and the new node point to the old second node.

110
00:10:03,309 --> 00:10:11,433
So, by doing this kind of plumbing, what we have ensured is that the new list looks like we have inserted a v before the v1.

111
00:10:11,458 --> 00:10:20,110
Actually we have inserted a new node in between v and v 2 and we have just changed the links to make it appear as though the new node as second and not first.

112
00:10:22,700 --> 00:10:25,029
So, here is the code for insert.

113
00:10:25,029 --> 00:10:29,822
So, as usual if you have an empty list insert is easy, we just have to change None to v.

114
00:10:29,847 --> 00:10:32,901
So insert and append both behave the same way with an empty list.

115
00:10:32,925 --> 00:10:35,563
We go from the empty list to list v.

116
00:10:35,587 --> 00:10:37,659
It doesn't matter whether you are inserting or appending.

117
00:10:37,730 --> 00:10:42,090
Otherwise, we create this new node and then we do this.

118
00:10:42,090 --> 00:10:47,744
Swapping of values between the current node that self is pointing to that is the head of the list and the new node.

119
00:10:47,768 --> 00:10:56,178
So, we exchange the values; we set self dot value to new node dot value and simultaneously new node dot value to self dot value using this python simultaneous assignment.

120
00:10:56,202 --> 00:11:02,159
Similarly, we take self dot next which was pointing to the next node and make it point to the new node.

121
00:11:02,159 --> 00:11:05,020
And the new node instead should point to what v were pointing to earlier.

122
00:11:05,044 --> 00:11:07,255
So, new node dot next is self dot next.

123
00:11:07,279 --> 00:11:09,018
So this is how we insert.

124
00:11:09,043 --> 00:11:18,580
And insert as we saw is a little bit more complicated than append because of having to handle the initial way in which l points to the list or self points to the list.

125
00:11:21,230 --> 00:11:23,379
What if we want to delete a node?

126
00:11:23,379 --> 00:11:27,240
So, how do we specify it to delete a node when we specify it by a value.

127
00:11:27,240 --> 00:11:31,230
Let's suppose you want to delete the second node in this list.

128
00:11:31,230 --> 00:11:32,569
How would we delete it?

129
00:11:32,593 --> 00:11:37,115
Again just as we did insert we would do some re-plumping or reconnection.

130
00:11:37,139 --> 00:11:43,509
So, we take the node that we want to delete and we just make the link that points to v2 to bypass it.

131
00:11:43,509 --> 00:11:47,049
So, we take the link from v 1 and make it directly point to v 3.

132
00:11:47,049 --> 00:11:54,766
So, in essence all the delete requires us to do is to reassign the pointer from before the delete node to the pointer after this deleted node.

133
00:11:54,790 --> 00:12:03,190
It actually does not physically remove that object from memory, but it just makes it inaccessible from the link array.

134
00:12:05,659 --> 00:12:10,149
So, we provide a value v and we want to remove the first occurrence of v.

135
00:12:11,000 --> 00:12:13,998
So, we scan the list for the first v.

136
00:12:14,022 --> 00:12:20,163
Now notice that in this plumbing procedure we need to be at v 1 in order to make it point to v 3.

137
00:12:20,187 --> 00:12:26,351
So, if you want to delete the next node then we are in good shape because we can take the next dot next and assign it to the current x.

138
00:12:26,376 --> 00:12:27,783
We should look onestep ahead.

139
00:12:27,807 --> 00:12:33,450
If we are already at v 2, then we have gone past v 1 and we can't go back to v 1 easily with the way we have set up our list.

140
00:12:33,450 --> 00:12:36,970
Because it only goes forwards, we can't go back to v1 and change it.

141
00:12:36,970 --> 00:12:42,017
So, what we will actually do is we will scan <NS>cough</NS> by looking at the next value.

142
00:12:42,041 --> 00:12:52,621
So, if the self dot next dot value is v that is the next node is to be deleted, then we bypass it by saying that the current node's next is not the next node that we had, but the next node's next.

143
00:12:52,645 --> 00:12:57,661
So, self dot next is reassigned to self dot next dot next.

144
00:12:57,686 --> 00:12:59,080
By pass the next node.

145
00:13:01,519 --> 00:13:10,899
So, as before like with insert, the only thing we have to be careful about is if we have to delete the first value in the list.

146
00:13:11,389 --> 00:13:22,220
So, if you want to delete the first value in the list exactly like we had with insert, the natural thing would be to now say that l should point to the second value in the list.

147
00:13:22,244 --> 00:13:33,850
But we cannot point l there because if we reassign the node that l points to, then it will create a new object and it will break the connection between the parameter we pass and the thing we get back.

148
00:13:34,250 --> 00:13:35,734
So, we use the same trick.

149
00:13:35,758 --> 00:13:42,850
What we do is we copy the value v2 from the next node.

150
00:13:42,950 --> 00:13:48,909
So, we just copy this value from here to here and then we delete v 2.

151
00:13:49,088 --> 00:13:51,973
We wanted to delete the first node.

152
00:13:51,998 --> 00:13:55,429
We are not allowed to delete the first node because we can't change what l points to.

153
00:13:55,453 --> 00:14:02,950
Instead we take the value in the second node which was v 2, copy it here and then pretend to delete v 2 by making the first node point to the third.

154
00:14:05,960 --> 00:14:08,669
So, here is a part of the delete function.

155
00:14:08,669 --> 00:14:16,470
So, first of all if you are looking for v and then we don't find it.

156
00:14:16,495 --> 00:14:18,668
Sorry, in this code it's called x.

157
00:14:18,693 --> 00:14:20,885
So, this is the deleting value x if you want.

158
00:14:21,139 --> 00:14:31,899
If we say that the list is empty then obviously we can't delete it because delete says if there is a node with value x then delete.

159
00:14:32,570 --> 00:14:35,231
So, if it is empty we do nothing.

160
00:14:35,255 --> 00:14:41,230
Otherwise, if the self dot value is x, the first node is to be deleted.

161
00:14:41,750 --> 00:14:47,565
Then if there is only one node then we are going from x to empty, this is easy.

162
00:14:47,589 --> 00:14:51,472
If there is no next node then we have only a singleton.

163
00:14:51,496 --> 00:14:53,769
Then we just set the value to be None and we are done.

164
00:14:53,874 --> 00:14:55,168
So, this is the easy case.

165
00:14:55,192 --> 00:15:00,634
But if it is not the first node I mean it is the first node and this is not also the only node in the list.

166
00:15:00,658 --> 00:15:06,779
Then what we do is what we said before we copy the next value, we pretend that we are deleting the second node.

167
00:15:06,779 --> 00:15:11,301
So, we copy the second value into the first value and we delete the next node by bypassing.

168
00:15:11,325 --> 00:15:13,149
So, this is the bypass.

169
00:15:13,149 --> 00:15:17,759
So, this is part of the function this is the <FW>diff</FW> the tricky part which is how you delete the first value.

170
00:15:17,759 --> 00:15:19,769
If it is only one value make it None.

171
00:15:19,769 --> 00:15:23,289
If not bypass the second node by copying the second node to the first node.

172
00:15:23,570 --> 00:15:28,177
If this is not the case, then we just walk down and find the first x to delete.

173
00:15:28,201 --> 00:15:40,120
So, we start as this is like our iterative append, we start pointing to self and so long as we have not reached the end of the list, if we find the next value is x then we bypass it.

174
00:15:40,490 --> 00:15:44,769
And if we reach the end of the list and we haven't found it we do nothing we just have to return.

175
00:15:45,080 --> 00:15:48,629
In this case it's not like append where when we reach the end of the list we have to append.

176
00:15:48,654 --> 00:15:53,225
Here, if we don't find an x by the time we reach the end of the list and there nothing to be done.

177
00:15:55,700 --> 00:16:02,588
So, just for completeness here is the full function so this was the first slide we saw which is the case when the value to be deleted is in the first node.

178
00:16:02,612 --> 00:16:07,090
This is the second case when we walk down the list looking for the first x to delete.

179
00:16:10,549 --> 00:16:17,350
So, just like append can be done both iteratively and recursively, we can also delete recursively.

180
00:16:17,350 --> 00:16:23,769
If it is the first node we handle it as a special way by moving the second value to the first and bypassing it as we did before.

181
00:16:23,769 --> 00:16:34,720
Otherwise, we just point to the next node and ask the list starting at the next node what is normally called the tail of the list to delete v from itself.

182
00:16:35,784 --> 00:16:41,820
So, the only thing that we have to remember in this is that if we reach the end of the list and we delete the last element.

183
00:16:41,845 --> 00:16:46,000
So, supposing it turns out the value v to be deleted is here.

184
00:16:46,000 --> 00:16:52,259
So, we come here and then we delete it, what will end up with is finding a value None.

185
00:16:52,259 --> 00:16:59,080
When we delete it from here, it is as though we take a singleton element v and delete v from a singleton and will create None.

186
00:16:59,389 --> 00:17:01,056
So, this is the base case.

187
00:17:01,081 --> 00:17:08,920
So, if we are recursively deleting as we go, whenever we delete from the last node it's as though we are deleting from a singleton list with value v.

188
00:17:08,920 --> 00:17:11,834
We are not allowed to create a value None at the end.

189
00:17:11,858 --> 00:17:23,698
So, we have to check when we create the next thing if we delete the next value and its value becomes None then we should remove that item from the list.

190
00:17:23,722 --> 00:17:27,559
So, this is the only tricky thing that when we do a recursive delete we have to be careful.

191
00:17:27,583 --> 00:17:30,160
After we delete we have to check what has happened.

192
00:17:31,339 --> 00:17:34,329
So, this part is the earlier part.

193
00:17:34,700 --> 00:17:38,160
Now, this is the recursive part. So, recursive part is fairly straightforward.

194
00:17:38,160 --> 00:17:46,740
This is the first part is only delete the first element from a list, but the recursive part we check if self dot next is equal to None then we delete recursively that's fine.

195
00:17:46,740 --> 00:17:48,121
So, this is the delete call.

196
00:17:48,145 --> 00:17:52,591
Now, after the delete is completed we check whether the next value has actually become None.

197
00:17:52,615 --> 00:17:56,551
Have we actually ended up at the last node and deleted the last node.

198
00:17:56,575 --> 00:17:58,377
If so, then we remove it.

199
00:17:58,401 --> 00:18:06,972
So, this we can either write self dot next is equal to self dot next dot next or we could even just write self dot next equal to None. which is probably a cleaner way of saying it because it can only happen in the last node.

200
00:18:06,996 --> 00:18:09,034
So, we make this node the last node.

201
00:18:09,527 --> 00:18:12,726
Remember, if the next node is None it's next of next will also be None.

202
00:18:12,758 --> 00:18:14,670
So, this has the same effect.

203
00:18:14,670 --> 00:18:16,380
self dot next dot next must be None.

204
00:18:16,404 --> 00:18:19,559
So we can also directly assign self dot next equal to None.

205
00:18:19,559 --> 00:18:21,747
and it would basically make this node the last node.

206
00:18:21,771 --> 00:18:31,299
So the only thing that we remember about recursive delete is when we reach the end of the list and we have deleted this list this becomes None then we should terminate the list here and remove this node.

207
00:18:34,009 --> 00:18:38,799
Finally, let's write a function to print out a list so that we can keep track of what's going on.

208
00:18:38,799 --> 00:18:44,184
So, we will print out a list by just constructing a python list out of it and then using str on the python list.

209
00:18:44,208 --> 00:18:47,209
So, we want to create a python list from the values in our list.

210
00:18:47,234 --> 00:18:53,339
So, we first initialize our list that we are going to produce for the empty list.

211
00:18:53,339 --> 00:19:00,180
If our list has nothing, then we return the string value of the empty list.

212
00:19:00,180 --> 00:19:07,259
Otherwise, we walk down the list and we keep adding each value using the append function.

213
00:19:07,259 --> 00:19:13,301
So, we keep appending each value that we have stored in each node building up a python list in this process.

214
00:19:13,325 --> 00:19:17,049
And finally, we return whatever is the string value of that list.

215
00:19:17,480 --> 00:19:21,910
So, let's look at some python code and see how this actually works.

216
00:19:25,069 --> 00:19:32,259
So, here we have code which exactly reflects what we did in the slides, we have chosen to use the recursive versions for both append and delete.

217
00:19:32,259 --> 00:19:42,039
So, we start with this <FW>initial</FW> initialization which sets a the initial value to be None by default or otherwise v as the argument provided.

218
00:19:42,039 --> 00:19:45,670
Then is empty just checks where the self dot value is None.

219
00:19:45,670 --> 00:19:54,220
We had written in more compact form in the slide by saying just return self dot value equal to equal to None but we have expanded it out as an if statement here.

220
00:19:54,220 --> 00:19:57,910
Now, <NS>cough</NS> this is the append function.

221
00:19:57,910 --> 00:20:03,910
So, append just checks if the current node is empty.

222
00:20:04,099 --> 00:20:07,125
Then, it puts it here.

223
00:20:07,150 --> 00:20:16,721
If you have reached the last node it creates a new node and makes the last node point to the new node otherwise it recursively appends.

224
00:20:18,280 --> 00:20:22,320
Then, we have this insert function here.

225
00:20:24,200 --> 00:20:26,500
So, this insert function.

226
00:20:28,009 --> 00:20:32,650
Again, if it is empty then it just creates a singleton list.

227
00:20:32,650 --> 00:20:37,589
otherwise it creates a new node and exchanges the first node and the new node.

228
00:20:37,589 --> 00:20:41,680
So, this particular thing here is the place where we create node.

229
00:20:41,839 --> 00:20:49,686
This swap the pointers so that the what self points to doesn't change but rather we create a reordering of the new node and the first node.

230
00:20:49,711 --> 00:20:54,730
So, the new node becomes a second node and the first node now has the value that we just added.

231
00:20:55,789 --> 00:20:59,619
Finally, we can come down to the recursive delete.

232
00:21:00,559 --> 00:21:04,000
Okay. So, the recursive deletes again says that.

233
00:21:10,039 --> 00:21:13,210
If the list is empty then we do nothing.

234
00:21:13,210 --> 00:21:20,896
Otherwise, if the first value is to be deleted then we have to be careful and we have make sure delete the second value by actually copying the second node into the first.

235
00:21:20,921 --> 00:21:31,029
If that is not the case and this will recursively delete, but then we finish the delete we have to delete the spurious empty node at the end of the list in case we have accidentally created.

236
00:21:31,029 --> 00:21:36,166
So, these two lines here just make sure that we don't leave a spurious empty node at the end of the list.

237
00:21:36,190 --> 00:21:41,140
And finally, we have this str function.

238
00:21:47,839 --> 00:21:53,710
This creates a python list from our values and eventually returns a string representation of that list.

239
00:21:54,740 --> 00:21:56,500
So, if we now

240
00:21:58,099 --> 00:22:00,910
run this by importing.

241
00:22:03,874 --> 00:22:13,349
Then we could say for instance that l is a list with value 0 and if we say print l.

242
00:22:13,349 --> 00:22:15,482
Then we will get this representation 0 to l.

243
00:22:15,506 --> 00:22:18,730
We could for instance put this in a loop and say.

244
00:22:20,210 --> 00:22:24,549
for i in range 1 say 11.

245
00:22:32,860 --> 00:22:35,423
l dot append i

246
00:22:36,710 --> 00:22:38,200
okay. And then

247
00:22:38,359 --> 00:22:44,242
If we at this point print L, then we get 0 to 10 as before.

248
00:22:44,266 --> 00:22:51,480
Now, we say l dot delete 4 for instance and we print l then 4 is gone and so on.

249
00:22:51,504 --> 00:22:57,519
If we say l dot insert twelve and print l.

250
00:22:59,210 --> 00:23:02,538
then 12 has to begin. So, you can check that this works.

251
00:23:02,563 --> 00:23:06,960
Notice that we are getting this empty bracket, this is a return value. So when we wrote this return,

252
00:23:06,960 --> 00:23:11,369
We wrote with the empty argument and then we get this empty tuple.

253
00:23:11,369 --> 00:23:16,119
You can just write a return with nothing and then it won't display this funny return value.

254
00:23:16,119 --> 00:23:21,849
But what is actually important is that the internal representation of our list is correctly changing with the functions that we have written.

