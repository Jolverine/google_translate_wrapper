1
00:00:01,915 --> 00:00:13,630
So for the last lecture this week we look at some useful things which will crop up and which do not fit anywhere else specific, and so we just combined a couple of them into this one single presentation.

2
00:00:13,640 --> 00:00:22,079
So we had an example when we were doing the input and print about how to prompt the user in case they had an invalid entry.

3
00:00:22,079 --> 00:00:30,660
So, we were trying to read a number and what we said was that we would input some string that the user provides.

4
00:00:30,660 --> 00:00:37,170
So, we give them a message saying enter a number, they provide us with the string and then we try to convert it using the int function.

5
00:00:37,170 --> 00:00:41,890
And this int function will fail if the user has provided a string which is not a valid integer.

6
00:00:41,890 --> 00:00:48,545
In which case, we will get a value error and if we get a value error, we print out a message saying try again,
and we go back.

7
00:00:48,569 --> 00:01:01,390
Ok and finally if we succeed, that is this try succeeds, the int works, and then we will exit from this try block, go to the else and the else will break out of the loops, so this we had already seen.

8
00:01:01,880 --> 00:01:10,299
Now, the question is what if we want to change this so that we do nothing, ok, we do not want to do this.

9
00:01:12,109 --> 00:01:20,370
So in other words, if the user does not present a valid value, instead of telling them why, we just keep saying enter a number.

10
00:01:20,370 --> 00:01:22,283
We have seen this many number of times right.

11
00:01:22,319 --> 00:01:25,523
You go to some user interface and you type something wrong, it doesn't tell you what is wrong.

12
00:01:25,548 --> 00:01:28,739
It just keeps going back and back and back and asking you to type again.

13
00:01:28,739 --> 00:01:31,599
So, how would you actually program something as unfriendly as that.

14
00:01:31,790 --> 00:01:36,480
So what we want to say is, if I come to this value error, do nothing.

15
00:01:36,480 --> 00:01:42,461
Now, the problem with python is that wherever you put this kind of a colon, it expects something after that.

16
00:01:42,486 --> 00:01:45,129
You cannot have an empty block.

17
00:01:45,129 --> 00:01:48,487
So, if I put a colon, there must be at least one statement after that.

18
00:01:48,512 --> 00:01:50,859
This is a syntactic rule of python.

19
00:01:50,859 --> 00:01:53,769
I cannot have an empty block, but here I want to do nothing.

20
00:01:53,769 --> 00:01:58,560
I want to recognize there is a value error and then go back here, that's fine.

21
00:01:58,560 --> 00:02:00,350
But I do not want to do anything else.

22
00:02:00,374 --> 00:02:02,560
So, how do I do nothing in python.

23
00:02:03,469 --> 00:02:08,530
So, the answer is that there is a special statement called pass.

24
00:02:08,530 --> 00:02:14,291
So pass is a statement which exists only to fill up spaces which need to be filled up.

25
00:02:14,315 --> 00:02:20,979
So when you have blocks like except or else, if you put the block name there you cannot leave it empty.

26
00:02:21,020 --> 00:02:25,330
In this case you have can use the word pass in order to do nothing.

27
00:02:30,319 --> 00:02:35,860
So supposing I have a list and I want to remove an element from the middle of the list.

28
00:02:35,912 --> 00:02:43,599
So one way to do this of course, is to take the the slice before the position and from that position, and then glue them together using  +  and so on.

29
00:02:43,789 --> 00:02:46,180
But, what if I want to directly do this.

30
00:02:46,189 --> 00:02:49,659
So, it turns out that there is a command called del.

31
00:02:49,669 --> 00:02:58,539
So if I say del l4, what it does is it effectively removes  l4  from the current set of values.

32
00:02:58,669 --> 00:03:04,090
And this automatically contracts the list, and shifts everything from position  5  onwards to the left by  1 .

33
00:03:04,090 --> 00:03:08,259
So let us just verify that this works the way it claims to work.

34
00:03:09,050 --> 00:03:17,409
So, supposing we set our list to be the values from  0  to  9 .

35
00:03:18,860 --> 00:03:22,030
Now, I say del l4.

36
00:03:23,240 --> 00:03:34,509
Then l becomes 0, 1, 2, 3, 5, 6, 7, 8, 9 because  l4  was the value  4  remember the position starts from  0 , and so it has actually deleted the value at the fourth position.

37
00:03:39,349 --> 00:03:49,942
This also works for dictionaries, so if you want to remove the value associated with the key k then you can use del d k, and it will remove the key k and whatever value is associated with it.

38
00:03:50,009 --> 00:03:53,919
So the key will now be considered to be an undefined key.

39
00:03:56,810 --> 00:04:02,500
So, in general we can take a name like  x  and say del x and what this will do is make x undefined.

40
00:04:02,500 --> 00:04:10,599
So supposing you wrote some junk code like this, we set x equal to 7, and then we said del x and then we ask y equal to x + 5.

41
00:04:10,599 --> 00:04:20,104
Now, here since  x  has been undefined, even though it had a value of  7 , this expression  x + 5  cannot be calculated because  x  no longer has a value.

42
00:04:20,129 --> 00:04:27,069
And what you end up with is the error that we saw before namely a name error, saying that the name x is not defined.

43
00:04:30,230 --> 00:04:33,543
So how would you go about checking if a name is defined.

44
00:04:33,569 --> 00:04:35,709
Well of course, you could use exception handling.

45
00:04:35,870 --> 00:04:42,858
So, supposing we want to assign a value to a name  x  only if  x  is undefined, then we can say try x.

46
00:04:42,883 --> 00:04:45,490
So this is trying to do something with  x .

47
00:04:45,515 --> 00:04:48,378
Remember that names can be anything, it could be functions.

48
00:04:48,402 --> 00:04:56,589
So, you can just write  x  because if it is currently in name of a function which takes no arguments, it will try to execute that function, and so it is perfectly valid to just write  x .

49
00:04:56,589 --> 00:05:00,595
But if  x  has no value, it will give a name error.

50
00:05:00,629 --> 00:05:06,930
So you can say try x , and if you happen to find a name error set it to  5  otherwise leave it untouched.

51
00:05:06,930 --> 00:05:09,910
So if  x  already has a value this will do nothing to  x .

52
00:05:09,910 --> 00:05:14,230
If  x  does not have a value then it will update the value of  x  to  5 .

53
00:05:15,829 --> 00:05:21,910
Now, usually what happens is that we want to check whether something has been defined or not so far.

54
00:05:21,910 --> 00:05:30,970
It is not a good idea to just leave it undefined and then use exception handling to do it because you might actually find strange things happening.

55
00:05:30,970 --> 00:05:41,350
So python provides us with a special value called None with a capital N, which is the special value used to define nothing, an empty value or a null value.

56
00:05:41,810 --> 00:05:46,199
Ok so we will find great use for it later on when we are defining our own data structures.

57
00:05:46,199 --> 00:05:51,670
But right now just think of None as a special value, there is only one value None and it denotes nothing.

58
00:05:52,100 --> 00:05:59,970
So the typical use is that when we want to check whether a name as a valid value, we can initialize it to None.

59
00:05:59,970 --> 00:06:02,110
Later on we can check if it is still None.

60
00:06:02,660 --> 00:06:05,110
So, initially we say x is equal to None.

61
00:06:05,110 --> 00:06:14,860
And finally, we go ahead and say, if x is not None, then set y equal to x, in other words, y equal to x will not be executed if  x  is still None.

62
00:06:15,470 --> 00:06:18,579
Now, notice that peculiar word is not.

63
00:06:18,579 --> 00:06:21,189
Ok so we are using is not equal to.

64
00:06:21,410 --> 00:06:25,920
There is exactly one value None in python in the space.

65
00:06:25,920 --> 00:06:35,560
So all Nones point to the same thing, so remember is was checking when we say l1 is l2, we are checking whether  l1  and  l2  point to the same list object.

66
00:06:35,560 --> 00:06:41,889
So we are asking whether  x  and the constant None point to the same None object and there is only one value None.

67
00:06:41,889 --> 00:06:45,810
Right so, x is None is the same as x equal to equal to None.

68
00:06:45,810 --> 00:06:49,569
So, x is not None is the same as x not equal to None.

69
00:06:49,670 --> 00:06:53,589
So, x is not None is much easier to read than x not equal to None.

70
00:06:53,589 --> 00:06:55,420
So, that is why we would write it this way.

71
00:06:57,379 --> 00:07:02,939
So, what we have seen are three useful things which we will use from time to time as you go long.

72
00:07:02,939 --> 00:07:10,298
One is the statement pass, which is a special statement that does nothing and can be used whenever you need an empty block.

73
00:07:10,323 --> 00:07:14,506
Then we saw the command del , which takes a valueand undefines it.

74
00:07:14,531 --> 00:07:19,779
The most normal way to use del is to remove something from a list or a dictionary.

75
00:07:19,910 --> 00:07:25,560
We would not normally want to just undefine a name which is holding a simple value.

76
00:07:25,560 --> 00:07:32,319
But, from a dictionary or a list we might want to remove a key or we might want to remove a position and del is very useful for that.

77
00:07:32,329 --> 00:07:44,620
And finally, we have seen that there is a special value None. which denotes a null value, it is a unique null value and this can be used to initialize variables to check whether they have been assigned sensible values later in the code.

