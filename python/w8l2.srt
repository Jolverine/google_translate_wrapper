1
00:00:02,960 --> 00:00:10,659
So, in the last lecture we looked at how to make iterative or inductive definitions more efficient than naive recursion.

2
00:00:11,275 --> 00:00:15,273
And we saw memoization and dynamic programming as tools to do this.

3
00:00:15,699 --> 00:00:19,599
So now, let's look at a typical problem and see how we can apply this technique.

4
00:00:21,345 --> 00:00:32,249
So, here is a problem of grid paths. So, we have a grid here can imagine there are roads which are arranged in a rectangular format and we can imagine that the intersections are numbered.

5
00:00:32,622 --> 00:00:35,011
So, we have 0, 0 at the bottom left corner.

6
00:00:35,534 --> 00:00:42,161
And in this case, we have 5, 10 because we have going across from left to right we have 1, 2, 3, 4, 5.

7
00:00:42,707 --> 00:00:45,227
and 10 going up.

8
00:00:45,251 --> 00:00:48,471
So, we have at 5, 10 the top right column.

9
00:00:49,884 --> 00:00:54,838
So, if these are roads, the constraint that we have is that one can only travel up or right.

10
00:00:54,863 --> 00:00:59,042
So, you can go up a road or you can go right, but you cannot come down, right?

11
00:00:59,067 --> 00:01:00,982
So, this is not allowed.

12
00:01:01,593 --> 00:01:03,868
So, these are one way road which go up and right.

13
00:01:04,472 --> 00:01:10,977
And what we want to ask is how many ways there are to go from the bottom left corner to the top right corner.

14
00:01:11,001 --> 00:01:17,127
So, we want to count the number of what are called grid paths. So, grid path is one which follows this, right?

15
00:01:17,643 --> 00:01:24,623
So, we want to know how many such different paths are there which take us from 0, 0 to 5, 10 only going up or right.

16
00:01:27,822 --> 00:01:29,671
So, here is one path drawn in blue.

17
00:01:31,501 --> 00:01:41,649
Here is a different path drawn in red and notice that these two paths actually start and different directions from the first point and they never meet. except at the target, so they do not overlap at all.

18
00:01:43,268 --> 00:01:45,637
On the other hand, we could have paths which overlap.

19
00:01:45,662 --> 00:01:54,551
So, this yellow path overlaps for part of its way with the blue path in this section And it also overlaps with the red path in 2 portions, right?

20
00:01:54,996 --> 00:02:02,851
So, there are many different ways in which we can choose to make these up and right moves and the question is how many total such different paths are there.

21
00:02:07,010 --> 00:02:11,295
So,there is a very standard and elegant combinatorial solution.

22
00:02:12,676 --> 00:02:16,978
So, one way of thinking about this is just to determine how many moves we have to make.

23
00:02:17,768 --> 00:02:21,792
We have to go from 0 to 5 in one direction and 0 to 10 in the other direction.

24
00:02:22,014 --> 00:02:26,305
So, we have to make a total number of 5 horizontal moves and 10 vertical moves.

25
00:02:26,544 --> 00:02:34,075
In other words, every path no matter which direction we start in and which move would which choice of moves you make must make fifteen steps.

26
00:02:34,313 --> 00:02:41,095
And of these 5 must be horizontal steps and 10 must be vertical steps because they all take us from 00 to 510.

27
00:02:42,444 --> 00:02:50,132
So, all we have to do since we know that these five steps are horizontal and 10 are vertical is to just demarcate which ones are horizontal, which are vertical.

28
00:02:50,568 --> 00:02:58,849
Now, once we know which one's a horizontal we know what sequence they come in because the first horizontal step takes us from column 0 to column 1, second one takes us from 1 to 2.

29
00:02:58,874 --> 00:03:01,677
So, we can't do it in any order other than that, right?

30
00:03:01,710 --> 00:03:07,376
So, we have in other words we have 15 slots where we can make moves.

31
00:03:07,401 --> 00:03:13,398
And then we just say first we make an up move then we make a right move then we make an up move and make another up move and so on.

32
00:03:13,558 --> 00:03:24,339
So, every path can be drawn out like this as 10 up moves and 5 right moves and if we fix the 5 right moves then the way we can then automatically all the remaining slots must be 10 up nodes or conversely, right?

33
00:03:24,364 --> 00:03:26,169
So, it is either 15 choose 5.

34
00:03:26,500 --> 00:03:30,256
It is the way of choosing 5 positions to make the right move out of the 15.

35
00:03:30,716 --> 00:03:46,319
And it turns out that the definition of 15 choose 5 is clearly the same as 15 choose 10 because we could also fix the 10 up nodes and the definition is, basically if you know the definitions, n choose k as n factorial from k factorial into n - k factorial right?

36
00:03:46,344 --> 00:03:54,360
So, this k and n - k basically says that 15 - 5 is 10, so we get a symmetric function in terms of k and n - k.

37
00:03:54,931 --> 00:04:01,374
So, in this case we can apply this formula as if you would like to call it that and directly get that the answer is 3003.

38
00:04:01,962 --> 00:04:08,660
So, there does not appear to be much to compute other than writing out large factorials and then seeing what the number comes.

39
00:04:11,360 --> 00:04:17,451
But the problem becomes more interesting if we constrain it by saying that some of these intersections are blocked.

40
00:04:18,102 --> 00:04:23,415
For instance, supposing there is some road work going on and we cannot go through this intersection 2, 4, right?

41
00:04:23,439 --> 00:04:30,025
So, this is the intersection 2, 4, second column and the fourth row counting from below, right?

42
00:04:30,049 --> 00:04:34,332
It is actually 2, 3 but 1 2 1 2 3 4.

43
00:04:34,357 --> 00:04:35,420
Yeah, 2,4.

44
00:04:36,427 --> 00:04:44,202
So now, if we can't go through this then any path which goes through this particular blocked intersection should no longer be counted.

45
00:04:44,227 --> 00:04:47,804
Out of this 3003, some paths are no longer valid paths.

46
00:04:48,122 --> 00:04:51,756
For instance, in the earlier thing the blue path that we had drawn actually goes through this.

47
00:04:52,248 --> 00:04:53,598
The red path does not.

48
00:04:54,217 --> 00:04:58,920
But the yellow path overlap with the blue path unfortunately
in this bad section so it also pass through this.

49
00:04:59,300 --> 00:05:02,900
So there are some paths which are allowed from the 3003 and some which are not.

50
00:05:03,241 --> 00:05:07,566
So, how do we determine how many paths survive this kind of block?

51
00:05:10,009 --> 00:05:18,466
So, again we can use a combinatorial argument in order to be blocked a path must go to 2, 4 and then from 2, 4 to 5, 5.

52
00:05:18,927 --> 00:05:27,855
So, if we could only count how many paths go from 0 0 to 2 4 and then how many paths go from 2, 4 to 5, 10, these are all the bad paths.

53
00:05:28,240 --> 00:05:31,601
So, we can count these bad paths and subtract them from the good paths.

54
00:05:31,927 --> 00:05:35,274
How do we count the bad paths well we can just solve a smaller version of the problem.

55
00:05:35,305 --> 00:05:37,800
So, we have an intermediate target, so we solve this grid.

56
00:05:38,026 --> 00:05:39,455
How many paths go from here to here?

57
00:05:40,171 --> 00:05:42,244
How many paths go from here to here, right?

58
00:05:42,597 --> 00:05:46,983
So, from 0 0 to 2 4 we get 4 + 2.

59
00:05:47,136 --> 00:05:49,644
Remember it is 10 + 5 it was at the target

60
00:05:49,669 --> 00:05:52,111
So, 10, 4 + 2 choose 2, so we get 15.

61
00:05:52,603 --> 00:05:57,088
And from here to here the difference is that we have to do in both directions.

62
00:05:57,373 --> 00:05:57,778
3 and

63
00:05:58,942 --> 00:06:04,222
And so, we have to go, sorry, we have to go up 6 and we have to go right 3, right?

64
00:06:04,276 --> 00:06:05,212
We are at 2, 4.

65
00:06:05,712 --> 00:06:08,450
So, we have to go from 4 to 10 and from 2 to 5.

66
00:06:08,759 --> 00:06:13,275
So, we have 6 + 3 choose 384 ways of going from 2, 4 to this.

67
00:06:13,481 --> 00:06:16,599
And each of the ways in the bottom can be combined with the way on the top.

68
00:06:16,624 --> 00:06:21,577
So we multiply this and we get 1260 paths will pass through this bad intersection.

69
00:06:21,981 --> 00:06:27,870
We subtract this from the original number 3003 and we get 1743 paths which remain.

70
00:06:28,902 --> 00:06:31,085
So, a combinatorial approach still works.

71
00:06:32,926 --> 00:06:37,067
Now, what happens if we put two such intersections ok?

72
00:06:37,091 --> 00:06:38,656
So we will, can do the same thing.

73
00:06:38,681 --> 00:06:41,831
We can count all the paths which get blocked because of the first intersection.

74
00:06:42,489 --> 00:06:47,680
We can, get count all the paths which pass through in this case 4, 4 is the second intersection which has been placed.

75
00:06:48,331 --> 00:06:50,950
So, we can count all these paths which pass through 4 4.

76
00:06:51,338 --> 00:06:54,029
So, this we know how to do we just computed it for 2, 4.

77
00:06:54,838 --> 00:06:59,743
But, the problem is that there are some paths like the yellow path which pass through both 2 4 and 4 4.

78
00:07:00,664 --> 00:07:02,045
So, we need a third count.

79
00:07:02,275 --> 00:07:06,497
We need to count paths which pass through both of these and make sure we don't double count it.

80
00:07:07,013 --> 00:07:09,537
So, one way is that we just add these back.

81
00:07:10,378 --> 00:07:14,331
So, this is something which is called in combinatorics, inclusion and exclusion.

82
00:07:14,569 --> 00:07:21,540
So, when we have these overlapping exclusions, then we have to count the overlaps and include them back and we have to keep doing this step by step.

83
00:07:21,565 --> 00:07:30,442
So, if we have three holes, we get an even more complicated inclusion exclusion formula and it rapidly becomes very complicated even to calculate the formula that we need to get.

84
00:07:30,870 --> 00:07:33,106
So is there a simpler way to do this?

85
00:07:35,932 --> 00:07:38,368
So, let us look at the inductive structure of the problem.

86
00:07:39,217 --> 00:07:46,094
Suppose, we say we want to get one step to intersection i, j.

87
00:07:47,015 --> 00:07:48,703
How can we reach this in one step?

88
00:07:49,344 --> 00:07:53,237
Since our roads only go left left to right and bottom to top.

89
00:07:53,697 --> 00:07:58,120
The only way we can reach i j is by taking a right edge from its left neighbor.

90
00:07:58,144 --> 00:08:04,388
So, we can go from i - 1 j to i j or we can go from below from i j - 1 to i j.

91
00:08:05,515 --> 00:08:10,173
Notice that if a path comes from the left it must be different from a path that comes from below.

92
00:08:11,126 --> 00:08:16,181
So, every path that comes from the left is different from every path that comes from below.

93
00:08:17,030 --> 00:08:19,467
So, we can just add these up, right?

94
00:08:19,914 --> 00:08:28,070
So, in other words if we say that paths i, j is the quantity we want to compute, we want to count the number of paths from 0 0 to i j.

95
00:08:28,745 --> 00:08:31,880
These paths must break up into two disjoint sets.

96
00:08:31,967 --> 00:08:40,300
Those which come from the left which recursively or inductively if you prefer to say is the exactly the quantity paths i - 1 j.

97
00:08:40,372 --> 00:08:42,579
How many paths are there which reach i - 1 j?

98
00:08:42,603 --> 00:08:47,100
Every one of these paths can be extended by a right edge to reach i, j and they will all be different.

99
00:08:47,680 --> 00:08:57,737
Similarly, paths i, j - 1 are all those paths which come from below because they all reach the point just below i j from there each of them can be extended in a unique way to i j.

100
00:08:58,697 --> 00:09:00,864
So, this gives us a simple inductive formula.

101
00:09:01,169 --> 00:09:05,459
paths i j is just the sum paths i - 1 j paths i j - 1.

102
00:09:06,316 --> 00:09:09,144
Then, we need to, of course, investigate the base cases.

103
00:09:09,168 --> 00:09:15,657
In this case, the real base case is just path 0, 0, how many ways can I go from 0, 0 and just stay in 0, 0?

104
00:09:15,927 --> 00:09:16,975
Well there is only one way.

105
00:09:17,316 --> 00:09:21,973
It is tempting to say 0 ways, but it is not 0 ways, it is one way otherwise nothing will happen. right?

106
00:09:22,167 --> 00:09:25,864
So, we have one way by just doing nothing to stay in 0 0.

107
00:09:26,792 --> 00:09:30,413
If we are now moving along the left column, right?

108
00:09:30,437 --> 00:09:35,204
So, moving along the left column then there is no paths coming from each left because we are already on the left most column.

109
00:09:35,564 --> 00:09:42,293
So, all the paths from to0 j must be extensions of path which have come from below to 0 j - 1.

110
00:09:42,567 --> 00:09:49,110
Similarly, if you are in the bottom row, there is no way to come from below because we are already on the lowest set of roads.

111
00:09:49,480 --> 00:09:53,173
So, paths i 0 can only come from the left from paths i - 1 0.

112
00:09:54,943 --> 00:10:08,926
So, this gives us a direct way to actually compute this even with holes because the only difference now is that if there is a hole. we just declare that no paths can reach that place.

113
00:10:09,379 --> 00:10:15,109
So, we just add an extra clause which says paths i j is 0 if there is a hole at i j.

114
00:10:15,569 --> 00:10:18,474
Otherwise, we use exactly the same inductor formulation.

115
00:10:18,850 --> 00:10:21,720
And now what happens is that I have to have a hole below me.

116
00:10:21,745 --> 00:10:27,545
If I have a hole below me, no paths can come from that direction, but because by definition paths of i j at that point is 0.

117
00:10:31,561 --> 00:10:44,886
So, once again if we now apply this and do this using the standard translation from the inductive definition to a recursive program, we will find that we will wastefully recompute the same quantity multiple times.

118
00:10:45,347 --> 00:10:48,388
For instance, paths 5, 10, okay?

119
00:10:48,413 --> 00:10:53,918
So, if we have paths 5, 10, it will require me to compute this and this, okay?

120
00:10:54,220 --> 00:10:58,220
These are the two sub problems for pass 5, 10, and the 4, 10 and 5, 9.

121
00:10:58,629 --> 00:11:13,236
But in turn, in order to compute 4 10 I will have to compute whatever is to its left and below it and in order to compute 5 9 I will also have to compute what is to its left and below it.

122
00:11:13,300 --> 00:11:19,522
And now what we find is that this quantity, ok, and the 4 9 is computed twice.

123
00:11:19,665 --> 00:11:24,355
Once because of the left neighbor of 5, 10 and once because of the neighbor below 5 10.

124
00:11:26,197 --> 00:11:36,410
So, as we saw before we could use memoization to make sure that we never compute i j twice by storing a table i, j and every time we compute a new value for i, j we store it in the table.

125
00:11:36,820 --> 00:11:43,561
And every time we look up we need to compute one we first check the table if it is already there, we look it up otherwise we compute it and store it.

126
00:11:45,267 --> 00:11:52,124
But since we know that it is a table and we know what the table structure is basically it's all entries of the form i, j.

127
00:11:52,509 --> 00:11:59,434
We can also see if we can fill up this table iteratively by just examining the sub problems in terms of their dependencies.

128
00:12:02,442 --> 00:12:07,775
So this in general, a node a value depends on things to its left and below.

129
00:12:08,139 --> 00:12:14,654
So, if there are no dependencies it must have nothing to its left and nothing below and there is only one such point namely 0 0.

130
00:12:14,678 --> 00:12:20,106
So, this is the only point which is the base case which has nothing to its left and nothing below.

131
00:12:20,130 --> 00:12:21,022
So, its value is directly.

132
00:12:21,675 --> 00:12:22,654
So, we start from here.

133
00:12:24,257 --> 00:12:27,876
So, remember that the base value at 0 0 is 1, okay?

134
00:12:29,081 --> 00:12:32,191
And now once we have done this, okay?

135
00:12:32,365 --> 00:12:37,312
It turns out to remember the row dependency it says i , 0 is i - 1, 0.

136
00:12:37,418 --> 00:12:42,499
So, we can fill up this because this has only one dependency which is known now.

137
00:12:43,019 --> 00:12:50,144
So, in this way I can fill up the entire row and say that along all along this row there is only one path namely the path that starts going right and keeps going right.

138
00:12:51,081 --> 00:13:01,047
Now, we can go up and see that this thing is also known, because it also depends only on the value below it and once that is known, then these two are known.

139
00:13:01,071 --> 00:13:02,132
So, I can add them up.

140
00:13:02,156 --> 00:13:07,128
Remember the sum of the value at any position is just the value to its left + the value to its bottom.

141
00:13:07,382 --> 00:13:16,358
And now I start to get some non trivial values and in this way I can fill up this table row by row and at each point when I come to something I will get fact that the dependency are known.

142
00:13:16,465 --> 00:13:18,715
The next row looks like this and the next row.

143
00:13:18,977 --> 00:13:20,655
Now, we come to the row with holes.

144
00:13:20,942 --> 00:13:33,416
So, for the row with holes wherever we hit a hole, instead of writing the value that we would normally get by adding its left and bottom neighbor, we deliberately put a 0 because that means that no path is actually allowed to propagate through that row.

145
00:13:34,154 --> 00:13:39,385
So now, when we come to the next row, the holes will automatically block the paths coming from the wrong direction.

146
00:13:39,509 --> 00:13:43,988
So, here for instance we have only 6 paths coming from the left because we have no paths coming from below.

147
00:13:44,108 --> 00:13:47,800
Similarly, we have 26 paths coming from the left and no paths coming from below.

148
00:13:48,354 --> 00:13:54,218
So this is how our inductive definition neatly allows us to deal with holes.

149
00:13:54,575 --> 00:13:59,750
And from that inductive definition we recognize the dependency structure and we imagined a memo table.

150
00:14:00,170 --> 00:14:07,554
And now we are filling up this memo table row by row so that at every point when we reach an i j value its dependent values are already known.

151
00:14:08,365 --> 00:14:14,873
So, we can continue doing this row by row and eventually we find that there are 1363 paths which avoid these two.

152
00:14:17,161 --> 00:14:19,774
So, we could also do the same thing in a different way.

153
00:14:19,854 --> 00:14:22,448
Instead of doing the bottom row we can do the left column.

154
00:14:22,986 --> 00:14:32,995
And the same logic says that we can go all the way up then we can start in the second column go all the way up and do this column by column and not unexpectedly we should get the same answer.

155
00:14:35,190 --> 00:14:37,798
There is a third way to do this, okay?

156
00:14:37,938 --> 00:14:46,038
So, once we have 1 at 0 0 then we can fill both the first element above it and the first element to its right.

157
00:14:46,062 --> 00:14:47,319
So, we can do this diagonal.

158
00:14:47,875 --> 00:14:52,293
Now, notice that any diagonal value like this one has both its entries, right?

159
00:14:52,318 --> 00:14:53,765
So, this has only one entry.

160
00:14:54,100 --> 00:14:59,959
So, I can now fill up this diagonal so I can go one more diagonal then I can go one more diagonal.

161
00:14:59,983 --> 00:15:02,303
So, we can also fill up this thing diagonal by diagonal.

162
00:15:02,443 --> 00:15:07,473
So, the dependency structure may not require us to fill it in a particular way.

163
00:15:07,497 --> 00:15:09,236
We might have very different ways to fill it up.

164
00:15:09,296 --> 00:15:15,200
All we want to do is systematically fill up this table in an iterative fashion not recursively.

165
00:15:15,225 --> 00:15:18,960
We don't want to call f of i j and then look at f of i - 1 j.

166
00:15:18,985 --> 00:15:22,157
We want to directly say when we reach i j we have the values we need.

167
00:15:22,624 --> 00:15:25,552
But the values we need could come in multiple different orders.

168
00:15:25,658 --> 00:15:33,865
So, we could have done it row wise, we could have done it column wise and here you see but it doesn't matter so long as we actually get all the values that we need.

169
00:15:37,216 --> 00:15:38,427
So, one small point.

170
00:15:39,251 --> 00:15:44,013
So, we have said that we can use memoization or we can use dynamic programming.

171
00:15:44,630 --> 00:15:48,791
One of the advantages of using dynamic programming is that avoids this recursive call.

172
00:15:48,879 --> 00:15:59,848
So, recursion we had mentioned earlier also in some earlier lecture appears come with a price because whenever you make a recursive call you have to suspend a computation store some value, restore those values.

173
00:15:59,873 --> 00:16:02,390
So there is a kind of administrative cost with recursion.

174
00:16:02,778 --> 00:16:12,887
So, actually though it looks like only a single operation when we call fib of n - 1 or f above n - 2, there is actually a cost involved with suspending this operation going there and coming back.

175
00:16:13,196 --> 00:16:17,834
So, saving on recursion is one important reason to move from memorization to dynamic programming.

176
00:16:18,919 --> 00:16:26,540
But what dynamic programming does, is to evaluate every value regardless of whether it is going to be useful for the final answer or not.

177
00:16:27,448 --> 00:16:30,808
So, in the grid path thing there is one situation where you can illustrate this.

178
00:16:30,914 --> 00:16:37,272
Imagine that we have these obstacles placed exactly one step inside the boundary.

179
00:16:37,739 --> 00:16:46,544
So now, if you want to reach this, it is very clear that I can only come all the way along the top row or all the way up the right most column.

180
00:16:46,568 --> 00:16:48,074
There is no other way I can reach there.

181
00:16:48,296 --> 00:16:55,898
So, anything which is inside these these positions there is no way to go from here out, so there is no point in counting all these values. okay?

182
00:16:56,845 --> 00:17:04,073
So, we have this region which is in the shadow of these obstacles which can never reach the final thing.

183
00:17:04,098 --> 00:17:10,828
So when we do memoization when we come back and exclusively explore it will never ask us to come here because it will never pass these bonds.

184
00:17:11,548 --> 00:17:21,629
On the other hand, our dynamic programming will blindly walk through everything, so it will do row by row column by column and it will eventually find the zeros but it will fill the entire n by n grid.

185
00:17:22,431 --> 00:17:25,430
So, in this case how many will memoization do it will do?

186
00:17:26,021 --> 00:17:31,681
So, basically only the boundary, so it will do only order m + n. Right?

187
00:17:32,101 --> 00:17:41,586
So, we have a memo table which has only a linear number of entries in terms of the rows and columns and a dynamic programming entry which is quadrilateral okay?

188
00:17:41,610 --> 00:17:44,118
If both are n it will be n square versus 2 n.

189
00:17:45,070 --> 00:17:50,132
So, this suggests that dynamic programming in this case is wastefully computing a vast number of entries.

190
00:17:50,172 --> 00:17:52,962
So, n square is much larger than 2 n remember, okay?

191
00:17:52,987 --> 00:17:57,783
So, it will take us enormous amount of time to compute it, if we just count the cost of the upper entry.

192
00:17:58,330 --> 00:18:04,440
But the flip side is that each entry that we need to add to the memo table requires one recursive call.

193
00:18:05,781 --> 00:18:14,232
So, the reality is that these recursive calls will typically cost you much more than the wastefulness of computing the entire table.

194
00:18:14,532 --> 00:18:24,048
So in general, even though you can analyze the problem and decide that memoization will result in many fewer new values being computed and dynamic programming.

195
00:18:24,519 --> 00:18:29,880
It is usually sound to just use dynamic programming as a default way to do the computation.

