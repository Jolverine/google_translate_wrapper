1
00:00:02,839 --> 00:00:06,730
So this is A final example to illustrate dynamic programming.

2
00:00:06,730 --> 00:00:10,240
So, we look at the problem of matrix multiplication.

3
00:00:10,820 --> 00:00:18,339
So if you remember how matrix multiplication works, you have to take two matrices with compatible entries.

4
00:00:18,339 --> 00:00:30,010
and then we compute A new matrix in which, for each entry there we have to multiply A row here by A column of the same length and add up the values.

5
00:00:30,010 --> 00:00:34,929
So, if we have A matrix which is m by n then this must have N times p.

6
00:00:35,420 --> 00:00:39,789
So, that we have A final answer which is m times p.

7
00:00:40,189 --> 00:00:41,469
So,

8
00:00:42,350 --> 00:00:45,009
In the final entry we have to

9
00:00:45,079 --> 00:00:50,409
We have to make m p entries in the final product matrix.

10
00:00:50,409 --> 00:00:54,000
and each of these entries requires us to compute the sum of these N entries.

11
00:00:54,000 --> 00:01:01,929
So, that takes us order n time, so the total work is usually easy to compute as m times N times p. So, a b i j

12
00:01:01,929 --> 00:01:07,209
It is this long sum and this sum has one, two, three up to N entries.

13
00:01:07,280 --> 00:01:10,769
So, computing matrix multiplication for two matrices.

14
00:01:10,769 --> 00:01:20,769
It has A very straightforward algorithm which is A triple nested loop which takes order m times n times p and if all of the dimensions are same this is an order n cube.

15
00:01:21,049 --> 00:01:24,770
Now, there are more clever ways of doing it, but that is not the purpose of this lecture.

16
00:01:24,795 --> 00:01:29,109
But the naive, straightforward way of multiplying two matrices is m times N times p.

17
00:01:29,659 --> 00:01:33,790
Our interest is when we have A sequence of such multiplications to do.

18
00:01:34,189 --> 00:01:38,980
Suppose we want to multiply three matrices together A times B times C.

19
00:01:39,500 --> 00:01:46,930
Then it turns out it doesn't matter whether we first multiply A B and then multiply C or we first multiply A and then multiply B C

20
00:01:47,569 --> 00:01:50,200
Okay. A times B C

21
00:01:50,299 --> 00:01:52,825
This is stated as the associative property.

22
00:01:52,850 --> 00:01:56,275
The order in which we group the multiplication does not matter just for normal numbers.

23
00:01:56,300 --> 00:02:01,670
We do six times three times two it does not matter whether you do six times three first or three times two first,

24
00:02:01,695 --> 00:02:03,609
finally, the result is going to be the same.

25
00:02:04,010 --> 00:02:08,680
So, the bracketing doesn't change the answer, the final value is the same.

26
00:02:08,680 --> 00:02:16,780
But, it turns out that it can have dramatic effects on the complexity of computing the answer. Why is this the case?

27
00:02:17,900 --> 00:02:21,789
So, suppose we have these matrices A B and C.

28
00:02:21,789 --> 00:02:28,150
And A and B have these columns and rows. So, A is basically A one by hundred matrix.

29
00:02:28,150 --> 00:02:32,189
It just has one row and hundred columns and B has A hundred rows and one column.

30
00:02:32,189 --> 00:02:37,569
C has one row and A hundred columns. So these are matrices which look like this.

31
00:02:40,430 --> 00:02:44,409
Now, what happens is that when i multiply B times C.

32
00:02:44,810 --> 00:02:50,199
Then i compute something which is hundred into one into hundred

33
00:02:50,629 --> 00:02:54,030
So, we will get an output which is A hundred by hundred matrix.

34
00:02:54,030 --> 00:02:57,340
So that has 10000 entries so it is going to take us 10000 steps.

35
00:02:57,340 --> 00:03:02,069
Now, when i multiply A by this, i am going to get one into hundred into hundred.

36
00:03:02,069 --> 00:03:09,849
That is another 10000 steps. So, if i multiply A after multiplying B and C then i do 10000 plus 10000 so i do 20000 steps.

37
00:03:09,979 --> 00:03:18,729
Now, if i do it the other way, if i take A times B first, then this whole thing collapses into A one by one single entry.

38
00:03:18,919 --> 00:03:29,800
So, i get one into hundred into one and in 100 steps i just collapse this row and this column into A single entry that is like computing one entry in A matrix multiplication the result thing is exactly the one entry.

39
00:03:29,800 --> 00:03:38,740
Now, i have this one entry and again i have to multiply it by this thing for each of the columns in this i will get one entry so that will take me 100 steps.

40
00:03:38,876 --> 00:03:42,419
So, i take A 100 steps to collapse this A B into A single cell.

41
00:03:42,444 --> 00:03:45,750
another 100 steps after that to multiply that result with C.

42
00:03:45,750 --> 00:03:50,550
So, instead of 20,000 steps i have done only 200 steps.

43
00:03:50,550 --> 00:03:57,969
The sequence of multiplications, remember multiplication is associative and it does not matter what you do you will get the same answer.

44
00:03:57,969 --> 00:04:06,699
The sequence in which you do the associative steps can dramatically improve or worsen the amount of time you spend doing this.

45
00:04:10,490 --> 00:04:15,250
So,  in general we have A sequence m one to m n.

46
00:04:15,250 --> 00:04:17,612
Each of them has some rows and columns.

47
00:04:17,637 --> 00:04:25,025
And what we are guaranteed is that each adjacent pair can be multiplied. So, R one C one R two C two the first two are such that C one is equal to R two.

48
00:04:25,050 --> 00:04:31,480
The number of columns in the first matrix is equal to the number rows in second matrix. Similarly, C two is equal to R three and so on.

49
00:04:31,480 --> 00:04:36,069
So, these dimensions are guaranteed to match. So, the matrix multiplication is always possible

50
00:04:36,069 --> 00:04:42,569
Our target is to find out the order for multiplication. So, we can at best do two matrices at A time.

51
00:04:42,569 --> 00:04:45,360
We only know how to multiply A times B.

52
00:04:45,360 --> 00:04:47,980
We cannot take three matrices and directly multiply them.

53
00:04:47,980 --> 00:04:56,399
If we have to do three matrices we have to do it in two steps, A times B and then C or B times C and then A. Now same way with N matrices we have to do two at A time

54
00:04:56,399 --> 00:04:59,810
But those two at A time could be complicated things. i could do m one two,

55
00:04:59,835 --> 00:05:04,079
then i can combine that and do that combination with m three or i can do m one two,

56
00:05:04,079 --> 00:05:08,110
m three four and then do combination of m one two multiply by m three four and so on.

57
00:05:08,110 --> 00:05:14,730
So, what is the optimal way of computing the product, what is the optimal way of computing brackets in other words.

58
00:05:14,730 --> 00:05:19,600
Brackets are essential in the computation. So when we say m one, m two, m three and m four.

59
00:05:19,600 --> 00:05:23,500
One way of computing it is to do this.

60
00:05:23,500 --> 00:05:28,196
Do m one m two, then m three. Another way of doing it would be to say do m one m two

61
00:05:28,221 --> 00:05:31,769
and then do that multiplied by m three and then m four and so on.

62
00:05:31,769 --> 00:05:33,779
So, there are different ways of bracketing.

63
00:05:33,779 --> 00:05:38,610
They correspond to different evaluation orders for this multiplication.

64
00:05:38,610 --> 00:05:46,689
We want to calculate without doing the actual. computation, which is the best sequence in which to do this calculation, so that we optimize the operations involved.

65
00:05:50,300 --> 00:05:52,449
So, what is the inductive structure?

66
00:05:53,540 --> 00:06:03,610
So, finally when we do this, remember we can only do two at A time. So at the end we must end with the multiplication which involves some group multiplied by something.

67
00:06:03,610 --> 00:06:07,720
It must look like this. i must have this whole thing collapsed to some m one prime,

68
00:06:07,720 --> 00:06:12,639
This whole thing collapsing to m two prime and i am finally multiplying m one prime with m two prime.

69
00:06:12,680 --> 00:06:15,610
In other words, m one prime

70
00:06:15,610 --> 00:06:19,389
For some k it is from m one all the way up to m k.

71
00:06:19,399 --> 00:06:29,949
Similarly m two prime is from k plus one up to n. So, this is my m one prime. This is my m two prime and this k is somewhere between one and n.

72
00:06:30,050 --> 00:06:34,329
So, in the worst case i could be doing m one.

73
00:06:34,329 --> 00:06:38,470
Then m two up to n. So, this whole thing is my M.

74
00:06:38,470 --> 00:06:41,500
The other worst case is i could have done m one to.

75
00:06:41,500 --> 00:06:44,310
not worst, but extreme cases m into.

76
00:06:44,310 --> 00:06:48,490
m one to N minus one i might have already computed and now i want to finally multiply it by m n.

77
00:06:48,490 --> 00:06:50,319
Also, it could be anywhere in between.

78
00:06:51,620 --> 00:06:54,806
So, if i just pick an arbitrary k then

79
00:06:54,831 --> 00:06:58,139
the first one has R one rows C k columns

80
00:06:58,139 --> 00:07:02,759
the second one has R k plus one rows ,C k C N columns but we know that C k is equal to R k plus one.

81
00:07:02,759 --> 00:07:08,050
So, this matrix will work. So, the final computation is going to be R one into C k into C n.

82
00:07:08,689 --> 00:07:10,569
That is m times N times p.

83
00:07:10,670 --> 00:07:15,339
It is the rows times the common number of columns row times the final number of columns.

84
00:07:15,529 --> 00:07:19,689
So, this final multiplication we know how much it is going to cost.

85
00:07:19,939 --> 00:07:28,449
And to this we have to recursively add the inductive cost of having computed two factors, how much time it will take us to take m one prime and how much time it will take us to do m two prime.

86
00:07:29,569 --> 00:07:30,850
So,

87
00:07:31,819 --> 00:07:35,110
We have that the cost of m one.

88
00:07:36,439 --> 00:07:45,819
when splitting at k is the cost of m one to m k plus the cost of m k plus m N plus the last multiplication R one times C k times C n.

89
00:07:46,399 --> 00:07:50,279
So, clearly this cost will vary for each k and there are many choices of k

90
00:07:50,304 --> 00:07:54,730
As we said there are N minus one choices of k. Anything from one to N minus one we can choose as <DNT<k.

91
00:07:54,889 --> 00:07:57,310
So, there are N minus one sub problems.

92
00:07:57,310 --> 00:08:05,170
So, in the longest common subsequence problem we had two sub problems. We could either drop the first letter A i or the second letter B j.

93
00:08:05,300 --> 00:08:09,209
And then, we have to consider two sub problems we had no way of knowing which is better.

94
00:08:09,209 --> 00:08:11,920
So, we did them both and took the maximum of the two.

95
00:08:11,920 --> 00:08:15,040
Now here we have N minus one different choices of k.

96
00:08:15,040 --> 00:08:17,589
We have no way of knowing which of these k is better.

97
00:08:18,230 --> 00:08:21,370
So, again we try all of them and take the minimum.

98
00:08:21,370 --> 00:08:24,939
there we were doing the maximum because we wanted the longest common subsequence.

99
00:08:24,939 --> 00:08:29,769
Here we want the minimum cost so we choose that k which would minimize this split.

100
00:08:30,019 --> 00:08:33,850
Also recursively each of those things would minimize their split also.

101
00:08:33,850 --> 00:08:35,769
So, that is the inductive structure.

102
00:08:36,379 --> 00:08:41,309
So, finally we say that the cost of multiplying m one to m n.

103
00:08:41,309 --> 00:08:44,049
is the minimum for all choices of k.

104
00:08:44,049 --> 00:08:52,230
Among the cost of multiplying m one to m k plus the cost of multiplying m k plus one to m N plus of course, for that choice of k.

105
00:08:52,230 --> 00:08:56,272
we have to do one final multiplication which is to take these resulting sub matrices.

106
00:08:56,297 --> 00:08:58,539
So, that is R one times C, k times C.

107
00:09:00,200 --> 00:09:04,169
So now, when we take this. So we have m one to m n.

108
00:09:04,169 --> 00:09:06,389
So, we have picked m one to m k.

109
00:09:06,389 --> 00:09:09,580
then as before what will happen is that we will have to split this somewhere.

110
00:09:09,580 --> 00:09:20,060
So, We will now end up having some segment which is neither m one nor m N it starts at some m j and goes to m k. So in general, we need to express this quantity for arbitrary left and right points.

111
00:09:20,085 --> 00:09:23,889
We can't assume that the left hand point is one, we can't assume the right hand point is N

112
00:09:24,740 --> 00:09:29,169
So, in general if we have A segment from m i to m j.

113
00:09:29,169 --> 00:09:35,559
then we want the smallest value among all the values of k from i to j minus one.

114
00:09:35,559 --> 00:09:43,509
we want the minimum cost. which occurs from computing the cost of m i to m k and m k plus one to m j.

115
00:09:43,509 --> 00:09:47,889
Also we want the cost of this final multiplication which is R i times C k times C j.

116
00:09:48,049 --> 00:09:51,389
So, this quantity we will write as cost i j.

117
00:09:51,389 --> 00:09:57,295
cost i j is the cost of computing the segment from m i to m j which involves picking the best k.

118
00:09:57,320 --> 00:10:04,659
So, m i to m j is called cost i j and we use the same recursive inductive definition to choose the list.

119
00:10:07,610 --> 00:10:11,820
So, the base case well if you are just looking at A segment of length 1.

120
00:10:11,820 --> 00:10:19,059
Supposing we just want to multiply one. matrix from 1 to 1 or 3 to 3 or 7 to 7 nothing is to be done.

121
00:10:19,059 --> 00:10:22,629
It is just the matrix that there is no multiplication of the cost is 0.

122
00:10:23,539 --> 00:10:24,879
So,

123
00:10:25,129 --> 00:10:35,710
we can then write out this cost i j equation saying the minimum over k or cost i k plus cost k plus one j plus R i C k C j which is the actual multiplication.

124
00:10:36,139 --> 00:10:47,529
And of course, i is always going to be less than equal to j because we are doing it from left to right. So, we can assume that the segment is given to us with two end points where i is less than or equal to j.

125
00:10:48,409 --> 00:10:56,279
So, if i is less than or equal to j and we look at cost i j as A kind of matrix where this whole area where i is greater than j is ruled out.

126
00:10:56,279 --> 00:11:04,809
So, we only look at this diagonal and the entries along the diagonal are the ones which are of the form i i so all these entries are initially 0.

127
00:11:05,120 --> 00:11:11,740
So, there is no cost involved with doing any multiplication from position j to position j along this diagonal.

128
00:11:12,830 --> 00:11:17,379
Now, in order to compute  i comma j i need to pick A k

129
00:11:18,289 --> 00:11:25,500
and i have to compute i k m.

130
00:11:25,500 --> 00:11:33,399
So, it turns out that this corresponds to saying that if i want to compute A particular entry i comma j then i need to choose A good k.

131
00:11:34,789 --> 00:11:39,580
i can express this in many different ways.

132
00:11:39,740 --> 00:11:41,409
So, i can say.

133
00:11:42,139 --> 00:11:43,210
pick.

134
00:11:44,120 --> 00:11:48,519
So, for example, supposing i want to add N into this particular thing.

135
00:11:48,950 --> 00:11:53,409
then i have to say pick the entry i to i.

136
00:11:53,870 --> 00:12:00,340
And then i want the entry i plus one to j which is this entry.

137
00:12:00,409 --> 00:12:08,019
So, these two entries i have to sum up. Otherwise, i have to take this entry and sum it up with this entry.

138
00:12:08,019 --> 00:12:15,779
If i choose this entry as my k point then i must add this entry to get it up and take this sum or i have to take this entry and add this.

139
00:12:15,779 --> 00:12:23,039
So, in general we could have order n values i need to compute.

140
00:12:23,039 --> 00:12:25,539
For this, i need all the values here and need all the values here.

141
00:12:25,879 --> 00:12:29,500
So, i need something to the left and to the bottom.

142
00:12:29,960 --> 00:12:35,830
But if i start from the diagonal then it becomes easy.

143
00:12:36,529 --> 00:12:40,299
If i have initially these values filled in.

144
00:12:43,490 --> 00:12:49,799
This is the base case. Then, to the left and below i can fill in this because i know which value is to the left.

145
00:12:49,799 --> 00:12:52,690
i know this value to the left and below, so i can fill up this diagonal.

146
00:12:52,690 --> 00:12:56,379
So, in the next step i can fill up this diagonal.

147
00:12:56,720 --> 00:12:59,529
So, i have all the values to left and below.

148
00:12:59,779 --> 00:13:10,379
At this entry i have the values. to the left and below, so i can fill up this diagonal. So, i can fill it up diagonal by diagonal. So, this is the order in which i can fill up this table using this inductive definition.

149
00:13:10,379 --> 00:13:12,610
So this is the way in which the dependency is there.

150
00:13:14,450 --> 00:13:20,700
So, this is the code for this particular procedure.

151
00:13:20,700 --> 00:13:23,980
You can go through it.

152
00:13:23,980 --> 00:13:26,049
You can follow from the notation specified.

153
00:13:26,299 --> 00:13:29,649
So, what we are doing is when we compute one entry

154
00:13:29,659 --> 00:13:33,429
So, supposing we are computing this one entry

155
00:13:33,429 --> 00:13:39,250
Then we want to compute the minimum across many different pairs, right, this entry this entry and so on.

156
00:13:39,250 --> 00:13:47,080
So, remember what we did when we computed the maximum longest common subword we assumed that the maximum was 0 and every time we saw A bigger value we updated it.

157
00:13:47,080 --> 00:13:48,990
Here we want the minimum entry.

158
00:13:48,990 --> 00:13:55,269
So, what we do is we assume the minimum is some large number and every time we see an entry if it is smaller than the minimum we reduce it.

159
00:13:55,340 --> 00:13:59,759
So, that is what is happening here when we start the loop.

160
00:13:59,759 --> 00:14:03,840
We assume that the value for R C is actually infinity. Now, what is infinity?

161
00:14:03,840 --> 00:14:09,759
We can take for instance the product of all the dimensions that appear in this problem.

162
00:14:09,799 --> 00:14:12,759
You know that the total dimension will not be more than that.

163
00:14:12,759 --> 00:14:20,139
So, you can take the product of all the dimensions you can take A very large number it does not matter something relative to the problem. So, we have not defined it in the code.

164
00:14:20,139 --> 00:14:22,320
The important thing is that we are computing minimum.

165
00:14:22,320 --> 00:14:27,419
So, instead of starting with 0 and uptating it when you do maximum you start with A large value and keep shrinking it.

166
00:14:27,419 --> 00:14:36,309
So, every time we find A sub problem which is smaller than the current value that we have seen, then we replace that value as the new minimum.

167
00:14:36,309 --> 00:14:48,309
So, that is all that is important here everything else is just A way to make sure that we go through this table diagonal after diagonal. For each diagonal we scan the row and the column and compute the minimum across all pairs in that row and column.

168
00:14:51,919 --> 00:14:56,379
So, as with this LCS problem.

169
00:14:56,379 --> 00:14:59,558
We have to fill an order n squared size table.

170
00:14:59,583 --> 00:15:08,370
But the main difference between LCS and this is that in order to fill an entry in the LCS thing we had to only look at only A constant number of entries.

171
00:15:08,370 --> 00:15:17,289
Right. So, order m n or order n square but the point was each entry takes constant time, so the effort involved is the same as the size of the table.

172
00:15:17,289 --> 00:15:21,720
m N table takes m N time. Now here unfortunately it is not the case.

173
00:15:21,720 --> 00:15:25,719
We saw that an entry will take time proportional to its distance from the diagonal.

174
00:15:25,744 --> 00:15:33,460
So, in general that will add an order n factor. So though we have order n squared by two entries actually order n squared entries.

175
00:15:33,460 --> 00:15:41,740
We have to account for order n work per entry because each entry has to scan the row to its left in the column below not just one entry away from it

176
00:15:41,740 --> 00:15:46,210
And so, this whole thing becomes order n cubed and not order n square.

177
00:15:46,210 --> 00:15:51,970
So, it is this is some point to keep in mind that there could be problems where the complexity.

178
00:15:53,419 --> 00:15:57,909
is bigger than the table size. We have seen this with past examples.

179
00:15:57,909 --> 00:16:06,220
That is the reason to do this example. In all the examples we saw before, Fibonacci we add A table which is linear in N and it took time linear N to fill it.

180
00:16:06,220 --> 00:16:15,220
For the grid path and for longest common subsequence we had tables which were N by N but it took only N by N time or m by N time to fill them.

181
00:16:15,220 --> 00:16:22,980
Here we have A table which is N by N but it takes N cubed time to fill it up because each entry requires more than constant time.

182
00:16:22,980 --> 00:16:25,690
It actually time takes time proportional to n.

