1
00:00:02,660 --> 00:00:05,320
So, in the previous lecture we saw quick sort.

2
00:00:05,540 --> 00:00:10,109
which works as follows, you first rearrange the elements with respect to pivot element

3
00:00:10,109 --> 00:00:12,580
which could be say the first value in the array.

4
00:00:12,580 --> 00:00:16,510
So, you partition A into the lower and upper parts, those which are smaller than pivot

5
00:00:16,640 --> 00:00:18,160
those which are bigger than pivot.

6
00:00:18,170 --> 00:00:22,960
You rearrange the array, so that pivot lies between the smaller and the bigger parts.

7
00:00:22,960 --> 00:00:25,660
and then you recursively sort the two partitions.

8
00:00:27,140 --> 00:00:30,989
And this was the actual python code which we ran.

9
00:00:30,989 --> 00:00:35,130
and we saw that it actually behaved not as well as merge sort.

10
00:00:35,130 --> 00:00:40,060
given a list of size 7500, we saw it took an appreciably long time.

11
00:00:41,990 --> 00:00:44,679
So, what is the worst case  behavior of quick sort?

12
00:00:45,350 --> 00:00:50,411
So, the worst case behavior of quick sort comes when the pivot is very far from the ideal value we want

13
00:00:50,412 --> 00:00:53,009
So, the ideal value we want is the median.

14
00:00:53,009 --> 00:00:59,500
The median would split the list into two equal parts and thereby divide the work into two equal halves.

15
00:00:59,630 --> 00:01:04,420
But, if the pivot happens to be either the minimum or the maximum value in the overall array,

16
00:01:04,420 --> 00:01:08,976
then supposing it is the maximum then every other value will go into the lower part and

17
00:01:08,977 --> 00:01:10,569
and nothing will go into the higher part.

18
00:01:10,640 --> 00:01:14,680
So, the recursive call it consists of sorting n - 1 elements.

19
00:01:14,680 --> 00:01:18,689
So, if one partition is empty, the other one has size n - 1.

20
00:01:18,689 --> 00:01:21,579
So, this would happen if the pivot is one of the extreme elements.

21
00:01:22,819 --> 00:01:25,769
And if this happens and this is the worst case,

22
00:01:25,769 --> 00:01:27,900
then in order to sort n elements,

23
00:01:27,900 --> 00:01:30,810
we have to recursively sort n - 1 elements.

24
00:01:30,810 --> 00:01:34,049
and we are still doing this order n work in order to rearrange the array.

25
00:01:34,049 --> 00:01:37,840
because we do not find this until we have sorted or gone through the whole array,

26
00:01:37,840 --> 00:01:39,129
and done the partition.

27
00:01:39,439 --> 00:01:42,312
So, this says T n is T n - 1 + n,

28
00:01:42,313 --> 00:01:48,489
and if we expand this, this comes out to be exactly the same recurrence that we saw for insertion sort and selection sort.

29
00:01:48,489 --> 00:01:51,393
So, T n would be 1 + 2 up to n.

30
00:01:51,394 --> 00:01:56,650
and this summation is just n into n + 1 by 2, which would be order n square.

31
00:01:58,909 --> 00:02:02,429
The even more paradox you could think about quick sort is that

32
00:02:02,430 --> 00:02:07,390
this would happen if the array is sorted either in the correct order or in the wrong order.

33
00:02:07,489 --> 00:02:12,310
Supposing we are trying to sort an ascending order and we already have an array in ascending order.

34
00:02:12,310 --> 00:02:14,729
then the first element will be the smallest element.

35
00:02:14,729 --> 00:02:18,931
So, this split will give us an array of  n - 1 on one side and 0 on the other side.

36
00:02:18,932 --> 00:02:20,259
and this will keep happening.

37
00:02:20,900 --> 00:02:25,180
So, the worst case of quick sort is actually an already sorted array.

38
00:02:25,180 --> 00:02:28,965
Remember, we saw that for insertion sort an already sorted array works well

39
00:02:28,966 --> 00:02:31,386
because the insert steps stops very fast.

40
00:02:31,387 --> 00:02:35,947
So quick sort is actually, in the worst case doing even worse than insertion sort,

41
00:02:35,948 --> 00:02:38,409
and specifically on already sorted arrays.

42
00:02:41,419 --> 00:02:48,129
However, it turns out that this is a very limited kind of worst case.

43
00:02:48,199 --> 00:02:54,669
And one can actually try and quantify the behavior of quick sort over every possible permutation.

44
00:02:54,669 --> 00:02:57,802
So, if we take an input array with n distinct values,

45
00:02:57,803 --> 00:03:02,422
we can assume that any permutation of these n values is equally likely

46
00:03:02,423 --> 00:03:07,840
and we can compute how much time quick sort takes on each of these different n permutation

47
00:03:07,840 --> 00:03:13,240
and assuming all are equally likely if we average out over n permutation, we can compute an average degree.

48
00:03:13,460 --> 00:03:16,990
Now, this sounds simple, but mathematically it is sophisticated.

49
00:03:16,990 --> 00:03:25,519
And sorting is one of the rare examples where you can meaningfully enumerate all the possible inputs and the probabilities of those inputs.

50
00:03:25,520 --> 00:03:34,710
But, if you do this calculation, it turns out, that in a precise mathematical sense, quick sort actually works in order n log n in the average.

51
00:03:34,710 --> 00:03:39,159
So, the worst case, though it is order n squared, actually happens very rare.

52
00:03:41,900 --> 00:03:47,379
So, the worst case actually arises because of a fixed choice of the pivot element.

53
00:03:48,110 --> 00:03:51,896
So, we choose the first element as the pivot in our algorithm.

54
00:03:51,897 --> 00:03:54,879
and so, in order to construct the worst case input

55
00:03:54,879 --> 00:03:59,620
if we always put the smallest or largest element at the first position, we get a worst case input

56
00:03:59,620 --> 00:04:05,229
This tells us that a sorted input either ascending or descending this worst case for our choice of pivot.

57
00:04:05,870 --> 00:04:08,879
Supposing, instead we wanted to choose the midpoint.

58
00:04:08,879 --> 00:04:11,080
we take the middle element in the array as our pivot.

59
00:04:11,080 --> 00:04:16,261
Then, again we can construct a worst case input by always putting the smallest value and working backwards,

60
00:04:16,262 --> 00:04:19,899
so that at every stage the middle value is the smallest or largest value.

61
00:04:19,899 --> 00:04:23,572
So, for any fixed choice of pivot,

62
00:04:23,573 --> 00:04:27,970
if we have a pivot which is picked in a fixed way in every iteration

63
00:04:27,970 --> 00:04:30,399
we can always reconstruct the worst case.

64
00:04:31,009 --> 00:04:36,430
However, If we change our strategy and say that each time we call quick sort,

65
00:04:36,500 --> 00:04:41,539
we randomly choose a value within the range of elements and pick that as the pivot,

66
00:04:41,540 --> 00:04:48,610
then it turns out that we can beat this order n square worst case and get an expected running time.

67
00:04:48,610 --> 00:04:52,660
again an average running time probabilistically of order n log n.

68
00:04:55,399 --> 00:04:59,160
So, as a result of this, because though it is worst case order n squared.

69
00:04:59,160 --> 00:05:02,829
but an average order n log n, quick sort is actually very fast.

70
00:05:03,199 --> 00:05:06,670
What we saw is it addresses one of the issue with merge sort.

71
00:05:06,670 --> 00:05:11,439
because by sorting or rearranging in place, we do not create extra space.

72
00:05:11,569 --> 00:05:18,519
What we have not seen, in which you can see if you read up another book somewhere, is that we can even eliminate the recursive part,

73
00:05:18,520 --> 00:05:24,699
we can actually make quick sort operate iteratively over the intervals on which we want to sort.

74
00:05:25,220 --> 00:05:31,600
So, quick sort as a result of this has turned out to be in practice, one of the most efficient sorting algorithms.

75
00:05:31,819 --> 00:05:34,959
And, when we have a utility like a spread sheet,

76
00:05:34,959 --> 00:05:37,445
where we have a button which says sort this column,

77
00:05:37,446 --> 00:05:42,339
then more often than not, the internal algorithm that is implemented is actually quick sort.

78
00:05:43,550 --> 00:05:47,920
We saw that python has a function l dot sort.

79
00:05:47,920 --> 00:05:50,299
which allows us to sort a list built-in .

80
00:05:50,300 --> 00:05:55,269
So, you might ask for example, what sorting algorithm is python using.

81
00:05:55,269 --> 00:05:58,800
Very often it will be quick sort, although in some cases,

82
00:05:58,800 --> 00:06:04,240
some algorithms will decide on the values in the list and apply different sorting algorithms according to the type of values.

83
00:06:04,279 --> 00:06:06,790
But, default usually is quick sort.

84
00:06:07,550 --> 00:06:11,439
So, before we proceed, let us try and validate our claim that

85
00:06:11,439 --> 00:06:18,310
quick sort's worst case behavior is actually tied to the description of the worst case input as an already sorted list.

86
00:06:25,069 --> 00:06:29,470
So, here we have as before, our python implementation of quick sort.

87
00:06:29,509 --> 00:06:33,430
in which we have just repeated the code we wrote before.

88
00:06:33,430 --> 00:06:36,870
Now, we are going to write another function which will do the following.

89
00:06:36,871 --> 00:06:41,189
It will shuffle the elements of a list, by repeatedly picking two indices and just swapping them.

90
00:06:41,189 --> 00:06:46,739
So, this will allow to take say a range output which is sorted and create a suitably random shuffle of the list.

91
00:06:46,740 --> 00:06:49,209
So, here is the code for it.

92
00:06:49,209 --> 00:06:50,350
So,

93
00:06:51,680 --> 00:06:55,030
So, it is very simple. You use a python library called random.

94
00:06:55,031 --> 00:06:57,588
which allows you some functions to generate random numbers.

95
00:06:57,589 --> 00:07:02,241
And one of the things that this libraryhas is this function rand range .

96
00:07:02,242 --> 00:07:06,507
So, rand range generates an integer in the range 0 to length of l - 1.

97
00:07:06,508 --> 00:07:08,350
So, we pass it a list.

98
00:07:08,350 --> 00:07:12,300
and we repeatedly pick two indices j and k in the range 0 to length of l - 1.

99
00:07:12,300 --> 00:07:13,720
Then we exchange l j and l k.

100
00:07:13,720 --> 00:07:17,320
and how many times we do is, well we just do it a large number of times in this case.

101
00:07:17,321 --> 00:07:21,456
Say, if we have 10,000 elements in the list, we will do this 5,000 times.

102
00:07:21,457 --> 00:07:23,889
We do it length of l by 2 times.

103
00:07:23,959 --> 00:07:25,612
So let us see how this works.

104
00:07:25,613 --> 00:07:28,967
So, we load as usual the python in terminal.

105
00:07:28,968 --> 00:07:32,439
And then we import quick sort.

106
00:07:32,439 --> 00:07:33,959
Then, we import randomize .

107
00:07:33,960 --> 00:07:39,905
So, you can do this. You can write python functions in multiple files and import them one after the other, and they will all get loaded.

108
00:07:39,920 --> 00:07:44,860
Now, as before we will say l for instance could be the list.

109
00:07:44,860 --> 00:07:49,990
So, let us also import sys and finish off that of that.

110
00:07:53,300 --> 00:07:54,999
recursion limit

111
00:07:55,900 --> 00:07:58,806
Because we know this is

112
00:07:59,720 --> 00:08:01,726
So, set a large recursion limit

113
00:08:01,840 --> 00:08:06,310
and now, we set up a fairly large list.

114
00:08:06,311 --> 00:08:08,410
we had done last few instances.

115
00:08:08,411 --> 00:08:12,923
7,500 down to zero. Right?

116
00:08:12,924 --> 00:08:19,750
And what we saw was that if we try to quick sort this list,

117
00:08:21,410 --> 00:08:25,240
it takes a long time, because it is 7500 and it is a worst case input .

118
00:08:26,360 --> 00:08:29,860
So, what we are going to try and do now is.

119
00:08:29,870 --> 00:08:32,159
And the same thing will happen even after it sorted.

120
00:08:32,159 --> 00:08:35,392
Because even after it is sorted is is still a worst case input except now it is in ascending order.

121
00:08:35,393 --> 00:08:37,990
So, both descending order and ascending order take a long time.

122
00:08:38,120 --> 00:08:42,786
So, now supposing we randomize l

123
00:08:43,519 --> 00:08:47,019
So, if you look at l now you can see that the numbers are no longer in order.

124
00:08:47,019 --> 00:08:51,138
So, you will see some 6,000, some 7,000s, 2,000s, and so on.

125
00:08:51,139 --> 00:08:53,980
So, our claim is that this will go faster.

126
00:08:54,049 --> 00:08:59,500
and indeed, you can see that if you run quick sort on this, it returns almost immediately.

127
00:09:00,710 --> 00:09:03,860
And it is not because quick sort has become any fastest because the order was random.

128
00:09:03,861 --> 00:09:07,000
Because again if we ran quick sort on the sorted list, again it is going to be sorted.

129
00:09:07,730 --> 00:09:15,580
So, this just demonstrates in a very effective way, that if we randomize the list and we run quick sort, it comes out immediately.

130
00:09:15,580 --> 00:09:19,570
But if we do not randomize it and if we ask quick sort to sort the sorted list,

131
00:09:19,570 --> 00:09:21,279
then it takes a long time.

132
00:09:21,679 --> 00:09:27,070
So, you could actually check that, for instance, if we go back to this list,

133
00:09:27,710 --> 00:09:34,419
and we make it say even something bigger like 10,000, maybe 15000 and then we randomize it.

134
00:09:37,700 --> 00:09:39,159
and then we sort it.

135
00:09:39,590 --> 00:09:41,317
it comes out very fast.

136
00:09:41,318 --> 00:09:45,539
So, this validates our claim that quick sort on an average is fast,

137
00:09:45,540 --> 00:09:49,149
it is only when you give it these very bad inputs which are the already sorted ones,

138
00:09:49,149 --> 00:09:51,039
that it behaves in a poor manner.

139
00:09:56,120 --> 00:10:00,909
Now, there is one more criterion that one has to be aware of when one is sorting later.

140
00:10:00,909 --> 00:10:05,440
So, very often sorting happens in stages on multiple attributes.

141
00:10:05,570 --> 00:10:06,809
For example,

142
00:10:06,809 --> 00:10:11,980
You might have a list of students who are listed in alphabetical order in the role list.

143
00:10:12,289 --> 00:10:15,730
After a quiz or a test, they all get marks.

144
00:10:15,730 --> 00:10:20,340
Now, you want to list them in order of marks, but where there are ties,

145
00:10:20,340 --> 00:10:22,769
where two of your students have the same marks,

146
00:10:22,769 --> 00:10:25,720
we want to continue to have them listed in alphabetical order.

147
00:10:26,090 --> 00:10:29,799
So, in other words you have an original order in alphabetical order.

148
00:10:29,799 --> 00:10:32,580
and then you take an another attribute namely marks.

149
00:10:32,580 --> 00:10:38,409
And sorting by marks should not disturb the sorting that already exists in alphabetical order.

150
00:10:38,990 --> 00:10:45,159
So, what this amounts to saying is that if we have two lists, two items in the original list which are equal.

151
00:10:45,710 --> 00:10:49,169
then they must retain the same order as they had after the sorting.

152
00:10:49,169 --> 00:10:53,049
So, I should not take two elements that are equal and swap them while sorting.

153
00:10:53,779 --> 00:10:57,429
And this would be crucial when we are using something like a spread sheet.

154
00:10:57,429 --> 00:11:01,990
Because if you sort by one column, you do not want to disturb the sorting that you did by another column.

155
00:11:05,360 --> 00:11:09,519
Unfortunately, quick sort, the way we have described it is not stable.

156
00:11:09,679 --> 00:11:16,240
Because, whenever we extend a partition in this partition stage or move the pivot to the center,

157
00:11:16,340 --> 00:11:22,090
what we end up doing is disturbing the order of elements which were already there in the unsorted list.

158
00:11:22,129 --> 00:11:26,919
So, we argued earlier that disturbing this order does not matter because anyway we are going to sort it.

159
00:11:26,919 --> 00:11:30,319
But, it does matter if the sorting has to be stable.

160
00:11:30,320 --> 00:11:35,980
If there was a reason why these elements were in a particular order, not for the current attribute or for the different attribute.

161
00:11:36,080 --> 00:11:39,970
and we remove them around, then we are destroying the original sorted array.

162
00:11:40,669 --> 00:11:45,179
On the other hand, merge sort we can see is actually stable,

163
00:11:45,179 --> 00:11:50,422
If we are careful to make sure that we always pick from one side consistently if the values are equal.

164
00:11:50,423 --> 00:11:52,500
So, when we are merging left and right,

165
00:11:52,500 --> 00:11:56,805
when we have the equal to case, we have to either put the element from left into the final list or right.

166
00:11:56,806 --> 00:11:58,899
So, if we consistently choose the left

167
00:11:58,899 --> 00:12:01,779
then we will always keep elements on the left to the left of the ones on the right.

168
00:12:02,179 --> 00:12:05,875
and therefore, it will remain a stable sort.

169
00:12:05,876 --> 00:12:08,950
Similarly, insertion sort will also be stable

170
00:12:08,950 --> 00:12:12,659
If we make sure that we move things backwards only if they are strictly smaller.

171
00:12:12,659 --> 00:12:17,049
When we go backwards and we find something which is equal to the current value, we stop the insertion.

172
00:12:17,049 --> 00:12:20,049
So, insertion sort,merge sort as stable sorts.

173
00:12:20,049 --> 00:12:22,515
quick sort as we have described it, is not stable,

174
00:12:22,516 --> 00:12:26,590
though it is possible to do a more careful implementation and make it stable.

