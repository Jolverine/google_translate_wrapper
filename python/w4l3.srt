1
00:00:02,246 --> 00:00:06,190
So, we saw that merge sort  is an order n log n sorting algorithm.

2
00:00:06,190 --> 00:00:10,509
but it has a couple of deficiencies which make it sometimes impractical.

3
00:00:10,849 --> 00:00:14,740
The main problem is that it requires extra space for merging

4
00:00:14,869 --> 00:00:19,076
And we also saw that it is difficult to implement merge sort  without using recursion

5
00:00:19,101 --> 00:00:22,108
And recursion carries its own cost in programming language.

6
00:00:24,841 --> 00:00:26,952
So, lets address the space problem.

7
00:00:27,736 --> 00:00:33,310
The extra space required by merge sort  is actually required in order to implement the merge function.

8
00:00:33,590 --> 00:00:38,500
and why do we need to merge? The reason we need to merge is that when we do a merge sort ,

9
00:00:38,500 --> 00:00:45,759
we have the initial list and then we split it into two parts but in general there may be items from the left.

10
00:00:45,759 --> 00:00:50,409
which are bigger than items from the right. So, for instance if we had say,

11
00:00:50,450 --> 00:00:54,250
even numbers in the left and odd numbers on the right.

12
00:00:54,550 --> 00:00:58,900
then we have to merge by taking numbers alternately from either side.

13
00:00:59,630 --> 00:01:09,646
So, if we could arrange that everything that is on the left side of our divided problem is smaller than everything on the right side of the divided problem.

14
00:01:10,593 --> 00:01:12,146
Then, we wouldn't need to merge it all.

15
00:01:12,699 --> 00:01:18,130
And this perhaps could save us this problem of requiring extra space for the merge.

16
00:01:20,689 --> 00:01:23,702
So, how would we do divide and conquer without merge.

17
00:01:25,243 --> 00:01:27,659
Assume that we knew the median value.

18
00:01:27,659 --> 00:01:33,716
Remember the median value in a set is the value such that half the elements are smaller than half are bigger.

19
00:01:35,649 --> 00:01:42,124
We could move all the values smaller than the median to the left half and all of those bigger than the median to the right half.

20
00:01:43,883 --> 00:01:52,180
As we will see, this can be done without creating a new array in time proportional to the length of the list.

21
00:01:53,816 --> 00:02:00,989
Having done this rearrangement, moving all the smaller values to the left half and the bigger values to the right half then

22
00:02:00,989 --> 00:02:06,443
we can recursively apply this divide and conquer strategy and sort the right and the left half separately.

23
00:02:07,519 --> 00:02:12,520
and since we have guaranteed that everything in the left half is smaller than  everything in the right half.

24
00:02:13,019 --> 00:02:17,050
This automatically means that after this divide and conquer step.

25
00:02:17,050 --> 00:02:24,442
We don't need to combine the answers in any non-trivial way because the left half is already below the right half, so we don't need to merge.

26
00:02:28,309 --> 00:02:34,210
So, if we apply this strategy then we would get a recursive equation exactly like merge sort .

27
00:02:34,210 --> 00:02:42,370
it could say that the time required to sort a list of length n requires us to first sort two lists of size n by 2.

28
00:02:42,370 --> 00:02:50,039
and we do order n  not for merging but, in order to decompose the list so that all the smaller values are in left and the right

29
00:02:50,064 --> 00:02:54,483
So, the rearranging step before we do the recursive step is what is order n.

30
00:02:54,508 --> 00:02:57,879
whereas merge was the step after the recursive step .

31
00:02:57,879 --> 00:03:03,879
which was order n in the previous case but If we solve the recurrence is the same one, so we get another order n log n algorithm.

32
00:03:08,730 --> 00:03:12,340
So, the big bottleneck with this approach is to find the median.

33
00:03:13,190 --> 00:03:20,663
So, remember that we said earlier that one of the benefits of sorting a list is that we can identify the median as the middle element after sorting.

34
00:03:21,890 --> 00:03:24,336
Now, here we are asking for the median before sorting.

35
00:03:24,996 --> 00:03:28,539
But, our aim is to sort so its kind of paradoxical.

36
00:03:28,539 --> 00:03:33,219
If we are requiring the output of the sorting to be the input to the sorting.

37
00:03:34,892 --> 00:03:42,580
So, this means that we have to try the strategy out with a more simplistic choice of element to split the list.

38
00:03:42,825 --> 00:03:46,780
So, instead of looking for the median, we just pick up some value in the list A.

39
00:03:46,780 --> 00:03:49,689
and use that as what is called a pivot element.

40
00:03:49,689 --> 00:03:56,769
we split A with respect to this pivot so that all the smaller elements are with the left and all the bigger elements are to the right.

41
00:04:00,223 --> 00:04:11,636
So, this algorithm is called quick sort it was invented by person called Tony Four in the 1960s and is one of the most famous sorting algorithms.

42
00:04:12,103 --> 00:04:16,082
So, we choose a pivot element which is usually the first element in the list of the array.

43
00:04:16,759 --> 00:04:22,500
We partition A into the lower part and the upper part with respect to this pivot end

44
00:04:22,500 --> 00:04:24,943
So, we move all the smaller elements to the left.

45
00:04:25,056 --> 00:04:28,149
and all the  bigger elements to the right with respect to the choice of pivotal

46
00:04:29,523 --> 00:04:34,163
and we make sure the pivot comes between the two, because we have picked up the first element in the array to be pivot

47
00:04:34,190 --> 00:04:41,036
So, after this we want to move it to the center between the lower and the upper bound and then, we recursively sort two partitions.

48
00:04:44,456 --> 00:04:48,910
So, here is a high level view of how quick sort would work on a typical list.

49
00:04:48,910 --> 00:04:56,143
So, suppose this is our list so we first identify the beginning of the list the first element as the pivot element.

50
00:04:56,950 --> 00:05:01,569
Now, for the remaining elements we have to figure out which ones are smaller and which ones are bigger

51
00:05:02,410 --> 00:05:11,510
So, without going into how we will do this, we end up identifying 32, 22 and 13 as the 3 elements which are smaller and marked  in yellow

52
00:05:11,810 --> 00:05:15,069
 and the other 4 elements which are marked  in green are large.

53
00:05:16,160 --> 00:05:20,040
So, the first step is to actually partition with respect to this criterion.

54
00:05:20,040 --> 00:05:24,810
So, we have to move these elements around, so that they come into two blocks.

55
00:05:24,810 --> 00:05:27,550
So, that 13, 32 and 22 come to the left.

56
00:05:27,550 --> 00:05:33,430
63 57, 91 and 78, to the right and the pivot element 43 comes in the middle.

57
00:05:33,947 --> 00:05:36,781
So, this is the rearranging step and now.

58
00:05:37,060 --> 00:05:41,529
we recursively sort the yellow bits and the green bits then assuming we can do that,

59
00:05:41,529 --> 00:05:45,480
we have a sorted array and notice that since all the yellow things are smaller than 43

60
00:05:45,480 --> 00:05:49,509
and all the green things are bigger than 43, no further merging is required.

61
00:05:52,243 --> 00:05:54,899
So, let us look at how partitioning works.

62
00:05:54,899 --> 00:05:59,350
So, here we have the earlier list and we have marked  43 as our pivot element.

63
00:05:59,350 --> 00:06:03,112
And we want to do a scan of the remaining elements and divide them into

64
00:06:03,137 --> 00:06:08,742
two groups, those smaller than 43, the yellow ones, those bigger than 43, the green ones and rearrange them.

65
00:06:09,962 --> 00:06:13,839
So, what we will do is we will keep two pointers yellow pointer and the green pointer

66
00:06:14,155 --> 00:06:19,081
And the general rule will be that at any given point, we will have at some distance.

67
00:06:19,355 --> 00:06:24,129
the yellow pointer which I will draw in orange which is visible and the green pointer.

68
00:06:24,129 --> 00:06:29,165
So, these will move in this order the orange pointer or the yellow pointer will always be behind the green pointer.

69
00:06:29,565 --> 00:06:35,397
and the inductive property that you will maintain is that these elements are smaller than or equal to 43

70
00:06:35,910 --> 00:06:40,643
these elements are bigger than 43 and these elements are unknown.

71
00:06:42,577 --> 00:06:46,139
So, what we are trying to do is, we are trying to move from left to right.

72
00:06:46,139 --> 00:06:49,589
and classify all the unknown elements each time we see an unknown element.

73
00:06:49,552 --> 00:06:52,660
we will shift the two pointers so that we maintain this property.

74
00:06:52,700 --> 00:06:57,699
that between 43 and the first pointer we have the elements smaller than  or equal to 43.

75
00:06:57,699 --> 00:07:01,494
between the first pointer and the second pointer we have the elements strictly greater 43

76
00:07:01,752 --> 00:07:05,230
and to the right of the green pointer we have those which are yet to be scanned.

77
00:07:07,023 --> 00:07:10,000
So, initially nothing is known, then we look at 32.

78
00:07:10,000 --> 00:07:14,618
Since 32 is smaller than  43, we move the yellow pointer and we also push the green pointer along

79
00:07:14,643 --> 00:07:16,949
So, the unknown thing starts in 22.

80
00:07:16,949 --> 00:07:22,060
there is nothing between the yellow and the green pointer indicated we have not yet found the value bigger than 43

81
00:07:22,949 --> 00:07:24,910
So, the same happens for 22.

82
00:07:24,910 --> 00:07:28,629
Now, when we see 78, we notice that 78 is bigger than 43.

83
00:07:29,008 --> 00:07:33,635
So now, we move only the green pointer and not  the yellow pointer , so Now, we have these 3 intervals as before

84
00:07:33,935 --> 00:07:36,819
So, remember that this is the part which is less than equal to 43.

85
00:07:36,819 --> 00:07:44,350
this is the part that is greater than 43 and this part is unknown right, we continue in this way.

86
00:07:44,350 --> 00:07:48,189
So, now we look at 63, so again 63 extends the green zone.

87
00:07:48,189 --> 00:07:52,540
57 extends a green zone 91 extends a green zone.

88
00:07:52,540 --> 00:07:56,470
Now, we have to do something when we find 13.

89
00:07:56,470 --> 00:08:00,100
So, 13 is an element now which has to be put into the yellow zone.

90
00:08:00,205 --> 00:08:03,705
So, one strategy would be to do a lot of shifting.

91
00:08:03,730 --> 00:08:07,360
We move 13 to where 22 is or after 22.

92
00:08:07,360 --> 00:08:10,180
and we push everything from 78 onwards to the right.

93
00:08:10,250 --> 00:08:15,220
But actually, a cleverer strategy is to say that  okay 13 must go here.

94
00:08:17,605 --> 00:08:21,519
So, we need to make space, but instead of making space

95
00:08:21,519 --> 00:08:30,399
we can say it does not matter to us we are eventually going to sort the green things anyway so how does it matter which way we sort that two. So, we will take this 78 and just move it to 13

96
00:08:30,399 --> 00:08:33,009
So, instead of doing any shifting.

97
00:08:33,009 --> 00:08:36,370
we just exchange the first element in the green zone.

98
00:08:36,370 --> 00:08:38,320
with the element we are seeing so far.

99
00:08:38,320 --> 00:08:41,980
that automatically will extend both the yellow zone and the green zone correctly.

100
00:08:42,080 --> 00:08:49,299
So, our next step is to identify 13 as smaller than  43 and swap it with 73.

101
00:08:49,399 --> 00:08:54,070
So now, we have reached an intermediate stage where to the right of the pivot we have

102
00:08:54,070 --> 00:08:58,360
scanned everything and we have classified them into those which are the smaller ones.

103
00:08:58,360 --> 00:08:59,889
and those which are the bigger ones.

104
00:09:00,716 --> 00:09:04,216
Now, it remains to push the yellow things to the left of the 43.

105
00:09:05,036 --> 00:09:09,165
Now, once again we have the same problem we saw when we included 13 in the yellow zone

106
00:09:09,190 --> 00:09:13,210
if we move 43 to the correct place, then we have to move everything here to the left.

107
00:09:13,404 --> 00:09:18,940
But, instead we can just take this 13 the last element in the yellow zone.

108
00:09:18,940 --> 00:09:22,149
and replace it there and not shift 32 and 22.

109
00:09:22,149 --> 00:09:26,759
So, this disturbs the order but anyway this is unsorted so it just remains unsorted.

110
00:09:27,632 --> 00:09:29,889
So, we do this and now we have.

111
00:09:30,001 --> 00:09:36,171
the array rearranged as we want it. So, all of these things to the left are smaller than the pivot.

112
00:09:37,244 --> 00:09:41,856
The pivot is in the middle. and everything to the right is bigger than the pivot.

113
00:09:46,550 --> 00:09:53,830
So, here is an implementation in python. So remember that quick sort is going to have like merge sort  and like binary research.

114
00:09:53,830 --> 00:09:58,690
we repeatedly apply it on smaller and smaller segments. So, in general we have to pass it

115
00:09:58,690 --> 00:10:03,523
to the list which we will call A and the end points of the segment the left and the right

116
00:10:03,548 --> 00:10:07,238
 something that we are doing a slice l  to r minus 1.

117
00:10:08,244 --> 00:10:13,210
So, if this slice is 1 or 0 in length we do nothing.

118
00:10:14,223 --> 00:10:17,320
Otherwise, we follow this partitioning strategy we had before.

119
00:10:17,708 --> 00:10:22,120
which is that we are sorting from l to r minus 1.

120
00:10:22,120 --> 00:10:24,220
So, the position l.

121
00:10:24,379 --> 00:10:25,899
So, this is the pivot.

122
00:10:26,793 --> 00:10:29,671
right So, we will initially put the yellow pointer here

123
00:10:31,011 --> 00:10:33,635
saying that the end of the yellow zone

124
00:10:34,005 --> 00:10:38,500
is actually just the pivot there is nothing there. So, yellow is l plus 1.

125
00:10:38,500 --> 00:10:50,781
And now we let green proceed and every time we see an element in the green the new green one, which is smaller than the the one which is the pivot so remember this is the pivot.

126
00:10:51,965 --> 00:10:55,661
right if  we ever we see a green, the next value

127
00:10:55,686 --> 00:10:57,580
to be checked is the smaller than or equal to

128
00:10:57,590 --> 00:11:01,714
we exchange, so that we bring this value to the end of the yellow zone.

129
00:11:01,739 --> 00:11:03,220
So, this is what we did in 13.

130
00:11:03,344 --> 00:11:07,590
and then, we move the yellow pointer as well otherwise if we see a value which is strictly bigger we move

131
00:11:07,590 --> 00:11:11,860
only the green pointer which is implicitly done by the for loop and we do not move the other

132
00:11:11,929 --> 00:11:14,883
So, at the end of this we have the pivot

133
00:11:15,894 --> 00:11:19,784
and then we have the less than equal to pivot and then we have the greater than pivot

134
00:11:19,809 --> 00:11:27,914
So this is that intermediate stage that we have achieved at the end of this rule. Now, we have to find the pivot and move it to the correct place. So, remember that the yellow

135
00:11:29,241 --> 00:11:32,769
yellow is pointing to the position beyond the last

136
00:11:32,769 --> 00:11:37,440
element smaller than that. So, yellow is always one value before beyond this.

137
00:11:37,440 --> 00:11:40,396
So, we take the yellow minus 1 value and exchange it with the left value.

138
00:11:41,414 --> 00:11:48,765
And now what we need to do is, we have now less than  p, p and greater than p

139
00:11:49,627 --> 00:12:01,350
and this is where yellow is right so, we need to go from 0 to yellow minus 1, we do not want to sort p  again, because p is already put in the correct place.

140
00:12:01,350 --> 00:12:06,391
So, we quick sort from L to yellow minus 1 and from yellow to the right end.

141
00:12:13,070 --> 00:12:17,070
So, here we have written the python code that we saw in the slide in a file.

142
00:12:17,070 --> 00:12:20,320
So, you can check that it is exactly the same code that we had in the slide.

143
00:12:20,345 --> 00:12:28,786
So, we can try and run it and verify that it works so we call python and we import this function

144
00:12:32,849 --> 00:12:37,699
So, remember that this is again a function with sorts inplace. So, if you want to sort something and see the effect

145
00:12:37,753 --> 00:12:40,870
we have to assign it a name and then sort that name and check the name afterwards

146
00:12:40,909 --> 00:12:48,137
So, let us for instance take a range of values from say 500 down to 0

147
00:12:50,287 --> 00:12:58,090
And now if we say quicksort l then we have to of course give it the end.

148
00:13:01,020 --> 00:13:02,468
then l gets correctly sorted.

149
00:13:04,311 --> 00:13:09,523
So, you cannot see all of it, but you can see it from 83 84 up to hundred and two upto 500

150
00:13:10,995 --> 00:13:16,480
Now, we have the same problem that we had with the insertion sort if we say 1000.

151
00:13:17,897 --> 00:13:22,596
right then we try quicksort this, you will get this, recursion depth because

152
00:13:22,621 --> 00:13:29,644
as we will see in the worst case actually quick sort behaves a bit like insertion sort and this is the bad case so.

153
00:13:29,669 --> 00:13:33,759
to get around this, we would have to do the usual thing, we have to import the SYS module.

154
00:13:33,759 --> 00:13:35,409
set the recursion limit.

155
00:13:36,658 --> 00:13:43,652
So, something suitably large say ten thousand or hundred thousand and then if we ask it to quicksort there is no problem ,

156
00:13:44,779 --> 00:13:49,299
So, this is another case where this recursion limit in python has to be manually set.

157
00:13:49,647 --> 00:13:56,929
And one thing we can see actually is that quicksort is not as good as we believe because if we were to

158
00:13:57,008 --> 00:14:02,080
For instance sort something of size say 7500.

159
00:14:05,504 --> 00:14:09,554
then it takes a visible amount of time. So, we saw that merge sort  which is n log n,

160
00:14:09,790 --> 00:14:13,700
would do 5,000 and 10000 and even 100,000 instantaneously.

161
00:14:13,832 --> 00:14:18,129
So, clearly quick sort is not behaving as well as merge sort .

162
00:14:18,165 --> 00:14:22,480
and We will see in fact that quick sort does not have an order n log n merge sort .

163
00:14:22,511 --> 00:14:25,090
behavior as we would have liked.

164
00:14:25,090 --> 00:14:28,539
And that is because we are not using the median, but the first value to split.

165
00:14:28,539 --> 00:14:33,909
So, we will see that in the next lecture. as to why quick sort is actually not a worst case order n log n algorithm

