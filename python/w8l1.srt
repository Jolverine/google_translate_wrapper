1
00:00:02,813 --> 00:00:09,010
So, we saw earlier that inductive definitions often provide a nice way to get a hold of the functions we want to compute.

2
00:00:09,836 --> 00:00:11,987
Now, we are familiar with induction for numbers.

3
00:00:12,310 --> 00:00:22,105
So for instance, we can define the factorial function in terms of the base case of 0 and in terms of the inductive case, saying f of n is n times factorial of n - 1.

4
00:00:23,421 --> 00:00:28,416
What we saw is we can also define inductively functions on structures like lists.

5
00:00:28,620 --> 00:00:40,063
So for instance, we can take an empty list as the base case and in the inductive case , we can separate the task of sorting a list into doing something with the initial element and nothing with the rest.

6
00:00:40,409 --> 00:00:43,896
So, insertion sort can be defined in terms of the insert function as follows.

7
00:00:44,219 --> 00:00:58,716
So, isort of the base case for the empty case just gives us the empty list and then if you want to sort a list with n elements , we pull out the first element and then we insert it into the result of inductively sorting the rest.

8
00:00:59,256 --> 00:01:10,776
So, this is a very attractive way of describing the dependency of the function that we want to compute, the value that we are trying to compute, on smaller values and it gives us a handle on how to go about computing it.

9
00:01:12,743 --> 00:01:18,141
So, the main benefit of an inductive definition is that it directly yields a recursive program .

10
00:01:18,841 --> 00:01:27,552
So, we saw this kind of a program for factorial which almost directly follows the definition saying that f of 0 is 1 and f of n is n into f of n - 1.

11
00:01:27,576 --> 00:01:30,720
So, we can just directly read it off more or less and translate it.

12
00:01:30,745 --> 00:01:37,530
The only thing we have done is we have taken care of some error case where if somebody feeds a negative number we will still say 1 and not go into a loop.

13
00:01:40,675 --> 00:01:49,989
So, in general when we have such inductive definitions what we do is we have some problems that we have to solve in order to get to the answer we are trying to reach.

14
00:01:50,564 --> 00:01:56,949
So for instance, to compute factorial of n, one of the things we need to do is compute factorial of n - 1.

15
00:01:57,396 --> 00:02:00,906
So, we call factorial of n - 1 a sub problem of factorial n.

16
00:02:01,606 --> 00:02:06,130
Now in turn factorial of n - 1 requires us to compute factorial n - 2.

17
00:02:06,670 --> 00:02:13,763
So, actually if you go down the chain, the factorial n sub problems are all the factorials for values smaller than n.

18
00:02:14,756 --> 00:02:26,245
Similarly, for insertion sort in order to sort the full list, we need to sort all the elements excluding the first one, what is called the tail of the list, and in turn we need to sort its tail and so on.

19
00:02:26,269 --> 00:02:33,751
So in general, when we do insertion sort we will find that we need to sort things a segment of the list.

20
00:02:33,775 --> 00:02:36,392
So, we can in general talk about x i to x j.

21
00:02:37,229 --> 00:02:48,342
And in all these cases what the induction inductive definition tells us is how to compute the actual value of f for our given input y by combining the solutions to these sub problems.

22
00:02:48,415 --> 00:02:55,657
For instance, in factorial we combine it by multiplying the current input with the result of solving it for the next smaller input.

23
00:02:55,681 --> 00:03:03,175
For insertion sort we combine it by inserting the first value into the result of solving the smaller input; that is the tail of the list.

24
00:03:06,814 --> 00:03:17,171
So, let us look at one particular problem which will highlight an issue that we have to deal with when we are looking at inductive specifications and naively translating them into programs.

25
00:03:17,964 --> 00:03:26,928
So, the Fibonacci numbers are a very famous sequence which are invented by Fibonacci and they occur in nature and they are very intuitive and most of you would have seen them.

26
00:03:27,168 --> 00:03:36,203
So, the Fibonacci numbers are 0, 1 and then you add so 1 + 1 is, 1 + 0 is 1, 1 + 1 is 2, 3, 5 and so on.

27
00:03:36,228 --> 00:03:39,055
So you just keep adding the previous two numbers and you go on.

28
00:03:39,389 --> 00:03:51,369
So the inductive definition says the zeroth Fibonacci number is 0, the first Fibonacci number is 1 and after that, for two onwards, the nth Fibonacci number is obtained by sub, by adding the previous two.

29
00:03:51,394 --> 00:03:54,423
So, the Fibonacci number 2 is Fibonacci 1 + Fibonacci 0.

30
00:03:55,316 --> 00:04:00,396
So, as before, we can directly translate this into an inductive into a recursive program .

31
00:04:00,760 --> 00:04:06,802
We can just write a Python function fib which says if n is 0 or n is 1, return the value n itself.

32
00:04:07,209 --> 00:04:08,378
So, if n is 0, return 0.

33
00:04:08,403 --> 00:04:09,709
If n is 1, you return 1.

34
00:04:10,089 --> 00:04:17,930
Otherwise you compute the value by recursive 2 recursively calling Fibonacci on n - 1, n - 2, add these two and return this value.

35
00:04:19,023 --> 00:04:25,316
So, here is a clear case of an inductive definition that has a natural recursive program extracted from it.

36
00:04:27,560 --> 00:04:30,228
So let's try to compute a value and see what happens.

37
00:04:30,579 --> 00:04:34,658
So, supposing we want to  compute Fibonacci of 5 using this definition.

38
00:04:35,345 --> 00:04:43,247
So, Fibonacci of 5 will go into the else clause and say we need to compute Fibonacci of n - 1, namely 4, and n - 2, namely 3.

39
00:04:43,280 --> 00:04:48,139
So, Fibonacci of 5 leaves us with two problems to compute: Fibonacci of 4. Fibonacci of 3.

40
00:04:48,772 --> 00:04:51,333
So, we do these in some order, let's go left to right.

41
00:04:51,357 --> 00:04:59,422
So, we pick Fibonacci of 4 and this in turn will require us to compute Fibonacci of 3 and Fibonacci of 2 by just applying the same definition to this value.

42
00:05:00,169 --> 00:05:02,659
Similarly, we go to the left of the two sub problems.

43
00:05:02,779 --> 00:05:04,645
Fibonacci of 3 requires 2 and 1.

44
00:05:05,692 --> 00:05:07,318
2 requires 1 and 0.

45
00:05:07,931 --> 00:05:12,634
Now, for 1 and 0, fortunately, we can exit without making a recursive call .

46
00:05:12,659 --> 00:05:15,651
If n is equal to 0 or n is equal to 1, we just return the value.

47
00:05:16,197 --> 00:05:20,206
So, we get back Fibonacci 1 is 1 and Fibonacci 0 is 0.

48
00:05:20,592 --> 00:05:26,519
So with this we can complete the computation of Fibonacci 2, we get values 1 + 0, in other words, 1.

49
00:05:26,543 --> 00:05:27,939
So, Fibonacci of 2 is 1.

50
00:05:28,579 --> 00:05:30,585
Now, we are back to Fibonacci of 3.

51
00:05:30,610 --> 00:05:35,105
So we have computed for Fibonacci of 3 the left case, Fibonacci of 2, and now we have to compute the right case.

52
00:05:36,165 --> 00:05:39,417
Once again we find the Fibonacci of one being a base case.

53
00:05:39,689 --> 00:05:43,583
It gives us 1 and now we can combine this and get Fibonacci of 3 is 2.

54
00:05:45,466 --> 00:05:47,577
So now, we are back to Fibonacci of 4.

55
00:05:48,730 --> 00:05:50,938
And we have computed the left side of Fibonacci of 4.

56
00:05:50,962 --> 00:05:52,658
So, we need to compute the right side.

57
00:05:52,958 --> 00:05:56,100
And now what happens is we end up having to compute Fibonacci of 2 again.

58
00:05:56,100 --> 00:06:00,486
Even though we already know the value, we naively have to execute Fibonacci of 2.

59
00:06:00,779 --> 00:06:04,450
All 1 and 0, again propagate the value 1 and 0 back up.

60
00:06:04,839 --> 00:06:06,147
Add them up and get 1.

61
00:06:06,974 --> 00:06:09,810
Now, we can compute Fibonacci of 4 is 2 + 1, 3.

62
00:06:10,317 --> 00:06:15,014
And now, we are finally back to the original call where we had to compute Fibonacci of 4 and Fibonacci of 3

63
00:06:15,039 --> 00:06:16,249
So, we have done with 4.

64
00:06:16,273 --> 00:06:18,195
So now, we want to do Fibonacci of 3.

65
00:06:18,788 --> 00:06:24,751
So, notice that we have already computed Fibonacci of 3, but this will blindly require us to call this function again.

66
00:06:25,405 --> 00:06:38,578
So, we will again have to execute this full tree, go all the way down, go all the way up and eventually, Fibonacci of 3 will ofcourse give us the same answer, namely 2, which we already knew, but we would not take exploit or we would not take advantage of the fact that we knew it.

67
00:06:39,311 --> 00:06:42,797
And in this way we get 3 + 2 and therefore, Fibonacci of 5 is 5.

68
00:06:43,910 --> 00:06:48,358
So, the point to note in this is that we are doing many things again and again.

69
00:06:48,382 --> 00:06:54,740
In particular, in this particular computation, the largest thing that we repeat is Fibonacci of 3, okay?

70
00:06:55,080 --> 00:07:02,944
So, as a result of this recomputation of the same value again and again, though we in principle only need n - 1 sub problems, right?

71
00:07:02,968 --> 00:07:06,682
If we have fib of 5, we need to fib of 4, fib of 3, fib of 2 and so on.

72
00:07:06,710 --> 00:07:08,580
So, n sub problems down to fib of 0.

73
00:07:08,980 --> 00:07:14,641
But some of these sub problems like in this case with Fibonacci of 3, we compute repeatedly in a wasteful way.

74
00:07:15,245 --> 00:07:24,022
As a result, we end up solving an exponential number of sub problems even though there are only order n actual problems to be solved.

75
00:07:27,977 --> 00:07:38,345
So, what we want to do is move away from this Naive recursive implementation of an inductive definition and try to walk towards never re-evaluating a sub problem.

76
00:07:39,645 --> 00:07:41,069
So, this is easy to do.

77
00:07:41,315 --> 00:07:48,877
If we could only remember the sub problems that we had solved before, then all we have to do is look up the value we already computed rather than recompute it.

78
00:07:49,964 --> 00:07:52,549
So, what we need is a kind of a table right?

79
00:07:52,576 --> 00:07:55,036
A table where we store the values we have computed.

80
00:07:55,576 --> 00:07:58,390
And before we go and compute a value, we first check the table.

81
00:07:58,903 --> 00:08:02,360
If the table has an answer, we take the table answer and go ahead.

82
00:08:02,820 --> 00:08:09,396
If the table doesn't have an answer, then we apply the recursive definition computed and then we add it to the table.

83
00:08:09,420 --> 00:08:17,297
So this table is normally called a memory table to memorize and from this we get this word - memoization.

84
00:08:17,519 --> 00:08:20,381
So, it is actually memo and not memorization.

85
00:08:20,405 --> 00:08:24,851
memoization in the sense of write yourself a memo, memo is like a reminder.

86
00:08:25,170 --> 00:08:27,481
Write yourself a reminder that this has been done before.

87
00:08:27,790 --> 00:08:39,330
So, memoization is the process by which when we are computing a recursive function we compute the values one at a time and as we compute them we store them in a table and look up the table before we recompute it.

88
00:08:42,140 --> 00:08:46,317
So, here is how our computation of Fibonacci of 5 would go if we keep a table.

89
00:08:46,620 --> 00:08:48,314
So, this is our table here, right?

90
00:08:48,338 --> 00:08:56,901
So we have a table where in some order, it doesn't really matter for now, in some order as and when we find Fibonacci of k for some k, we just record it.

91
00:08:57,674 --> 00:09:04,131
And notice that this table is empty even though we know the base case of Fibonacci of 0 and Fibonacci of 1 are 0 and 1 respectively.

92
00:09:04,409 --> 00:09:09,757
We don't assume, "You know it", because it will come out as the first time we hit the base case, it will come out of the recursive definition .

93
00:09:10,445 --> 00:09:12,075
So let's see how it goes, right?

94
00:09:12,100 --> 00:09:16,260
So, we start Fibonacci of 5; as usual it says do 4 and 3.

95
00:09:16,733 --> 00:09:20,096
4 says do 3 and 2, 3 says do 2 and 1.

96
00:09:20,570 --> 00:09:30,723
2 says do 1 and 0 and now from our basic case, the base case in the function we will get back that fib of 1 is 1.

97
00:09:30,929 --> 00:09:34,244
So, we store this in the table this is the first value we have actually computed.

98
00:09:34,897 --> 00:09:36,595
Notice we did not assume we knew it.

99
00:09:36,619 --> 00:09:39,450
When we came to it in the base case, we put it into the table.

100
00:09:40,012 --> 00:09:42,459
Same way fib of 0 is 0.

101
00:09:42,667 --> 00:09:44,714
We didn't know it before we put it in the table.

102
00:09:45,574 --> 00:09:48,570
Now, we come up and we realize that fib of 2 is now available to us.

103
00:09:48,570 --> 00:09:49,834
It's 1 + 0 is 1.

104
00:09:49,908 --> 00:09:51,117
So, we store that in the table.

105
00:09:51,142 --> 00:09:53,842
We say for k equal to 2, fib of k is 1.

106
00:09:54,862 --> 00:09:58,994
Now, we come back to fib of 3 and now we go down and it asks us to compute fib of 1 again.

107
00:09:59,641 --> 00:10:04,769
Now, although this doesn't take us any work because it's a base case, we don't actually exploit that fact.

108
00:10:04,769 --> 00:10:08,112
We first look in the table and say is there an entry for k equal to 1?

109
00:10:08,136 --> 00:10:10,494
Yes, there is and so we pick it up.

110
00:10:10,518 --> 00:10:16,382
So we highlight in orange the fact that this value was actually not recomputed, but looked up in the table.

111
00:10:17,082 --> 00:10:20,061
So, from 1 + 1 we now have Fibonacci of 3 is 2.

112
00:10:20,435 --> 00:10:26,993
Now, we go back up to Fibonacci of 4 and it asks us to compute the second half of its sub problems, namely Fibonacci of 2.

113
00:10:27,633 --> 00:10:30,466
Once again we find that there is an entry for 2 in our table.

114
00:10:30,570 --> 00:10:40,027
So, we mark it in orange and we just take the value from the table without expanding and computing the tree again as we had done before when we did the Naive computation.

115
00:10:40,607 --> 00:10:43,389
So now, 2 + 1 is 3, so we have Fibonacci of 4.

116
00:10:44,169 --> 00:10:50,873
So, we have to now go back and compute the other branch of Fibonacci of 3, but once again 3 as an argument k is in our table.

117
00:10:50,898 --> 00:10:52,892
So, we have an entry here for 3.

118
00:10:52,917 --> 00:10:55,896
So we can just look up the Fibonacci of 3 and say, "Oh! It's 2."

119
00:10:56,490 --> 00:10:58,628
So once again we mark it in orange, okay?

120
00:10:58,945 --> 00:11:04,465
So, now we have Fibonacci of 3 5 is 3 + 2 and that is 5 and then now it is a new value so we enter that.

121
00:11:04,971 --> 00:11:13,268
So notice, therefore, that every value we computed, we expanded the tree or even looked up the base case only once, according to the function definition.

122
00:11:13,828 --> 00:11:28,517
Every subsequent time we needed a value, we just look it up in the table and you can see that the table grows exactly as many times, as much as there are sub problems to be solved and we never solved sub problem twice; in the sense of computing it twice, we solved it by looking at the table.

123
00:11:31,548 --> 00:11:37,704
So, this is a very easy step to incorporate into our Fibonacci table Fibonacci function .

124
00:11:37,837 --> 00:11:39,224
So, we just add this red code.

125
00:11:39,249 --> 00:11:42,589
So the green lines are those we once we have already had before.

126
00:11:43,137 --> 00:11:49,931
So now what Fibonacci says is the first thing you do when you get a number is try and look up the table right?

127
00:11:49,958 --> 00:11:56,093
So, if there is a value, Fibonacci of n, which is defined then return that value.

128
00:11:57,287 --> 00:12:00,222
Otherwise, we go through the recursive computation.

129
00:12:00,247 --> 00:12:06,862
So, this is the usual computation which will make a recursive call and eventually come up with a new value, which is the value for this particular n.

130
00:12:07,815 --> 00:12:13,337
So, before we return this value back, as a result of this function we store it in the table.

131
00:12:13,361 --> 00:12:20,776
Henceforth, if this value n is ever invoked again we never have to look up the thing we never have to compute it, it will be in the table right?

132
00:12:21,076 --> 00:12:22,622
So, it's very simple as we said.

133
00:12:22,816 --> 00:12:26,909
When you get a argument n for which you want to compute the function you first store it in.

134
00:12:26,909 --> 00:12:27,558
Check the table.

135
00:12:27,583 --> 00:12:29,383
If it is there in the table you do not do anything more.

136
00:12:29,408 --> 00:12:30,748
You just return the table value.

137
00:12:31,115 --> 00:12:38,060
If it is not in the table you apply your recursive definition to compute it; just like you would normally having computed it.

138
00:12:38,327 --> 00:12:45,925
You first store it in the table so that future accesses to this function will work without having to do this recursion and then you return the value you compute.

139
00:12:50,102 --> 00:12:52,911
So, this can work for any combination of arguments you just need a table.

140
00:12:52,936 --> 00:12:55,398
In Python terms, you just need a dictionary okay?

141
00:12:55,440 --> 00:13:01,119
For every combination of arguments if that value has been computed before, that key will be there in our table.

142
00:13:01,559 --> 00:13:06,697
So, this table in general, in Python would be a dictionary and not a list because the arguments could be any particular values.

143
00:13:07,001 --> 00:13:11,895
They could, some could be string, some could be numbers, they need not be continuous and so on, okay, right?

144
00:13:12,275 --> 00:13:19,761
So, we basically for given the particular combination of arguments we look up whether that combination of keys is there in the dictionary.

145
00:13:19,950 --> 00:13:21,438
If so, we look it up and return it.

146
00:13:21,909 --> 00:13:26,308
Otherwise, we compute a new value for this combination, store it and then return it.

147
00:13:27,845 --> 00:13:38,204
So we have lost over a few things for instance, typically, if you want to really write this properly in Python we have to use some exceptions and all that but this is more or less the skeleton of what we need.

148
00:13:42,105 --> 00:13:49,845
So, this brings us to the main topic that we want to do this week in a couple of lectures which is called dynamic programming.

149
00:13:50,391 --> 00:13:56,125
So, dynamic programming is just a strategy to further optimize this memoized recursion.

150
00:13:56,865 --> 00:14:02,481
So, this memoized recursion tells us that we will store values into a table as and when they are computed.

151
00:14:03,261 --> 00:14:11,670
Now it's very clear that in an inductive definition we need to get to the base case and then work ourselves backwards up from the base case, to the case we have at hand.

152
00:14:12,383 --> 00:14:17,878
That means there are always some values, some base values for which no further values need to be computed.

153
00:14:17,902 --> 00:14:20,318
These values are automatically available to us.

154
00:14:20,958 --> 00:14:27,398
So, we have some problems which have sub problems and some other problems which have no sub problems.

155
00:14:27,891 --> 00:14:35,347
So, if a problem has a sub problem, we can't get the problem, we can't get Fibonacci of 5 unless we solve its sub problems, Fibonacci 4 and 3.

156
00:14:35,980 --> 00:14:40,785
Okay, if we have a base case that Fibonacci of 1 or Fibonacci of 0, we don't have any sub problems.

157
00:14:40,809 --> 00:14:41,852
We can solve them directly.

158
00:14:42,480 --> 00:14:44,153
So, this is a kind of dependency.

159
00:14:44,178 --> 00:14:52,153
So, we have to solve the sub problems in the dependency order we cannot get something which is dependent on something else until that something else has been solved.

160
00:14:52,735 --> 00:14:57,512
But a little thought tells us that this dependency order must be acyclic.

161
00:14:57,585 --> 00:14:59,256
We cannot have a cycle of dependencies.

162
00:14:59,281 --> 00:15:06,616
So one value like 5 depends on 3 and 3 depends on 1 and 1 again depends on 5, because there will be no way to actually resolve this, okay?

163
00:15:07,092 --> 00:15:14,921
So, there will always be a starting point and we can solve the sub problems in directly in the order of the dependencies, instead of going through the recursive false.

164
00:15:14,946 --> 00:15:17,357
We don't have to follow the inductive structure .

165
00:15:17,675 --> 00:15:21,454
You can directly say okay tell me which are all the sub problems which don't need anything.

166
00:15:21,672 --> 00:15:22,305
Solve them.

167
00:15:22,625 --> 00:15:25,792
Which are all the ones which depend only on these, solve them and so on.

168
00:15:26,210 --> 00:15:28,176
So, this gives us an iterative way.

169
00:15:28,379 --> 00:15:31,666
If you look at the sub problem for Fibonacci of 5, for example.

170
00:15:31,857 --> 00:15:35,994
It says that it requires all these sub problems, but the dependency is very straightforward.

171
00:15:36,507 --> 00:15:37,958
5 depends on 4 and 3.

172
00:15:38,389 --> 00:15:39,764
4 depends on 3 and 2.

173
00:15:40,308 --> 00:15:41,542
3 depends on 2 and 1.

174
00:15:42,253 --> 00:15:45,730
2 depends on 1 and 0 and 0 and 1 have no dependencies.

175
00:15:46,254 --> 00:15:49,373
So, we can start at the bottom and then work ourselves up.

176
00:15:49,415 --> 00:15:53,013
We can say, "Okay, Fibonacci of 0 needs no dependency, so let me write a value for it."

177
00:15:53,627 --> 00:15:56,134
Fibonacci of 1 needs no dependency, so let me write a value for it.

178
00:15:56,783 --> 00:16:01,552
Now, we see that for Fibonacci of two, both the things that it needs have been computed.

179
00:16:01,799 --> 00:16:06,975
So, I can write fib 2 in the table directly without even going to it from 5.

180
00:16:07,108 --> 00:16:08,435
I am not coming down from 5.

181
00:16:08,799 --> 00:16:14,579
We are just directly filling up the table; just keeping track of which values depend on which values.

182
00:16:14,604 --> 00:16:22,894
So, assuming that we know the function we can calculate this dependency and just compute the values as and when the dependencies are satisfied.

183
00:16:22,919 --> 00:16:25,750
So now, we have 1 and 2, so we can compute the Fibonacci of 3.

184
00:16:26,241 --> 00:16:27,027
So, we just compute it.

185
00:16:27,797 --> 00:16:32,153
We have 3 and 4 so we can compute, I mean 2 and 3, so we can compute Fibonacci of 4.

186
00:16:32,866 --> 00:16:36,478
And finally, we have 2 and fib 3 and fib 4, we can get fib 5.

187
00:16:37,272 --> 00:16:40,155
So, this value as you can see becomes now a linear computation .

188
00:16:40,180 --> 00:16:43,053
I can just walk up from 0 to 5 and fill in the value.

189
00:16:43,078 --> 00:16:44,649
In fact, this is what we do by hand, right?

190
00:16:44,674 --> 00:16:45,131
When I can.

191
00:16:45,171 --> 00:16:47,151
You ask me for the 10th Fibonacci number.

192
00:16:47,399 --> 00:16:48,207
I can write it out.

193
00:16:48,232 --> 00:16:51,394
I can say, "Oh 0 0 + 1 is 1 1".

194
00:16:51,419 --> 00:16:57,468
Then 1 + 1 is 2, 2 + 1 is 3, 5, 8, 13, 21, 34.

195
00:16:57,493 --> 00:17:02,729
So clearly it's not a very complicated process as it seems to be when we have this exponential recursion thing.

196
00:17:03,059 --> 00:17:09,950
I can do it on the fly more or less because all I have to do is keep generating the values in a sequence and this is the virtue of dynamic programming.

197
00:17:10,038 --> 00:17:17,700
It converts this recursion into a kind of iterative process where you fill up the values that you would normally fill up in the memo table by 

198
00:17:17,700 --> 00:17:22,341
By recursion you fill them up iteratively starting with ones which have no dependences.

199
00:17:25,067 --> 00:17:28,386
So then the dynamic programming version of Fibonacci is just this iterative loop.

200
00:17:28,639 --> 00:17:34,226
So, it says that you start from with value 0 and 1 to be the value themselves.

201
00:17:34,250 --> 00:17:38,408
That's what we earlier said was that if n is 0 or 1 then you return n.

202
00:17:38,640 --> 00:17:42,093
So here we just store it into the table directly, we say fib table of 0 is 0.

203
00:17:42,359 --> 00:17:43,571
fib table of 1 is 1.

204
00:17:43,595 --> 00:17:45,419
And then we walk from 2 to n.

205
00:17:45,660 --> 00:18:00,823
So, in Python notation, the range end is n + 1 and at each stage, we just take the i th value to be the sum of the i - 1, i - 2 values which we have already computed because we are going in this particular order, because we have recognized the dependency order goes from 0 to n.

206
00:18:01,865 --> 00:18:05,617
And finally, the answer we need is the nth entry, so we just return that.

207
00:18:09,873 --> 00:18:18,621
So, to summarize the basic idea to make Naive recursion more efficient is to never compute something twice and to never compute something twice.

208
00:18:18,646 --> 00:18:26,429
We store the values we compute in what we call a memo table this is called memoization and we always look up the table before we make a recursive call.

209
00:18:27,559 --> 00:18:37,444
Now, this can be further optimized, so we avoid making recursive calls  all together and we just directly fill in the table in dependency order which must be acyclic.

210
00:18:37,469 --> 00:18:39,360
Otherwise this for problem will not be solvable.

211
00:18:40,175 --> 00:18:46,443
And this converts a recursive evaluation into a iterative evaluation  which is often much more efficient.

