#!/usr/bin/python3.7

# Google translate API using python googletrans API
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 22/11/2020

import sys
import argparse
import re
import googletrans

def main():
	parser = argparse.ArgumentParser(description='Google translation wrapper using python googletrans API\n\nAuthor: Jom Kuriakose\nemail: jom@cse.iitm.ac.in\nDate: 22/11/2020\n\nLanguages supported are:\n'+str(googletrans.LANGUAGES)+'\n\nExample: ./google_translate_v3.py -v -s en -d hi -f txt -i input.txt -o output.txt\nThis command converts input txt file in English to output in Hindi.\n\nExample: ./google_translate_v3.py -v -s en -d hi -f srt -i input.srt -o output.srt\nThis command converts input srt file in English to output in Hindi.',formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--verbose', help='Verbose mode (default=False)', required=False, action='store_true')
	parser.add_argument('-s','--source', help='Source language tag (default=en)', required=False, default='en')
	parser.add_argument('-d','--dest', help='Destination language tag (default=hi)', required=False, default='hi')
	parser.add_argument('-i','--input', help='Input filename', required=True)
	parser.add_argument('-o','--output', help='Output filename', required=True)
	parser.add_argument('-f','--filetype', help='The input file format can be either .txt or .srt (default=txt)', required=False, default='txt')
	parser.add_argument('-a','--attempts', type=int, help='Number of attempts to translate. More than one attempt is needed, because the googletrans API used is not official and can have network issues sometimes. (default=3)', required=False, default=3)
	args = parser.parse_args()

	code_name=sys.argv[0]
	success=0
	src_file=args.input
	dest_file=args.output
	src_lang=args.source
	dest_lang=args.dest
	attempts=0
	max_attempts=args.attempts
	verbose=args.verbose
	successOrFail=['FAILED','SUCCESS']
	file_type=args.filetype
	file_types=['srt','txt','.srt','.txt']
	
	if not(file_type in file_types):
		if(verbose):
			print(file_type+' not supported. Use txt or srt formats.')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	
	# Check if the input file in exist and readable
	if(file_type in ['txt','.txt']):
		try:
			with open(src_file, encoding='utf-8') as f:
				contents = f.read()
		except Exception as e:
			if(verbose):
				print('Input file reading failed.')
				print(str(e))
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	
	if(file_type in ['srt','.srt']):
		try:
			with open(src_file, encoding='utf-8') as f:
				lines = f.readlines()
			contents = ''
			for line in lines:
				if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
					contents += line
		except Exception as e:
			if(verbose):
				print('Input file reading failed.')
				print(str(e))
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()

	# Check if the number of characters are less than 15k
	text = contents.strip().split()
	len_chars = sum(len(word) for word in text)
	if(len_chars>=15000):
		if(verbose):
			print('Number of characters in input file should be less than 15000 for the script to work. In srt file the time information is not counted for calculating the number of characters.')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()

	from googletrans import Translator
	#translator = Translator(proxies=None, raise_exception=True)
	translator = Translator()
	try:
		lang_detect = translator.detect(contents)
		if(verbose):
			print(src_file+': Source language '+str(lang_detect))
		if(lang_detect.lang==src_lang):
			if(verbose):
				print('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect.lang+') are same.')
		else:
			if(verbose):
				print('Source language tag inputed in command('+src_lang+') and language detected in '+src_file+'('+lang_detect.lang+') are different.')
				print('Check input file and source language tag.')
			print(src_file+' '+str(success)+' '+successOrFail[success])
			sys.exit()
	except Exception as e:
		if(verbose):
			print('Source language detection failed.')
			print(str(e))	

	# Check if the language tags exist
	if(checkKey(googletrans.LANGUAGES,src_lang)==0):
		if(verbose):
			print('Source language '+src_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()

	if(checkKey(googletrans.LANGUAGES,dest_lang)==0):
		if(verbose):
			print('Destination language '+dest_lang+' not supported')
		print(src_file+' '+str(success)+' '+successOrFail[success])
		sys.exit()
	
	while attempts<max_attempts:
		try:
			result = translator.translate(contents, src=src_lang, dest=dest_lang)
			break
		except Exception as e:
			attempts += 1
			if(verbose):
				print('Attempt: '+str(attempts)+' failed. Trying again...')
				print(str(e))
			continue

	if attempts==max_attempts:
		if(verbose):
			print('Try running again\nSince unofficial googletrans api is used, there can be network inconsistencies.\nPlease try again in that case.')
	else:
		if(file_type in ['txt','.txt']):
			try:
				with open(dest_file, 'w') as f:
					f.write(result.text)
			except Exception as e:
				if(verbose):
					print('Ouptut file writing failed.')
					print(str(e))
				print(src_file+' '+str(success)+' '+successOrFail[success])
				sys.exit()

		if(file_type in ['srt','.srt']):
			try:
				with open(src_file, encoding='utf-8') as f:
					lines = f.readlines()
				counter=0
				first_line=0
				result_split=result.text.split('\n')
				for line in lines:
					if(first_line==0):
						with open(dest_file, 'w') as f:
							f.write(line)
						first_line += 1
						continue
					if re.search('^[0-9]+$', line) is None and re.search('^[0-9]{2}:[0-9]{2}:[0-9]{2}', line) is None and re.search('^$', line) is None:
						with open(dest_file, 'a') as f:
							f.write(result_split[counter]+'\n')
						counter += 1
					else:
						with open(dest_file, 'a') as f:
							f.write(line)

			except Exception as e:
				if(verbose):
					print('Ouptut file writing failed.')
					print(str(e))
				print(src_file+' '+str(success)+' '+successOrFail[success])
				sys.exit()
			
		success=1
		if(verbose):
			print('Attempt: '+str(attempts+1))
			print('Source Language: '+googletrans.LANGUAGES[src_lang]+'\nDestination Language: '+googletrans.LANGUAGES[dest_lang])
			print('Translation successfully')
			
	print(src_file+' '+str(success)+' '+successOrFail[success])

# Check if the key is present in dictionary
def checkKey(dictionary, key):
	if key in dictionary.keys():
		return 1
	else:
		return 0

# Main function
if __name__ == "__main__":
	main()
